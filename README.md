# The Probablistic Call-by-Need Lambda Calculus

This repository contains source code, inputs and outputs for the automated diagram generation and automated induction
for our paper 'Contextual Equivalence in a Probabilistic Call-by-Need Lambda-Calculus'.
