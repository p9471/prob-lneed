-----------------------------------------------------------------------------
-- |
-- Module      :  LRSX.Unification.Unification 
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--  
-- The unification algorithm for higher-order 
-- expressions with recursive bindings and 
-- special environments for small step reductions
--
-----------------------------------------------------------------------------
module LRSX.Unification.Unification where
import Data.Function
import Data.List
import Data.Maybe
--
import Debug.Trace
--
import LRSX.Language.Syntax
import LRSX.Language.Fresh

import LRSX.Util.Util
import LRSX.Unification.UnificationProblem
 
import LRSX.Language.ContextDefinition
import qualified Data.Set as Set
import qualified Data.Map as Map
import LRSX.Matching.ConstraintInclusion
-- ====================================================================================================================
-- UNIFICATION
-- ====================================================================================================================



-- ********************************************************************************************************************
-- The unification function performs the whole unification and builds a tableau
-- ********************************************************************************************************************

unification :: Problem -> Tableau
unification problem 
  | Just err <- checkGammaUnifInp (gamma problem,
                                   delta1 (delta problem),
                                   delta2 (delta problem)) = error $ "unification: input check fails:" ++ show err ++ "\nInputProblem\n" ++ show problem

unification problem = expand problem 
 where
  expand pb 
--    | trace ("expanding...") ((not $ solved $ gamma pb )&& False) = undefined
--    | trace (show $ gamma pb) False = undefined
--    | trace (show $ delta pb) False = undefined
   | solved (gamma pb)             
   , Empty <- unifyStep  (heur (expandDelta4Constraints pb) (expandAllContexts pb) (expandAllEnvs pb) ) (pb) 
   , Empty <- unifyStep [applyFailRulesDelta4] pb = 
       let sol             = composeAll (solution pb)
           instExpr        = applySolution sol (gamma problem)
           instDelta       = applySolution sol (delta3 $ delta pb)    
           capcon          = checkNCCs (delta2 $ delta pb) instDelta
                             -- checkNonCaptureConstraints instDelta
       in case checkLVCGamma instExpr of 
           Nothing -> 
                 if isNothing capcon -- check capture constraints
                 then -- consistency check for the solution: 
                     if (any (\(x :=?=: y) -> x /= y) (expressionEq instExpr)) && (nullGamma $ delta4 $ delta pb) 
                       then error $ "\nPANIC: Solution is not a solution\n" 
                             ++  "\nInstExpr:\n" ++  (unlines $ map (\(x :=?=: y) -> "\n" ++ show x ++ "\n=?=" ++ "\n" ++ show y) (expressionEq instExpr))
                             ++  "\nInstExpr:\n" ++ show (instExpr)
                             ++  "\nComposedSol\n" ++ show sol
                             ++  "\nCurrentState\n"
                             ++  "\nSol\n" ++ show (solution pb)
                             ++  "\nDelta\n" ++ show (delta pb)                             
                             ++  "\nInput\n" ++ show (gamma problem)
                             
                       else  
                       Solved pb{solution=sol} -- (instExpr, sol, delta pb,pb)
              else Node pb [Fail $ "capture constraint fail" ++ (show (fromJust capcon))]
           Just err -> Node pb [Fail $ "LVC failure:" ++ err ++ " " ++ (show (expressionEq instExpr))]
  expand pb 
      | solved (gamma pb) = 
            case unifyStep [applyFailRulesDelta4] pb of
                Some problems -> Node pb (map expand problems)
                UnifFail err  -> Node pb [Fail err]
                Empty -> case unifyStep (heur (expandDelta4Constraints pb) (expandAllContexts pb) (expandAllEnvs pb)) pb of 
                           Some [] -> error $ "in expand " ++ show (pb) -- show $ Node (solution pb, gamma pb, delta pb) []
                           Some problems -> Node pb (map expand problems)
                           UnifFail err  -> Node pb [Fail err]
                           Empty          -> error $ "in expand " ++ show (pb) -- show $ Node (solution pb, gamma pb, delta pb) []
      | otherwise = 
                case unifyStep (heur (expandDelta4Constraints pb) (expandAllContexts pb) (expandAllEnvs pb)) pb of 
                    Some []       -> error $ "in expand " ++ show (pb) -- show $ Node (solution pb, gamma pb, delta pb) []
                    Some problems -> Node pb (map expand problems)
                    UnifFail err  -> Node pb [Fail err]
                    Empty         -> error $ "in expand " ++ show (pb) -- show $ Node (solution pb, gamma pb, delta pb) []
 
  heur flagExpandDelta4 flagAllContexts flagAllEnvs = 
         [applyFailRules
         ,applyFailRules . commuteProblem
         ,applyFirstOrderRules
         ,applyFirstOrderRules . commuteProblem
         ,applyContextRules 
         ,applyContextRules . commuteProblem 
         ,applyEnvRules
         ,applyEnvRules . commuteProblem
         ,applyGuessRules
         ,applyGuessRules . commuteProblem
         ]
         ++ (if flagExpandDelta4 
                then [expandDelta4ConstraintsIfPossible
                     ,expandDelta4ConstraintsIfPossible . commuteDelta4
                     ]
                else [])
         ++ (if flagAllContexts 
                then [applyFullGuessRules]   -- guess all context vars
                else [])
         ++ (if flagAllEnvs 
                then [applyFullGuessRulesEnv]   -- guess all context vars
                else [])
         
  unifyStep [] pb = Empty
  unifyStep (uniffn:xs) pb =
    case uniffn pb of 
      Empty -> unifyStep xs pb
      Some [] -> unifyStep xs pb
      Some ps -> Some ps
      UnifFail err -> UnifFail err

-- ********************************************************************************************************************
-- The tableau of the unification algorithm

data Tableau = Node Problem [Tableau] 
             | Fail String
             | Solved Problem -- (Gamma,Solution,Delta,Problem) -- Solved nodes: Unified equations, solution, constraints on environments, final problem

             
instance Show Tableau where
 show t = showTableau [1] t
  where
   showTableau ind (Node p xs) = 
     indent ind ++ "Open: " ++ show (solution p,gamma p, delta p) ++ "\n" ++ (concat [showTableau (if length xs > 1 then ind ++ [i] else ind) x | (i,x) <- zip [1..] xs])
   showTableau ind (Fail a)   = indent ind ++ "FAIL: " ++ show a ++ "\n"
   showTableau ind (Solved pb) = 
       let sol = solution pb
           gam = applySolution sol (inputGamma pb)
           del = delta pb
       in indent ind ++ "Solved: Unified Equations:" ++ show (gam,sol,del) ++ "\n"
     
   indent ind = let px = (concat $ intersperse "." (map show ind)) 
                in  px ++ (replicate (mx - (length px)) ' '     ) ++ ": "
   mx = 10
   branchDepth i (Node _ [x]) = branchDepth i x
   branchDepth i (Node _ xs) =  let a = [branchDepth (i ++ [v]) x  | (v,x) <- zip [1..] xs]
                                in if null a then i else maximumBy (compare `on` (length . show)) a   
   branchDepth i _ = i 
    
-- helper function for the tableau:

-- compute solved and open leaves 
solvedAndOpenLeaves (Node a []) = [Node a []]
solvedAndOpenLeaves (Node _ cs) = concatMap solvedAndOpenLeaves cs
solvedAndOpenLeaves (Fail x)    = []
solvedAndOpenLeaves (Solved x)  = [Solved x] 

-- compute the solved leaves only    
solvedLeaves (Node _ cs) = concatMap solvedLeaves cs
solvedLeaves (Fail _)    = []
solvedLeaves (Solved x)  = [x] 

-- compute all leaves 
allLeaves (Node a []) = [Node a []]
allLeaves (Node _ cs) = concatMap allLeaves cs
allLeaves (Fail x)    = [Fail x]
allLeaves (Solved x)  = [Solved x] 

-- extract Delta
getDeltaFromSolved (Solved pb) = delta pb 
-- ********************************************************************************************************************


-- ********************************************************************************************************************
-- Result of one step of the unification algorithm 
data UnifResult = UnifFail String  -- unification fails
                | Empty            -- no unification rule applicable
                | Some [Problem]   -- new problems after one step
 deriving(Show)
-- ********************************************************************************************************************

  
-- ********************************************************************************************************************
-- Fail rules
-- ********************************************************************************************************************
-- the function applyFailRules checks whether a fail rule is applicable to the problem,
-- if yes, then UnifFail is delivered as result, otherwithe Empty is delivered.
-- the extra fail rule, which is only applied to the final state, and checks whether
-- the constraints in Delta4 are unsolvable is not included in applyFailRules, it is
-- available via applyFailRulesDelta4

applyFailRules :: Problem -> UnifResult
applyFailRules problem@(Problem{solution=sol,gamma=gam,delta=del})
--
--  Fail-Rules for variable-equations:
--  RULE: (Sol,Gamma cup {x = y}, Delta) --> Fail if x/= y
--
  |  (eq:_) <- [eq | eq@(VarLift s1 x :=?=: VarLift s2 y) <- (varLiftEq gam), x /= y]
    =  UnifFail $ show eq 
--    
--  Fail-Rules for expression-equations:
--
 | (eq:_) <- (  
--  RULE: (Sol,Gamma cup {f r1 ... rn = var y}, Delta) --> Fail
                [eq | eq@((Fn name args) :=?=: VarL _) <- (expressionEq gam)]
--  RULE: (Sol,Gamma cup {f r1 ... rn = letrec ....}, Delta) --> Fail
             ++ [eq | eq@((Fn _ _) :=?=: (Letrec _ _)) <- (expressionEq gam)]
--  RULE: (Sol,Gamma cup {f r1 ... rn = letrec ....}, Delta) --> Fail
             ++ [eq | eq@((Fn _ _) :=?=: (FlatLetrec _ _)) <- (expressionEq gam)]
--  RULE: (Sol,Gamma cup {f r1 ... rn = g r1' ... rn'}, Delta) --> Fail
             ++ [eq | eq@((Fn name _) :=?=: (Fn name' _)) <- (expressionEq gam), name /= name']
--  RULE: (Sol,Gamma cup {var x = letrec ...}, Delta) --> Fail
             ++ [eq | eq@(VarL  _ :=?=: (Letrec _ _)) <- (expressionEq gam)]
--  RULE: (Sol,Gamma cup {var x = letrec ...}, Delta) --> Fail
             ++ [eq | eq@(VarL  _ :=?=: (FlatLetrec _ _)) <- (expressionEq gam)]
--  RULE: (Sol,Gamma cup {S = t ...}, Delta) --> Fail, if S is proper subterm of t and t /= D[t'] for some context variable D
             ++ [eq | eq@(vars@(MetaExpression s0 s) :=?=: t) <- (expressionEq gam)
                    , (properSubTermOf vars t)
                    , not (isMetaCtxtSubst t)]
--  RULE: (Sol,Gamma cup {S = D[t] ...}, Delta) --> Fail, if S is subterm of t and D in Delta1 (i.e. D is non-empty)
             ++ [eq | eq@(vars@(MetaExpression s0 s) :=?=: MetaCtxtSubst s1 srt1 d1 e1)   <- (expressionEq gam)
                    , (subTermOf vars e1)
                    , (MetaCtxt emptySubstitution srt1 d1) `elem` (delta1 del)]
          )
    = UnifFail $ show eq
--    
--  Fail-Rules for environment-equations:
--
--  RULE:(Sol,Gamma cup env :=: EE[z,s]; EE'[z,s'];env') -> Fail since same z occurs twice as binder in the same environment
  | (eq:_) <- (  [eq | eq@(l :=?=: r) <- environmentEq gam
                  , not (null [ ee | (MetaChain sub _ ee z s) <- metaChains r
                  ,(MetaChain sub' _ ee' z' s') <- metaChains r
                  , ee /= ee'
                  , z == z'])
                ]   
--  RULE: (Sol,Gamma cup env :=: EE[z,s]; z=s';env') -> Fail since same z occurs twice as binder in the same environment
             ++ [eq | eq@(l :=?=: r) <- environmentEq gam
                  , (MetaChain sub _ ee z s) <- metaChains r
                  , (y:=_) <- (bindings r)
                  , y == z
                ]
--  RULE: (Sol,Gamma cup {env = {}} -> Fail, if env is non-empty, i.e. 
--   env=b;env'  or env=EE[z,s];env'  or env=E;env' with E in Delta2
             ++ [eq | eq@(left :=?=: right) <- (environmentEq gam)
                       ,isEmpty right
                       ,  (not (null (bindings left )) 
                        || not (null (metaChains left)) 
                        || not (null [ v | v <- metaEnvVars left
                                     , v `elem` (delta2 del)] ))
                ]
             )
       = UnifFail $ show eq

 | Just (eq@(ctxt@(MetaCtxtSubst sub1 sortD1 nameD1 s1) :=?=: (MetaCtxtSubst sub2 sortD2 nameD2 s2)),rest)
     <- splitBy (\(l:=?=:r) 
                   -> case (l,r) of 
                       (MetaCtxtSubst sub1 srtD1 d1 s, MetaCtxtSubst sub2 srtD2 d2 s') ->  
                                (MetaCtxt sub1 srtD1 d1) `elem` (delta1 del)
                             && (MetaCtxt sub2 srtD2 d2) `elem` (delta1 del)
                             && (  (   isNothing (Map.lookup (srtD1,srtD2) (pfxTable $ contextDefinitions problem))
                                    && isNothing (Map.lookup (srtD2,srtD1) (pfxTable $ contextDefinitions problem)))
                                    || null[() |    
                                           (a,b) <- (( case (Map.lookup (srtD1,srtD2) (pfxTable $ contextDefinitions problem)) of 
                                                            Just xs -> xs
                                                            Nothing -> [])
                                                  ++ ( case (Map.lookup (srtD2,srtD1) (pfxTable $ contextDefinitions problem))  of
                                                            Just xs -> xs
                                                            Nothing ->[]))
                                           , a /= (UserDefinedSort False "TRIV") || (b /= UserDefinedSort False "TRIV") ]
                                )
                       _    -> False) 
                (expressionEq gam)        
   = UnifFail $ show eq
 
--
-- Otherwise, no Fail-rule is applicable:
--       
 | otherwise = Empty 

-- 
-- Fail-Rule for Delta4, checked only once at the end of unification
--    First, check whether there are constraint equations 
--    in Delta4, then check if the constraints are solvable
--    (done by the function solvableChains) 
--

applyFailRulesDelta4 :: Problem -> UnifResult
applyFailRulesDelta4 problem@(Problem{solution=sol,gamma=gam,delta=del})
 | eqs <- (environmentEq $ delta4 del)
 , not (null eqs)
  , leftEI <- [v | (left :=?=: _) <- eqs, v <- metaEnvVars left]
  , deltaEI <- [ei | ei <- leftEI, ei `elem` (delta2 del)]
  , Just err <- solvableChains problem
    = UnifFail $ "unsolvable E1,...,En = EE[z,s]" 
-- Otherwise, the constraints are solvable and thus the fail-rule does not deliver a RULE:
 | otherwise = Empty 
-- ********************************************************************************************************************


 
-- ********************************************************************************************************************
 
-- ********************************************************************************************************************
-- First-Order Cases
-- ********************************************************************************************************************
-- applyFirstOrderRules tries to apply a first-order unification rule to the problem
-- if no rule is applicable, then Empty is delivered, otherwise the result is Some [problem] which is
-- the subsequent unification problem (no branching is required, and thus the result is a singleton) 
--
applyFirstOrderRules :: Problem -> UnifResult
applyFirstOrderRules problem@(Problem{solution=sol,gamma=gam,delta=del})
--
--  RULE: (Sol,Gamma cup {X = Y}, Delta) --> (Sol circ {X |-> Y}, Gamma[Y/X], Delta [Y/X])
--         where X is a Meta-Variable, Y can be meta or concrete
--
 | Just ((varx@(MetaVarLift _ x) :=?=: vary),rest) 
     <-  splitBy (\(l:=?=:r) -> case l of 
                                 MetaVarLift _ x -> True
                                 _                 -> False) (varLiftEq gam)
    = let sol' = (MapVarLift $ varx :|-> vary):sol
          gam' = subst vary varx (gam{varLiftEq = rest}) 
          del' = subst vary varx del
      in Some [problem{solution=sol',gamma=gam',delta=del'}]
--
--  RULE: (Sol,Gamma cup {x = x}, Delta) --> (Sol, Gamma, Delta)
--         where x is a concrete variable
--
  |  Just (eq@((VarLift _ x) :=?=: (VarLift _ y)),rest) 
       <-  splitBy (\(l :=?=: r) 
                        -> case (l,r) of 
                            (VarLift _ x,VarLift _ y) -> x == y
                            _                         -> False) 
                   (varLiftEq gam)
    = Some [problem{gamma=gam{varLiftEq = rest}}]
--    
--
--  RULE: (Sol,Gamma cup {S = s}, Delta) --> (Sol circ {S |-> s}, Gamma[s/S], Delta [s/S])
--         if S is not a proper subterm of s
--         not that proper is meant syntactically, 
--         i.e. S is a proper subterm of D[S] 
--         even if D may be the empty context
--         (the first-order rule is then not applicable, first the context variable has to be guesses empty/non-empty)                                        
--  
 | Just ((vars@(MetaExpression _ s) :=?=: t),rest) 
     <- splitBy (\(l:=?=:r) 
                    -> case l of 
                        MetaExpression _  s -> not (properSubTermOf l r )   
                        _                   -> False                    ) 
                (expressionEq gam)
    = let sol' = (MapExpr $ vars :|-> t):sol
          gam' = subst t vars (gam{expressionEq = rest}) 
          del' = subst t vars  del
      in Some [problem{solution=sol',gamma=gam',delta=del'} ]

--
--  RULE: (Sol,Gamma cup {f vect(X1).s1...vect(Xn).sn = f vect(Y1).t1...vect(Yn).tn}, Delta) --> (Sol, Gamma cup {si=ti} cup {X11=Y11,...,Xnmn=Ynmn}, Delta)
--        where vect(Zi) = Zi1...Zimi, note that all binders are processed in one blow (to avoid higher-order-expression-equations)
--
 | Just (((Fn name args) :=?=: Fn name' args'),rest) 
     <-   splitBy (\(l:=?=:r) 
                      -> case (l,r) of 
                          (Fn name _, Fn name' _) -> name == name'
                          _                       -> False         ) 
                  (expressionEq gam)
    = let (vs,es) = -- computes the new variable equations and the new expression equations
            let (vareqs,expreqs) = unzip (zipWith compute args args') -- 
            in (concat vareqs,concat expreqs)
          -- compute variable equations and expression equations  
          -- case for variable positions
          compute (VarPos v1) (VarPos v2)
            = let vareq = v1 :=?=: v2
              in ([vareq],[])
          -- case for higher-order binder:
          compute b1@(Binder v1 e1) b2@(Binder v2 e2)
            | length v1 == length v2 = 
                let vareq = zipWith (:=?=:) v1 v2
                    expreq = [e1 :=?=: e2]
                in (vareq,expreq)
            | otherwise = error ("found binders with different arity: "  ++ show b1 ++ " and " ++ show b2)
          -- error handling:
          compute b1 b2 =
            error ("Binder found at variable position with: " ++ show b1 ++ " and " ++ show b2)
          -- update Gamma:
          gam' = (gam{expressionEq = es ++ rest, varLiftEq = vs ++ (varLiftEq gam)}) 
      in Some [problem{gamma=gam'} ]
--  
--  RULE: (Sol,Gamma cup {var X = var Y}, Delta) --> (Sol, Gamma cup {X=Y}, Delta)
--  (for all cases of X,Y (Meta or concrete)
--  
  | Just ((VarL varx :=?=: VarL vary),rest)
       <-  splitBy (\(l:=?=:r) -> 
                        case (l,r) of 
                             (VarL _, VarL _) -> True
                             _                -> False) 
                   (expressionEq gam)
     = let gam' = gam{ varLiftEq = (varx:=?=:vary):(varLiftEq gam)
                      , expressionEq = rest} 
       in Some [problem{gamma=gam'}]
--  
--  RULE: (Sol,Gamma cup {x.s = y.t}, Delta) --> (Sol, Gamma cup {x=y,s=t}), Delta 
--  (for all cases of X,Y (Meta or concrete)
-- 
  | ((var1 := e1) :=?=: (var2 := e2):rest) <-  (bindingEq gam)
     = let gam' = gam{ varLiftEq = (var1:=?=:var2):(varLiftEq gam)
                      , expressionEq = (e1 :=?=: e2):(expressionEq gam)
                      , bindingEq = rest} 
      in Some [problem{gamma=gam'}]
--  
--  RULE: (Sol,Gamma cup {letrec env1 in s1 = letrec env2 in s2}, Delta) --> (Sol, Gamma cup {env1=env2,s1=s2}, Delta)
--  
  | Just ((Letrec env1 s1 :=?=: Letrec env2 s2),rest) 
      <- splitBy (\(l:=?=:r) -> 
                        case (l,r) of 
                         (Letrec _ _, Letrec _ _) -> True
                         _                        -> False) 
                 (expressionEq gam)
     = let gam' = gam{environmentEq = (env1:=?=:env2):(environmentEq gam)
                     ,expressionEq = (s1 :=?=: s2):rest} 
       in Some [problem{gamma=gam'}] 
  | Just ((FlatLetrec env1 s1 :=?=: FlatLetrec env2 s2),rest) 
      <- splitBy (\(l:=?=:r) -> 
                        case (l,r) of 
                         (FlatLetrec _ _, FlatLetrec _ _) -> True
                         _                        -> False) 
                 (expressionEq gam)
     = let gam' = gam{environmentEq = (env1:=?=:env2):(environmentEq gam)
                     ,expressionEq = (s1 :=?=: s2):rest} 
       in Some [problem{gamma=gam'}] 
--      
-- RULE: (Sol,Gamma cup {{} = {}},Delta) -> (Sol,Gamma,Delta)
--     
  | Just (env1:=?=: env2,rest) 
      <- splitBy  
            (\(l :=?=: r) -> isEmpty l && isEmpty r) 
            (environmentEq gam)
     = Some [problem{gamma=gam{environmentEq = rest}} ]
--      
-- no first-order rule applicable:
--
  | otherwise = Empty
-- ********************************************************************************************************************

-- ********************************************************************************************************************
-- Rules for Contexts 
-- ********************************************************************************************************************
-- The rules for contexts work on expression equations, where at least on side of the
-- equation is a context. However, for guessing a context variable to be empty or non-empty
-- it suffices that the context variable occurs somewhere in Delta 
-- the rules are split into two functions:
-- applyGuessRules to guess contexts to be empty/non-empty
-- applyContextRules for all other rules
-- (reason: one can play around with different heuristics for applying the guess rules)
--

applyFullGuessRules problem@(Problem{solution=sol,gamma=gam,delta=del})
 | q@((MetaCtxt _ srt name):r) <- [x | x@(MetaCtxt sub srt name) <- (contextVarsInSol $ composeAll sol)
                                   , not (x `elem` (delta1 del) || (MetaCtxt emptySubstitution srt name) `elem` (delta1 del)) ]
                                   
   = 
      let problem1 = 
                let oldDel_1 = delta1 del
                    newDel_1 = (MetaCtxt emptySubstitution  srt name):oldDel_1
                in  problem{delta = del{delta1 = newDel_1}}
          problem2 = 
                let sol' = (MapExpr $ MetaCtxt emptySubstitution  srt name :|-> hole):sol
                    gam' = subst hole (MetaCtxt emptySubstitution srt name) gam
                    del' = subst hole (MetaCtxt emptySubstitution srt name) del
               in problem{solution=sol',gamma=gam', delta=del'}
      in Some [problem1,problem2]
-- no guess rule applicable
 | otherwise = Empty
--      
-- Guessing context-variables as empty/non-empty
-- 
-- if there is a context-variable D in Gamma on Top, but not in Delta1,
-- then branch into two problems: either D is guessed as empty and replaced by the empty context,
-- or D is guessed as non-empty and thus added to Delta1
-- (Sol,Gamma cup D[s]=t,Delta) -> (Sol[[.]/D],(Gamma cup {s=t})[[.]/D],Delta[[.]/D]])  | (Sol,Gamma,Delta1 cup {D}) 
--  if D occurs in Gamma and D not in Delta1
applyGuessRules problem@(Problem{solution=sol,gamma=gam,delta=del})
 | Just ((ctxt@(MetaCtxtSubst sub srt name s) :=?=: _),rest)
     <- splitBy (\(l:=?=:r) 
                    -> case (l,r) of 
                        (MetaCtxtSubst sub srt cname s, _) -> not ((MetaCtxt sub srt cname) `elem` (delta1 del) || (MetaCtxt emptySubstitution srt cname) `elem` (delta1 del))
                        _ -> False) 
                (expressionEq gam)
   = 
      let problem1 = 
                let oldDel_1 = delta1 del
                    newDel_1 = (MetaCtxt emptySubstitution  srt name):oldDel_1
                in  problem{delta = del{delta1 = newDel_1}}
          problem2 = 
                let sol' = (MapExpr $ MetaCtxt emptySubstitution  srt name :|-> hole):sol
                    gam' = subst hole (MetaCtxt emptySubstitution srt name) gam
                    del' = subst hole (MetaCtxt emptySubstitution srt name) del
               in problem{solution=sol',gamma=gam', delta=del'}
      in Some [problem1,problem2]
-- no guess rule applicable
 | otherwise = Empty
--
--
--     
applyContextRules problem@(Problem{solution=sol,gamma=gam,delta=del})
--
-- RULE: (Sol,Gamma cup {A[s] = f s1 ... sn},Delta) -> (Sol cup {A |-> f s1 ... A' ... sn}, Gamma cup {si =?= A'[s]}, Delta[f s1 .... A' .... sn/A]) 
--  if A in Delta1, and for each strict position of f_i 
--
--
 | Just ((ctxt@(MetaCtxtSubst sub sortD cname s) :=?=: rhs),rest)
     <- splitBy (\(l:=?=:r) 
                    -> case (l,r) of 
                        (MetaCtxtSubst sub sortD cname s, rhs) -> 
                           case rhs of 
                                Fn fname _ -> (MetaCtxt sub sortD cname) `elem` (delta1 del)
                                Letrec _ _ -> (MetaCtxt sub sortD cname) `elem` (delta1 del)
                                FlatLetrec _ _ -> (MetaCtxt sub sortD cname) `elem` (delta1 del)
                                VarL _     -> (MetaCtxt sub sortD cname) `elem` (delta1 del)
                                _          -> False
                        _ -> False) 
                (expressionEq gam)
 = 
  let unfolds = Map.lookup (ctxtToString sortD) (grammar $ contextDefinitions problem)
  in  case unfolds of 
         Nothing -> error $ "no unfolds for ctxt of srt " ++ ctxtToString sortD
         Just ufs ->
           let fv = fresh problem
               go (u:us) fv = let (u',fv') = makeExprWithConstraintsFresh u fv
                                  (us',fv'') = (go us fv')
                              in (u':us', fv'')
               go [] fv = ([],fv)
               (rufs,fresh') = go [(case r of {Right x -> x; Left (x,y) -> x}) | r <- ufs] fv
               rufs' = filter (\(a,b,c,d) -> not (isHole a)) rufs
               newlhs =  map (\(r,a,b,c) -> (subst s (Hole emptySubstitution) r,r,a,b,c)) rufs'
           in Some [problem{gamma=gam{expressionEq = (newl :=?=: rhs):rest}
                   ,solution=(MapExpr (MetaCtxt sub sortD cname :|-> c)):sol
                   ,fresh=fresh'
                   ,delta = applySolution 
                               [(MapExpr (MetaCtxt sub sortD cname :|-> c))] 
                               (del{delta1 = ndel1 ++ (delta1 del), delta2 = ndel2 ++ (delta2 del), delta3 = ndel3 ++ (delta3 del)})
                   }
                    | (newl,c,ndel1,ndel2,ndel3) <- newlhs
                   ]
 
       

 | Just ((ctxt@(MetaCtxtSubst sub1 sortD1 nameD1 s1) :=?=: (MetaCtxtSubst sub2 sortD2 nameD2 s2)),rest)
     <- splitBy (\(l:=?=:r) 
                   -> case (l,r) of 
                       (MetaCtxtSubst sub1 srtD1 d1 s, MetaCtxtSubst sub2 srtD2 d2 s') ->  
                                (MetaCtxt sub1 srtD1 d1) `elem` (delta1 del)
                             && (MetaCtxt sub2 srtD2 d2) `elem` (delta1 del)
                             && (   isJust (Map.lookup (srtD1,srtD2) (pfxTable $ contextDefinitions problem))
                                 || isJust (Map.lookup (srtD2,srtD1) (pfxTable $ contextDefinitions problem))
                                )
                             && let xs = case  (Map.lookup (srtD1,srtD2) (pfxTable $ contextDefinitions problem)) of
                                            Just a -> a
                                            Nothing -> []
                                    ys = case (Map.lookup (srtD2,srtD1) (pfxTable $ contextDefinitions problem)) of
                                            Just a -> a
                                            Nothing -> []
                                    
                                in not $ null [()| (a,b) <- xs ++ ys, a /= (UserDefinedSort False "TRIV") || (b /= UserDefinedSort False "TRIV") ]
                       _    -> False) 
                (expressionEq gam) 
 = --
  Some $
   -- prefix rule D1[s1] =?= D2[s2] 
   --  
   --    pfxTable(cl(D1),cl(D2)) = K1,K2
   --    D1 -> D1'         cl(D1') = K1
   --    D2 -> D1'D2'      cl(D2') = K2
   --    s1 =?= D2'[s2]
   --
   (case Map.lookup (sortD1,sortD2) (pfxTable $ contextDefinitions problem) of
         Just ks ->
            [let (_nameD1Prime:_nameD2Prime:fresh') = fresh problem
                 nameD1Prime  = ctxtToString k1 ++ _nameD1Prime
                 nameD2Prime  = ctxtToString k2 ++ _nameD2Prime
                 sctxtD2Prime = if isTriv k2 then s2 else MetaCtxtSubst emptySubstitution k2 nameD2Prime s2
                 rhsNew       =  sctxtD2Prime
                 ctxtD1Prime  = if (isTriv k1) then Hole sub1 else MetaCtxt sub1 k1 nameD1Prime
                 sctxtD1Prime = if (isTriv k1) then id else \hole -> MetaCtxtSubst sub2 k1 nameD1Prime hole
                 ctxtD2Prime  =  if (isTriv k2) then Hole emptySubstitution else MetaCtxt emptySubstitution k2 nameD2Prime
                 solNew   = [MapExpr (MetaCtxt sub1 sortD1 nameD1 :|-> ctxtD1Prime)
                            ,MapExpr (MetaCtxt sub2 sortD2 nameD2 :|-> sctxtD1Prime ctxtD2Prime)]
                 del1New = ctxtD1Prime:(delta1 del)
                 delNew  = applySolution solNew $ del{delta1=del1New}
                 problemNew = problem{delta=delNew,gamma=gam{expressionEq = (s1 :=?=: rhsNew):rest}, fresh=fresh', solution=solNew ++ sol}
             in  problemNew
              | (k1,k2) <- ks, k1 /= UserDefinedSort False "TRIV", k2 /= UserDefinedSort False "TRIV"]
         Nothing -> []
   )
   ++
   -- prefix rule D2[s2] =?= D1[s1] 
   --  
   --    pfxTable(cl(D2),cl(D1)) = K1,K2
   --    D2 -> D2'         cl(D2') = K1
   --    D1 -> D2'D1'      cl(D1') = K2
   --    D1'[s1] =?= s2
   --
     (case Map.lookup (sortD2,sortD1) (pfxTable $ contextDefinitions problem) of
         Just ks ->
            [let (_nameD1Prime:_nameD2Prime:fresh') = fresh problem
                 lhsNew   = if isTriv k2 then s1 else MetaCtxtSubst emptySubstitution k2 nameD1Prime s1
                 nameD1Prime = ctxtToString k2 ++ _nameD1Prime
                 nameD2Prime = ctxtToString k1 ++ _nameD2Prime
                 ctxtD2Prime = if isTriv k1 then Hole sub2 else MetaCtxt sub2 k1 nameD2Prime
                 sctxtD2Prime = if isTriv k1 then id else \hole -> MetaCtxtSubst sub1 k1 nameD2Prime hole
                 ctxtD1Prime = if isTriv k2 then Hole emptySubstitution else MetaCtxt emptySubstitution k2 nameD1Prime
                 solNew   = [MapExpr (MetaCtxt sub2 sortD2 nameD2 :|-> ctxtD2Prime)
                            ,MapExpr (MetaCtxt sub1 sortD1 nameD1 :|-> sctxtD2Prime ctxtD1Prime)]

                 del1New = ctxtD2Prime:(delta1 del)
                 delNew  = applySolution solNew (del{delta1=del1New})
              
                 problemNew = problem{delta=delNew,gamma=gam{expressionEq = (s2 :=?=: lhsNew ):rest}, fresh=fresh', solution=solNew++sol}
             in  problemNew
              | (k1,k2) <- ks]
         Nothing -> []
   )
   ++
   -- fork rule
   --   D1[s1] =?= D2[s2]
   --   forktable(cl(D1),cl(D2)) = K1,K2,K3,d
   --   D1 |-> D3^K1[d[D4^K2[.],D5^K3[s2]]
   --   D2 |-> D3^K1[d[D4^K2[s1],D5^K3[.]]
   --   delete eq
     (case Map.lookup (sortD1,sortD2) (frkTable $ contextDefinitions problem) of
         Just ks ->
            [let (_nameD3:_nameD4:_nameD5:fresh') = fresh problem
                 nameD3 = ctxtToString k1 ++ _nameD3
                 nameD4 = ctxtToString k2 ++ _nameD4
                 nameD5 = ctxtToString k3 ++ _nameD5
                 (d,fresh'')  = makeExprFresh dctxt fresh'
                 sctxtD3 = if isTriv k1 then id else \hole -> MetaCtxtSubst sub2 k1 nameD3 hole
                 sctxtD4 = if isTriv k2 then id else \hole -> MetaCtxtSubst emptySubstitution k2 nameD4 hole
                 sctxtD5 = if isTriv k3 then id else \hole -> MetaCtxtSubst emptySubstitution k3 nameD5 hole
                 ctxtD4 =  if isTriv k2 then Hole emptySubstitution else MetaCtxt emptySubstitution k2 nameD4
                 ctxtD5 =  if isTriv k3 then Hole emptySubstitution else MetaCtxt emptySubstitution k3 nameD5 
                 solNew   = [MapExpr (MetaCtxt sub1 sortD1 nameD1 :|-> sctxtD3 (subst (sctxtD5 s2) (MultiHole 2) (subst ctxtD4 (MultiHole 1) d)))
                            ,MapExpr (MetaCtxt sub2 sortD2 nameD2 :|-> sctxtD3 (subst ctxtD5 (MultiHole 2) (subst (sctxtD4 s1) (MultiHole 1) d)))
                             ]
                 delNew = applySolution solNew del
                 problemNew = problem{delta=delNew, gamma=gam{expressionEq = rest}, fresh=fresh'', solution=solNew++sol}
             in  problemNew
              | (k1,k2,k3,dctxt) <- ks]
         Nothing -> []
   )
  


-- no context rule applicable:
--
 | otherwise = Empty
-- ********************************************************************************************************************


-- ********************************************************************************************************************
-- Rules for Environments
-- ********************************************************************************************************************
-- The rules for enviroments  work on environment equations
--
applyEnvRules :: Problem -> UnifResult
applyEnvRules problem@(Problem{solution=sol,gamma=gam,delta=del})
--  
--  RULE: (Sol,Gamma cup {E = env}, Delta) --> (Sol cup {E |-> env}, Gamma,Delta[env/E])
--        if env is nonempty 
--  (env-E1)
--  
  | ((left :=?=: r),rest):_ 
      <-  [splitting | splitting@(left :=?=: r,rest) <- splits (environmentEq gam)
                               ,isMetaEnv left
                               ,isSingleton  (metaEnvVars left)
                               ,let [e] = metaEnvVars left
                               ,e `notElem` (delta2 del) 
                                || (not (null (bindings r))) 
                                || (not (null (metaChains r))) 
                                || (not (null [v | v <- metaEnvVars r, v `elem` (delta2 del)])) 
                               ]
    ,[Env sub e] <- (metaEnvVars left)
     = 
       let sol' = (MapEnv $ Env sub e :|-> r): sol
           gam' = gam{environmentEq = rest}
           del' = substEnv r e del 
       in Some [problem{solution=sol',gamma=gam',delta=del'}] 
--  
--  RULE: (Sol,Gamma cup {E1...En = {}}, Delta) --> (Sol cup {E1 |-> {},...,En |-> {}}, Gamma,Delta[{}/E1,...,{}/En])
--  if for all i: Ei not in Delta2 (i.e. all Ei must be empty) 
-- (env-E2)
--  
  | ((left :=?=: r),rest):_ 
      <-  [splitting | splitting@(left :=?=: r,rest) <- splits (environmentEq gam)
                               ,null (bindings left)
                               ,null (metaChains left)
                               ,isEmpty r
                               ,not (null $ metaEnvVars left)
                               ,and [e `notElem` (delta2 del) | e <- metaEnvVars left] 
                               ]
    ,es <- (metaEnvVars left)
     = 
       let sol' = [(MapEnv $ ei :|-> empty) | ei <- es ] ++ sol
           gam' = gam{environmentEq = rest}
           del' = foldr (\e d -> substEmpty e d) del [q | Env sub q <- es]
       in Some [problem{solution=sol',gamma=gam',delta=del'}]        
--  
--  RULE: (Sol,Gamma cup {E = E1,...,En}}, Delta) -> |_forall i: (Sol, Gamma cup {E=E1,...,En}, Delta2 cup {Ei})
--   if E in Delta2, if for all i: Ei not in Delta2 (i.e. guess some Ei to be non-empty)
-- (env-E3) 
--  
  | ((left :=?=: r),rest):_ 
      <-  [splitting | splitting@(left :=?=: r,rest) <- splits (environmentEq gam)
                               ,isMetaEnv left
                               ,isSingleton (metaEnvVars left)
                               ,let [e] = metaEnvVars left
                               ,null (bindings r)
                               ,null (metaChains r)
                               ,not (null (metaEnvVars r))
                               ,let es = metaEnvVars r
                               ,e `elem` (delta2 del)
                               ,and [ei `notElem` (delta2 del) | ei <- es] 
                               ]
    ,[e] <- (metaEnvVars left)
    ,es <- metaEnvVars r
     =  Some [problem{delta = del{delta2 = ei:(delta2 del)}} | ei <- es] 

--  
--  RULE: (Sol,Gamma cup {b1;env1 = env2}, Delta --> |_Possibilites (Sol, Gamma cup ..., Delta)
--   several cases to match the binding b1 against something in env2 (details see below)
--  (env-b)
--  
  | (((left :=?=: right),rest):_) <- [(left :=?=: right,rest) | splitting@(left :=?=: right,rest) <- splits (environmentEq gam)
                                                              , hasBinding left
                                                              , not (null (bindings right) && null (metaEnvVars right) && null (metaChains right))]
  ,((b1,bindings_left'):_) <- splits (bindings left)
     =
      Some $
--  RULE: (Sol,Gamma cup {b1;env1 = env2}, Delta --> |forall b2;env2' = env2 (Sol, Gamma cup {env1=env2; b1=b2} ..., Delta)
              [ -- for each binding b2 from right
                let gam'   = gam {bindingEq = (b1 :=?=: b2):bindingEq gam
                                 ,environmentEq = (left' :=?=: right'):rest}
                    left'  = left{bindings = bindings_left'}
                    right' = right{bindings = bindings_right'}
                in problem{gamma=gam'}                           
                | (b2,bindings_right') <- splits (bindings right)
              ]
--  RULE: (Sol,Gamma cup {b1;env1 = env2}, Delta --> |forall E;env2' = env2 (Sol cup {E -> b1;E'}, Gamma cup {env1 = E';env2'}, Delta[b1;E'/E])
              ++
              [ -- for each metaEnv E from right
                let (name_e':fresh') = fresh problem
                    e' = Env emptySubstitution ("E" ++ name_e')
                    b1e' = empty{bindings=[b1],metaChains=[],metaEnvVars=[e']}
                    sol' = (MapEnv $  Env emptySubstitution e :|-> b1e'):sol
                    gam' = gam {environmentEq = (left' :=?=: right'):rest}
                    left' = left{bindings = bindings_left'}
                    right' = right{metaEnvVars = e':metaEnvVars_right'}
                    del' = substEnv b1e' e del
                in problem{solution=sol',fresh=fresh',gamma=gam',delta=del'}                           
                | (Env sub e,metaEnvVars_right') <-  splits (metaEnvVars right)
              ]
              ++
--  RULE: (Sol,Gamma cup {b1;env1 = env2}, Delta --> |forall EE[z,s];env2' = env2 
--     case 1:
--                (Sol cup {EE[z,s] |-> z=A[s]}, 
--                 Gamma cup {env1 = env2'; b1 = z=A[s]}, 
--                 Delta[z=A[s]/EE[z,s])
--     case 2:
--             |  (Sol cup {EE[z,s] |-> z=A[var X]; EE'[X,s]}, 
--                 Gamma cup {env1 = EE'[X,s];env2'; b1 = z=A[var X]}, 
--                 Delta[z=A[var X],EE'[X,s]/EE[z,s]])
--     case 3:
--             |  (Sol cup {EE[z,s] |-> EE1[z,var Y];Y=A[var X];EE2[X,s]}, 
--                 Gamma cup {env1 = EE1[z,var Y];EE2[X,s];env2'; b1 = z=A[var X]}, 
--                 Delta[EE1[z,var Y];Y=A[var X];EE2[X,s]/EE[z,s]])
--     case 4:
--             |  (Sol cup {EE[z,s] |-> EE1[z,var Y];Y=A[s]}, 
--                 Gamma cup {env1 = EE1[z,var Y];env2'; b1 = Y=A[s]}, 
--                 Delta[EE1[z,var Y];Y=A[s]/EE[z,s]])

-- analogous for VV-Chains where the A-Ctxt is not generated, and VV-Chains are splitted into VV-Chains

              (concat [ -- for each metaChain EE from right
--     case 1:
                 [let 
                      sol' = (MapCV (MetaChain emptySubstitution typ ee z s :|-> sigma_right)):sol
                      (name_ctxtname:fresh') = fresh problem
                      ctxtname = "A" ++ name_ctxtname
                      z_A_s = \z s ->
                                case typ of 
                                 (UserDefinedChain ch) -> (z := MetaCtxtSubst emptySubstitution (UserDefinedSort False ch) ctxtname s)
                                 VarChain -> (z := s)
                      gam' = gam{environmentEq =(left' :=?=: right'):rest
                                ,bindingEq = (b1 :=?=: z_A_s z s):(bindingEq gam)
                                }
                      sigma_right = (\z s -> binding (z_A_s z s))
                      left' = left{bindings=bindings_left'}
                      right' = right{metaChains = metaChains_right'}
                      del' = substChainVar sigma_right (MetaChain emptySubstitution  typ ee z s) del
                  in problem{fresh = fresh',gamma = gam',solution=sol',delta=del'}
                  ,
--     case 2:
                  let 
                      sol' = (MapCV $ MetaChain emptySubstitution  typ ee z s :|-> sigma_right):sol
                      sigma_right = \z s -> (binding (z_A_vletX' z)){metaChains = [ee' s]}
                      (name_ctxtname:name_x':name_ee':fresh') = fresh problem
                      ctxtname = "A" ++ name_ctxtname
                      x' =  MetaVarLift emptySubstitution  ("X"++name_x')
                      ee' = \s -> case typ of 
                              (UserDefinedChain ch) -> MetaChain emptySubstitution  (UserDefinedChain ch) ("EE" ++ name_ee') x' s 
                              VarChain -> MetaChain emptySubstitution  VarChain ("VV" ++ name_ee') x' s 
                              
                      z_A_vletX' = \z -> case typ of 
                                       (UserDefinedChain ch) -> (z := MetaCtxtSubst emptySubstitution  (UserDefinedSort False ch) ctxtname (VarL x'))
                                       VarChain -> (z:= (VarL x'))
                      gam' = gam{environmentEq =(left' :=?=: right'):rest
                                ,bindingEq = (b1 :=?=: z_A_vletX' z):(bindingEq gam)
                                }
                      left' = left{bindings=bindings_left'}
                      right' = right{metaChains = (ee' s):metaChains_right'}
                      del' = substChainVar sigma_right (MetaChain emptySubstitution  typ ee z s) del 
                      
                  in problem{fresh = fresh',gamma = gam',solution=sol',delta=del'}
                  ,
--     case 3:
                  let 
                      sol' = (MapCV $ MetaChain emptySubstitution  typ ee z s :|-> sigma_right):sol
                      sigma_right = \z s -> (binding x1_A_vletX2){metaChains = [ee1 z,ee2 s]}
                      (name_ctxtname:name_x1:name_x2:name_ee1:name_ee2:fresh') = fresh problem
                      ctxtname = "A" ++ name_ctxtname
                      x1 =  MetaVarLift emptySubstitution ("X"++name_x1)
                      x2 =  MetaVarLift emptySubstitution ("X"++name_x2)
                      ee1 = \z -> 
                             case typ of
                               (UserDefinedChain ch) -> MetaChain emptySubstitution  (UserDefinedChain ch) ("EE" ++ name_ee1) z (VarL x1)                               
                               VarChain -> MetaChain emptySubstitution  VarChain ("VV" ++ name_ee1) z (VarL x1)
                      ee2 = \s ->
                             case typ of 
                               (UserDefinedChain ch) -> MetaChain emptySubstitution  (UserDefinedChain ch) ("EE" ++ name_ee2) x2 s
                               VarChain -> MetaChain emptySubstitution  VarChain ("VV" ++ name_ee2) x2 s
                      x1_A_vletX2 = case typ of 
                                       (UserDefinedChain ch) -> (x1 := MetaCtxtSubst emptySubstitution  (UserDefinedSort False ch) ctxtname (VarL x2))                                       
                                       VarChain -> (x1 := VarL x2) 
                      gam' = gam{environmentEq =(left' :=?=: right'):rest
                                ,bindingEq = (b1 :=?=: x1_A_vletX2):(bindingEq gam)
                                }
                      left' = left{bindings=bindings_left'}
                      right' = right{metaChains = [ee1 z,ee2 s]++metaChains_right'}
                      del' = substChainVar sigma_right (MetaChain emptySubstitution typ ee z s) del
                      
                  in problem{fresh = fresh',gamma = gam',solution=sol',delta=del'}
                 ,
--     case 4:
                  let 
                      sol' = (MapCV $ MetaChain emptySubstitution typ ee z s :|-> sigma_right):sol
                      sigma_right= \z s -> (binding (x1_A_s s)){metaChains = [ee' z]}
                      (name_ctxtname:name_x1:name_ee':fresh') = fresh problem
                      ctxtname = "A" ++ name_ctxtname
                      x1 =  MetaVarLift emptySubstitution ("X"++name_x1)
                      ee' = \z -> 
                             case typ of 
                              (UserDefinedChain ch) -> MetaChain emptySubstitution (UserDefinedChain ch) ("EE" ++ name_ee') z (VarL x1)
                              VarChain -> MetaChain emptySubstitution VarChain ("VV" ++ name_ee') z (VarL x1)
                      x1_A_s = \s -> 
                                case typ of 
                                 (UserDefinedChain ch) -> (x1 := MetaCtxtSubst emptySubstitution (UserDefinedSort False ch) ctxtname s)
                                 VarChain -> (x1 := s)
                      gam' = gam{environmentEq =(left' :=?=: right'):rest
                                ,bindingEq = (b1 :=?=: (x1_A_s s)):(bindingEq gam)
                                }
                      left' = left{bindings=bindings_left'}
                      right' = right{metaChains = (ee' z):metaChains_right'}
                      del' = substChainVar sigma_right (MetaChain emptySubstitution typ ee z s) del
                  in problem{fresh = fresh',gamma = gam',solution=sol',delta=del'}
                 ]
                 

                | ((MetaChain sub typ ee z s),metaChains_right') <- splits (metaChains right)
              ])
           -- for all b1 in the left

          
-- RULE: (Sol,Gamma cup {E1;...;En = EE1[x1,s1]; ...; EEk[xk;sk]; E1';...;Em'},Delta)
--   -->  (Sol circ sigma,Gamma, (Delta1,Delta2 cup Delta2',Delta3, Delta4 cup j=1,...,n{E1,j; ...; En,j = EEj[xj,sj]} )
--  where sigma = i=1 to n {Ei |-> Ei,1 ; ... ; Ei,k+m} cup j=1...m{Ej' |-> E1,k+j;...;En k+j}
--    and Delta2' is a minimal subset of {Eij} s.t.
--    * Ei in Delta2 => some Ei,h in Delta2', 
--    * Ej' in Delta2 implies some Eh,k+j in Delta2',and
--    * some Eh,j in Delta2' for all 1 <= j <= k
-- (envCh)

 | ((left :=?=: right,rest):_)
     <- [splittings | splittings@(left :=?=: right,rest) <- splits (environmentEq gam)
         , not (hasBinding left)
         , not (hasBinding right)          
         , not (hasMetaChain left)
         , (length (metaEnvVars left) > 1)
         , (hasMetaChain right || ((length (metaEnvVars right)) > 1))
         ] 
  =
   let frsh1       = (fresh problem)
       bigEsLeft   = metaEnvVars left
       bigEsLeftNumbered = zip [1..] bigEsLeft
       bigEsRight  = metaEnvVars right      
       chainsRight = metaChains right
       k           = length chainsRight
       n           = length bigEsLeft
       m           = length bigEsRight
       newEs       = let names = map (\x -> Env emptySubstitution $ "E" ++ x) (take (n*(k+m)) frsh1)
                         indices = [(i,j) | i <- [1..n], j <- [1..k+m]]
                     in zip indices names
       frsh2 =  drop (n*(k+m)) frsh1                     
       newEsGroupedInRows    = map (map snd) $ groupBy ((==) `on` (fst . fst)) newEs -- vect of rows
       newEsGroupedInColumns = map (map snd) $ groupBy ((==) `on` (snd . fst)) $ (sortBy (\((a1,b1),c1) ((a2,b2),c2) -> compare (b1,a1) (b2,a2)) newEs) -- vect of columns
      {-  These are the new Es as a matrix:
      
       E_1,1 ... E_1,k+m
       ...
       E_i,1 ... E_i,k+m
       ...
       E_n,1 ....E_n,k+m
      -}
       
       -- select one of each column of the first k colums
       delta2InitsForChVars :: [[EnvVar]]
       delta2InitsForChVars =
         let cols = take k newEsGroupedInColumns
             go [] = [[]]
             go (col:cols) =  [sel:rest | (sel,_) <- splits col, rest <- (go cols)]
         in go cols
       -- the selection for each E' in Delta2: due to minimality it has to respect the choices before
       ePrimesWithNewEij :: [(EnvVar,[EnvVar])]
       ePrimesWithNewEij = zip bigEsRight (drop k newEsGroupedInColumns)  -- (E_i' |- E...E)

       delta2ForBigEiRights :: [EnvVar] -> [(EnvVar,[EnvVar])] -> [ [EnvVar] ]
       delta2ForBigEiRights someDeltaInit [] = [someDeltaInit]
       delta2ForBigEiRights someDeltaInit ((bigEiRight,newEijs):xxs)
         | bigEiRight `elem` (delta2 del) =
            case [e | e <- someDeltaInit, e `elem` newEijs]
              of [] -> concat [delta2ForBigEiRights (sel:someDeltaInit) xxs | (sel,_) <- splits newEijs]
                 (_:_) -> delta2ForBigEiRights someDeltaInit xxs
         | otherwise = delta2ForBigEiRights someDeltaInit xxs
       delta2Step2ForERights :: [[EnvVar]]         
       delta2Step2ForERights = concat [delta2ForBigEiRights delta2Init ePrimesWithNewEij | delta2Init <- delta2InitsForChVars]

       eWithNewEij :: [(EnvVar,[EnvVar])]
       eWithNewEij = zip bigEsLeft newEsGroupedInRows
       delta2ForBigEiLefts :: [EnvVar] -> [(EnvVar,[EnvVar])] -> [[EnvVar]]
       delta2ForBigEiLefts someDeltaInit [] = [someDeltaInit]
       delta2ForBigEiLefts someDeltaInit ((bigEi,newEijs):xxs)
         | bigEi `elem` (delta2 del) =
            case [e | e <- someDeltaInit, e `elem` newEijs]
              of [] -> concat [delta2ForBigEiLefts (sel:someDeltaInit) xxs | (sel,_) <- splits newEijs]
                 (_:_) -> delta2ForBigEiLefts someDeltaInit xxs
         | otherwise = delta2ForBigEiLefts someDeltaInit xxs
         
       delta2Step3ForELefts = concat [delta2ForBigEiLefts delta2Init eWithNewEij | delta2Init <- delta2Step2ForERights]
       sigma1 = zipWith (\ei es -> MapEnv (ei:|-> empty{metaEnvVars = es})) bigEsLeft newEsGroupedInRows
       bigEsInDelta2WithRow = [(i,ei,es) | (i,ei,es) <- (zipWith (\(i,ei) es -> (i,ei,es)) bigEsLeftNumbered newEsGroupedInRows), ei `elem` (delta2 del)]
       sigma2 = zipWith (\ei es -> MapEnv (ei :|-> empty{metaEnvVars = es})) bigEsRight (drop k newEsGroupedInColumns)
       newEquations = zipWith (\ch es -> empty{metaEnvVars = es} :=?=: empty{metaChains=[ch]}) chainsRight (take k newEsGroupedInColumns)
       sigma = sigma1 ++ sigma2
       sol' = sigma ++ sol
       gam'         = applySolution sigma gam{environmentEq=rest}
       res=
         [problem{ solution=sol'
                   , gamma=gam'
                   , fresh=frsh2
                    ,delta= applySolution sigma (del{ delta2 = (delta2 del) ++ delta2Prime
                                                   , delta4 =(delta4 del){environmentEq = environmentEq (delta4 del) ++ newEquations}})}
           | delta2Prime <-  minimizeSets delta2Step3ForELefts
           ]
    in  (Some res) 
--    
-- no environment rule applicable:
--
 | otherwise = Empty

-- ********************************************************************************************************************
-- expandDelta4Constraints expands constraints E1;E2 =?= EE[z,s] 
-- if there are non-capture constraints (E1,E2) or (E2,E1)
expandDelta4ConstraintsIfPossible :: Problem -> UnifResult
expandDelta4ConstraintsIfPossible problem@(Problem{solution=sol,gamma=gam,delta=del})
 | Just (eq@(left :=?=: right),rest)
     <- splitBy (\eq -> fst (del4constrSplitable (splitCapCon (delta3 del)) eq)) (environmentEq (delta4 del))
 ,[envVar1,envVar2] <- metaEnvVars left
 ,[ch@(MetaChain sub typ ee z s) ] <- metaChains right
 ,[(Env sub1 e1),(Env sub2 e2)] <- if snd  (del4constrSplitable (splitCapCon (delta3 del)) eq)
                                     then [envVar2,envVar1] -- commute
                                      else [envVar1,envVar2]
 =  Some [
      let (_name_ee1:_name_ee2:_name_ctxt:_name_zi:fresh') =  (fresh problem)
          name_ee1 = case typ of 
                      VarChain -> "VV" ++ _name_ee1
                      _ -> "EE" ++ _name_ee1
          name_ee2 = case typ of 
                      VarChain -> "VV" ++ _name_ee2
                      _ -> "EE" ++ _name_ee2
          name_ctxt =  "A" ++ _name_ctxt
          name_zi   =  "X" ++ _name_zi
          zi        = ((MetaVarLift emptySubstitution name_zi))
          actxt  = case typ of 
                     (UserDefinedChain ch) ->    \h  -> MetaCtxtSubst emptySubstitution (UserDefinedSort False ch) name_ctxt h
                     VarChain ->   id
          ee1 = \z -> MetaChain sub typ name_ee1 z (actxt (VarL zi))
          ee2 = \s -> MetaChain sub typ name_ee2 zi s
          solNew = MapCV (MetaChain sub typ ee z s :|-> \z s -> empty{metaChains = [(ee1 z), (ee2 s)]})
          eqsNew = [empty{metaEnvVars=[Env sub1 e1]} :=?=: empty{metaChains=[ee2 s]}
                   ,empty{metaEnvVars=[Env sub2 e2]} :=?=: empty{metaChains=[ee1 z]}]
          del' = applySolution [solNew] (del{delta4=(delta4 del){environmentEq = rest}})
          gam' = gam{environmentEq = (environmentEq gam) ++ eqsNew}
          sol' = solNew:sol 
      in 
           
           problem{gamma=gam',solution=sol',delta=del',fresh=fresh'}
      ,
      let 
          eqsNew = [empty{metaEnvVars=[Env sub1 e1]} :=?=: empty
                   ,empty{metaEnvVars=[Env sub2 e2]} :=?=: empty{metaChains=[ ch ]}]
          gam' = gam{environmentEq = (environmentEq gam) ++ eqsNew}
          del' = (del{delta4=(delta4 del){environmentEq = rest}})
       in problem{gamma=gam',delta=del'}
      ,
      let 
          eqsNew = [empty{metaEnvVars=[Env sub1 e1]} :=?=: empty{metaChains=[ch]}
                   ,empty{metaEnvVars=[Env sub2 e2]} :=?=: empty]
          gam' = gam{environmentEq = (environmentEq gam) ++ eqsNew}
          del' = (del{delta4=(delta4 del){environmentEq = rest}})
       in problem{gamma=gam',delta=del'}
       ]
       
expandDelta4ConstraintsIfPossible problem@(Problem{solution=sol,gamma=gam,delta=del}) = Empty

del4constrSplitable del3splitted (left :=?=: right)
 | null (metaChains left)
 , null (bindings left)
 , null (metaEnvVars right)
 , null (bindings right)
 , [(Env sub1 e1),(Env sub2 e2)] <- metaEnvVars left
 , [MetaChain sub typ ee z s ] <- metaChains right
 , Just set <- Map.lookup (SCMetaEnv (Env sub1 e1)) del3splitted
 , SCMetaEnv (Env sub2 e2) `Set.member` set   
 = (True,False)  -- True and the envVars are in the right order
del4constrSplitable del3splitted (left :=?=: right)
 | null (metaChains left)
 , null (bindings left)
 , null (metaEnvVars right)
 , null (bindings right)
 , [(Env sub2 e2),(Env sub1 e1)] <- metaEnvVars left
 , [MetaChain sub typ ee z s ] <- metaChains right
 , Just set <- Map.lookup (SCMetaEnv (Env sub1 e1)) del3splitted
 , SCMetaEnv (Env sub2 e2) `Set.member` set   
 = (True,True)  -- True but the bindings have to be commuted
 | otherwise 
 = (False,False)
-- ********************************************************************************************************************
  
 
-- ********************************************************************************************************************
-- Check whether Delta4 chains are solvable
-- solvableChains checks whether the environment constraints in Delta4 are solvable
-- It returns Nothing, if the constraints are solvable, and Just text, otherwise, where text is some error message 
-- ********************************************************************************************************************
solvableChains :: Problem -> Maybe String
solvableChains  problem@(Problem{solution=sol,gamma=gam,delta=del}) =  run 
  where   
   run = 
       let distribs fn = goAll fn eqs freshInp
           res fn = let allChecks =
                            [(\x -> case checkNCCs (delta2 $ del) x of 
                                    Nothing -> Nothing -- 16i05 checkLVNabla3 x
                                    Just err -> Just (show err))  (applySolution sol del3)
                            | sol <- map composeAll $ combine (distribs fn)
                            ]
                    in if any isNothing allChecks 
                                        then Nothing -- found solution
                                        else Just ("Solvability Check failed: Found no solution:\n Delta3:" 
                                                          ++ show del3 ++ "\n" ++ "(Solutions,applied to del3):" 
                                                          ++ show [ (sol,applySolution sol del3) | sol <- map composeAll $ combine (distribs fn)])
                    
       in     res (\n1 n2 -> (n1^2*(n2+1)+n2))
   eqs = (environmentEq $ delta4 del)                               -- constraints in Delta 4   Ei1,...,Ein =?= EEi[z,s]
   leftEI = [v | (left :=?=: _) <- eqs, v <- metaEnvVars left]      -- E11,...,E1n,...,Em1,...Emn = all left hand sides of constraints
   del2EI = [ei | ei <- leftEI, ei `elem` (delta2 del)]             -- {E11,...,E1n,...,Em1,...Emn} cup Delta2, i.e. such Eij that are non-empty
   del3 = (delta3 del)                                              -- Delta 3
   freshInp = fresh problem 
   combine [] = [[]]
   combine (d:ds) = [sol++sols | sol <- d, sols <- (combine ds)]   
   
   goAll fn  [] _ = []
   goAll fn (eq:eqs) f = 
    let (dist,fresh1) = goB fn eq f
    in   (dist:(goAll fn eqs fresh1))
       
   goB f equation@(left :=?=: right) fresh = 
    let
        leftEI = metaEnvVars left
        [MetaChain sub typ ee z s] = metaChains right
        n = length leftEI
        delE = [v | v <- leftEI, v `elem` (del2EI)]   
        n1 = length delE
        n2 = n - n1
        maxbound =  f n1 n2 
        dfs i b fv 
            | i > b = (fv,[])
        dfs i b fv = let (d,fv') = go equation i fv
                         (fv'',ds) = dfs (i+1) b fv'
                     in (fv'',d:ds)
        (freshOut,d) = dfs 1 maxbound fresh           
    in  (concat d,freshOut)
   go equation@(left :=?=: right) bound' fresh0 = 
    let 
        leftEI = metaEnvVars left
        [MetaChain sub typ ee z s] = metaChains right
        (names_a,fresh1) = splitAt (bound'-1) fresh0
        (names_x,fresh2) = splitAt (bound'-1) fresh1
        (x:a:freshrest) = fresh2
        (varsA) = 
                  [case typ of
                     (UserDefinedChain ch) ->  MetaCtxtSubst emptySubstitution  (UserDefinedSort False ch) (ch ++ name_a)
                     VarChain -> id
                   | name_a <- names_a]
        (varsX) = [MetaVarLift emptySubstitution  ("X" ++ name_x) | name_x <- names_x]
        newBindingsGEQ3 = zipWith 
                           (\y r -> y:=r) 
                           (z:varsX) 
                           ((zipWith 
                                (\a x -> (a (VarL x))) 
                                (varsA) 
                                varsX)++[s])
        newBindings1 = [z:=s]
        newBindings2 = [z:=case typ of 
                             (UserDefinedChain ch) -> MetaCtxtSubst emptySubstitution (UserDefinedSort False ch) (ch ++ a) (VarL (MetaVarLift emptySubstitution ("X" ++ x)))                             
                             VarChain ->  (VarL (MetaVarLift emptySubstitution  ("X" ++ x)))
                        ,(MetaVarLift emptySubstitution  ("X" ++ x)) := s]
        toBeDistributedBindings = if bound' == 1 then newBindings1 
                                    else if bound' == 2 then newBindings2 
                                          else newBindingsGEQ3
        distribute :: [EnvVar] -> [Binding] -> [[SolutionEntry]]
        distribute [e] bs = [[(MapEnv (e :|-> empty{bindings=bs}))]]
        distribute (e:es) bs = [(MapEnv (e :|-> empty{bindings=here})):rest |  (here,next) <- (divide bs)
                               , rest <- distribute es next 
                               ]
        distributions = let res = distribute leftEI  toBeDistributedBindings in res -- trace ("ALL dist" ++ (unlines $ map show res)) res
        distributions' = let res = [dist | dist <- distributions, isValid dist] in res --trace ("VALID dist" ++ (unlines $ map show res)) res
        isValid dist = null [e | (MapEnv (e :|-> env)) <- dist, isEmpty env && e `elem` del2EI]
    in  (distributions',freshrest)

  
-- ********************************************************************************************************************
-- Commute all equations
-- ********************************************************************************************************************
commuteProblem prob = prob {gamma = gam'}
  where
   gam = gamma prob
   gam' = gam { expressionEq = map commute $ expressionEq gam 
              , bindingEq = map commute $ bindingEq gam
              , varLiftEq = map commute $ varLiftEq gam              
  
              , environmentEq = map commute $ environmentEq gam              
              }
              
commuteDelta4 prob = prob {delta = del'}
  where
   del = delta prob
   del4 = delta4 del
   del4' = del4 { expressionEq = map commute $ expressionEq del4 
                , bindingEq = map commute $ bindingEq del4
                , varLiftEq = map commute $ varLiftEq del4             
                , environmentEq = map commute $ environmentEq del4              
              }
   del' = del{delta4=del4'}
              
-- ********************************************************************************************************************

applyFullGuessRulesEnv problem@(Problem{solution=sol,gamma=gam,delta=del})
 | (q@(Env sub name):r) <- [x | x@(Env sub name) <- (envVarsInSol $ composeAll sol)
                                   , not (x `elem` (delta2 del) || (Env emptySubstitution name) `elem` (delta2 del)) ]
                                   
    = let problem1 = 
                let oldDel_2 = delta2 del
                    newDel_2 = q:oldDel_2
                in  problem{delta = del{delta2 = newDel_2}}
          problem2 = 
                let sol' = (MapEnv $ (Env sub name) :|-> empty):sol
                    gam' = substEnv empty name gam
                    del' = substEnv empty name del
               in problem{solution=sol',gamma=gam', delta=del'}
      in Some [problem1,problem2]
-- no guess rule applicable
 | otherwise = Empty

 
 
applyCtxtUnfolding problem@(Problem{solution=sol,gamma=gam,delta=del})
   = let candidates = [ x | x@(MetaCtxt sub srt name) <- (contextVarsInSol $ composeAll sol), (x `elem` (delta1 del) || (MetaCtxt emptySubstitution srt name) `elem` (delta1 del))] 
         go (x:xs) problems = go xs $ concat [branch x p | p <- problems]
         go []     problems = problems
     in go candidates [problem]
     
     
     
      
      
                   
                   
                   
 

branch (MetaCtxt sub srt name) problem@(Problem{solution=sol,gamma=gam,delta=del}) =
      let unfolds = Map.lookup (ctxtToString srt) (grammar $ contextDefinitions problem)
      in   
       case unfolds of 
         Nothing -> error $ "no unfolds for ctxt of srt " ++ ctxtToString srt
         Just ufs ->
           let fv = fresh problem
               go (u:us) fv = let (u',fv') = makeExprWithConstraintsFresh u fv
                                  (us',fv'') = (go us fv')
                              in (u':us', fv'')
               go [] fv = ([],fv)
               (rufs,fresh') = go [(case r of {Right x -> x; Left (x,y) -> x}) | r <- ufs] fv
               rufs' = filter (\(a,b,c,d) -> not (isHole a)) rufs
               
           in   [
                 let substitution = (MapExpr (MetaCtxt sub srt name :|-> c))
                 in problem{   gamma=applySolution [substitution] gam
                              ,solution=substitution:sol
                              ,fresh=fresh'
                              ,delta = applySolution [substitution] (del{delta1 = ndel1 ++ (delta1 del), delta2 = ndel2 ++ (delta2 del), delta3 = ndel3 ++ (delta3 del)})
                              }
                    | (c,ndel1,ndel2,ndel3) <- rufs'
                   ]
 