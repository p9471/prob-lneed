-----------------------------------------------------------------------------
-- |
-- Module      :  LRSX.Util.Util
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--  
-- several utility functions, which are mainly functions on lists
--
-----------------------------------------------------------------------------

module LRSX.Util.Util where
import Data.Function
import qualified Data.Map.Strict as Map
import Data.List
import Data.Maybe
import Data.Char
import qualified Data.Set as Set

-- | 'splitBy' gets a predicate on list elements and a list
--
--  * it returns 'Nothing' if no element fulfills the predicate
--
--  * it returns 'Just' (x,xs) if x is the first element in the
--    list that fulfills the predicate and xs is the list without x
splitBy :: (a -> Bool) -> [a] -> Maybe (a,[a])
splitBy _ [] = Nothing    
splitBy p (x:xs)
 | p x = Just (x,xs)
 | otherwise =
     case splitBy p xs of
       Just (y,ys) -> Just (y,(x:ys))     
       Nothing -> Nothing
       
       
-- | 'splits' splits a list into pairs (x,xs) where x is an element and xs are 
-- all other elements of the list       

splits :: [a] -> [(a,[a])]
splits list = 
 [ let (xs,x:ys) = splitAt i list in (x,(xs++ys)) | i <- [0..length list-1]]

-- | 'splits2' splits a list xs into 3-tupels (before,x,after), s.t. 
-- @xs = before ++ [x] ++ after@
--
-- e.g. @splits2 [1,2,3] = [([],1,[2,3]),([1],2,[3]),([1,2],3,[])]@
splits2 :: [a] -> [([a],a,[a])] 
splits2 list = 
 [ let (xs,x:ys) = splitAt i list in (xs,x,ys) | i <- [0..length list-1]]
 
-- | 'splits3' splits a list xs into 3-tupels (before,x,after), s.t. 
-- @xs = before ++ [x] ++ after@, but in reversed order compared to 'splits2':
--
-- e.g. @splits3 [1,2,3] = [([1,2],3,[]),([1],2,[3]),([],1,[2,3])]@
splits3 :: [a] -> [([a],a,[a])] 
splits3 list = 
 [ let (xs,x:ys) = splitAt i list in (xs,x,ys) | i <- [length list-1,length list-2..0]]
 
-- |  'assign' xs ys computes all assignments of elements of ys to elements of xs
-- the result is a list of assignments
-- each assignment is a list of pairs (x,ys') where the first components x
-- are the list xs and ys' is the list of assigned elements of ys 
-- 
-- example:
--
-- @
-- assign [1,2] ['A','B'] =
--  [[(1,['A','B']),(2,[])]
--  ,[(1,['A']),(2,['B'])]
--  ,[(1,['B']),(2,['A'])]
--  ,[(1,[]),(2,['A','B'])]]
-- @
assign :: [a] -> [b] -> [[(a,[b])]]
assign xs ys        = 
 let n = length xs
     nums = partitionInN n ys
     numberedxs = zip [1..] xs
     assignments = map (map (\x -> (fst (head x),map snd x))) $ map (groupBy (\x y -> fst x == fst y)) $ map (sortBy (compare `on` fst))  nums
  in    [merge numberedxs ((sortBy (compare `on` fst) (asgn))) |    asgn <- assignments]     
 where
    merge ((i,a):zs) yss@((j,b):us)
     | i == j  = (a,b):(merge zs us)
     | otherwise = (a,[]):merge zs yss
    merge ((_,a):zs) [] = (a,[]):(merge zs [])
    merge _ _ = [] 

    
partitionInN :: (Enum a, Num a) => a -> [b] -> [[(a, b)]]
partitionInN n objs = [zip xs objs | xs <- gen n (length objs)] 
 where
  gen _ 0   = [[]]  
  gen k len = [  i:xs | i <- [1..k], xs <- (gen k (len-1))]

-- | 'replacePos' i e xs replaces the element at position i in xs by e
-- note that the first element has position 1 (not 0!)
replacePos :: Int -> b -> [b] -> [b]
replacePos i e list = 
 let (xs,_:as) = splitAt (i-1) list
 in (xs ++ (e:as))

-- ''isSingleton' checks whether a list has exactly one element
isSingleton :: [a] -> Bool
isSingleton [_] = True
isSingleton _ = False

-- 'allDifferent' checks whether all elements of a list a different
allDifferent :: Ord a => [a] -> Bool
allDifferent xs = go (sort xs)
 where
  go [_] = True
  go []  = True
  go (a:b:ys)
   | a == b = False
   | otherwise = go (b:ys)   


   
findJoinPointsBy' ::  (a -> b -> Bool) -> [a] -> [b] -> [([a], [b])]  
findJoinPointsBy' predicate list1 singleton =
 go [] (zip list1 ([1..]::[Integer])) 
 where 
    a = head singleton 
    go before ((l,_):list)
        | l `predicate` a = (reverse (l:before),singleton):(go (l:before) list)
        | otherwise = go (l:before) list
    go _ [] = [] 
    
    
findJoinPointsBy ::  (a -> b -> Bool) -> [a] -> [b] -> [([a], [b])]    
    
findJoinPointsBy predicate list1 list2 =
 let
  joinAtLength l l1 l2 =
   let list1p = take l l1 
       list2p = take l l2
       joins  = [(list1prefix,list2prefix)
                  | list1prefix <- inits list1p
                  , list2prefix <- inits list2p
                  , not (null list1prefix)
                  , not (null list2prefix)
                  , predicate (last list1prefix) (last list2prefix)
                  , length (list1prefix) + length (list2prefix) == l
                 ]
   in if null joins then Nothing else Just joins
  maxLength = (length list1) + (length list2)                          
 in concat  
     [fromJust joins | i <- [1..maxLength]
            ,let joins = joinAtLength i list1 list2
            ,isJust joins]
            
listEq :: Eq a => [a] -> [a] -> Bool     
listEq xs ys = (and [x `elem` ys | x <- xs]) && (and [y `elem` xs | y <- ys])

-- | 'fst4' compute the first component of a 4-tuple
fst4 :: (a,b,c,d) -> a
fst4 (a,_,_,_) = a
-- | 'snd4' compute the second component of a 4-tuple
snd4 :: (a,b,c,d) -> b
snd4 (_,b,_,_) = b
-- | 'trd4' compute the third component of a 4-tuple
trd4 :: (a,b,c,d) -> c
trd4 (_,_,c,_) = c
-- | 'fth4' compute the fourth component of a 4-tuple
fth4 :: (a,b,c,d) -> d
fth4 (_,_,_,d) = d

-- | 'fastLazyNubOn' p xs removes duplicates in a list xs
--   using the function p to compute the key value of the list elements.
fastLazyNubOn :: Ord a => (t -> a) -> [t] -> [t]
fastLazyNubOn p xs = go p xs (Set.empty)
 where 
   go _ [] _ = []
   go pr (x:ys) mp 
     | Set.member (pr x) mp = go pr ys mp
     | otherwise = x:(go pr ys (Set.insert (pr x) mp))

-- | 'fastLazyNub' xs removes duplicates in a list xs
fastLazyNub :: Ord a => [a] -> [a]
fastLazyNub xs = go xs (Set.empty)
 where 
   go [] _ = []
   go (x:ys) set
     | x `Set.member` set = go ys set
     | otherwise = x:(go ys (Set.insert x set))
   
    
lookupPrefix ::
      [Char] 
      -> [([Char], (t1, t2) -> t)] 
      -> t1 
      -> t2 
      -> Maybe (t, Int, [Char])    
lookupPrefix _ []  _ _ = Nothing
lookupPrefix input ((k,r):xs) z s
 | k `isPrefixOf` input
 , (a:_) <- drop (length k) input
 , if isAlpha (head k) then  not (isAlpha a)  else True
 = let rest =(drop (length k) input) in Just (r (z,s),length k,rest)
 | k `isPrefixOf` input
 , null (drop (length k) input)
 = let rest =(drop (length k) input) in Just (r (z,s),length k,rest)
 | otherwise           = lookupPrefix input xs z s     
 
 
-- iterative deepening

dfsWithBound  :: Integral b => (a -> Bool)    -- Zieltest (goal)
     -> (a -> [a])     -- Nachfolgerfunktion (succ)
     -> [a]            -- Liste der zu bearbeitenden Knoten
     -> b        -- Tiefe
     -> Maybe (a, [a]) -- Ergebnis: Just (Zielknoten,Pfad) oder Nothing
     
dfsWithBound goal succfn stack maxdepth = 
  go   [(k,maxdepth,[k]) | k <- stack] 
 where
  go [] = Nothing -- nothing found
  go ((k,i,p):r) 
   | goal k = Just (k,p) -- found goal
   | i > 0  = go ([(k',i -1, k':p) | k' <- succfn k] ++ r)  
   | otherwise =  go r -- bound reached
             
idfs :: Integral b => 
        b   -- Bound
        -> (a -> Bool) -- goal test
                -> (a -> [a])  -- successor function 
                -> [a]         -- current list l
                -> Maybe (a,[a])     -- Resutlt: Just (goal,path) or Nothing
                
idfs maxbound goal succfn stack = 
 let -- compute all searches until the bound exceeds
     allSearches = [dfsWithBound goal succfn stack i | i <- [1..maxbound]]
 in
   case filter isJust allSearches of
     [] -> Nothing -- Nothing found
     (r:_) -> r   -- return first result
     
divide :: [t] -> [([t], [t])]
divide [] = [([],[])]
divide (x:xs) = 
  [(x:ys,zs) | (ys,zs) <- divide xs]
  ++
  [(ys,x:zs) | (ys,zs) <- divide xs]
  
minimizeSets :: Ord a => [[a]] -> [[a]]  
minimizeSets listOflists =
  let listOfSetsWD = map Set.fromList listOflists
      setOfSets = Set.fromList listOfSetsWD
      listOfSets = Set.elems setOfSets
  in [Set.elems set | (set,rest) <- splits listOfSets, all (== False) [(r `Set.isSubsetOf` set) | r <- rest]]

-- !!! is like !! but with better error message 
(!!!) :: Show a => [a] -> Int -> a
xs !!! i = if length xs <= i then error $ "!!! " ++ show xs ++ show i
            else xs !! i

lookupBy :: (t -> a -> Bool) -> t -> [a] -> Maybe a            
lookupBy p x xs = case  [y | y <- xs, p x y] of
                    [] -> Nothing
                    (z:_) -> Just z
lookupOrId :: Ord k => k -> Map.Map k k -> k    
lookupOrId key mp = 
    case Map.lookup key mp of
         Nothing -> key
         Just v -> v
lookupAll ::  Eq a => a -> [(a, t)] -> Maybe [t]         
lookupAll key list = case [v | (k,v) <- list, k == key] of
                      [] -> Nothing
                      vals -> Just vals
-- small helper function 
zipWithFresh op [] ys = ([],ys) 
zipWithFresh op (x:xs) (y:ys) = 
 let (rest,fresh') = (zipWithFresh op xs ys) 
 in ((x `op` y):rest,fresh')                      
