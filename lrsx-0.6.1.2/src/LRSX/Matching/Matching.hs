-----------------------------------------------------------------------------
-- |
-- Module      :  LRSX.Matching.Matching
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--  
-- The matching algorithm for lambda-expressions with recursive bindings
-- 
--
-----------------------------------------------------------------------------

module LRSX.Matching.Matching where
import Data.Function
import Data.Maybe
import Data.List
--
import Debug.Trace
--
import LRSX.Language.Syntax
import LRSX.Language.ContextDefinition
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import LRSX.Language.Fresh
import LRSX.Util.Util

import LRSX.Unification.UnificationProblem
import LRSX.Matching.ConstraintInclusion  hiding(trace)
import LRSX.Language.Alpha hiding(trace)
import LRSX.Language.Alpha.Rename
import LRSX.Language.Alpha.Simplify hiding(trace)
import LRSX.Language.Alpha.Similar  hiding(trace)
import LRSX.Language.Alpha.Equivalence hiding(trace)
import LRSX.Language.Alpha.Refresh hiding(trace)
-- ====================================================================================================================
-- Matching
-- ====================================================================================================================

-- assumption: Variables are on the left-hand side, all variables on the right hand side are treated as constants
-- nabla contains the given constraints for the right hand side
-- and delta contains the needed constraints for the left hand side
-- Matching will check that the given constraints imply the needed constraints.


-- ********************************************************************************************************************
-- The matching function performs the whole matching and builds a tableau
-- ********************************************************************************************************************

matching :: Problem -> Tableau

#ifdef DEBUG
matching problem 
 | trace (("\n\nMATCHING STARTS WITH PROBLEM\n" ++  show (gamma problem) ++ "\n")) False = undefined
#endif   

matching problem = 

  -- ---------------------------------------------------------------------------------------------------------------------------------------
  -- Initial checks on the input problem
  --
  --   check whether the LVC holds for the input equations
  case checkLVCGamma (gamma problem) of 
    Nothing ->  
       -- check the occurence restrictions for the left hand sides of the equations: we use the same function as for 
       -- the unification algorithm, but replace the right hand sides of the equations by a constant (since only the lhs have to be checked)
       case check problem{gamma=let gam  = (gamma problem)
                                    env' = [l :=?=: empty |  l :=?=: r <- environmentEq gam]
                                    ex'  = [l :=?=: Fn "__REPLACED_RHS" [] | l :=?=: r <- expressionEq gam]
                                    var' = [l :=?=: ConstantVarLift emptySubstitution "_X" | l :=?=: r <- varLiftEq gam]
                                in gam{environmentEq=env',expressionEq=ex',varLiftEq=var'}
                         }  of 
                  Nothing -> expand [1] problem
                  Just err -> Node problem [Fail $ "Occurence restrictions in input:" ++ err]
    Just err ->  Node problem [Fail $ "LVC failure in input:" ++ err]
  -- ---------------------------------------------------------------------------------------------------------------------------------------    
 where
  -- the expand function expands the problem until it is solved or fails. 
  -- the parameter i is used to number the steps (only for nicer output of the derivation tree)
  expand i pb  
--   | trace (show $ gamma pb) False = undefined  
   -- the problem is solved (gamma is empty) and no further step is applicable
   | solved (gamma pb)             
   , Empty <- matchStep  heur (pb) = 
       let -- compose the solution
           sol= composeAll (solution pb)
           -- apply the matcher to the equations
           instExpr = applySolution sol (gamma problem)
           -- apply the solution to the NCCs in delta3
           instDelta = applySolution sol (delta3 $ delta pb)    
           -- check the noncapture constraints 
           capcon  = checkNCCs (delta2 $ delta pb) instDelta  
           -- compute the NCCs that are implied by the LVC
           nccLVC = splitFlatToNCC (concatMap getNCCsEnvLVC $ concatMap  getEnvironments (concat [ [s,t] | s :=?=: t <- expressionEq instExpr]))
           -- check whether the given NCCs in nabla (or the alphaBase) implies the NCCs in Delta3
           (str,capConImplication,derivedNCCs) = nccImplies (delta2 $ delta pb) (nccLVC ++ (delta3 $ nabla  pb))  instDelta
       in 
           -- check the LVC of the instantiated expressions
           case checkLVCGamma instExpr of     
            Nothing -> 
             -- check whether the capture constraints are implied by Nabla
             case capConImplication of
              Nothing -> 
               -- check non-capture constraints for Delta3
               case capcon of 
                 Nothing -> -- solution found, produce a Solved-Leaf
                  -- trace str $
                   Solved (pb{solution=sol
                           -- add the NCCs from the LVC to nabla
                          ,nabla=(nabla pb){delta3=((delta3 (nabla pb)) ++ (nccLVC ))}
                          ,delta=(delta pb){delta3=(delta3 (delta pb)) ++ derivedNCCs}
                          ,mpLogging = 
                                       str ++ concat 
                                               ["\nMATCHINGLOG: " ++ line 
                                                | line <- ["START MATCHING"
                                                          ,"Matching ended successfully"
                                                          ,"Gamma:" ++ show (gamma problem)
                                                          ,"Delta:" ++ show (delta problem) 
                                                          ,"Nabla:" ++ show (nabla problem) 
                                                          ,"OUTPUT" 
                                                          ,"  Instantiated Expressions:" ++ show instExpr
                                                          ,"  Gamma                   :" ++ show (gamma pb)
                                                          ,"  Delta                   :" ++ show (delta pb) 
                                                          ,"  Nabla                   :" ++ show (nabla pb)
                                                          ,"  NCCs           " ++ showSplitted (splitCapCon (delta3 (delta pb)))
                                                          ,"  are implied by " ++ showSplitted (splitCapCon (delta3 (nabla pb)))
                                                          ,"STOP MATCHING"
                                                          ]
                                               ]})
                                               
                 Just err -> -- capture constraints in Delta3 are violated 
                                     error ( "PANIC: THIS SHOULD NOT HAPPEN, Matching fails for capture constraint fail:"  ++ err )
                                                 
              Just err -> -- NCCs in Delta are not implied by the NCCs in Nabla
#ifdef DEBUG                                                    
                 trace ("\nMATCHING:" ++
                        "\n Gamma:" ++ show (gamma problem) ++
                        "\n Delta:" ++ show (delta problem) ++
                        "\n Nabla:" ++ show (nabla problem) ++
                        "\n OUTPUT:" ++
                        "\n Instantiated Expressions:" ++ show instExpr ++
                        "\n Gamma:" ++ show (gamma pb) ++
                        "\n Delta:" ++ show (delta pb) ++
                        "\n Nabla:" ++ show (nabla pb)++
                        "\n NCCs:      " ++ showSplitted (splitCapCon (delta3 (delta pb))) ++
                        "\n ARE NOT IMPLIED by " ++ showSplitted (splitCapCon (delta3 (nabla pb))) ++
                        "\n INCLUSION ENGINE START\n"      ++ str ++
                        "\n INCLUSION ENGINE STOP\n" 
                      )
#endif                
                      -- produce a failure node
                      (Node pb [Fail $ "capture constraint implication fails: " ++ err])
            Just err -> -- LVC does not hold
#ifdef DEBUG
               trace ("Matching fails for WEAKDVC")
#endif                 
                     -- produce a failure node
                     Node pb [Fail $ "LVC failure " ++ err ++ " " ++ (show (expressionEq instExpr))]
  -- problem is not solved, so apply a matching step:                     
  expand (i:is) pb =
   case matchStep heur pb of 
     Some problems -> case problems of
                        xs ->  Node pb (map (\(j,p) -> expand (j:i:is) p) (zip [1..] xs))
     MatchFail err  ->  (Node pb [Fail err])
     Empty         -> error $ "PANIC: in match expand: " ++ (take 1000 $ show (pb)) 
     
  -- heuristic to apply the matching rules, first the fail rules, then the first order rules,
  -- then the context rules, then the guess rules and finally the rules for environments
  heur = [applyFailRules
         ,applyFirstOrderRules
         ,applyContextRules
         ,applyGuessRules
         ,applyEnvRules
         ]
     
  matchStep [] pb = Empty
  matchStep (matchfn:xs) pb =
    case matchfn pb of 
      Empty -> matchStep xs pb
      Some [] -> matchStep xs pb
      Some ps -> Some ps
      MatchFail err -> MatchFail err

-- ********************************************************************************************************************
-- The tableau of the matching algorithm
-- is an n-ary tree where leaves are Solved- or Fail-Nodes
data Tableau = Node Problem [Tableau] 
             | Fail String
             | Solved Problem 

instance Show Tableau where
 show t = showTableau [1] t
  where
   showTableau ind (Node p xs) = 
     indent ind ++ "Open: "  ++ "\n"
     ++ "   Gamma:\n" 
     ++ "     " ++ show (gamma p) ++ "\n"
     ++ "   Sol:\n"
     ++ "     " ++ (intercalate "\n     " (map show (solution p))) ++ "\n" 
     ++ "   Delta:\n"
     ++ "     " ++ (show (delta p)) ++ "\n"
     ++ "   ConstanstDelta:\n"
     ++ "     " ++ (show (nabla p)) ++ "\n"
     ++ "\n" ++ (concat [showTableau (if length xs > 1 then ind ++ [i] else ind) x | (i,x) <- zip [1..] xs])
   showTableau ind (Fail a)   = indent ind ++ "FAIL: " ++  a ++ "\n"
   showTableau ind (Solved pb) = 
            let sol = solution pb
                gam = applySolution sol (inputGamma pb)
                del = delta pb
            in indent ind ++ "Solved: Unified Equations:" ++ show gam ++ "\n  Solution:" ++ show sol ++ "\n"
   indent ind = let px = (concat $ intersperse "." (map show ind)) 
                in  px ++ (replicate (mx - (length px)) ' '     ) ++ ": "
   mx = 10 -- length (show $ branchDepth [1] t) 
   branchDepth i (Node _ [x]) = branchDepth i x
   branchDepth i (Node _ xs) =  let a = [branchDepth (i ++ [v]) x  | (v,x) <- zip [1..] xs]
                                in if null a then i else maximumBy (compare `on` (length . show)) a   
   branchDepth i _ = i 
    
-- some helper functions for the tableau:
failLeaves (Node a cs) = concatMap failLeaves cs
failLeaves (Fail x) = [x]
failLeaves _ = []

-- compute solved and open leaves 


solvedAndOpenLeaves (Node a []) = [Node a []]
solvedAndOpenLeaves (Node _ cs) = concatMap solvedAndOpenLeaves cs
solvedAndOpenLeaves (Fail x)    = []
solvedAndOpenLeaves (Solved x)  = [Solved x] 

-- compute the solved leaves only    
solvedLeaves (Node _ cs) = concatMap solvedLeaves cs
solvedLeaves (Fail _)    = []
solvedLeaves (Solved x)  = [x] 

-- compute all leaves 
allLeaves (Node a []) = [Node a []]
allLeaves (Node _ cs) = concatMap allLeaves cs
allLeaves (Fail x)    = [Fail x]
allLeaves (Solved x)  = [Solved x] 
-- ********************************************************************************************************************
   
   

-- ********************************************************************************************************************
-- Result of one step of the matching algorithm 
 
data MatchResult = MatchFail String  -- ^ matching fails
                  | Empty            -- ^ no matching rule applicable
                  | Some [Problem]   -- ^ new problems after one step
 deriving(Show)
-- ********************************************************************************************************************

 
-- | applyFailRules tries to apply a failure to a state of the matching algorithm 
applyFailRules :: Problem -> MatchResult
applyFailRules problem@(Problem{solution=sol,gamma=gam,delta=del,nabla=cdel})
-- =========================================================
-- Context-Equation fail:
-- 
-- D1[s] =?= D2*[t] and one of the following cases holds:
--   
--     * case 1: D1 \not\in Delta1, D2* \not\in Nabla1, 
--               and prefix(class(D2),class(D1)) does not contain a pair (class(D2),K)
--               and class(D1) does not contain the empty context
--               Then D1 |-> D2D1'  is impossible and also D1 |-> [.] 
--               Thus fail.
-- 
--     * case 2: D1 \in Delta1, D2* \not\in Nabla1, 
--               then D2 is more general than D1 and D1 cannot be [.] 
--               Thus fail.
-- 
--     * case 3: D1 \in Delta1, D2* \in Nabla1, 
--               and prefix(class(D2),class(D1)) does not contain a pair (class(D2),K).
--               Then D1 |-> D2D1'  is impossible and D1 cannot be [.]
--               Thus fail.

 | Just (eq@(ctxt@(MetaCtxtSubst sub1 sortD1 nameD1 s1) :=?=: (ConstantCtxtSubst sub2  sortD2 nameD2 s2)),rest)
     <- splitBy (\(l:=?=:r) 
                   -> case (l,r) of 
                       (MetaCtxtSubst sub1 srtd1 d1 s, ConstantCtxtSubst sub2 srtd2 d2 s') ->  
                            (    
                                ((MetaCtxt sub1 srtd1 d1) `notElem` (delta1 del))
                             && ((ConstantCtxt sub2 srtd2 d2) `notElem` (delta1 cdel))
                             && ((ConstantCtxt emptySubstitution srtd2 d2) `notElem` (delta1 cdel))
                             && (let k1k2 = (Map.lookup (srtd2,srtd1) (pfxTable $ contextDefinitions problem))
                                 in isNothing k1k2 || ((null [() | (k1,k2) <- (fromJust k1k2), srtd2 == k1]))
                                )
                             && case Map.lookup (ctxtToString srtd1) (grammar $ contextDefinitions problem) of
                                  Nothing -> False
                                  Just xs -> let r = (Hole emptySubstitution) `notElem` (concat [rs | u <- xs, let rs=case u of {Right (a,b,c,d) -> [a]; Left ((a,_,_,_),(a',_,_,_)) -> [a,a']}]) in r
                                )
                             ||
                            (   
                                (let res =  
                                                ((MetaCtxt sub1 srtd1 d1) `elem` (delta1 del))
                                            && ((ConstantCtxt sub2 srtd2 d2) `notElem` (delta1 cdel))
                                            && ((ConstantCtxt emptySubstitution srtd2 d2) `notElem` (delta1 cdel))
                                 in res)
                            )                                
                              ||
                            (    
                                ((MetaCtxt sub1 srtd1 d1) `elem` (delta1 del))
                             && (((ConstantCtxt sub2 srtd2 d2) `elem` (delta1 cdel))
                                || ((ConstantCtxt emptySubstitution srtd2 d2) `elem` (delta1 cdel)))
                             && (let k1k2 = (Map.lookup (srtd2,srtd1) (pfxTable $ contextDefinitions problem))
                                 in isNothing k1k2 || (let q = (null [() | (k1,k2) <- (fromJust k1k2), srtd2 == k1]) in q)
                            ))
                       _    -> False) 
                (expressionEq gam) 
                

 =  MatchFail $ (show eq)                
-- =====================================================================                
-- Fail for variable equations:
-- \n{x} =?= \n{y}  or \n{x}alpha = \n{x}alpha' and alpha /= alpha'
  |  (eq:_) <- [eq | eq@(VarLift substx x :=?=: VarLift substy y) <- (varLiftEq gam), x /= y || not (substx `simsubst` substy)]
    =  MatchFail $ show eq 
  -- \n{x} =?= Y* 
  |  (eq:_) <- [eq | eq@((VarLift substx x) :=?=: (ConstantVarLift  substy y)) <-  (varLiftEq gam)] 
  =  MatchFail $ show eq 
  -- X* =?= n{x}
  |  (eq:_) <- [eq | eq@((ConstantVarLift  substx x) :=?=: (VarLift  substy y)) <-  (varLiftEq gam)]
    =  MatchFail $ show eq 
  -- X* =?= Y*  or X*alpha =?= X*alpha' and alpha /= alpha'
  |  (eq:_) <- [eq | eq@((ConstantVarLift  substx x) :=?=: (ConstantVarLift  substy y)) <-  (varLiftEq  gam), x /= y || not (substx `simsubst` substy)] 
    =  MatchFail $ show eq 
 -- Fail for expression equations
  -- var x =?= t  
  --  with t = f t1 ... tn or t = letrec ... or t = S*  or t = D*[t']
  | (eq:_) <- [eq | eq@(VarL _ :=?=: rhs) <- (expressionEq gam)
                  , case rhs of 
                        (Fn _ _) -> True
                        (Letrec _ _) -> True
                        (FlatLetrec _ _) -> True
                        (Constant _ _) -> True
                        (ConstantCtxtSubst _ _ _ _) -> True
                        _ -> False
               ] = MatchFail $ show eq
               
               
  -- f t1 ... tn = t 
  --  with t = f' s1 .. sn, or t = letrec ..., or t = S*, or t= var y, or t=D*[s]  
  | (eq:_) <- [eq | eq@(Fn f args :=?=: rhs) <- (expressionEq gam)
                  , case rhs of 
                        (Fn f' _) -> f /= f' 
                        (Letrec _ _) -> True
                        (FlatLetrec _ _) -> True
                        (Constant _ _) -> True
                        (VarL _) -> True
                        (ConstantCtxtSubst _ _ _ _) -> True
                        _ -> False
               ] = MatchFail $ show eq

  -- letrec ... = t 
  --  with t = f s1 .. sn, or t = S*, or t= var y, or t=D*[s]  
  | (eq:_) <- [eq | eq@(Letrec _ _ :=?=: rhs) <- (expressionEq gam)
                  , case rhs of 
                        (Fn _ _) -> True
                        (Constant _ _) -> True
                        (FlatLetrec _ _) -> True
                        (VarL _) -> True
                        (ConstantCtxtSubst _ _ _ _) -> True
                        _ -> False
               ] = MatchFail $ show eq
  | (eq:_) <- [eq | eq@(FlatLetrec _ _ :=?=: rhs) <- (expressionEq gam)
                  , case rhs of 
                        (Fn _ _) -> True
                        (Constant _ _) -> True
                        (Letrec _ _) -> True
                        (VarL _) -> True
                        (ConstantCtxtSubst _ _ _ _) -> True
                        _ -> False
               ] = MatchFail $ show eq
  -- S* = t 
  --  with t = f s1 .. sn, t=letrec..., or t = S'*, or t= var y, or t=D*[s]  
  -- or S* alpha =?= S* alpha' with alpha /= alpha'
  | (eq:_) <- [eq | eq@(Constant sub name :=?=: rhs) <- (expressionEq gam)
                  , case rhs of 
                        (Fn _ _) -> True
                        (Constant sub1 name1) -> name /= name1 || (not $ sub `simsubst` sub1)
                        (Letrec _ _) -> True
                        (FlatLetrec _ _) -> True
                        (VarL _) -> True
                        (ConstantCtxtSubst _ _ _ _) -> True
                        _ -> False
               ] = MatchFail $ show eq
  -- D*[s] =?= t and t /= D*[s']  
  | (eq:_) <- [eq | eq@(ConstantCtxtSubst sub srt name s :=?=: rhs) <- (expressionEq gam)
                  , case rhs of 
                        (ConstantCtxtSubst sub1 srt1 name1 s1) -> not (srt1 == srt && sub `simsubst` sub1 && name == name1)
                        _ -> True -- Fail in any other case 
               ] = MatchFail $ show eq
               

   -- D[s] =?= D1*[t'] and D1* not in Nabla1 
  | (eq:_) <- [eq | eq@(MetaCtxtSubst sub srt name s :=?=: rhs) <- (expressionEq gam)
                  , ((MetaCtxt sub srt name) `elem` (delta1 del) || (MetaCtxt emptySubstitution srt name) `elem` (delta1 del))
                  , case rhs of 
                        (ConstantCtxtSubst sub1 srt1 name1 s1) ->  not ((ConstantCtxt sub1 srt1 name1 `elem` (delta1 cdel))
                                                                        || (ConstantCtxt emptySubstitution srt1 name1 `elem` (delta1 cdel)))
                                                                
                        _ -> False
               ]
     = MatchFail $ show eq
-- ============================================================
-- Rules for environment equations
-- Fail: env = {} and env is not empty
   | (eq:_) <- [eq | eq@(left :=?=: right) <- (environmentEq gam)
               ,isEmpty right 
               ,  (not (null (bindings left )) 
                  || not (null (metaChains left)) 
                  || not (null (constantChains left))
                  || not (null [v | v <- constantEnvVars left])
                  || not (null [ v | v <- metaEnvVars left, v `elem` (delta2 del)]))]
    = MatchFail $ show eq
-- Fail: {} = env and env is not empty
--
   | (eq:_) <- [eq | eq@(left :=?=: right) <- (environmentEq gam)
                   , isEmpty left 
                   , not (null (bindings right )) 
                     || not (null (constantChains right))
                     || not (null [v | v <- constantEnvVars right])
                ]
       = MatchFail $ show eq
-- Fail: b;env = env' and env' has no bindings
--
   | (eq:_) <- [eq | eq@(left :=?=: right) <- (environmentEq gam)
                   , not (null (bindings left))
                   , null (bindings right)
                ]
       = MatchFail $ show eq
-- Fail: EE^K1;env = env' and env' has no bindings, and env' has no EE'^K2* with  sort K1 >= sort K2
-- TODO
   | (eq:_) <- [eq | eq@(left :=?=: right) <- (environmentEq gam)
                   , not (null (metaChains left))
                   , null (bindings right)
                   , null [ () |  ConstantChain sub1 typ1 name1 z1 s1 <- constantChains right, typ1 == VarChain]
                   , or [ null  [ () |  ConstantChain sub1 typ1 name1 z1 s1 <- constantChains right
                              , typ1 == typ]
                     | (MetaChain sub typ name z s) <- metaChains left]
                ]
       = MatchFail $ "UUU" ++ (show eq)
-- Fail: env = EE*^K2 and env /= E;env' and env /= EE'^K1 with   sort K1 >= sort K2
--
   | (eq:_) <- [eq | eq@(left :=?=: right) <- (environmentEq gam)
                   , not (null (constantChains right))
                   , null (metaEnvVars left)
                   , or [null [() | MetaChain sub1 typ1 name1 z1 s1 <- metaChains left
                                  , typ1 == typ || typ == VarChain
                                  ]
                         && null [() | (ConstantChain sub1 typ1 name1 z1 s1) <- constantChains left, typ1 == typ, name1 == name, sub1 `simsubst` sub]
                        | (ConstantChain sub typ name z s) <- constantChains right
                        ]
                     
                ]
       = MatchFail $ show eq
--  EE*[z,s];env =?= env' and env' /= EE*[z',s'];env''       
   | (eq:_) <- [eq | eq@(left :=?=: right) <- (environmentEq gam)
                   , not (null (constantChains left))
                   , or [null [() | ConstantChain sub1 typ1 name1 z1 s1 <- constantChains right
                                  , name == name1]
                        | (ConstantChain sub typ name z s) <- constantChains left
                        ]
                ]
       = MatchFail $ show eq
--  E*;env =?= env' and env' /= E*;env''       
--  E*alpha1;env =?= env' and env' /= E*alpha2;env'' with alpha2 = alpha1
   | (eq:_) <- [eq | eq@(left :=?=: right) <- (environmentEq gam)
                   , not (null (constantEnvVars left))
                   , or [null [() | (Env sub1 name1) <- constantEnvVars right
                                  , name == name1 && sub `simsubst` sub1]
                        | (Env sub name) <- constantEnvVars left
                        ]
                ]
       = MatchFail $ show eq
--  env =?= E*;env' and env /= E*;env'' and env /= E1;env''   
--  env =?= E*alpha1;env and env /= E*alpha2 with alpha1 /= alpha2 and env /= E1;env''
   | (eq:_) <- [eq | eq@(left :=?=: right) <- (environmentEq gam)
                   , not (null (constantEnvVars right))
                   , null (metaEnvVars left)
                   , or [null [() | (Env sub1 name1) <- constantEnvVars left
                                  , name == name1 && sub `simsubst` sub1]
                        | (Env sub name) <- constantEnvVars right
                        ]
                ]
       = MatchFail $ show eq
--  E1;....;En =?= E1*;....;Em* all Ei in Delta2, and all E*j are not in Nabla2
  | (eq@(left :=?=: right,rest):_) 
      <- [splittings | splittings@(left :=?=: right,rest) <- splits (environmentEq gam)
         , not (hasBinding left) && not (hasBinding right)
         , null (metaChains left)
         , null (constantChains left)
         , not (null (metaEnvVars left))
         , not (null (constantEnvVars right))
         , null (constantEnvVars left)
         , all (\(v@(Env sub e1)) -> v `elem` (delta2 del) || (Env emptySubstitution e1) `elem` (delta2 del)) (metaEnvVars left)
         , all (\(v@(Env sub e1)) -> not ((v `elem` (delta2 cdel)) || ((Env emptySubstitution e1) `elem` (delta2 cdel)))) (constantEnvVars right)
         ]
   = MatchFail $ "all envvars in rhs are more general than envvars on right" ++ (show eq)
 | otherwise = Empty


 
-- =======================================
-- First-Order Cases
-- =======================================
applyFirstOrderRules :: Problem -> MatchResult
applyFirstOrderRules problem@(Problem{solution=sol,gamma=gam,delta=del,nabla=cdel})
-- 
-- RULE: Sol,Gamma cup {X = Y}, Delta --> Sol circ {X |-> Y}, Gamma[Y/X], Delta [Y/X] and X = ...  and X has an empty substitution
--
 | Just ((varx@(MetaVarLift sub x) :=?=: vary),rest) 
     <-  splitBy (\(l:=?=:r) -> case l of 
                                 MetaVarLift s x -> True
                                 _                 -> False) (varLiftEq gam)
 , isEmptySubstitution sub                                 
    = let sol' = (MapVarLift $ varx :|-> vary):sol
          gam' = subst vary varx (gam{varLiftEq = rest}) 
          del' = subst vary varx del
      in Some [problem{solution=sol',gamma=gam',delta=del'}]
--
--  RULE: Sol,Gamma cup {x = x}, Delta --> Sol, Gamma, Delta and the same substitution is applied to both occurences of x
--
  |  Just (eq@((VarLift subx x) :=?=: (VarLift suby y)),rest) 
       <-  splitBy (\(l :=?=: r) -> case (l,r) of 
                                     (VarLift subx x,VarLift suby y) -> x == y && subx `simsubst` suby
                                     _                             -> False) (varLiftEq gam)
  , subx `simsubst` suby                                    
    = Some [problem{gamma=gam{varLiftEq = rest}}]
--
-- RULE: Sol,Gamma cup {x* = x*}, Delta --> Sol, Gamma, Delta and the same substitution is applied to both occurences of x*
--
  |  Just (eq@((ConstantVarLift subx x) :=?=: (ConstantVarLift suby y)),rest) 
       <-  splitBy (\(l :=?=: r) -> case (l,r) of 
                                     (ConstantVarLift subx x,ConstantVarLift suby y) -> x == y && subx `simsubst` suby
                                     _                             -> False) (varLiftEq gam)
  , subx `simsubst` suby                                     
    = Some [problem{gamma=gam{varLiftEq = rest}}]
    
-- 
--  RULE: Sol,Gamma cup {S = s}, Delta --> Sol circ {S |-> s}, Gamma[s/S], Delta [s/S] if S is not contained in s
--
 | Just ((vars@(MetaExpression sub s) :=?=: t),rest) 
     <- splitBy (\(l:=?=:r) -> case l of 
                                MetaExpression sub s -> if (properSubTermOf l r) then error "BUG!:MetaExpression occurs on rhs" else isEmptySubstitution sub
                                _                    -> False                      ) (expressionEq gam)
 , isEmptySubstitution sub                                
    = let sol' = (MapExpr $ vars :|-> t):sol
          gam' = subst t vars (gam{expressionEq = rest}) 
          del' = subst t vars  del
      in Some [problem{solution=sol',gamma=gam',delta=del'} ]
-- 
--  RULE: Sol,Gamma cup {S* = S*} ---> Sol,Gamma
--
 | Just ((vars@(Constant sub s) :=?=: t),rest) 
     <- splitBy (\(l:=?=:r) -> case (l,r) of 
                                (Constant sub1 n1,Constant sub2 n2) -> n1 == n2 && sub1 `simsubst` sub2
                                _                    -> False                      ) (expressionEq gam)
    = let gam' = (gam{expressionEq = rest}) 
      in Some [problem{gamma=gam'} ]
-- 
--  RULE: Sol,Gamma cup {f s1...sn = f t1..tn}, Delta --> Sol, Gamma cup {si=ti}, Delta
-- 
 | Just (((Fn name args) :=?=: Fn name' args'),rest) 
     <-   splitBy (\(l:=?=:r) -> case (l,r) of 
                                  (Fn name _, Fn name' _) -> name == name'
                                  _                       -> False         ) (expressionEq gam)
    = let sol' = sol
          (vs,es) = computeAll args args'
          gam' = (gam{expressionEq = es ++ rest, varLiftEq = vs ++ (varLiftEq gam)}) 
          del' = del
          compute (VarPos v1) (VarPos v2)
            = let vareq = v1 :=?=: v2
              in ([vareq],[])
          compute b1@(Binder v1 e1) b2@(Binder v2 e2)
            | length v1 == length v2 = let 
                                          vareq = zipWith (:=?=:) v1 v2
                                          expreq = [e1 :=?=: e2]
                                       in (vareq,expreq)
            | otherwise = error ("found binders with different arity: "  ++ show b1 ++ " and " ++ show b2)
          compute b1 b2 =
            error ("Binder found at variable position with: " ++ show b1 ++ " and " ++ show b2)            
          computeAll args1 args2 = 
            let (vareqs,expreqs) = unzip (zipWith compute args1 args2) -- (,),(,)
            in (concat vareqs,concat expreqs)
      in Some [problem{solution=sol',gamma=gam',delta=del'} ]
      
--  
--  RULE: Sol,Gamma cup {var X = var Y}, Delta --> Sol, Gamma cup {X=Y}, Delta 
--  (for all cases of X,Y (Meta or concrete)
-- 
  | Just ((VarL varx :=?=: VarL vary),rest) 
       <-  splitBy (\(l :=?=: r) -> case (l,r) of 
                                     (VarL _, VarL _) -> True
                                     _                    -> False) (expressionEq gam)
     = let sol' = sol
           gam' = gam{ varLiftEq = (varx:=?=:vary):(varLiftEq gam)
                      , expressionEq = rest} 
           del' = del
      in Some [problem{solution=sol',gamma=gam',delta=del'} ]
-- 
--  RULE: Sol,Gamma cup {x.s = y.t}, Delta --> Sol, Gamma cup {x=y,s=t}, Delta 
--  (for all cases of X,Y (Meta or concrete)
-- 
  | ((var1 := e1) :=?=: (var2 := e2):rest) <-  (bindingEq gam)
     = let sol' = sol
           gam' = gam{ varLiftEq = (var1:=?=:var2):(varLiftEq gam)
                      , expressionEq = (e1 :=?=: e2):(expressionEq gam)
                      , bindingEq = rest} 
           del' = del
      in Some [problem{solution=sol',gamma=gam',delta=del'} ]
-- 
--  RULE: Sol,Gamma cup {letrec env1 in s1 = letrec env2 in s2}, Delta --> Sol, Gamma cup {env1=env2,s1=s2}, Delta 
-- 
  | Just ((Letrec env1 s1 :=?=: Letrec env2 s2),rest) 
      <- splitBy (\(l:=?=:r) -> case (l,r) of 
                                 (Letrec _ _, Letrec _ _) -> True
                                 _                       -> False) (expressionEq gam)
     = let sol' = sol
           gam' = gam{  environmentEq = (env1:=?=:env2):(environmentEq gam)
                      , expressionEq = (s1 :=?=: s2):rest} 
           del' = del
      in Some [problem{solution=sol',gamma=gam',delta=del'}] 

  | Just ((FlatLetrec env1 s1 :=?=: FlatLetrec env2 s2),rest) 
      <- splitBy (\(l:=?=:r) -> case (l,r) of 
                                 (FlatLetrec _ _, FlatLetrec _ _) -> True
                                 _                                -> False) (expressionEq gam)
     = let sol' = sol
           gam' = gam{  environmentEq = (env1:=?=:env2):(environmentEq gam)
                      , expressionEq = (s1 :=?=: s2):rest} 
           del' = del
      in Some [problem{solution=sol',gamma=gam',delta=del'}] 

--
-- Rule: Gamma cup C*subs[s] = C*subs[t] --> Gamma cup s = t
--

        
   | Just ((ConstantCtxtSubst sub1 srt1 n1 s1 :=?=: ConstantCtxtSubst sub2 srt2 n2 s2),rest) 
            <- splitBy (\(l :=?=: r) -> case (l,r) of
                                         (ConstantCtxtSubst sub1 srt1 n1 s1, ConstantCtxtSubst sub2 srt2 n2 s2) -> n1 == n2 && srt1 == srt2 && sub1 == sub2
                                         _                  -> False) (expressionEq gam)
     = let sol' = sol
           gam' = gam{expressionEq = (s1 :=?=: s2): rest} 
           del' = del
        in Some [problem{gamma=gam',delta=del'}]
        
-- Rule: Gamma cup {} = {} --> Gamma
   | Just (env1 :=?=: env2,rest) <- splitBy (\(l :=?=: r) -> isEmpty l && isEmpty r) (environmentEq gam)
     = let sol' = sol
           gam' = gam{environmentEq = rest}
        in Some [problem{gamma=gam'} ]
-- no first-order rule applicable:
  | otherwise = Empty
-- =======================================

      
      

-- RULE: Gamma cup {D_subst[s] = t} --> Gamma cup s=t [[._subst]/D_subst]  |  Gamma cup D_subst[s]=t  Delta1 cup D_subst
-- only if t /= D[t'] where t notin Nabla1       
applyGuessRules problem@(Problem{solution=sol,gamma=gam,delta=del,nabla=cdel})
 | Just (((MetaCtxtSubst sub srt name s) :=?=: t),_)
        <- splitBy 
            (\(l :=?=: r) -> case (l,r) of
                            (MetaCtxtSubst sub srt name s,r) -> case r of 
                                                                    ConstantCtxtSubst sub0 srt0 name0 t0 -> 
                                                                         (ConstantCtxt sub0 srt0 name0) `elem` (delta1 cdel)
                                                                      || (ConstantCtxt emptySubstitution srt0 name0) `elem` (delta1 cdel)
                                                                    other -> not ((MetaCtxt sub srt name) `elem` (delta1 del))
                            
                            _ -> False)
            (expressionEq gam)
     
    = let problem1 = 
             let oldDel_1 = delta1 del
                 newDel_1 = (MetaCtxt sub srt name):oldDel_1
             in  problem{delta = del{delta1 = newDel_1}}
          problem2 = 
             let sol' = (MapExpr $ MetaCtxt sub srt name :|-> (Hole sub) ):sol
                 gam' = subst (Hole sub) (MetaCtxt sub srt name) gam
                 del' = subst (Hole sub) (MetaCtxt sub srt name) del
             in problem{solution=sol',gamma=gam', delta=del'}
      in Some [problem1,problem2]
-- no guess rule applicable
 | otherwise = Empty
-- =======================================
      
      
-- =======================================
-- Rules for Contexts 
-- =======================================
      
applyContextRules problem@(Problem{solution=sol,gamma=gam,delta=del,nabla=cdel})
-- D[s] = t with t = f s1...sn, t=letrec ..., or t = var v and D in delta1
--  ==> unfold D by its grammar
 | Just ((ctxt@(MetaCtxtSubst sub srt cname s) :=?=: rhs),rest)
     <- splitBy (\(l:=?=:r) 
                    -> case (l,r) of 
                        (MetaCtxtSubst sub srt cname s, rhs) -> 
                           case rhs of 
                                Fn fname _ -> (MetaCtxt sub srt cname) `elem` (delta1 del)
                                Letrec _ _ -> (MetaCtxt sub srt cname) `elem` (delta1 del)
                                FlatLetrec _ _ -> (MetaCtxt sub srt cname) `elem` (delta1 del)                                
                                VarL _     -> (MetaCtxt sub srt cname) `elem` (delta1 del)
                                Constant _ _ -> (MetaCtxt sub srt cname) `elem` (delta1 del)
                                _          -> False
                        _ -> False) 
                (expressionEq gam)
 = 
  let unfolds = Map.lookup (ctxtToString srt) (grammar $ contextDefinitions problem)
  in  case unfolds of 
         Nothing -> error $ "no unfolds for ctxt of srt " ++ show srt
         Just ufs' ->
           let ufs = [(a,b,c,d) | r <- ufs', let (a,b,c,d) = case r of Right x -> x
                                                                       Left (x,y) -> y]
               fv = fresh problem
               go (u:us) fv = let (u',fv') = makeExprWithConstraintsFresh u fv
                                  (us',fv'') = (go us fv')
                              in (u':us', fv'')
               go [] fv = ([],fv)
               (rufs,fresh') = go ufs fv
               rufs' = filter (\(a,b,c,d) -> not (isHole a)) rufs
               newlhs =  map (\(r,a,b,c) -> (subst s (Hole emptySubstitution) r,r,a,b,c)) rufs'
           in Some [problem{gamma=gam{expressionEq = (newl :=?=: rhs):rest}
                   ,solution=(MapExpr (MetaCtxt sub (srt) cname :|-> c)):sol
                   ,fresh=fresh'
                   ,delta = applySolution 
                                  [(MapExpr (MetaCtxt sub (srt) cname :|-> c))]
                                  del{delta1 = ndel1 ++ (delta1 del), delta2 = ndel2 ++ (delta2 del), delta3 = ndel3 ++ (delta3 del)}
                   }
                    | (newl,c,ndel1,ndel2,ndel3) <- newlhs
                   ]

-- PREFIX RULE: D1[s1] =?= D2*[s2]  and D1 in Delta1 iff D2* in Nabla1
-- pfxTable(cl(D2),cl(D1)) = K1,K2
--    if cl(D2) /= K1 
--     then ignore, since we cannot restrict the class further for matching
--    else 
--      D1 -> D2 D1'  cl(D1') = K2
--      D1'[s1] =?= s2
--      OR if D1 not in Delta1 D1 |-> [.]  as branch
                   
 | Just ((ctxt@(MetaCtxtSubst sub1 sortD1 nameD1 s1) :=?=: (ConstantCtxtSubst sub2 sortD2 nameD2 s2)),rest)
     <- splitBy (\(l:=?=:r) 
                   -> case (l,r) of 
                       (MetaCtxtSubst sub1 srtd1 d1 s, ConstantCtxtSubst sub2 srtd2 d2 s') ->  
                               let r1 = 
                                      (    
                                      ((MetaCtxt sub1 srtd1 d1) `elem` (delta1 del))
                                      == ((ConstantCtxt sub2 srtd2 d2) `elem` (delta1 cdel)
                                          || ((ConstantCtxt emptySubstitution srtd2 d2) `elem` (delta1 cdel)))
                                      )    
                                   r2 =  let k1k2 = (Map.lookup (srtd2,srtd1) (pfxTable $ contextDefinitions problem))
                                         in isJust k1k2 && (not (null [() | (k1,k2) <- (fromJust k1k2), srtd2 == k1,k1 /= UserDefinedSort False "TRIV", k2 /= UserDefinedSort False "TRIV"])) -- since both ctxts are in delta1
                                in  r1 && r2
                       _    -> False) 
                (expressionEq gam)
 = 
  let 
   w1 = 
  
     (case Map.lookup (sortD2,sortD1) (pfxTable $ contextDefinitions problem) of
         Just ks ->
            [let (_nameD1Prime:fresh') = fresh problem
                 nameD1Prime  = ctxtToString k2 ++ _nameD1Prime
                 sctxtD1Prime = if isTriv k2 then id else \hole ->  MetaCtxtSubst emptySubstitution k2 nameD1Prime hole
                 ctxtD1Prime  = if isTriv k2 then Hole emptySubstitution else   MetaCtxt emptySubstitution k2 nameD1Prime 
                 lhsNew       = sctxtD1Prime s1
                 solNew      = [MapExpr (MetaCtxt sub1 sortD1 nameD1 :|-> 
                                  ConstantCtxtSubst sub2 sortD2 nameD2 
                                   ctxtD1Prime)]
                 del1New = (delta1 del)
                 delNew  = applySolution solNew del{delta1=del1New}
              
                 problemNew = problem{delta=delNew,gamma=gam{expressionEq = (lhsNew :=?=: s2 ):rest}, fresh=fresh', solution=solNew++sol}
             in  problemNew
              | (k1,k2) <- ks,sortD2 == k1,k1 /= UserDefinedSort False "TRIV", k2 /= UserDefinedSort False "TRIV"]
         Nothing -> []
     )
{- impossible     
   w2 = 
     (if (MetaCtxt sub1 (sortD1) nameD1) `elem` (delta1 del) then [] else
       case Map.lookup (ctxtToString sortD1) (grammar $ contextDefinitions problem) of
            Nothing ->  []
            Just xs -> if (Hole emptySubstitution) `elem` (concat [rs | u <- xs, let rs=case u of {Right (a,b,c,d) -> [a]; Left ((a,_,_,_),(a',_,_,_)) -> [a,a']}]) then 
                           [let
                              sol' = (MapExpr $ MetaCtxt sub1 sortD1 nameD1 :|-> Hole sub1)
                              gam' = gam{expressionEq = s1 :=?=: (ConstantCtxtSubst sub2 (sortD2) nameD2 s2):rest}
                              del' = applySolution [sol'] del
                            in problem{delta=del',gamma=gam',solution=sol':sol} 
                          ]    
                         else [])
                         
                         w = w1 ++ w2 -}
  in Some w1


-- PREFIX RULE (2): D1[s1] =?= D2*[s2]  and D1 \not in Delta1 iff D2* \not in Nabla1
-- pfxTable(cl(D2),cl(D1)) = K1,K2
--    if cl(D2) /= K1 
--     then 
--     D1 |-> [.]  as branch
                   
 | Just ((ctxt@(MetaCtxtSubst sub1 sortD1 nameD1 s1) :=?=: (ConstantCtxtSubst sub2  sortD2 nameD2 s2)),rest)
     <- splitBy (\(l:=?=:r) 
                   -> case (l,r) of 
                       (MetaCtxtSubst sub1 srtd1 d1 s, ConstantCtxtSubst sub2 srtd2 d2 s') ->  
                            (    
                                ((MetaCtxt sub1 srtd1 d1) `notElem` (delta1 del))
                             && ((ConstantCtxt sub2 srtd2 d2) `notElem` (delta1 cdel))
                             && ((ConstantCtxt emptySubstitution srtd2 d2) `notElem` (delta1 cdel))
                             && case Map.lookup (ctxtToString srtd1) (grammar $ contextDefinitions problem) of
                                  Nothing -> False
                                  Just xs -> let r = (Hole emptySubstitution) `elem` (concat [rs | u <- xs, let rs=case u of {Right (a,b,c,d) -> [a]; Left ((a,_,_,_),(a',_,_,_)) -> [a,a']}]) in r -- trace (show r) r
                                )
                       _    -> False) 
                (expressionEq gam) 
 = --
  Some $
                           [let
                              sol' = (MapExpr $ MetaCtxt sub1 sortD1 nameD1 :|-> Hole sub1)
                              gam' = gam{expressionEq = s1 :=?=: (ConstantCtxtSubst sub2 sortD2 nameD2 s2):rest}
                              del' = applySolution [sol'] del
                            in problem{delta=del',gamma=gam',solution=sol':sol}]
 
 -- no context rule applicable:
 | otherwise = Empty
-- =======================================




-- =======================================
-- Rules for Environments
-- =======================================
applyEnvRules :: Problem -> MatchResult
applyEnvRules problem@(Problem{solution=sol,gamma=gam,delta=del,nabla=cdel})
-- E*;env1 = E*;env2 ==> env1 = env2
  | (eq@(left :=?=: right),rest):_ 
      <-  [splitting | splitting@(left :=?=: right,rest) <- splits (environmentEq gam)
                     ,  not (null 
                               [(e1,e2) | (e1@(Env sub1 v1)) <- constantEnvVars left
                                        , (e2@(Env sub2 v2)) <- constantEnvVars right                                     
                                        , v1 == v2 
                                        , (sub1 `simsubst` sub2)])
                     ]
  , ((env1,env2):_) <-   [(left',right') | (e1@(Env sub1 v1),left') <- splits (constantEnvVars left)
                              , (e2@(Env sub2 v2),right') <- splits (constantEnvVars right)
                              , v1 == v2 
                              , (sub1 `simsubst` sub2)]
                                          
     = 
       let 
           gam' = gam{environmentEq = (left{constantEnvVars=env1} :=?=: right{constantEnvVars=env2}):rest}
       in Some [problem{gamma=gam'}] 
--  ----
--  RULE: Sol,Gamma cup {E = env}, Delta --> Sol cup {E |-> env}, Gamma,... if env is nonempty 
--  ----
--  ok for matching
  | ((left :=?=: r),rest):_ 
      <-  [splitting | splitting@(left :=?=: r,rest) <- splits (environmentEq gam)
                               ,isMetaEnv left
                               ,isSingleton  (metaEnvVars left)
                               , null (constantEnvVars left)
                               , null (constantChains left)
                               ,let [e@(Env sub v)] = metaEnvVars left
                               ,e `notElem` (delta2 del) 
                                 ||
                                (e `elem` (delta2 del) &&  not (isEmpty r))
                                 ||
                                (e `elem` (delta2 del) &&  not (null [e' | e' <- (constantEnvVars r), e' `elem` delta2 (nabla problem)]))                                  
                               ]
    ,[e@(Env sub v)] <- (metaEnvVars left)
     = 
       let sol' = (MapEnv $ e :|-> r): sol
           gam' = gam{environmentEq = rest}
           del' = substEnv r v del 
                   
       in Some [problem{solution=sol',gamma=gam',delta=del'}] 
--  ----
--  RULE: Sol,Gamma cup {E1...En = {}}, Delta --> Sol cup {Ei |-> {}}, Gamma,... if for all i: Ei not in Delta2 
--  ----
  | ((left :=?=: r),rest):_ 
      <-  [splitting | splitting@(left :=?=: r,rest) <- splits (environmentEq gam)
                               ,null (bindings left)
                               ,null (metaChains left)
                               ,isEmpty r
                               ,not (null $ metaEnvVars left)
                               ,and [e `notElem` (delta2 del) | e <- metaEnvVars left] 
                               ]
    ,es <- (metaEnvVars left)
    = 
       let sol' = [(MapEnv $  ei :|-> empty) | ei <- es ] ++ sol
           gam' = gam{environmentEq = rest}
           del' = foldr (\e d -> substEmpty e d) del [v | Env s v <- es ]
       in Some [problem{solution=sol',gamma=gam',delta=del'}] 
--  ----
--  RULES: Sol,Gamma cup {...=b1;env1}, Delta --> ...
--  ----
  | (((left :=?=: right),rest):_) <- [(left :=?=: right,rest) | splitting@(left :=?=: right,rest) <- splits (environmentEq gam)
                                                              , hasBinding right
                                                              , not (null (bindings left) && null (metaEnvVars left) && null (metaChains left))]
  , (b1:bindings_right') <- (bindings right)
     =
      Some $ ---------------
--          concat $
--           [ 
              [ -- for each binding b1 from right
                let gam' = gam { bindingEq = (b2 :=?=: b1):bindingEq gam
                               ,  environmentEq = (left' :=?=: right'):rest}
                    left' = left{bindings = bindings_left'}
                    right' = right{bindings = bindings_right'}
                in problem{gamma=gam'}                           
                | (b2,bindings_left') <- splits (bindings left)
              ]
              ++
              [ -- for each metaEnv E from left
                let (name_e':fresh') = fresh problem
                    e' = Env emptySubstitution ("E" ++ name_e')
                    b1e' = empty{bindings=[b1],metaChains=[],metaEnvVars=[e']}
                    sol' = (MapEnv $  ex :|-> b1e'):sol
                    gam' = gam {environmentEq = (left' :=?=: right'):rest}
                    left' = left{metaEnvVars = e':metaEnvVars_left'}
                    right' = right{bindings = bindings_right'}
                    del' = substEnv b1e' e del
                    
                in problem{solution=sol',fresh=fresh',gamma=gam',delta=del'}                           
                         
                | (ex@(Env sub e),metaEnvVars_left') <-  splits (metaEnvVars left) -- , e `elem` (delta2 del) -- not empty e
              ]
              ++
              (concat [ -- for each metaChain EE from left
                -- 4 rules!
                  
                 [let 
                      
                      -- EE[z,s]; env =?= b1; env2
                      -- case
                      -- EE[z,s] -> b1
                      sol' = (MapCV (MetaChain sub typ ee z s :|-> sigma_left)):sol
                      (name_ctxtname:fresh') = fresh problem
                      ctxtname = case typ of 
                                    UserDefinedChain srt -> srt ++ name_ctxtname
                      z_A_s = \z s ->
                                case typ of 
                                 UserDefinedChain srt -> (z := MetaCtxtSubst emptySubstitution (UserDefinedSort False srt) ctxtname s)
                                 VarChain -> (z := s)
                      gam' = gam{environmentEq =(left' :=?=: right'):rest
                                ,bindingEq = (z_A_s z s :=?=: b1):(bindingEq gam)
                                }
                      sigma_left = (\z s -> binding (z_A_s z s))
                      right' = right{bindings=bindings_right'}
                      left' =  left{metaChains = metaChains_left'}
                      del'  = substChainVar sigma_left (MetaChain sub typ ee z s) del
                  in problem{fresh = fresh',gamma = gam',solution=sol',delta=del'}
                  ,
                  let 
                      -- EE[z,s]; env =?= b1; env2
                      -- case
                      -- EE[z,s] -> z=A[X']; EE'[X',s] and z=A[X'] =?= b1 ...
                      sol' = (MapCV $ MetaChain sub typ ee z s :|-> sigma_left):sol
                      sigma_left = \z s -> (binding (z_A_vletX' z)){metaChains = [ee' s]}
                      (name_ctxtname:name_x':name_ee':fresh') = fresh problem
                      ctxtname = case typ of 
                                      UserDefinedChain srt -> srt ++ name_ctxtname
                      
                      x' =  MetaVarLift emptySubstitution ("X"++name_x')
                      ee' = \s -> case typ of 
                              UserDefinedChain srt -> MetaChain emptySubstitution (UserDefinedChain srt) ("EE^" ++ name_ee') x' s 
                              VarChain -> MetaChain emptySubstitution VarChain ("VV" ++ name_ee') x' s 
                      z_A_vletX' = \z -> case typ of 
                                       UserDefinedChain srt -> (z := MetaCtxtSubst emptySubstitution (UserDefinedSort False srt) ctxtname (VarL x'))
                                       VarChain -> (z:= (VarL x'))
                      gam' = gam{environmentEq =(left' :=?=: right'):rest
                                ,bindingEq = (z_A_vletX' z :=?=: b1):(bindingEq gam)
                                }
                      left' = left{metaChains = (ee' s):metaChains_left'}
                      right' = right{bindings=bindings_right'}
                      del' = substChainVar sigma_left (MetaChain sub typ ee z s) del 
                  in problem{fresh = fresh',gamma = gam',solution=sol',delta=del'}
                  ,
                  let 
                      -- EE[z,s]; env =?= b1; env2
                      -- case
                      -- EE[z,s] -> EE1[z,X1]; X1=A[X2]; EE2[x2,s] and X1=A[X2] =?= b1 ...
                      -- EE[z,s] -> EE1[z,A'[X1]]; X1=A[X2]; EE2[x2,s] and X1=A[X2] =?= b1 ...
                      sol' = (MapCV $ MetaChain sub typ ee z s :|-> sigma_left):sol
                      sigma_left = \z s -> (binding x1_A_vletX2){metaChains = [ee1 z,ee2 s]}
                      (name_ctxtname:name_ctxtnameA':name_x1:name_x2:name_ee1:name_ee2:fresh') = fresh problem
                      ctxtname = case typ of 
                                     UserDefinedChain srt -> srt ++ name_ctxtname
                      ctxtnameA' = case typ of 
                                               UserDefinedChain srt -> srt ++ name_ctxtnameA'
                      ctxtA' = \inhole -> case typ of 
                                 UserDefinedChain srt -> (MetaCtxtSubst emptySubstitution (UserDefinedSort False srt) ctxtnameA' inhole)
                                 VarChain -> inhole
                      x1 =  MetaVarLift emptySubstitution ("X"++name_x1)
                      x2 =  MetaVarLift emptySubstitution ("X"++name_x2)
                      ee1 = \z -> 
                             case typ of
                               UserDefinedChain srt -> MetaChain emptySubstitution (UserDefinedChain  srt) ("EE" ++ name_ee1) z (ctxtA' (VarL x1))
                               VarChain -> MetaChain emptySubstitution VarChain ("VV" ++ name_ee1) z (ctxtA' (VarL x1))
                      ee2 = \s ->
                             case typ of 
                               UserDefinedChain srt -> MetaChain emptySubstitution (UserDefinedChain  srt) ("EE" ++ name_ee2) x2 s
                               VarChain -> MetaChain emptySubstitution VarChain ("VV" ++ name_ee2) x2 s
                      x1_A_vletX2 = case typ of 
                                       (UserDefinedChain srt) -> (x1 := MetaCtxtSubst emptySubstitution (UserDefinedSort False srt) ctxtname (VarL x2))
                                       VarChain -> (x1 := VarL x2) 
                      gam' = gam{environmentEq =(left' :=?=: right'):rest
                                ,bindingEq = (x1_A_vletX2 :=?=: b1):(bindingEq gam)
                                }
                      right' = right{bindings=bindings_right'}
                      left' = left{metaChains = [ee1 z,ee2 s]++metaChains_left'}
                      del' = substChainVar sigma_left (MetaChain sub typ ee z s) del
                  in problem{fresh = fresh',gamma = gam',solution=sol',delta=del'}
                 ,
                  let 
                      -- sol' = (MapEnv $ metaChain typ ee z s :|-> sigma_right):sol
                      -- EE[z,s]; env =?= b1; env2
                      -- case
                      -- EE[z,s] -> EE'[z,A'[X1]];X1=A[s] and  X1=A[s] =?= b1 ...
                      sol' = (MapCV $ MetaChain sub typ ee z s :|-> sigma_left):sol
                      sigma_left= \z s -> (binding (x1_A_s s)){metaChains = [ee' z]}
                      (name_ctxtname:name_ctxtnameA':name_x1:name_ee':fresh') = fresh problem
                      ctxtname = case typ of UserDefinedChain srt -> srt ++ name_ctxtname
                      ctxtnameA' = case typ of UserDefinedChain srt -> srt ++ name_ctxtnameA'
                      ctxtA' = \inhole -> case typ of 
                                 UserDefinedChain srt -> (MetaCtxtSubst emptySubstitution (UserDefinedSort False srt) ctxtnameA' inhole)
                                 VarChain -> inhole

                      x1 =  MetaVarLift emptySubstitution ("X"++name_x1)
                      ee' = \z -> 
                             case typ of 
                              UserDefinedChain srt -> MetaChain emptySubstitution (UserDefinedChain srt) ("EE" ++ name_ee') z (ctxtA' (VarL x1))
                              VarChain -> MetaChain emptySubstitution VarChain ("VV" ++ name_ee') z (ctxtA' (VarL x1))
                      x1_A_s = \s -> 
                                case typ of 
                                 UserDefinedChain srt -> (x1 := MetaCtxtSubst emptySubstitution (UserDefinedSort False srt) ctxtname s)
                                 VarChain -> (x1 := s)
                      gam' = gam{environmentEq =(left' :=?=: right'):rest
                                ,bindingEq = ((x1_A_s s) :=?=: b1):(bindingEq gam)
                                }
                      right' = right{bindings=bindings_right'}
                      left' = left{metaChains = (ee' z):metaChains_left'}
                      del' = substChainVar sigma_left (MetaChain sub typ ee z s) del
                  in problem{fresh = fresh',gamma = gam',solution=sol',delta=del'}
                 ]
                 
                -- already changed
                | ((MetaChain sub typ ee z s),metaChains_left') <- splits (metaChains left)
              ])
          

--  RULE E1;env = E2;....  ---> E1 |-> E1' E2;   E1';env = ...  (all cases for E1 in parallel)
-- should be ok????:
  | ((left :=?=: right,rest):_) 
      <- [splittings | splittings@(left :=?=: right,rest) <- splits (environmentEq gam)
         , not (null (metaEnvVars left))
         , not (null (constantEnvVars right))
         , -- exists E2 s.t. E2 is not empty (in Nabla2)
           (or
            [(Env sub e2') `elem` (delta2 cdel) || (Env emptySubstitution e2') `elem` (delta2 cdel) 
              | (Env sub e2') <- (constantEnvVars right)
            ])
           -- or at least one E1 on the lhs is not in Delta1:
           || 
           ((any (\(v@(Env sub e1)) -> not (v `elem` (delta2 del) || (Env emptySubstitution e1) `elem` (delta2 del))) (metaEnvVars left)))
         ] 
       -- choose e2:
     , e2@(Env sub e2'):e2s <- if  -- if there exists at least one E1 on the lhs not in Delta2, then E2 can be choosen arbitrarily (here the first one)
                                   ((any (\(v@(Env sub e1)) -> not (v `elem` (delta2 del) || (Env emptySubstitution e1) `elem` (delta2 del))) (metaEnvVars left))) 
                                 then constantEnvVars right
                                 else -- filter only those E2 which are in delta2
                                      head 
                                        [(Env sub e2'):r | (Env sub e2',r) <- (splits (constantEnvVars right)), (Env sub e2') `elem` (delta2 cdel) || (Env emptySubstitution e2') `elem` (delta2 cdel)
                                        ]
   = 
    Some [
      let
         (name_e1':fresh') = fresh problem
         e1' = Env emptySubstitution ("E" ++ name_e1')
         sol' = (MapEnv (Env sub e1 :|-> empty{metaEnvVars=[e1'],constantEnvVars=[e2]})):sol
         gam' = gam {environmentEq = (left' :=?=: right'):rest}
         left' = left{metaEnvVars = (e1'):e1s}
         right' = right{constantEnvVars = e2s} 
         del' = substEnv empty{metaEnvVars=[e1'],constantEnvVars=[e2]} e1 del
                          
      in problem{delta=del',solution=sol',gamma=gam',fresh=fresh'} 
       | (Env sub e1,e1s) <- splits (metaEnvVars left)
       , if -- e2 is non-empty
            (e2 `elem` (delta2 cdel) ||(Env emptySubstitution e2') `elem` (delta2 cdel))
            then True -- E1 may be in Delta2 or not in Delta2
            else -- e2 is empty or non-empty, then E1 must not be in Delta2
              not( (Env sub e1) `elem` (delta2 del) || (Env emptySubstitution e1) `elem` (delta2 del))
       ]
       
 
--  RULE EE1[z1,s1];E1,..env = EE2[z,s];....  
--   --->  (all cases for E1,EE1[z',s'] in parallel)
--   
--    possibilities
--      - E1 -> E1' EE2[z,s]

--      - EE1[z1,s1] -> EE2[z,s]  and z1 = z; s1 = s as equations!
--      ~ EE1[z1,s1] -> EE2[z,s]  and z1 = z; A[s1] = s as equations!

--      - EE1[z1,s1] -> EE1'[z1,vlet X]; EE2[z,s] and  X1 = z2  and EE1'[z',vlet X] remains in lhs 

--      - EE1[z1,s1] -> EE2[z,s];EE1'[X,s1] and z1 = z, vlet X = s and EE1'[X,s1] remain in lhs
--      ~ EE1[z1,s1] -> EE2[z,s];EE1'[X,s1] and z1 = z, A[vlet X] = s and EE1'[X,s1] remain in lhs          

--      - EE1[z1,s1] -> EE1'[z1,vlet X1]; EE2[z,s]; EE1''[X2; s1]
--         and X1 = z: vlet X2 = s as equation and EE1'[z1,vlet X1]; EE1'''[X2,s] remain in lhs
--      ~ EE1[z1,s1] -> EE1'[z1,vlet X1]; EE2[z,s]; EE1''[X2; s1]
--         and A[X1] = z: A'[vlet X2] = s as equation and EE1'[z1,vlet X1]; EE1'''[X2,s] remain in lhs

--   if the type of chains is the same then ok
--   there is an additional case where the type of EE1 is AChain and the type of EE2 is VarChain! 
  | ((left :=?=: right,rest):_) 
      <- [splittings | splittings@(left :=?=: right,rest) <- splits (environmentEq gam)
         -- , --not (hasBinding left) && 
           -- not (hasBinding right) -- && (null $ constantEnvVars right)
         ] 
     , ee2@(ConstantChain sub0 typee2 nameee2 z s):ee2s <- constantChains right
     , ee1s <- metaChains left
     , e1s  <- metaEnvVars left
     , constantChainsLeft <- [cCL | cCL@(ConstantChain subCL typCL nameCL zCL sCL) <- constantChains left, nameCL == nameee2, subCL `simsubst` sub0]
     , not ((null $ e1s) && (null  ee1s) && null constantChainsLeft)
   = Some $
      -- EE*[z,s];env =?= EE*[z',s'];env' =
      -- =>
      -- z =?= z' 
      -- s =?= s'
      -- env =?= env'
     [
      let
         newVarEq  = (zCL :=?=: z):(varLiftEq gam)
         newExprEq = (sCL :=?=: s):(expressionEq gam)
         newLeft   = left{constantChains = restLeft} 
         newRight  = right{constantChains = ee2s}
         newEnvEq  = (newLeft :=?=: newRight):(rest)
         gam' = gam{varLiftEq = newVarEq, expressionEq = newExprEq, environmentEq = newEnvEq}
      in problem{gamma = gam'}
       |
        (ConstantChain subCL typCL nameCL zCL sCL,restLeft) <- splits (constantChains left), nameCL == nameee2, subCL `simsubst` sub0
      ]
      ++    
   
--      - E1 -> E1' EE2[z,s]
     [
      let
         (name_e1':fresh') = fresh problem
         e1' = Env emptySubstitution ("E" ++ name_e1')
         sol' = (MapEnv (Env sub e1 :|-> empty{metaEnvVars=[e1'], constantChains=[ee2]})):sol
         gam' = gam {environmentEq = (left' :=?=: right'):rest}
         left' = left{metaEnvVars = (e1'):e1s}
         right' = right{constantChains = ee2s} 
         del' = substEnv empty{metaEnvVars=[e1'], constantChains=[ee2]} e1 del
      in problem{delta=del',solution=sol',gamma=gam',fresh=fresh'} | (Env sub e1,e1s) <- splits (metaEnvVars left)
      ]
      
      
      
      
      ++
      concat
       [ 
        --      - EE1[z1,s1] -> EE2[z,s]  and z1 = z; s1 = s as equations!
        --      ==> EE1[.,.] |-> EE2[.,A[.]]
--          z1 =?= z2
--          s1 ? s2'
     
         [let
          sol'   = (MapCV $ (ee1 :|-> sigma_right)):sol
          sigma_right = \z s -> empty{constantChains = [ConstantChain sub0 typee2 nameee2 z (subst s (Hole emptySubstitution)  ctxt)]}  
          gam'   = gam {environmentEq = (left' :=?=: right'):rest
                       , expressionEq = (s1:=?=:s2'):(expressionEq gam)
                       , varLiftEq     = (z1:=?=:z):(varLiftEq gam)}
          left'  = left{metaChains = ee1s}
          right' = right{constantChains = ee2s} 
          del'   = substChainVar sigma_right ee1 del
         in problem{delta=del',solution=sol',gamma=gam'} 
                  |(ctxt,s2') <- case typee1 of 
                         UserDefinedChain srt -> getContextBySplit problem (UserDefinedSort False srt) s
                         VarChain -> [(Hole emptySubstitution,s)]

        ]
        
        
        ++
        --      - EE1[z1,s1] -> EE1'[z1,vlet X]; EE2[z,s] and  s1 = s 
        --    EE1[.,.] -> EE1'[.,A[vlet z]];EE2[z,A[.]]  
        --    s1 =?= s2' 
        --  where A is fresh
        
        [
         let
          sol'   = (MapCV $ (ee1 :|-> sigma_right)):sol
          sigma_right = \parz s -> (empty{metaChains=[ee1' parz],constantChains=[ConstantChain sub0 typee2 nameee2 z (subst s (Hole emptySubstitution)  ctxt)]})
          (name_ee1':name_x:name_a:fresh') = fresh problem
          -- x = "X" ++ name_x
          -- meta_x      = MetaVariableLet x
          actxt  = case typee1 of 
                    UserDefinedChain srt -> MetaCtxtSubst emptySubstitution (UserDefinedSort False srt) (srt ++ name_a)
                    VarChain -> id
          ee1'   = \parz -> MetaChain emptySubstitution typee1 ((prefixEE typee1) ++ name_ee1') parz (actxt (VarL z))
          gam'   = gam {environmentEq = (left' :=?=: right'):rest
                       ,expressionEq = (s1 :=?=: s2'):(expressionEq gam)}
          left'  = left{metaChains = (ee1' z1):ee1s}
          right' = right{constantChains = ee2s} 
          del'   = substChainVar sigma_right ee1 del
         in problem{delta=del',solution=sol',gamma=gam',fresh=fresh'} 
           | (ctxt,s2') <- case typee1 of 
                         UserDefinedChain srt -> getContextBySplit problem (UserDefinedSort False srt) s
                         VarChain -> [(Hole emptySubstitution,s)]
        ]
        ++
        --      - EE1[z1,s1] -> EE2[z,s];EE1'[X,s1] and EE1'[X,s1] remain in lhs  
--
--                  EE1[.,.] -> EE2[.,s];EE1'[X,.]
--                  z1 = z       
--                  A[vlet X] = s
                  
        -- special cases: 
        --        EE1[z1,s1] -> VV2[z,s];EE1'[X,s1] and z1 = z, vlet X = s
        --        VV1[z1,s1] -> VV2[z,s];VV1'[X,s1] and z1 = z, vlet X = s
        [
         let
          sol'   = (MapCV $ ee1:|-> sigma_right):sol
          sigma_right = \parz pars -> empty{metaChains = [ee1' pars], constantChains =[ConstantChain sub0 typee2 nameee2 parz s]}
          (name_ee1':name_x:name_a:fresh') = fresh problem
          a = "A" ++ name_a
          ctxtAroundx = case typee2 of 
                         UserDefinedChain srt -> MetaCtxtSubst emptySubstitution (UserDefinedSort False srt) a
                         VarChain -> id
          x = "X" ++ name_x
          meta_x      = MetaVarLift emptySubstitution x
          ee1'   = \pars1 -> MetaChain emptySubstitution typee1 ((prefixEE typee1) ++ name_ee1') meta_x pars1
          gam'   = gam {environmentEq = (left' :=?=: right'):rest
                       , varLiftEq     = (z1 :=?=: z):(varLiftEq gam)
                       ,expressionEq  = (ctxtAroundx (VarL meta_x) :=?=: s):(expressionEq gam)
                       }
          left'  = left{metaChains = (ee1' s1):ee1s}
          right' = right{constantChains = ee2s} 
          del'   = substChainVar sigma_right ee1 del
         in problem{delta=del',solution=sol',gamma=gam',fresh=fresh'} 
        ]
        ++
        --      - EE1[z1,s1] -> EE1'[z1,vlet X1]; EE2[z,s]; EE1''[X2; s1] 
        --         and X1 = z; A[vlet X2] = s as equation and EE1'[z1,vlet X1]; EE1'''[X2,s] remain in lhs
        
                
--
--                  EE1[.,.] -> EE1'[.,A0[vlet X1]]; EE2[z,s]; EE1''[X2,.]
--                  X1 = z       
--                  A[vlet X2] = s
--                  and EE1'[z1,vlet X1]; E1''[X2,s1] remain

        [
          let
              sol'   = (MapCV $ ee1 :|-> sigma_right):sol
              sigma_right = \parz pars -> (empty{metaChains=[meta_ee1' parz ,meta_ee1'' pars]
                                                ,constantChains =[ConstantChain sub0 typee2 nameee2 z s]})
              (name_ee1':name_x1:name_ee1'':name_x2:name_a:name_a0:fresh') = fresh problem
              a =case typee2 of UserDefinedChain srt -> srt ++ name_a
              a0 = case typee1 of UserDefinedChain srt -> srt ++ name_a0
              ctxtAroundx2 = case typee2 of 
                              UserDefinedChain srt -> MetaCtxtSubst emptySubstitution (UserDefinedSort False srt) a
                              VarChain -> id
              ctxtAroundx1 = case typee1 of 
                              UserDefinedChain srt -> MetaCtxtSubst emptySubstitution (UserDefinedSort False srt) a0
                              VarChain -> id
              x1 = "X" ++ name_x1
              x2 = "X" ++ name_x2
              ee1' = (prefixEE typee1) ++ name_ee1'
              ee1'' = (prefixEE typee1) ++ name_ee1''          
              meta_x1      = MetaVarLift emptySubstitution x1
              meta_x2      = MetaVarLift emptySubstitution x2
              meta_ee1'   = \parz -> MetaChain emptySubstitution typee1 ee1' parz (ctxtAroundx1 (VarL meta_x1))
              meta_ee1''  = \pars -> MetaChain emptySubstitution typee1 ee1'' meta_x2 pars
              gam'   = gam {environmentEq = (left' :=?=: right'):rest
                           ,varLiftEq      = (meta_x1 :=?=: z):(varLiftEq gam)
                           ,expressionEq  = (ctxtAroundx2 (VarL meta_x2) :=?=: s):(expressionEq gam)
                           }
              left'  = left{metaChains = (meta_ee1' z1):(meta_ee1'' s1):ee1s}
              right' = right{constantChains = ee2s} 
              del'   = substChainVar sigma_right ee1 del
          in problem{delta=del',solution=sol',gamma=gam',fresh=fresh'} 
        ]
        
      | (ee1@(MetaChain sub typee1 nameee1 z1 s1),ee1s) <- splits (metaChains left),  (typee2 == VarChain) || typee2 == typee1
       ]      
      
 -- no environment rule applicable:
 | otherwise = Empty
-- =======================================


-- getContextBySplit computes for a given context sort K and a constant expression s
--  all (d^K,t)  s.t. d^K[t] = s
--  it uses the matching algorithm and it gets as first input a matching problem
--  to resuse the fresh names and the nabla NCCs from the problem

getContextBySplit problem  srt s = 
  let (_nameCtxt:_nameSVar:fresh') = fresh problem
      nameCtxt = case srt of 
                   UserDefinedSort b s -> (ctxtToString srt) ++ _nameCtxt
      nameExpr = "S" ++ "_nameSVar"
      ctxtVar = MetaCtxtSubst emptySubstitution srt nameCtxt exprVar
      exprVar = MetaExpression emptySubstitution nameExpr
      gam = emptyGamma {expressionEq = [ ctxtVar :=?=: s] }
      matchingProblem = Problem {
                         solution = [],
                         gamma = gam,
                         delta = Delta{delta1=[],delta2=[],delta3=[],delta4=emptyGamma},
                         nabla = nabla problem, -- ?
                         fresh = fresh',
                         functions = functions problem,
                         inputGamma = gam,
                         inputDelta = Delta{delta1=[],delta2=[],delta3=[],delta4=emptyGamma},
                         mpLogging = [],
                         expandDelta4Constraints = False,
                         expandAllContexts = True,
                         expandAllEnvs     = True,
                         contextDefinitions = contextDefinitions problem,
                         alphaBase=[]
                         }
      table = matching matchingProblem
      solutionPB = solvedLeaves table
      results   = [applySolution sol (MetaCtxt emptySubstitution srt nameCtxt,exprVar) |   pbs <- solutionPB, let sol = solution pbs]
  in results      

