-----------------------------------------------------------------------------
-- |
-- Module      :  LRSX.Matching.ConstraintInclusion
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--  
-- Treating NonCapture- or BV-Constraints and Subsumption on them. 
-- Required for joining critical pairs.
--
-----------------------------------------------------------------------------

module LRSX.Matching.ConstraintInclusion where
import Data.Function
import Data.Maybe
import Data.List
import LRSX.Language.ContextDefinition
import LRSX.Language.Alpha
import LRSX.Language.Alpha.Similar
import qualified Data.Set as Set
import qualified Data.Map.Strict as Map
--
-- import Debug.Trace
--
import LRSX.Language.Syntax
import LRSX.Util.Util
import LRSX.Unification.UnificationProblem
trace a b = b
-- =====================================================
-- data Domain a b = Domain a | Codomain a |  Comp b
--  deriving(Eq,Ord,Show)
data Dom = Vars SyntaxComponent         -- All Variables of a Component
         | Cod SubstItem                -- Codomain of a fresh renaming 
         | SingleVar VarLift            -- A single variable
  deriving(Eq,Show)


-- =================================================================
-- Symbolic computation of captured variables (over approximation)  
  
cvSymbolically :: SyntaxComponent -> [Dom]  
cvSymbolically  comp =
 let 
   res =   
     case comp of 
      (SCVar  (VarLift (Substitution sublist) v)) ->
          case sublist of 
            ((AlphaVarLift name num):rest) 
              | name == v -> [ (Cod (AlphaVarLift name num)) ]
            other -> (SingleVar (VarLift emptySubstitution v)):(varSymbolicallySubst (Substitution sublist))
      (SCVar  (MetaVarLift (Substitution sublist) v)) ->
          case sublist of 
            ((AlphaMetaVarLift name num):rest) 
              | name == v -> [ (Cod (AlphaMetaVarLift name num)) ]
            other -> (SingleVar (MetaVarLift emptySubstitution v)):(varSymbolicallySubst (Substitution sublist))
      (SCVar  (ConstantVarLift (Substitution sublist) v)) ->
          case sublist of 
            ((AlphaConstantVarLift name num):rest) 
              | name == v -> [ (Cod (AlphaConstantVarLift name num)) ]
            other -> (SingleVar (ConstantVarLift emptySubstitution v)):(varSymbolicallySubst (Substitution sublist))
      (SCExpr (MetaExpression (Substitution sublist) e))  ->
          case sublist of 
            ((AlphaExpr name num):rest) 
              | name == e -> [ (Cod (AlphaExpr name num)) ]
            other -> (Vars (SCExpr $ MetaExpression emptySubstitution e)):(varSymbolicallySubst (Substitution sublist))
            
      (SCExpr (Constant (Substitution sublist) e))  ->
          case sublist of 
            ((AlphaConstant name num):rest) 
              | name == e -> [ (Cod (AlphaConstant name num))]
            other -> (Vars (SCExpr $ Constant emptySubstitution e)):(varSymbolicallySubst (Substitution sublist))
            
      (SCCtxt (MetaCtxt (Substitution sublist) srt name)) ->
          case sublist of 
            ((AlphaCtxt srt' name' num):rest) 
              | name' == name -> [ (Cod (AlphaCtxt srt' name num)) ]
            other -> (Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name)):(varSymbolicallySubst (Substitution sublist))
      (SCCtxt (ConstantCtxt (Substitution sublist) srt name)) ->
          case sublist of 
            ((AlphaConstantCtxt srt' name' num):rest) 
              | name' == name -> [ (Cod (AlphaConstantCtxt srt' name num)) ]
            other -> (Vars (SCCtxt $ ConstantCtxt emptySubstitution srt name)):(varSymbolicallySubst (Substitution sublist))
      (SCMetaEnv (Env (Substitution sublist) name)) ->
          case sublist of 
            ((AlphaEnv  name' num):rest) 
              | name' == name -> [  (Cod (AlphaEnv name num))]
            other -> (Vars (SCMetaEnv $ Env emptySubstitution  name)):(varSymbolicallySubst (Substitution sublist))
      (SCConstantEnv (Env (Substitution sublist) name)) ->
          case sublist of 
            ((AlphaConstantEnv  name' num):rest) 
              | name' == name -> [ (Cod (AlphaConstantEnv name num))]
            other -> (Vars (SCConstantEnv $ Env emptySubstitution name)):(varSymbolicallySubst (Substitution sublist))
      (SCMetaChain  (Substitution sublist) typ name) ->
          case sublist of 
            ((AlphaChain typ name' num):rest) 
              | name' == name -> [ (Cod (AlphaChain typ name num))]
            other -> (Vars (SCMetaChain emptySubstitution typ name)):(varSymbolicallySubst (Substitution sublist))
      (SCConstantChain (Substitution sublist) typ name) ->
          case sublist of 
            ((AlphaConstantChain typ name' num):rest) 
              | name' == name -> [ (Cod (AlphaConstantChain typ name num))]
            other -> (Vars (SCConstantChain emptySubstitution typ name)):(varSymbolicallySubst (Substitution sublist))
 in res -- trace (show comp ++ "\n" ++ show res) res            

            
varSymbolicallySubst (Substitution sub) = go sub
  where 
    go [] = []
    go ((SubstSet set):rest) = (go set) ++ (go rest)
    go ((AlphaVarLift name num):rest) = (Cod (AlphaVarLift name num)):(go rest)
    go ((AlphaMetaVarLift name num):rest) = (Cod (AlphaMetaVarLift name num)):(go rest)
    go ((AlphaConstantVarLift name num):rest) = (Cod (AlphaConstantVarLift name num)):(go rest)    
    go ((AlphaChain t name num):rest) = (Cod (AlphaChain t name num)):(go rest)    
    go ((AlphaConstantChain t name num):rest) = (Cod (AlphaConstantChain t name num)):(go rest)    
    go ((AlphaEnv name num):rest) = (Cod (AlphaEnv name num)):(go rest)    
    go ((AlphaConstantEnv name num):rest) = (Cod (AlphaConstantEnv name num)):(go rest)    
    go ((AlphaCtxt srt name num):rest) = (Cod (AlphaCtxt srt name num)):(go rest)
    go ((AlphaConstantCtxt srt name num):rest) = (Cod (AlphaConstantCtxt srt name num)):(go rest)    
    go ((LVAlphaChain t name num):rest) = (Cod (AlphaChain t name num)):(go rest)    
    go ((LVAlphaConstantChain t name num):rest) = (Cod (AlphaConstantChain t name num)):(go rest)    
    go ((LVAlphaEnv name num):rest) = (Cod (AlphaEnv name num)):(go rest)    
    go ((LVAlphaConstantEnv name num):rest) = (Cod (AlphaConstantEnv name num)):(go rest)    
    go ((CVAlphaCtxt srt name num):rest) = (Cod (AlphaCtxt srt name num)):(go rest)
    go ((CVAlphaConstantCtxt srt name num):rest) = (Cod (AlphaConstantCtxt srt name num)):(go rest)    
    go ((AlphaConstant name num):rest) = (Cod (AlphaConstant name num)):(go rest)
    go ((AlphaExpr name num):rest) = (Cod (AlphaExpr name num)):(go rest)    
    go (x:xs) = error $ "In ConstraintInclusion: " ++ show x
    
   
varSymbolically :: SyntaxComponent -> [Dom]  
varSymbolically  comp = 
 let 
  res =  
   case comp of 
      (SCVar  (VarLift (Substitution sublist) v)) ->
          case sublist of 
            ((AlphaVarLift name num):rest) 
              | name == v -> [ (Cod (AlphaVarLift name num)) ]
            other -> (SingleVar (VarLift emptySubstitution v)):(varSymbolicallySubst (Substitution sublist))
      (SCVar  (MetaVarLift (Substitution sublist) v)) ->
          case sublist of 
            ((AlphaMetaVarLift name num):rest) 
              | name == v -> [ (Cod (AlphaMetaVarLift name num)) ]
            other -> (SingleVar (MetaVarLift emptySubstitution v)):(varSymbolicallySubst (Substitution sublist))
      (SCVar  (ConstantVarLift (Substitution sublist) v)) ->
          case sublist of 
            ((AlphaConstantVarLift name num):rest) 
              | name == v -> [ (Cod (AlphaConstantVarLift name num)) ]
            other -> (SingleVar (ConstantVarLift emptySubstitution v)):(varSymbolicallySubst (Substitution sublist))
      (SCExpr (MetaExpression (Substitution sublist) e))  ->
             (Vars (SCExpr $ MetaExpression emptySubstitution e)):(varSymbolicallySubst (Substitution sublist))
            
      (SCExpr (Constant (Substitution sublist) e))  ->
             (Vars (SCExpr $ Constant emptySubstitution e)):(varSymbolicallySubst (Substitution sublist))
            
      (SCCtxt (MetaCtxt (Substitution sublist) srt name)) ->
             (Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name)):(varSymbolicallySubst (Substitution sublist))
             
      (SCCtxt (ConstantCtxt (Substitution sublist) srt name)) ->
             (Vars (SCCtxt $ ConstantCtxt emptySubstitution srt name)):(varSymbolicallySubst (Substitution sublist))
             
      (SCMetaEnv (Env (Substitution sublist) name)) ->
             (Vars (SCMetaEnv $ Env emptySubstitution  name)):(varSymbolicallySubst (Substitution sublist))
             
      (SCConstantEnv (Env (Substitution sublist) name)) ->
             (Vars (SCConstantEnv $ Env emptySubstitution name)):(varSymbolicallySubst (Substitution sublist))
             
      (SCMetaChain  (Substitution sublist) typ name) ->
             (Vars (SCMetaChain emptySubstitution typ name)):(varSymbolicallySubst (Substitution sublist))
             
      (SCConstantChain (Substitution sublist) typ name) ->
             (Vars (SCConstantChain emptySubstitution typ name)):(varSymbolicallySubst (Substitution sublist))
 in res 
   -- trace ("varSym(" ++ show comp ++ ")=" ++ show res) res
            
nccImplies :: [EnvVar]
                            -> NonCaptureConstraints
                            -> NonCaptureConstraints
                            -> (String, Maybe String,NonCaptureConstraints)
nccImplies delta2 nablaCon deltaCon =
 let 
     splitNabla    = (unfoldSplittedNCCs (splitCapCon nablaCon))
     splitDeltaSet = (splitCapCon deltaCon)
     splitDelta    = unfoldSplittedNCCs splitDeltaSet
     splitNablaAlpha = splitCapConAlpha nablaCon
     
     check (s@(SCVar (VarLift (Substitution []) v)),d@(SCVar (VarLift (Substitution []) v')))
       -- case (s,d) = (x,y) where x and y are concrete variables which are different and where no substitution is involved:
       | v /= v' = ("NCCIMPLLOG:  processing (s,d) = " ++ show (s,d)
                    ++ "\n (s,d) holds since both are different concrete variables without a substitution"
                   ,Nothing,[])
     -- *******************************************************************++
     -- Constraint (s,d) holds since it can be inferred from nabla 
     --  or s or d is a fresh symbol, where for the case s = d we have to check 
     --  if s and d must be instantiated with non-capturing instances      
     check (s,d) 
       | (s,d)  `elem` splitNabla = ("NCCIMPLLOG:  processing (s,d) = " ++ show (s,d)
                                     ++ "\n (s,d) holds since (s,d) is contained in the given nabla base"
                                    ,Nothing,[])
        | checkNCCWithAlpha splitNablaAlpha (s,d)  = ("NCCIMPLLOG:  processing (s,d) = " ++ show (s,d)
                                                        ++ "\n (s,d) holds since (s,d) is contained in the given nabla base modulo a renaming of renamings", Nothing,[(s,d)])
--        | isFreshSym s 
--        , d == s
--        , Nothing <- checkSC delta2 s d splitDeltaSet = ("\nNCCIMPLLOG: processing (s,d) = " ++ show (s,d)
--                                                    ++
--                                                    "\nNCCIMPLLOG: (s,d) holds, since s is a fresh symbol"
--                                                    ,Nothing)
       | isFreshSym s 
       , d /= s                                 = ("\nNCCIMPLLOG: processing (s,d) = " ++ show (s,d)
                                                   ++
                                                   "\nNCCIMPLLOG: (s,d) holds, since s is a fresh symbol"
                                                   ,Nothing,[])
       | isFreshSym d 
       , d /= s                                 = ("\nNCCIMPLLOG: processing (s,d) = " ++ show (s,d)
                                                   ++
                                                   "\nNCCIMPLLOG: (s,d) holds, since d is a fresh symbol"
                                                   ,Nothing,[])       
     -- *************************************************************              
     -- SEVERAL CHECKS WHETHER THE CONSTRAINT (s,d) HOLDS ALREADY,
     -- SINCE ALL GROUND INSTANTIATIONS WHICH VIOLATE (s,d) DO NOT
     -- FULFILL THE WEAK DVC 
     -- ****************************************************************
                                    
     -- (E,v) in Delta & (E,E) in nabla
     check (e@(SCConstantEnv (Env (Substitution []) _)),d)
       | (e,e) `elem` splitNabla  = ("NCCIMPLLOG:  processing (s,d) = " ++ show (e,d)
                                     ++ "\n (s,d) holds since (d,d) is contained in the given nabla base"
                                    ,Nothing,[])
     -- (CC,v) in Delta & (CC,CC) in nabla and sort(CC) = TRIV
     check (cc@(SCConstantChain (Substitution []) VarChain name),d)
       | (cc,cc) `elem` splitNabla  = ("NCCIMPLLOG:  processing (s,d) = " ++ show (cc,d)
                                     ++ "\n (s,d) holds since (d,d) is contained in the given nabla base"
                                    ,Nothing,[])
     -- (u,E) in Delta & (E,E) in nabla
     check (s,e@(SCConstantEnv (Env (Substitution []) _)))
       | (e,e) `elem` splitNabla  = ("NCCIMPLLOG:  processing (s,d) = " ++ show (s,e)
                                     ++ "\n (s,d) holds since (d,d) is contained in the given nabla base"
                                    ,Nothing,[])
     -- (u,CC) in Delta & (CC,CC) in nabla
     check (s,cc@(SCConstantChain (Substitution []) typ name))
       | (cc,cc) `elem` splitNabla  = ("NCCIMPLLOG:  processing (s,d) = " ++ show (s,cc)
                                     ++ "\n (s,d) holds since (d,d) is contained in the given nabla base"
                                    ,Nothing,[])
     -- (u,D) in Delta & (D,D) in nabla ? ja da CV(D) = emptyset 
     check (s,d@(SCCtxt (ConstantCtxt (Substitution []) srt name)))
       | (d,d) `elem` splitNabla =
                          ("NCCIMPLLOG:  processing (s,d) = " ++ show (s,d)
                       ++ "\n (s,d) holds since (d,d) is contained in the given nabla base"
                       ,Nothing,[])
     -- (x,U) and (U,x) in nabla for U = x,X,D,E,Ch,
     check (s,d)
      | case s of 
         (SCVar (ConstantVarLift (Substitution []) v)) -> True
         (SCVar (VarLift (Substitution []) v)) -> True
         _ -> False
      , case d of 
         (SCVar (ConstantVarLift (Substitution []) v)) -> True
         (SCVar (VarLift (Substitution []) v)) -> True
         (SCCtxt (ConstantCtxt (Substitution []) _ _)) -> True
         (SCConstantEnv (Env (Substitution []) _)) -> True
         (SCConstantChain (Substitution []) _ _) -> True
         _ -> False
      , s /= d
      , (d,s) `elem` splitNabla  = ("NCCIMPLLOG:  processing (s,d) = " ++ show (s,d)
                                    ++ "\n (s,d) holds since (d,s) is contained in the given nabla base"
                                    ,Nothing,[(s,d)])
     -- (Ch,U) and (U,Ch) in nabla for U = x,X,D,E,Ch',  where class(Ch)  = Triv
     check (s,d)
      | case s of 
         (SCConstantChain (Substitution []) VarChain _) -> True
         _ -> False
      , case d of 
         (SCVar (ConstantVarLift (Substitution []) v)) -> True
         (SCVar (VarLift (Substitution []) v)) -> True
         (SCCtxt (ConstantCtxt (Substitution []) _ _)) -> True
         (SCConstantEnv (Env (Substitution []) _)) -> True
         (SCConstantChain (Substitution []) _ _) -> True
         _ -> False
      , s /= d
      , (d,s) `elem` splitNabla  = ("NCCIMPLLOG:  processing (s,d) = " ++ show (s,d)
                                    ++ "\n (s,d) holds since (d,s) is contained in the given nabla base"
                                    ,Nothing,[(s,d)])
     check (s,d)  
       | isNothing (nccAbstractNew (s,d)) =            
                                    ("\nNCCIMPLLOG: processing (s,d) = " ++ show (s,d)
                                    ++                          
                                    "\nNCCIMPLLOG: capture constraint (s,d) is not implied by constraint-base but by alpha renaming: adding " 
                                    ++ show (s,d) 
                                    ,Nothing,[(s,d)])
       | otherwise =        let str =      "\nNCCIMPLLOG: processing (s,d) = " ++ show (s,d)
                                        ++ "\nNCCIMPLLOG: Capture Constraint not implied by constraint-base: "
                                        ++"\nNCCIMPLLOG: Constraint Base:" ++  show (splitNabla)
                                        ++"\nNCCIMPLLOG: Needed constraints: " ++ show (splitDelta)
                                        ++"\nNCCIMPLLOG: Alpha-Engine:\n"
                                        ++ show (nccAbstractNew (s,d)) 
                                        ++ "\nNCCIMPLLOG: STOPPED WITHOUT SUCCESS" 
                            in (str, Just str,[])
       
     allchecks = map check splitDelta
 in 
  if all isNothing (map snd3 allchecks) then 
      let (s,t,u) = (intercalate "\n" $ map fst3 allchecks, Nothing, splitFlatToNCC (concatMap trd3 allchecks)) in 
         -- trace s 
         (s,t,u)
   else let (a,b,c) = (head $ filter (isJust . snd3) allchecks) in (a,b,splitFlatToNCC c)
   
fst3 (a,b,c) = a
snd3 (a,b,c) = b
trd3 (a,b,c) = c
     
-- similar to splitBVCon, splitCapCon takes Contraints (s,d) and
-- splits them into (Var-construct,set of Capturing-constructs)
-- e.g. (letrec E in \X.S, letrec E1; E2 in (app Y [.]))
-- results in (E,{E1,E2}), (X,{E1,E2}), (S,{E1,E2))
-- (assume that all constructs are constants)

type SplittedNCCs =  Map.Map SyntaxComponent (Set.Set SyntaxComponent)   
  
  
  
isFreshCod (Cod _) = True
isFreshCod _ = False


     
       
  
  
    
  
    


           


-- nccAbstractNew a p =
   -- let x1 = nccAbstractNew1 a p
       -- x2 = nccAbstractNew2 a p -- this Definition will be used
   -- in if x1 == Nothing && x2 == Nothing then Nothing
                                        -- else if isJust x1 && isJust x2 then x2
                                                                       -- else error $ "here:\n" ++ "old: " ++ show x1 ++ "\nnew: " ++ show x2
 

nccAbstractNew (s,d) = 
    let
        vardom_s = varSymbolically  s 
        bvdom_d  = cvSymbolically  d 
        checkset = [(i,j) | i <- vardom_s, j <- bvdom_d]
        -- x vs y  ok if x y are concrete and different
        checkTrace v1 v2 
          | otherwise = check v1 v2
        check q@(SingleVar (VarLift _ v1)) p@(SingleVar (VarLift _ v2)) 
          | v1 == v2 = Just $ 
                                                    "Alpha-Engine can't infer NCC:\n"
                                                ++  "NCC (s,d) = " ++ show (s,d) ++ "\n"
                                                ++  "Reason: var(s) contains " ++ show q ++ " and bv(d) contains " ++ show p
          | otherwise = Nothing
        check q@(SingleVar _) p@(SingleVar _) = Just $ 
                                                    "Alpha-Engine can't infer NCC:\n"
                                                ++  "NCC (s,d) = " ++ show (s,d) ++ "\n"
                                                ++  "Reason: var(s) contains " ++ show q ++ " and bv(d) contains " ++ show p
        -- x vs fr(x) ok
        -- check q@(SingleVar item) p@(SingleVarCod v) = Nothing
        -- x vs BV(I') => FAIL
        check q@(SingleVar item) p@(Vars item') = Just $ 
                                                    "Alpha-Engine can't infer NCC:\n"
                                                ++  "NCC (s,d) = " ++ show (s,d) ++ "\n"
                                                ++  "Reason: var(s) contains " ++ show q ++ " and bv(d) contains " ++ show p
        -- x vs Cod alpha -> Ok, sinc Cod is always fresh                                                
        check q@(SingleVar item) p@(Cod _)          = Nothing
        
        -- fr(x) vs x ok
--         check q@(SingleVarCod _) p@(SingleVar _) = Nothing
        
        -- fr(x) vs fr(y)  ok unless x = y
        -- check q@(SingleVarCod u) p@(SingleVarCod v) 
--           | u == v = Just $ 
--                                                     "Alpha-Engine can't infer NCC:\n"
--                                                 ++  "NCC (s,d) = " ++ show (s,d) ++ "\n"
--                                                 ++  "Reason: var(s) contains " ++ show q ++ " and bv(d) contains " ++ show p
--           
--           | otherwise = Nothing
        -- fr(x) vs BV(I') ok
--         check q@(SingleVarCod item) p@(Vars item') = Nothing
        -- fr(x) vs Cod alpha -> Ok, sinc Cod is always fresh                                                
--         check q@(SingleVarCod item) p@(Cod _)          = Nothing
        
        -- FV(I) vs x  => FAIL since x may be in FV(I)
        check q@(Vars item) p@(SingleVar v) = Just $
                                                    "Alpha-Engine can't infer NCC:\n"
                                                ++  "NCC (s,d) = " ++ show (s,d) ++ "\n"
                                                ++  "Reason: var(s) contains " ++ show q ++ " and bv(d) contains " ++ show p
        -- FV(I) vs fresh(x) => NOFAIL, since x is fresh w.r.t. FV(I) 
--         check q@(Vars item) p@(SingleVarCod v) = Nothing
        -- FV(I) vs BV(I') => FAIL
        check q@(Vars item) p@(Vars item') = Just $ 
                                                    "Alpha-Engine can't infer NCC:\n"
                                                ++  "NCC (s,d) = " ++ show (s,d) ++ "\n"
                                                ++  "Reason: var(s) contains " ++ show q ++ " and bv(d) contains " ++ show p
        -- FV(I) vs Cod alpha -> Ok, sinc Cod is always fresh                                                
        check q@(Vars item) p@(Cod _)          = Nothing
--
        -- BV(I) vs x  => FAIL since x may be in FV(I)
        check q@(Vars item) p@(SingleVar v) = Just $
                                                    "Alpha-Engine can't infer NCC:\n"
                                                ++  "NCC (s,d) = " ++ show (s,d) ++ "\n"
                                                ++  "Reason: var(s) contains " ++ show q ++ " and bv(d) contains " ++ show p
        -- FV(I) vs fresh(x) => NOFAIL, since x is fresh w.r.t. BV(I) 
--         check q@(Vars item) p@(SingleVarCod v) = Nothing
        -- FV(I) vs BV(I') => FAIL
        check q@(Vars item) p@(Vars item') = Just $ 
                                                    "Alpha-Engine can't infer NCC:\n"
                                                ++  "NCC (s,d) = " ++ show (s,d) ++ "\n"
                                                ++  "Reason: var(s) contains " ++ show q ++ " and bv(d) contains " ++ show p
        -- FV(I) vs Cod alpha -> Ok, sinc Cod is always fresh                                                
        check q@(Vars item) p@(Cod _)          = Nothing
--
        -- Vars(I) vs x  => FAIL since x may be in FV(I)
        check q@(Vars item) p@(SingleVar v) = Just $
                                                    "Alpha-Engine can't infer NCC:\n"
                                                ++  "NCC (s,d) = " ++ show (s,d) ++ "\n"
                                                ++  "Reason: var(s) contains " ++ show q ++ " and bv(d) contains " ++ show p
        -- Vars(I) vs fresh(x) => NOFAIL, since x is fresh w.r.t. BV(I) 
--         check q@(Vars item) p@(SingleVarCod v) = Nothing
        -- Vars(I) vs BV(I') => FAIL
        check q@(Vars item) p@(Vars item') = Just $ 
                                                    "Alpha-Engine can't infer NCC:\n"
                                                ++  "NCC (s,d) = " ++ show (s,d) ++ "\n"
                                                ++  "Reason: var(s) contains " ++ show q ++ " and bv(d) contains " ++ show p
        -- Vars(I) vs Cod alpha -> Ok, sinc Cod is always fresh                                                
        check q@(Vars item) p@(Cod _)          = Nothing
--
        -- Cod(alpha) vs x => ok, since Cod is fresh and x is not fresh
        check q@(Cod item) p@(SingleVar v) = Nothing 
--         - Cod(I) vs fresh(x) => NOFAIL, since x is fresh wrt all Cods
--         check q@(Cod item) p@(SingleVarCod v) = Nothing
        -- Cod(alpha) vs BV(I) ok
        check q@(Cod item) p@(Vars item') =Nothing
                                              
        -- Cod(alpha1) vs Cod(alpha2) 
        -- Fail if alpha_1 and alpha_2 are same, else ok
        
        check q@(Cod (alpha1)) p@(Cod (alpha2))
          | alpha1 == alpha2 =     Just $ 
                                                    "Alpha-Engine can't infer NCC:\n"
                                                ++  "NCC (s,d) = " ++ show (s,d) ++ "\n"
                                                ++  "Reason: var(s) contains " ++ show q ++ " and bv(d) contains " ++ show p
          | otherwise = Nothing
                                                
         
        
 
    in if all (isNothing) [checkTrace i j | (i,j) <- checkset]
          then
         Nothing
        else
         Just (intercalate ";" $ map fromJust $ filter isJust [check i j | (i,j) <- checkset])



-- the helper function splitBVCon takes Contraints (s,s') and
-- splits them into (Var-construct,set of BV-constructs)
-- e.g. (letrec E in \X.S, letrec E1; E2 in (app Y S3))
-- results in (E,{E1,E2,S3}), (X,{E1,E2,S3}), (S,{E1,E2,S3))
-- (assume that all constructs are constants)
splitBVCon :: NonCaptureConstraints -> SplittedNCCs -- Map.Map SyntaxComponent (Set.Set SyntaxComponent)   
splitBVCon capcon =
 Map.fromList
 [(s',Set.unions $ map snd grp)
  | grp@((s',_):_) <- 
    (
     groupBy ((==) `on` fst) $
      sortBy (compare `on` fst) $
       [(s',getSCBoundVars d) | (s,d) <- capcon
                              , s' <- Set.elems (getSCAllVars s)]
    )           
 ]    

-- removeFreshSymbols :: SplittedNCCs -> SplittedNCCs 
-- removeFreshSymbols mp =
--     foldr (\key oldmap -> 
--                   Map.insert key (Set.filter (\x -> x == key || (not (isFreshSym x))) (mp Map.! key)) oldmap) mp (Map.keys mp)
 

checkNCCs delta2 capcon =
 let m = Map.assocs $ splitCapCon capcon 
     go ((s,d):rest)
      | (Set.member s d) = case checkSC delta2 s d capcon of 
                              Just str -> Just str
                              Nothing -> go rest
      | otherwise = go rest
     go [] = Nothing
  in go m

checkSC delta2 s d capcon = 
          case s of 
           (SCVar v) ->  Just ("The NCC: (s,d) = " ++ show (s,d) ++ "fails, since s occurs in d and s  \n Original NCCs:" ++ show capcon)
           (SCExpr (VarL v)) -> error "should not occur"
           (SCExpr (Fn f _)) -> error "should not occur"
           (SCExpr (Letrec _ _)) -> error "should not occur"
           (SCExpr (FlatLetrec _ _)) -> error "should not occur"
           (SCExpr (MetaExpression _ _)) -> Nothing
           (SCExpr (Constant _ _))       -> Nothing     
           (SCExpr (Hole _)) -> error "should not occur"
           (SCExpr (MetaCtxt sub srt name)) -> error "should not occur"
           (SCExpr (MetaCtxtSubst sub srt name _)) -> error "should not occur"
           (SCExpr (ConstantCtxt sub srt name)) -> error "should not occur"
           (SCExpr (ConstantCtxtSubst _ _ _  _)) -> error  "should not occur"
           (SCCtxt (VarL v)) -> error "should not occur"
           (SCCtxt (Fn f _)) -> error "should not occur"
           (SCCtxt (Letrec _ _)) -> error "should not occur"
           (SCCtxt (FlatLetrec _ _)) -> error "should not occur"
           (SCCtxt (MetaExpression _ _)) -> error "should not occur"
           (SCCtxt (Constant _ _)) -> error "should not occur"
           (SCCtxt (Hole _)) -> error "should not occur"
           (SCCtxt (MetaCtxt sub srt name)) -> Nothing
           (SCCtxt (MetaCtxtSubst sub srt name _)) -> error "should not occur"
           (SCCtxt (ConstantCtxt sub srt name)) -> Nothing
           (SCCtxt (ConstantCtxtSubst _ _ _ _)) -> error  "should not occur"
           (SCMetaEnv (Env sub name)) ->  
             if (Env sub name) `elem` delta2 || (Env emptySubstitution name) `elem` delta2 then
               Just ("The NCC: (s,d) = " ++ show (s,d) ++ "fails, since s occurs in d and s in non-empty \n Original NCCs:" ++ show capcon)
                                                                                           else 
               Nothing
           (SCConstantEnv (Env sub name)) ->  
             if (Env sub name) `elem` delta2 || (Env emptySubstitution name) `elem` delta2 then
               Just ("The NCC: (s,d) = " ++ show (s,d) ++ "fails, since s occurs in d and s in non-empty \n Original NCCs:" ++ show capcon)
                                                                                           else 
               Nothing
           (SCMetaChain sub typ name) -> Nothing
           (SCConstantChain sub typ name) -> Nothing
  
  
splitCapCon :: NonCaptureConstraints -> SplittedNCCs
splitCapCon capcon =
 Map.fromList
 [ (s',Set.unions $ map snd grp)
  | grp@((s',_):_) <- 
    (
     groupBy ((==) `on` fst) $
      sortBy (compare `on` fst) $
       [(s',getSCCapVars d) | (s,d) <- capcon
                              , s' <- Set.elems (getSCAllVars s)]
    )           
 ]    
 
unfoldSplittedNCCs mp = 
 nub [(s,u) | (s,dset) <- Map.assocs mp, u <- Set.elems dset] 
 
reFoldUnfoldedNCCs :: [(SyntaxComponent,SyntaxComponent)] -> SplittedNCCs 
reFoldUnfoldedNCCs us = Map.fromList 
                          [(s',Set.fromList $ map snd grp)
                            | grp@((s',_):_) <- 
                              (groupBy ((==) `on` fst) $ sortBy (compare `on` fst) us )]
-- a show function for splitted constraints:
showSplitted mp = unlines $ map (\x -> "    " ++ x) $ lines $ "\n{\n" ++ (unlines $ map show [(d, Set.elems set) | (d,set) <- Map.assocs mp]) ++ "}\n"
  

-- conversion from splitted NCCs into usual NCCs by forming expressions and contexts
splitToNCC :: SplittedNCCs -> NonCaptureConstraints
splitToNCC mp =
   [(Left (scItemToExpr t),scItemToScope d)|(t,dset) <- Map.assocs mp, d <- Set.elems dset]
 
splitFlatToNCC list = 
   [(Left (scItemToExpr t),scItemToScope d)|(t,d) <- list]
   
scItemToScope (SCVar v) = Letrec 
                            empty{bindings = [v := (Fn "__internalc" [])]} 
                            (Hole (Substitution [])) 
scItemToScope (SCExpr e) = e 
scItemToScope (SCCtxt c) = c 
scItemToScope (SCMetaEnv e) = Letrec empty{metaEnvVars=[e]} (Hole (Substitution []))
scItemToScope (SCConstantEnv e) = Letrec empty{constantEnvVars=[e]} (Hole (Substitution []))
scItemToScope (SCMetaChain sub typ name) = 
    Letrec 
        empty{metaChains=[MetaChain 
                            sub 
                            typ 
                            name 
                            (VarLift (Substitution []) ("__internalvar_" ++ name))
                            (Fn "__internalc" [])]}
        (Fn "__internalc" [])
scItemToScope (SCConstantChain sub typ name) = 
    Letrec 
        empty{constantChains=[ConstantChain 
                            sub 
                            typ 
                            name 
                            (VarLift (Substitution []) ("__internalvar_" ++ name))
                            (Fn "__internalc" [])]}
        (Fn "__internalc" [])
                            
  
scItemToExpr (SCVar v)         = VarL v
scItemToExpr (SCExpr e)        = e 
scItemToExpr (SCCtxt c)        = c
scItemToExpr (SCMetaEnv e)     = Letrec empty{metaEnvVars=[e]} (Fn "__internalc" [])
scItemToExpr (SCConstantEnv e) = Letrec empty{constantEnvVars=[e]} (Fn "__internalc" [])
scItemToExpr (SCMetaChain sub typ name) = 
    Letrec 
        empty{metaChains=[MetaChain 
                            sub 
                            typ 
                            name 
                            (VarLift (Substitution []) ("__internalvar_" ++ name))
                            (Fn "__internalc" [])]}
        (Fn "__internalc" [])
scItemToExpr (SCConstantChain sub typ name) = 
    Letrec 
        empty{constantChains=[ConstantChain 
                            sub 
                            typ 
                            name 
                            (VarLift (Substitution []) ("__internalvar_" ++ name))
                            (Fn "__internalc" [])]}
        (Fn "__internalc" [])

  
type SplittedNCCsWithAlpha = Map.Map SyntaxComponent (Map.Map SyntaxComponent (Set.Set (Substitution,Substitution)))
{-

    )           
 ]   -}

 

splitCapConAlpha :: NonCaptureConstraints -> SplittedNCCsWithAlpha
splitCapConAlpha capcon = 
 f    [(s',d',alpha,alphad) | (s,d) <- capcon
                            , (s',alpha) <-   (getSCAllVarsAndAlpha s)
                            , (d',alphad) <- getSCCapVarsAndAlpha d
                            ] 
  where  
    f inp = Map.fromList $ 
              map (\l -> (
                        (\(a,b,c,d) -> a) (head l), 
                          Map.fromList $ 
                          map (\m -> ( (\(b,c,d) -> b) (head m), Set.fromList $ map (\(b,c,d) -> (c,d)) m)) $
                          groupBy ((==) `on` (\(b,c,d) -> b)) $
                          sortBy 
                              (compare `on` (\(b,c,d) -> b)) 
                              (map (\(a,b,c,d) -> (b,c,d)) l)
                          )
                      
                      
                      ) 
                      $ (groupBy ((==) `on` (\(a,b,c,d) -> a)) 
                      $ sortBy (compare `on` (\(a,b,c,d) -> a)) inp)
                      
checkNCCWithAlpha :: SplittedNCCsWithAlpha -> (SyntaxComponent,SyntaxComponent) -> Bool
checkNCCWithAlpha splitNablaAlpha (s,d) =
  let
     subs = getSubstFromSC s
     subd = getSubstFromSC d
     s' = dropSubstFromComp s
     d' = dropSubstFromComp d
  in case Map.lookup s' splitNablaAlpha of 
        Just dmap -> case Map.lookup d' dmap of
                          Just set -> checkSet set (subs,subd)
                          Nothing -> False
        Nothing -> False
        
checkSet set (eta,eta') = 
  -- first check whether eta and eta' contain a same a_{U,i} component, ????
  -- if so then fail.
--   substToFlatList eta
--   substToFlatList eta'
--   
--   
--   undefined
  -- then check if set contains some (eta1,eta1') s.t. eta ~~~ eta1  and eta ~~~ eta1'
  -- where a_{U,i} ~~~ a_{U,j}  
   not $ null [ (seta,seta')   | (seta,seta') <- Set.elems set, simsubst eta seta, simsubst eta' seta']
   
--    (simSubstWeakAsym eta seta && simSubstWeakAsym eta' seta')]