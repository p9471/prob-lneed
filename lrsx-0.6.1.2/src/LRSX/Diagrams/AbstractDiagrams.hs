-----------------------------------------------------------------------------
-- |
-- Module      :  LRSX.Diagrams.AbstractDiagrams
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--  
-- Handling of forking and commuting diagrams to encode them as a termination 
-- problem for term rewrite systems with free variables on right hand side
-- also handling the interface to the termination prover AProVE and the
-- certifier CeTA
--
-----------------------------------------------------------------------------
-- todos:
--  rewrite this as a standalone backend
-- include all information in the rules as they are delivered by the matcher, i.e. rulename and variant!
--  add a parser to read diagrams from file (the best thing here is iff we can read the LONG output including all information)
-- , compose diagrams by transformation and finally split the
--   diagram sets to make termination proofs simpler
--  also the expansion diagrams must be handled, here it may perhaps be better to mark them as expansion diagrams?
-- don't know...
-- be careful: when SR reductions are added to the given SR-reduction!


-- note that the parser already rebuilds the commuting diagrams!
 
module LRSX.Diagrams.AbstractDiagrams where 
import Data.List
import Data.Maybe
import Data.Char
import System.Process
import System.Environment
import System.Directory
import Text.PrettyPrint
import Debug.Trace
import LRSX.Reduction.Rules(ruleSubsumedByRule,weaken,buildUnionMap,Command(..))
import LRSX.Diagrams.AbstractDiagrams.Types
import LRSX.Interface.AbstractDiagramParser (parseDiagrams,lexInput)
import qualified LRSX.Interface.Parser as P
import qualified LRSX.Interface.Lexer as L
import qualified Data.Map.Strict as Map

-- read diagrams as output by LONG
parseAndLex = parseDiagrams . lexInput

showDiagSet (DiagSet nameAsItem dep ds@(D b _)) = 
  "Set of " ++ (if b then "commuting" else "forking") ++ " diagrams for " ++ showItem nameAsItem 
  ++ "\n====================\n" ++
  (if null dep then [] else 
     (("Dependencies are diagrams for: " ++ intercalate "," (map  showItem dep )) ++ "\n====================\n" ))
  ++   showDiagrams ds ++ "\n\n"
  

showDiagrams (D b ds) =
  let prefix = if b then "Commuting diagram: " else "Forking diagram: "
      ds'    = map ((\x -> prefix ++ x) . showDiagram)  ds
  in unlines ds'      
      

showLab Tau = Left "tau"
showLab (Var v) = Left v 
showLab (Rule name var) = 
  if var < 0 then Left name else Right (name, show var)

fromDir LEFT str = "<-" ++ str ++ "-"
fromDir RIGHT str = "-" ++ str ++ "->"

showDiagram (lhs :~~> rhs) = showSequence lhs ++ " ~~> " ++ showSequence rhs
showSequence = intercalate " . " . map showItem
  
itemToFile (Trans dir lab) = case showLab lab of
                                Left str -> str
                                Right (str,str2) -> str
                                
itemToFile (SR dir lab) = case showLab lab of
                                Left str -> "SR" ++ str
                                Right (str,str2) -> "SR" ++ str
                                
itemToFile x = error ("itemToFile:" ++ show x)                                
  
  
showItem  (SR dir (Plus lab)) = fromDir dir (case showLab lab of 
                                                Left str -> "SR," ++ str ++ ",+"
                                                Right (str,str2) ->  "SR," ++ str ++ ",+," ++ str2)
showItem  (Trans dir (Plus lab)) = fromDir dir (case showLab lab of 
                                                Left str -> str ++ ",+"
                                                Right (str,str2) ->  str ++ ",+," ++ str2)
showItem  (SR dir (Star lab)) = fromDir dir (case showLab lab of 
                                                Left str -> "SR," ++ str ++ ",*"
                                                Right (str,str2) ->  "SR," ++ str ++ ",*," ++ str2)
showItem  (Trans dir (Star lab)) = fromDir dir (case showLab lab of 
                                                Left str -> str ++ ",*"
                                                Right (str,str2) ->  str ++ ",*," ++ str2)
showItem  (SR dir lab) = fromDir dir (case showLab lab of 
                                                Left str -> "SR," ++ str 
                                                Right (str,str2) ->  "SR," ++ str ++ "," ++ str2)
showItem  (Trans dir lab)  = fromDir dir (case showLab lab of 
                                                Left str ->  str 
                                                Right (str,str2) ->  str ++ "," ++ str2)
showItem  (Answer) = "<-ANSWER-"
showItem  (SingleName s) = s
showItem  (DoubleName s i) = show s ++ show i
showitem  (Exception str) = "\n EXCEPTION: " ++ show str ++ error "STOP NOW!"
      

-- run f  = -- putStrLn $ map diagramsToFreeVarsTerminationProblem $ 
--         mapM_ putStrLn $ map showDiagSet $  groupDiagrams $ nub $ map (joinVariants . removeAlpha) $ parseAndLex f  

convertLongToShort ufile content =
    do 
     (us,es)  <- case ufile of 
                    Just unionsfile -> 
                             do contents <- readFile unionsfile
                                let (r,st) = P.parseSRulesAndCommands (L.lexInput contents)
                                let stdRules = P.psStandardReductions st
                                let transRules = P.psTransformations st
                                let answerRules = P.psAnswerRules st
                                let commands = P.psOverlapCommands st
                                let unions = P.psUnionCommands st
                                let expands = P.psExpandCommands st
                                let ctxtdef = P.psCtxtDefn st
                                let finfo0 = P.toFunInfo (P.psFunInfo st)
                                return (unions,expands)
                    Nothing -> return  ([],[])
     let umap = buildUnionMap us
     let rawDiagrams = (parseAndLex content) 
     let expansions =  map joinVariants (procExpands es) -- should be added to each set where needed#
     let grps =  groupDiagrams expansions $ nub $ map (simplifyDiagramWithoutVariants . joinVariants . removeAlpha) $ map (applyUnions umap) $ rawDiagrams             
     sequence_ (map (putStrLn . showDiagSet) grps)
--      let resultingDiagSetGroups = [(trans,getAllDependencies trans grps) | (DiagSet trans _ _) <- grps]
     -- mapM_ putStrLn $ map (showDiagSets) (map snd resultingDiagSetGroups)

convertLongToTRSs ufile content =
   do 
     (us,es)  <- case ufile of 
                    Just unionsfile -> do 
                                contents <- readFile unionsfile
                                let (r,st) = P.parseSRulesAndCommands (L.lexInput contents)
                                let stdRules = P.psStandardReductions st
                                let transRules = P.psTransformations st
                                let answerRules = P.psAnswerRules st
                                let commands = P.psOverlapCommands st
                                let unions = P.psUnionCommands st
                                let expands = P.psExpandCommands st
                                let ctxtdef = P.psCtxtDefn st
                                let finfo0 = P.toFunInfo (P.psFunInfo st)  
                                return (unions,expands)
                    Nothing -> return  ([],[])
     let umap = buildUnionMap us
     let rawDiagrams = (parseAndLex content) 
     let expansions =  map joinVariants (procExpands es) -- should be added to each set where needed#
     let grps =  groupDiagrams expansions $ nub $ map (simplifyDiagramWithoutVariants . joinVariants . removeAlpha) $ map (applyUnions umap) $ rawDiagrams             
     let resultingDiagSetGroups = [(trans,getAllDependencies trans grps) | (DiagSet trans _ _) <- grps]
     mapM_ putStrLn $ map (\(t,gr) -> "# TRS for " ++ showItem t ++ "\n"  ++ (diagramsToFreeVarsTerminationProblem (combineDiagSets gr))) (resultingDiagSetGroups)
 
convertLongToITRSs ufile content =
   do 
     (us,es)  <- case ufile of 
                    Just unionsfile -> do 
                                contents <- readFile unionsfile
                                let (r,st) = P.parseSRulesAndCommands (L.lexInput contents)
                                let stdRules = P.psStandardReductions st
                                let transRules = P.psTransformations st
                                let answerRules = P.psAnswerRules st
                                let commands = P.psOverlapCommands st
                                let unions = P.psUnionCommands st
                                let expands = P.psExpandCommands st
                                let ctxtdef = P.psCtxtDefn st
                                let finfo0 = P.toFunInfo (P.psFunInfo st)  
                                return (unions,expands)
                    Nothing -> return  ([],[])
     let umap = buildUnionMap us
     let rawDiagrams = (parseAndLex content) 
     let expansions =  map joinVariants (procExpands es) -- should be added to each set where needed#
     let grps =  groupDiagrams expansions $ nub $ map (simplifyDiagramWithoutVariants . joinVariants . removeAlpha) $ map (applyUnions umap) $ rawDiagrams             
     let resultingDiagSetGroups = [(trans,getAllDependencies trans grps) | (DiagSet trans _ _) <- grps]
     mapM_ putStrLn $ map (\(t,gr) -> "# TRS for " ++ showItem t ++ "\n"  ++ (diagramsToITRSTerminationProblem (combineDiagSets gr))) (resultingDiagSetGroups)
         
--      return grps
--      putStrLn $ (showDiagSets grps)
     

automatedInduction atpPath ufile content outprefix =
  do 
     (us,es)  <- case ufile of 
                    Just unionsfile -> 
                             do contents <- readFile unionsfile
                                let (r,st) = P.parseSRulesAndCommands (L.lexInput contents)
                                let stdRules = P.psStandardReductions st
                                let transRules = P.psTransformations st
                                let answerRules = P.psAnswerRules st
                                let commands = P.psOverlapCommands st
                                let unions = P.psUnionCommands st
                                let expands = P.psExpandCommands st
                                let ctxtdef = P.psCtxtDefn st
                                let finfo0 = P.toFunInfo (P.psFunInfo st)                    
                                return (unions,expands)                    
                    Nothing -> return  ([],[])
     let umap = buildUnionMap us
     let rawDiagrams = (parseAndLex content) 
     let expansions =  map joinVariants (procExpands es) -- should be added to each set where needed#
     let grps = groupDiagrams expansions $ nub $ map (simplifyDiagram . joinVariants . removeAlpha) $ map (applyUnions umap) $ rawDiagrams    
     let resultingDiagSetGroups = [(trans,getAllDependencies trans grps) | (DiagSet trans _ _) <- grps]
     mapM_ putStrLn $ map (showDiagSets) (map snd resultingDiagSetGroups)
     let terminationProblems = (map (\(a,b) -> (itemToFile a,(diagramsToFreeVarsTerminationProblem . combineDiagSets) b)) resultingDiagSetGroups)
     sequence_
        [ do 
           putStrLn $ "\nStarting automated induction for transformation " ++  show n
           let fname = (outprefix ++ "." ++ n ++  ".trs")
           putStrLn ("... write TRS to " ++ show fname)
           writeFile fname b
           putStrLn ("... prove termination ...")
           (exitcode,proof,err) <- readCreateProcessWithExitCode (shell ("timeout 120 java -Xmx6000m -jar " ++ atpPath ++ "aprove.jar -m wst -C ceta -p xml -s " ++ atpPath ++ "ceta.strategy " ++ fname ++ " 120")) ""
           putStr ("... result of termination proof: ")
           let result = case (lines proof) of 
                         [] -> "MAYBE"
                         (x:xs) -> x
           putStrLn result
           case result of 
                "YES" -> do
                          putStrLn ("... preparing proof output")
                          let xmlproofinp = unlines $ tail $ lines proof -- sed -e '1d' t.aprove.xml > t.tmp
                          let xmlfilename = (fname ++ ".tmp")
                          writeFile xmlfilename xmlproofinp
                          (_,xmlproof,_) <- readCreateProcessWithExitCode (shell ("java -Xmx2000m -jar " ++ atpPath ++ "saxon9.jar " ++ xmlfilename++  " " ++ atpPath ++ "aproveToCPF2.xsl")) ""
                          let xmlprooffile = (fname ++ ".tmp.xml")
                          writeFile xmlprooffile xmlproof
                          (_,htmlproof,_) <- readCreateProcessWithExitCode (shell ("xsltproc " ++ atpPath ++ "cpfHTML.xsl " ++ xmlprooffile )) ""
                          let htmlfile = fname ++ ".proof.html"
                          writeFile htmlfile htmlproof
                          putStrLn $ "... wrote termination proof to "  ++ show htmlfile
                          putStrLn $ "... starting certification of termination proof using CeTA"
                          (cetaexitcode,cetaout,cetaerr) <- readCreateProcessWithExitCode (shell ("" ++ atpPath ++ "ceta " ++ xmlprooffile)) ""
                          putStrLn $ "... answer of CeTA: " 
                          putStrLn (unlines (map (\l -> "...   " ++ l) (lines cetaout)))
                          case (take 31 cetaout) of 
                               "CERTIFIED <trsTerminationProof>" ->   putStrLn $ "... induction for transformation " ++ show n ++ " done. QED"
                               _                                 -> do
                                                                      putStrLn "... giving up, since certification of termination proof failed."
                                                                      error "STOPPED without success!" 
                _     -> do
                          putStrLn "... giving up, since no termination proof was found" 
                          error "STOPPED without success!"
            | (n,b) <- terminationProblems]

automatedInductionITRS atpPath ufile content outprefix =
  do 
     (us,es)  <- case ufile of 
                    Just unionsfile -> 
                             do contents <- readFile unionsfile
                                let (r,st) = P.parseSRulesAndCommands (L.lexInput contents)
                                let stdRules = P.psStandardReductions st
                                let transRules = P.psTransformations st
                                let answerRules = P.psAnswerRules st
                                let commands = P.psOverlapCommands st
                                let unions = P.psUnionCommands st
                                let expands = P.psExpandCommands st
                                let ctxtdef = P.psCtxtDefn st
                                let finfo0 = P.toFunInfo (P.psFunInfo st)                    
                                return (unions,expands)                    
                    Nothing -> return  ([],[])
     let umap = buildUnionMap us
     let rawDiagrams = (parseAndLex content) 
     let expansions =  map joinVariants (procExpands es) -- should be added to each set where needed#
     let grps = groupDiagrams expansions $ nub $ map (simplifyDiagram . joinVariants . removeAlpha) $ map (applyUnions umap) $ rawDiagrams    
     let resultingDiagSetGroups = [(trans,getAllDependencies trans grps) | (DiagSet trans _ _) <- grps]
     mapM_ putStrLn $ map (showDiagSets) (map snd resultingDiagSetGroups)
     let terminationProblems = (map (\(a,b) -> (itemToFile a,(diagramsToITRSTerminationProblem . combineDiagSets) b)) resultingDiagSetGroups)
     sequence_
        [ do 
           putStrLn $ "\nStarting automated induction for transformation " ++  show n
           let fname = (outprefix ++ "." ++ n ++  ".itrs")
           putStrLn ("... write ITRS to " ++ show fname)
           writeFile fname b
           putStrLn ("... prove termination ...")
           (exitcode,proof,err) <- readCreateProcessWithExitCode (shell ("timeout 120 java -Xmx6000m -jar " ++ atpPath ++ "aprove.jar -m wst -p html " ++ fname ++ " 120")) ""
           putStr ("... result of termination proof: ")
           let result = case (lines proof) of 
                         [] -> "MAYBE"
                         (x:xs) -> x
           putStrLn result
           case result of 
                "YES" -> do
                          putStrLn ("... preparing proof output")
                          let xmlproofinp = unlines $ tail $ lines proof -- sed -e '1d' t.aprove.xml > t.tmp
                          let htmlfile = fname ++ ".proof.html"
                          writeFile htmlfile proof
                          putStrLn $ "... wrote termination proof to "  ++ show htmlfile
                          
                _     -> do
                          putStrLn "... giving up, since no termination proof was found" 
                          error "STOPPED without success!"
            | (n,b) <- terminationProblems]

{-     
     #!/bin/bash
export JAVAPATH=/usr/bin
#local/sabel/java/jre1.7.0_25/bin
$JAVAPATH/java -Xmx6000m -jar aprove.jar -m wst -C ceta -p xml -s ceta.strategy $1 300 > t.aprove.xml 2> t.aprove.err
head -n 1 t.aprove.xml
cp t.aprove.xml $1.aprove.xml
cp t.aprove.err $1.aprove.err


$JAVAPATH/java -Xmx2000m -jar saxon9.jar t.tmp aproveToCPF2.xsl > t.proof.xml
cp t.proof.xml $1.proof.xml

cp t.proof.html $1.proof.html
rm t.tmp
./ceta t.proof.xml > $1.ceta.out 2> $1.ceta.err
-}
     
 
--      print $ map (map showItem) dgs
     
--      print mp
combineDiagSets :: [DiagSet] -> Diagrams
combineDiagSets xs@((DiagSet i dep (D b ds)):_) = D b (nub $ concat [ds | DiagSet i dep (D b ds) <- xs])
      
showDiagSets xs = 
  let trans = [i | DiagSet i dep (D b ds) <- xs]
      diagrams = nub $ [(D b ds) | DiagSet i dep (D b ds) <- xs]
      dependecies = nub $  [(i,dep) | DiagSet i dep (D b ds) <- xs]
  in 
    "Combined sets of diagrams for " ++ (intercalate ", " (map showItem trans)) ++ "\n" ++
    "Computed Dependencies are: \n\t" ++ (intercalate "\n\t" (map (\(b,dep) -> showItem b ++ " requires " ++ (intercalate ", " (map showItem dep))) dependecies)) ++ "\n" ++
    (concatMap showDiagrams diagrams)

getAllDependencies trans grps =
   nub $ getAllDep trans grps [] 
  where 
    getAllDep trans grps haveold =
      let direct = [(d,dep) | d@(DiagSet i dep _) <- grps, i == trans]
          ds = map fst direct
          deps = concatMap snd direct
          have = (trans:haveold)
          rec  = concat [getAllDep x grps have | x <- (deps \\ have)]
      in (ds ++ rec)
       
   

computeAllDependencyGroups mp = 
   let l = Map.keys mp 
       run [] = []
       run (x:list) =  
           let grp = getWCC mp x [x]
           in  grp:(run (list \\ grp))
      
         
   in run l
   
             
getWCC :: Map.Map Item [Item] -> Item -> [Item] -> [Item]                                
getWCC mp trans old =  
  let succs = case Map.lookup trans mp of
                 Nothing -> error $ (showItem trans) ++ "not found in dependency graph"
                 Just x -> x
      succs' = (nub succs) \\ old
      recWCC = concat [getWCC mp d new | d <- succs']
      new = nub (old ++ succs')
  in nub $ new  ++ recWCC
      
       
buildMap grps = foldr (flip buildMapIt) Map.empty grps
  
buildMapIt mp (DiagSet nameAsItem deps set) 
 = foldr (\d mp -> 
           case Map.lookup d mp of 
             Nothing -> Map.insert d [nameAsItem] mp
             Just xs -> Map.insert d (nub $ nameAsItem:xs) mp
             ) mp' deps
  where mp' = case Map.lookup nameAsItem mp of 
                Nothing -> Map.insert nameAsItem deps mp
                Just xs -> Map.insert nameAsItem (nub $ deps ++ xs) mp
 
        
computeDependencies (DiagSet nameAsItem _ ds@(D b diagrams)) =  
      let dep = (nub $ concat  [    (map remPlus lhs')
                                  ++ (map remPlus rhs')
                                | (lhs :~~> rhs) <- diagrams
                                , let lhs' = filter isTrans lhs
                                , let rhs' = filter isTrans rhs
                                ]
                ) \\ [nameAsItem]
      in (DiagSet nameAsItem dep ds)
  where 
   remPlus (Trans dir (Plus r)) = Trans dir r
   remPlus (Trans dir r)        = Trans dir r    
 

  
  
procExpands  [] = []
procExpands ((EXPAND rname rhss f):rest) =
     [[ruleNametoItem rname] :~~> [ruleNametoItem rname']     | rname' <- rhss] ++ (procExpands rest)
  where
    ruleNametoItem (True,name,var,False)  = SR LEFT (Rule name var)
    ruleNametoItem (True,name,var,True)   = SR LEFT (Plus (Rule name var))
    ruleNametoItem (False,name,var,False) = Trans LEFT (Rule name var)
    ruleNametoItem (False,name,var,True)  = Trans LEFT (Plus (Rule name var))

  
       
-- remove alpha transitions
removeAlpha :: Diagram -> Diagram
removeAlpha (lhs :~~> rhs) =
   (concatMap removeAlphaR lhs) :~~> (concatMap removeAlphaR rhs)
 where
   removeAlphaR (SR dir lab)   =  [SR dir lab]
   removeAlphaR (Trans dir (Rule "alpha" _)) = []
   removeAlphaR (Trans dir (Plus (Rule "alpha" _))) = []
   removeAlphaR (Trans dir (Rule "alphaRepair" _)) = []
   removeAlphaR (Trans dir (Plus (Rule "alphaRepair" _))) = []
   removeAlphaR x = [x]
   
-- join variants, by setting all variant numbers to -1   

joinVariants :: Diagram -> Diagram
joinVariants (lhs :~~> rhs) = 
  (map go lhs :~~> map go rhs)
  where 
   go (SR dir (Rule name var))   =  SR dir (Rule name (-1))
   go (SR dir (Plus (Rule name var)))   =  SR dir (Plus (Rule name (-1)))
   go (Trans dir (Rule name var))   =  Trans dir (Rule name (-1))
   go (Trans dir (Plus (Rule name var)))   =  Trans dir (Plus (Rule name (-1)))
   go x = x

-- apply unions
applyUnions unionmap (lhs :~~> rhs) = 
  (map go lhs :~~> map go rhs)
  where 
   go (SR dir (Rule name var))   =  case weaken unionmap (True,name,var,False) of
                                        (_,name',var',_) -> (SR dir (Rule name' var'))
   
   go (SR dir (Plus (Rule name var)))   = case weaken unionmap (True,name,var,True) of
                                           (_,name',var',_) -> (SR dir (Plus (Rule name' var')))

   go (Trans dir (Rule name var))   =  case weaken unionmap (False,name,var,False) of
                                           (_,name',var',_) -> Trans dir (Rule name' var')
                                           
                                           
   go (Trans dir (Plus (Rule name var)))   =  case weaken unionmap (False,name,var,True) of
                                                (_,name',var',_) -> (Trans dir (Plus (Rule name var)))
                                                
   go x = x
  
   
   
-- group diagrams by transformation, note that transformation can be found
-- as right most step of the left hand side of a diagram.

groupDiagrams expansions = map procGroup . groupBy groupFn . sortBy compareFn
 where
   compareFn (lhs1 :~~> _) (lhs2 :~~> _) = compare (last lhs1) (last lhs2)
   groupFn   (lhs1 :~~> _) (lhs2 :~~> _) = let t1 = last lhs1
                                               t2 = last lhs2
                                           in  (getLabel t1) == (getLabel t2)
   procGroup gs@((lhs :~~> _):_)  =  let 
                                       gs' =  gs ++ expansions
                                     in computeDependencies (DiagSet (last lhs) [] (D (isF lhs) gs'))
   isF lhs = all (== LEFT) (map getDir lhs)
   

-- main function: diagramsToFreeVarsTerminationProblem

-- | simplify a diagram by join -T,+-> . -T-> and -T-> . -T,+->
--   into -T,+->  for transformations and reduction -T->
simplifyDiagram :: Diagram -> Diagram
simplifyDiagram (lhs :~~> rhs) =
    (simplify lhs) :~~> (simplify rhs)
 where
 simplify ((SR dir1 (Plus (Rule name1 variant1))):(SR dir2 (Rule name2 variant2)):xs)
     | name1 == name2
     , dir1 == dir2 
     , variant1 == variant2
       = simplify ((SR dir1 (Plus (Rule name1 variant1))):xs)
 simplify ((SR dir1 (Plus (Rule name1 variant1))):(SR dir2 (Plus (Rule name2 variant2))):xs)
     | name1 == name2
     , dir1 == dir2 
     , variant1 == variant2
       = simplify ((SR dir2 (Plus (Rule name2 variant2))):xs)
 simplify ((Trans dir1 (Plus (Rule name1 variant1))):(Trans dir2 (Rule name2 variant2)):xs)
     | name1 == name2
     , dir1 == dir2 
     , variant1 == variant2
       = simplify ((Trans dir1 (Plus (Rule name1 variant1))):xs)
 simplify ((Trans dir1 (Plus (Rule name1 variant1))):(Trans dir2 (Plus (Rule name2 variant2))):xs)
     | name1 == name2
     , dir1 == dir2 
     , variant1 == variant2
       = simplify ((Trans dir2 (Plus (Rule name2 variant2))):xs)
    
 simplify (x:y:ys) = x:(simplify (y:ys))
 simplify (x:xs) =   x:(simplify xs)
 simplify [] = []
 
 
simplifyDiagramWithoutVariants :: Diagram -> Diagram
simplifyDiagramWithoutVariants (lhs :~~> rhs) =
    (simplify lhs) :~~> (simplify rhs)
 where
 simplify ((SR dir1 (Plus (Rule name1 variant1))):(SR dir2 (Rule name2 variant2)):xs)
     | name1 == name2
     , dir1 == dir2 
       = simplify ((SR dir1 (Plus (Rule name1 variant1))):xs)
 simplify ((SR dir1 (Plus (Rule name1 variant1))):(SR dir2 (Plus (Rule name2 variant2))):xs)
     | name1 == name2
     , dir1 == dir2 
       = simplify ((SR dir2 (Plus (Rule name2 variant2))):xs)
 simplify ((Trans dir1 (Plus (Rule name1 variant1))):(Trans dir2 (Rule name2 variant2)):xs)
     | name1 == name2
     , dir1 == dir2 
       = simplify ((Trans dir1 (Plus (Rule name1 variant1))):xs)
 simplify ((Trans dir1 (Plus (Rule name1 variant1))):(Trans dir2 (Plus (Rule name2 variant2))):xs)
     | name1 == name2
     , dir1 == dir2 
       = simplify ((Trans dir2 (Plus (Rule name2 variant2))):xs)
    
 simplify (x:y:ys) = x:(simplify (y:ys))
 simplify (x:xs) =   x:(simplify xs)
 simplify [] = []
 
-- | calculate the set of all SR-Labels of a set of diaggrams
class HasLabel a where
  getLabel :: a -> [Label]

instance HasLabel Label where
 getLabel (Rule r var) = [Rule r var]
 getLabel (Plus label)  = getLabel label
 getLabel (Star label)  = getLabel label
 getLabel _              = []

instance HasLabel Item where
 getLabel (SR dir label) = getLabel label
 getLabel (Trans dir label) =getLabel label
 getLabel _ = []

instance HasLabel Diagram where
 getLabel (a :~~> b) = nub $ (concatMap getLabel a) ++ (concatMap getLabel b)

instance HasLabel Diagrams where
 getLabel (D _ ds) = nub $ concatMap getLabel ds

-- | calculuate the name of a transformation

getTransName (Trans dir l) = case l of 
                              (Rule s var) -> Just (s,var)
                              (Plus (Rule s var)) -> Just (s,var)
                              _ ->            Nothing
getTransName _ = Nothing

getDir (SR dir _) = dir
getDir (Trans dir _) = dir
getDir Answer = LEFT

-- | compute all variable names of a set of diagrams

class HasVar a where
 getVarDiag :: a -> [String]

instance HasVar Label where
  getVarDiag (Var x) = [x]
  getVarDiag _       = []

instance HasVar Item where
  getVarDiag (SR dir lab )    = getVarDiag lab
  getVarDiag (Trans dir lab ) = getVarDiag lab
  getVarDiag (Answer)    = []
  getVarDiag other = error (show other)

instance HasVar Diagram where
 getVarDiag (a :~~> b)   = nub $ (concatMap getVarDiag a) ++ (concatMap getVarDiag b)

instance HasVar Diagrams where
 getVarDiag (D _ xs) = nub $ (concatMap getVarDiag xs)


-- | Apply a substitiution 

class HasSubst a where
 applySubst :: [(String,Label)] -> a -> a 

instance HasSubst Item where
 applySubst s (SR  dir lab)    = SR    dir (applySubst s lab)
 applySubst s (Trans  dir lab) = Trans dir (applySubst s lab)
 applySubst _ Answer = Answer
 applySubst _ _ = undefined
 
instance HasSubst Label where
 applySubst s (Var v)  =
  case lookup v s of
     Nothing -> error $ "in applyS: variable " ++ v ++ " not found in " ++ show s
     Just sth -> sth
 applySubst s (Plus l) = Plus (applySubst s l)
 applySubst s (Star l) = Star (applySubst s l)
 applySubst _ a = a

instance HasSubst Diagram where
 applySubst s (a :~~> b) = map (applySubst s) a :~~> map (applySubst s) b
 
 
 
 -- ===================================================================================
-- Calculating variable interpretations
--  Input: Diagrams with variables
--  Output: Expanded diagrams without variables:
--            - all occuring SR-labels replace the variables
--            - non-occurring SR-labels are replaced by tau
-- ===================================================================================

allVariableInterpretations :: Diagrams -> Diagrams
allVariableInterpretations dia@(D b ds) = 
      D b (concatMap allVariableInterpretationsOneDiagram ds)
 where  
    allVariableInterpretationsOneDiagram diagram = 
          let occuringVariables = getVarDiag diagram
              variableInterpretations = makeVariableInterpretations occuringVariables labels                
          in map (\s -> applySubst s diagram) variableInterpretations
    labels = getLabel dia ++ [Tau]
    -- makeVariableInterpretations receives the variables and the labels and 
    -- generates all possible substitutions
    makeVariableInterpretations vars ls =
     let numberOfVars = (genericLength vars)::Integer
         combs        = combinations numberOfVars ls
         interpretations = map (\comb -> zip vars comb) combs
     in interpretations
    -- combinations calculuates for a set of elements and a number i all combinations of elements of length i
    combinations 0 _  = [[]]
    combinations i elems  = [l:xs | l <- elems, xs <- (combinations (i-1) elems)]

    


-- ===================================================================================
-- Removing transitive closures
--  Input: Diagrams with + 
--  Output: Expanded diagrams without + but with number variables, i.e. ERSARS
-- ===================================================================================

removePlusses :: Diagrams     -- Diagrams with +
                 -> [String]  -- fresh names 
                 -> Diagrams  -- diagrams as NARS
removePlusses (D b xs) names = D b (go xs names)
 where 
  -- go goes through the diagrams
  go  []           _ = []
  go  (d:ds) ns =  let (rules,names') = removePlussesOneDiagram d ns
                            in  rules ++ (go ds names')


removePlussesOneDiagram :: Diagram                  -- Diagram with +
                           -> [String]              -- fresh names
                           -> ([Diagram], [String]) -- pair consisting of the diagrams without + and the non-used fresh names

removePlussesOneDiagram (lhs :~~> rhs) freshnames = 
 let (lhs',rules,n')   = removePlusLeft lhs freshnames  -- remove + on lhs
     (rhs',rules',n'') = removePlusRight rhs n'         -- remove + on rhs 
 in ((lhs' :~~> rhs'):(rules ++ rules'),n'')            -- results in a new rule  lhs' :~~> rhs' and additional rules

-- Removing + on left hand side
removePlusLeft :: [Item] -> [String] -> ([Item], [Diagram], [String])
removePlusLeft lhs freshnames =
  let l = genericLength lhs                      -- l = Anzahl der Symbole in der lhs
      (names,unusednames) = splitAt l freshnames -- wir ber"ucksichtigen l viele neue Namen 
      old_and_new =  (zip lhs names)             -- Abbildung: altes Symbol auf neuen Namen
      newlhs = map (\(a,b) -> if isPlus a then SingleName b else a) old_and_new -- Neue lhs: Ersetze + durch neuen Namen, ansonsten unver"andert
      -- erzeugen der zus"atzlichen Regeln: 
      -- genKLists rennt durch die alte lhs und gleichzeitig durch die neue lhs
      genKLists [] [] = [] 
      genKLists (a:as) ls@(_:lefths)
        -- wenn ein + gefunden wird, m"ussen Regeln erzeugt werden
        | isPlus a = (genLRules a ls) ++ genKLists as lefths
        -- anderenfalls mache weiter
        | otherwise = genKLists as lefths
      genKLists _ _ = undefined
      genLRules (SR dir (Plus lab)) klist = 
        -- I_j ist von der Form <SR,+,l
        -- klist ist gerade K_jK_j-1...K_1-T->
        -- erzeuge zwei Regeln (vgl. Contraction Rules, IJCAR Papier):
        [((SR dir lab):(tail klist)) :~~> klist  -- <-SR,l- . K_j-1 . ... K_1 ~~> K_j K_j-1 . ... K_1-T->
        , ((SR dir lab):klist) :~~> klist        -- <-SR,l- . K_j K_j-1 . ... K_1 ~~> K_j K_j-1 . ... K_1-T->
        ]
      genLRules _ _ = []
 in (newlhs, genKLists lhs newlhs,unusednames)
      

-- removing + on right hand side
removePlusRight :: [Item]  -> [String] -> ([Item], [Diagram], [String])
removePlusRight rhs freshnames =
  let l = length rhs                                  -- l = Anzahl der Symbole in der rhs
      (names,unusednames) = splitAt l freshnames      -- wir ber"ucksichtigen l viele neue Namen
      old_and_new =  (zip rhs names)                  -- Abbildung: altes Symbol auf neuer Name
      -- Verdopple: Wenn <sr,+,l-Symbol dann erzeuge (w(K),<-sr,l) ansonsten: Wenn I dann erzeuge (I,I)
      doubly = map (\(a,b) -> if isPlus a then (DoubleName b VarK,removePlus a) else (a,a)) old_and_new
      revs = reverse doubly -- drehe die Liste um
      -- gehe durch die revs liste zum Erzeugen der Expansion-Rules, vgl. IJCAR-Paper
      rgo xs = let (_,rs) = span (\x -> not (isDouble (fst x))) xs -- finde erste DoubleName-Symbol (ist das rightmost I_j, IJCAR-Paper)
               in if null rs then [] -- wenn es keins gibt, dann fertig
                  -- sonst
                  else let wj = (fst $ head rs) -- w(K)
                           lj = (snd $ head rs) -- <-sr,l- = L_j
                           lntolj = reverse (map snd rs) -- Ln ... L_j
                           rule1 =[(toKPlusOne wj)] :~~> [wj,lj] -- Regel: w(K+1) ~~> w(K)L_j
                           rule2 = [(toOne wj)] :~~> lntolj      -- Regel: w(1)   ~~> Ln ... Lj
                           wmrule = if null (filter (isDouble) (map fst (tail rs))) then [] -- gibt es noch ein Plus-Symbol davor
                                    else  let (lis',wmr:_) =span (\x -> not (isDouble (fst x))) (tail rs) -- finde das Plus
                                          in [[((toKPlusOne wj))] :~~> (((fst wmr):reverse (map snd lis'))++[lj])]
                                            -- erzeuge Regel w_j(k+1) ~~> w_m(k)L_m-1...L_j
                       in (rule1:[rule2]) ++ wmrule ++ (rgo $ tail rs)
      -- Berechnen der neuen rechten Seite
      newrhs = go revs       
      go xs = let (lis,rs) = span (\x -> not (isDouble (fst x))) xs  -- suche rechtestes Plus
              in if null rs then (map fst $ reverse lis)
                 else (fst $ head rs):(map fst $ reverse lis)
  in (newrhs, rgo revs ,unusednames)
 where
  -- einige kleine Hilfsfunktionen
  -- isDouble pr"uft ob ein DoubleName-Label vorliegt
   isDouble (DoubleName _  _) = True
   isDouble _ = False
  -- Konvertiere in k+1 Symbol
   toKPlusOne (DoubleName a _) = (DoubleName a KPlusOne)
   toKPlusOne _ = undefined
  -- Konvertiere 1
   toOne (DoubleName a _) = (DoubleName a One)
   toOne _  = undefined
  -- Entferne Plus aus dem Item
   removePlus (SR dir (Plus l)) = SR dir l
   removePlus (Trans dir (Plus l)) = Trans dir l
   removePlus x = x 
   

-- ===================================================================================
-- Convert diagrams to ERSARS
-- first remove the variables, then the transitive closures
-- ===================================================================================

toERSARS :: Diagrams -> Diagrams
toERSARS ds =
 let names = ["W" ++ show i | i <- ([1..] :: [Integer])]
     diagrams1 = allVariableInterpretations ds
     diagrams2 = removePlusses diagrams1 names
 in diagrams2
 
-- Data type for terms and term rewrite systems and integer term rewrite systems 

data Term = Fn String [Term] -- function symbol
          | TVar String      -- Term variable 
          | IVar String      -- Integer variable
 deriving(Show)

-- Term rewrite rule

data TRSRule = Term :--> Term
 deriving(Show)

type ITRS = [ITRSRule]

-- Interger term rewrite rule
type ITRSRule = (TRSRule,Constraint)
type Constraint = String 

-- Hilfsfunktionen zum Ausdrucken

printTerm :: Term -> String
printTerm (Fn str []) = str 
printTerm (Fn str term) = str ++ "(" ++ (concat . intersperse "," . map printTerm $ term) ++ ")"
printTerm (TVar v) = v
printTerm (IVar v) = v

printTRSRule :: ITRSRule -> String
printTRSRule ((a :--> b ),[]) = printTerm a ++ " -> " ++ printTerm b
printTRSRule ((a :--> b ),c)  = printTerm a ++ " -> " ++ printTerm b  ++ " :|: " ++ c

printITRS :: ITRS -> String
printITRS trs = unlines $ map printTRSRule trs


-- conversion: ERSARS to ITRS
diagramsToITRS (D _ ds) tvar = 
  -- uebersetze jedes Diagramm einzeln (Achtung: Die Eingabe muss schon in NARS vorliegen)
  map (\d -> diagramToITRSRule d tvar) ds

diagramToITRSRule (a :~~> b) tvar = ((itemsToTerm (reverse a) tvar) :--> (itemsToTerm (reverse b) tvar),constraint)
 where 
  -- Erzeuge Constraint: Wenn die Regel eine Zahlvariable enth"alt:
  constraint = if containsNumberVar  a || containsNumberVar b then "k > 1" else ""
      where
       containsNumberVar [] = False
       containsNumberVar ((DoubleName _ (VarK)):xs) = True
       containsNumberVar ((DoubleName _ (KPlusOne)):xs) = True
       containsNumberVar (x:xs) = containsNumberVar xs
  -- Erzeuge Term aus Liste von Items
  itemsToTerm [] tvar = tvar
  itemsToTerm ((SingleName s):xs) tvar = Fn s [(itemsToTerm xs tvar)    ]
  itemsToTerm ((DoubleName s (VarK)):xs) tvar = Fn (s) [IVar "k - 1",itemsToTerm xs tvar]
  itemsToTerm ((DoubleName s One):xs) tvar = Fn (s) [IVar "1",itemsToTerm xs tvar]
  itemsToTerm ((DoubleName s (KPlusOne)):xs) tvar = Fn (s) [IVar "k",itemsToTerm xs tvar]
  itemsToTerm ((SR dir r):xs) tvar = Fn (mkString r "SR") [itemsToTerm xs tvar]
  itemsToTerm ((Trans dir r):xs) tvar = Fn (mkString r "T" ) [itemsToTerm xs tvar]
  itemsToTerm (Answer:[]) tvar = Fn ("Answer") []
  -- String erzeugen (wird als Funktionssname im ITRS verwendet)
  mkString (Rule str var)
    | var < 0 = \x -> (x ++ (replMinus str))  
  mkString (Rule str var) = \x -> (x ++ (replMinus str) ++ show var)
  mkString (Tau) = \x ->  x ++ "TAU"
  mkString x = error (show x)
  replMinus ('-':xs) = "Minus" ++ (replMinus xs)
  replMinus (x:xs) = x:(replMinus xs)
  replMinus [] = []



-- =============================================
-- conversion: ERSARS to TRS with free variables

diagramsToTRS :: Diagrams    -- Diagramme (ERSARS)
                 -> Term     -- Termvariable, die die TRS-Regeln eingef"ugt wird
                 -> ITRS
diagramsToTRS (D _ ds) tvar = 
  -- uebersetze jedes Diagramm einzeln (Achtung: Die Eingabe muss schon in NARS vorliegen)
  map (\d -> diagramToTRSRule d tvar) ds

-- einzelnes Diagramm
diagramToTRSRule :: Diagram -> Term -> (TRSRule, String)
diagramToTRSRule (a :~~> b) tvar = ((itemsToTerm (reverse a) tvar) :--> (itemsToTerm (reverse b) tvar),constraint)
 where 
  -- Erzeuge Constraint: Wenn die Regel eine Zahlvariable enth"alt:
  constraint = if containsNumberVar  a || containsNumberVar b then "" else ""
      where
       containsNumberVar [] = False
       containsNumberVar ((DoubleName _ (VarK)):_) = True
       containsNumberVar ((DoubleName _ (KPlusOne)):_) = True
       containsNumberVar (_:xs) = containsNumberVar xs
  -- Erzeuge Term aus Liste von Items
  itemsToTerm [] tv = tv
  itemsToTerm ((SingleName s):xs) tv = Fn s [(itemsToTerm xs tv)    ]
  itemsToTerm ((DoubleName s (VarK)):xs) tv = Fn (s) [IVar "k",itemsToTerm xs tv]
  itemsToTerm ((DoubleName s One):xs) tv = Fn (s) [IVar "s(k)",itemsToTerm xs tv]
  itemsToTerm ((DoubleName s (KPlusOne)):xs) tv = Fn (s) [IVar "s(k)",itemsToTerm xs tv]
  itemsToTerm ((SR dir r):xs) tv = Fn (mkString r "SR") [itemsToTerm xs tv]
  itemsToTerm ((Trans dir r):xs) tv = Fn (mkString r "T" ) [itemsToTerm xs tv]
  itemsToTerm (Answer:[]) _ = Fn ("Answer") []
  itemsToTerm _ _ = undefined
  -- String erzeugen (wird als Funktionssname im ITRS verwendet)
  mkString (Rule str var)
    | var < 0 = \x -> (x ++ (str))  
  mkString (Rule str var) = \x -> (x ++ str ++ show var)

  
  mkString (Tau) = \x ->  x ++ "TAU"
  mkString x = error (show x)
   
    
 
diagramsToFreeVarsTerminationProblem :: Diagrams -> String
diagramsToFreeVarsTerminationProblem  ds =
 -- Header
  "(STRATEGY INNERMOST)\n" ++
  "(VAR x k)\n" ++
  "(RULES\n" ++
 -- Regeln drucken
  (printITRS  itrs ++ ")")
  where 
   -- Diagramme in ERSARS umwandeln
   (D b diagrams') = toERSARS $ ds
   -- doppelte l"oschen
   unfolded_diagrams = D b (nub $ diagrams')
   -- ITRS erzeugen
   itrs = diagramsToTRS unfolded_diagrams (TVar "x")


diagramsToITRSTerminationProblem :: Diagrams -> String
diagramsToITRSTerminationProblem diagrams =
 -- Header
  "(VAR x k)\n" ++
  "(RULES\n" ++
 -- Regeln drucken
   (printITRS  itrs ++ ")")
   where 
    -- Diagramme in ERSARS umwandeln
    (D b diagrams') = toERSARS $ diagrams
    -- doppelte l"oschen
    unfolded_diagrams = D b (nub $ diagrams')
    -- ITRS erzeugen
    itrs = diagramsToITRS unfolded_diagrams (TVar "x")

 
