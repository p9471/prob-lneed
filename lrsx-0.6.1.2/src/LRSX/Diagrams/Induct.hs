-----------------------------------------------------------------------------
-- |
-- Module      :  LRSX.Diagrams.Induct
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--  
-- Handling of forking and commuting diagrams to encode them as a termination 
-- problem for term rewrite systems with free variables on right hand side
-- also handling the interface to the termination prover AProVE and the
-- certifier CeTA
--
-----------------------------------------------------------------------------

module LRSX.Diagrams.Induct where 
import Data.List
import Data.Char
import System.Process
import System.Environment
import System.Directory
import Text.PrettyPrint
import Debug.Trace
import LRSX.Reduction.Rules(ruleSubsumedByRule)
-- main function: diagramsToFreeVarsTerminationProblem

-- =============================================================================
-- Representation of diagrams

-- Set of abstract diagrams
data DiagSet = DiagSet 
                Item      -- Name der Transformation des Diagramssatzes 
                Diagrams  -- Diagrammmenge
              | EmptyDiagSet
               deriving(Show,Eq)

-- Diagrams is a list of abstract diagrams
data Diagrams = D Bool [Diagram] deriving(Show,Eq,Ord)


-- =============================================================================

-- | fromD  converts 'Diagrams' into a list of 'Diagram' by removing the outer constructor
fromD :: Diagrams -> [Diagram]
fromD (D _ xs) = xs
data Diagram =  [Item] :~~> [Item]
  deriving(Show,Eq,Ord)

-- Example
-- [SR (Rule "no-lcase4"),Trans (Rule "cp-in-ch")] :~~> [Trans (Rule "cp-in-ch"),SR (Rule "no-lcase4")]

data Item     = SR Label                 -- standard reduction with label
              | Trans Label              -- transformation with label
              | Answer                   -- abstract symbol for answers
              | SingleName String        -- a 0-ary name (w-symbols, IJCAR-paper)
              | DoubleName String IntSym -- a unary name (w'-symbols, IJCAR-paper)
                                         -- argument is an int symbol
              | Exception String         -- if a diagram is not closed
  deriving(Show,Eq,Ord)

-- | check whether an 'Item' is an 'SR'-item
isSR :: Item -> Bool  
isSR (SR _) = True
isSR _ = False

-- | check whether an 'Item' is a 'Trans'-item
isTrans :: Item -> Bool
isTrans (Trans _) = True
isTrans _ = False


-- labels
data Label    = Rule String -- rule with its name
              | Tau         -- \tau (abstract value)
              | Var String  -- variable
              | Plus Label  -- transitiv closure
              | Star Label  -- reflexive-transitive closure
 deriving(Show,Eq,Ord)


-- number symbols
data IntSym = VarK      -- Number variable K
            | KPlusOne  -- K+1
            | One       -- 1
 deriving(Show,Eq,Ord)

class HasPlus a where
  isPlus :: a -> Bool

instance HasPlus Item where
  isPlus (SR l)    = isPlus l
  isPlus (Trans l) = isPlus l
  isPlus _         = False

instance HasPlus Label where
  isPlus (Plus _)  = True
  isPlus _         = False

  
-- simplifyDiagramSet  :: [Diagram] -> [Diagram]
simplifyDiagramSet unionMap xs = simplifyDiagramSet' xs xs
  where
    simplifyDiagramSet' [] xs = []
    simplifyDiagramSet' (d:ds)  xs
      | or [d `diagramSubsumedBy` e | e <- xs, d /= e] = trace ("withdraw: " ++ (show d)) (simplifyDiagramSet' ds xs)
      | otherwise = d:(simplifyDiagramSet' ds xs)
      
    compareD d1 d2 = if d1 `diagramSubsumedBy` d2 then LT 
                                                  else compare d1 d2
                        
      
    rule1@(lhs1 :~~> rhs1) `diagramSubsumedBy` rule2@(lhs2 :~~> rhs2) = 
       if 
        (lhs1 `sequenceSubsumedBy` lhs2) &&  (rhs1 `sequenceSubsumedBy` rhs2)
        then trace (show (rule1,rule2)) True else False
                         
 
    [] `sequenceSubsumedBy` []  = True 

    
    
    ((SR (Rule r)):sequence1) `sequenceSubsumedBy` ((SR (Rule r')):sequence2)
      |  let rs  = (True,r,1,False)
             rs' = (True,r',0,False)
         in ruleSubsumedByRule unionMap rs rs' = sequence1 `sequenceSubsumedBy` sequence2  --  --SR,a-->SEQ1    vs.  --SR,b-->SEQ2 ==> SEQ1 vs SEQ2  and b = {a,...}
    ((SR (Rule r)):sequence1) `sequenceSubsumedBy` (SR (Plus (Rule r')):sequence2)
      | let rs  = (True,r,1,False)
            rs' = (True,r',0,False)
         in ruleSubsumedByRule unionMap rs rs' =   (sequence1 `sequenceSubsumedBy` sequence2)  --  --SR,a-->SEQ1    vs.  --SR,b+-->SEQ2 ==> SEQ1 vs SEQ2 and b = {a,...}
                                                    ||
                                                   (sequence1 `sequenceSubsumedBy` ((SR (Plus (Rule r'))):sequence2))  --  --SR,a-->SEQ1    vs.  --SR,b+-->SEQ2 ==> SEQ1 vs --SR,b+--> SEQ2 and b = {a,...}
                 
    ((Trans (Rule r)):sequence1) `sequenceSubsumedBy` ((Trans (Rule r')):sequence2)
      |  let rs  = (True,r,1,False)
             rs' = (True,r',0,False)
         in ruleSubsumedByRule unionMap rs rs'  = sequence1 `sequenceSubsumedBy` sequence2  --  --T,a-->SEQ1    vs.  --T,b-->SEQ2 ==> SEQ1 vs SEQ2 and b = {a,...}
    ((Trans (Rule r)):sequence1) `sequenceSubsumedBy` (Trans (Plus (Rule r')):sequence2)
      |  let rs  = (True,r,1,False)
             rs' = (True,r',0,False)
         in ruleSubsumedByRule unionMap rs rs'  =   (sequence1 `sequenceSubsumedBy` sequence2)  --  --T,a-->SEQ1    vs.  --T,b+-->SEQ2 ==> SEQ1 vs SEQ2 and b = {a,...}
                                                ||
                                              (sequence1 `sequenceSubsumedBy` ((Trans (Plus (Rule r'))):sequence2))  --  --T,a-->SEQ1    vs.  --T,b+-->SEQ2 ==> SEQ1 vs --T,b+--> SEQ2 and b = {a,...}
 
    (_:_) `sequenceSubsumedBy` _ = False
    _ `sequenceSubsumedBy` (_:_) = False

-- | simplify a diagram by join -T,+-> . -T-> and -T-> . -T,+->
--   into -T,+->  for transformations and reduction -T->
simplifyDiagram :: Diagram -> Diagram
simplifyDiagram (lhs :~~> rhs) =
    (simplify lhs) :~~> (simplify rhs)
 where
 simplify ((SR (Plus (Rule l))):(SR (Rule r)):xs)
     | l == r = simplify ((SR (Plus (Rule l))):xs)
     
 simplify ((SR (Rule l)):((SR (Plus (Rule r))):xs))
     | l == r = simplify ((SR (Plus (Rule r))):xs)
     
 simplify ((SR (Plus (Rule l))):(SR (Plus (Rule r))):xs)
     | l == r = simplify ((SR (Plus (Rule l))):xs)
     
 simplify ((Trans (Plus (Rule l))):(Trans (Rule r)):xs)
     | l == r = simplify ((Trans (Plus (Rule l))):xs)
     
 simplify ((Trans (Rule l)):((Trans (Plus (Rule r))):xs))
     | l == r = simplify ((Trans (Plus (Rule r))):xs)
     
 simplify ((Trans (Plus (Rule l))):((Trans (Plus (Rule r))):xs))
     | l == r = simplify ((Trans (Plus (Rule r))):xs)
     
 simplify (x:y:ys) = x:(simplify (y:ys))
 simplify (x:xs) =   x:(simplify xs)
 simplify [] = []
-- | calculate the set of all SR-Labels of a set of diaggrams
class HasLabel a where
  getLabel :: a -> [Label]

instance HasLabel Label where
 getLabel (Rule r) = [Rule r]
 getLabel (Plus label)  = getLabel label
 getLabel (Star label)  = getLabel label
 getLabel _              = []

instance HasLabel Item where
 getLabel (SR label) = getLabel label
 getLabel _          = []

instance HasLabel Diagram where
 getLabel (a :~~> b) = nub $ (concatMap getLabel a) ++ (concatMap getLabel b)

instance HasLabel Diagrams where
 getLabel (D _ ds) = nub $ concatMap getLabel ds

-- | calculuate the name of a transformation
getTransName :: Item -> Maybe String
getTransName (Trans l) = case l of 
                          (Rule s) -> Just s
                          _ -> Nothing
getTransName _ = Nothing

-- | compute all variable names of a set of diagrams

class HasVar a where
 getVarDiag :: a -> [String]

instance HasVar Label where
  getVarDiag (Var x) = [x]
  getVarDiag _       = []

instance HasVar Item where
  getVarDiag (SR lab) = getVarDiag lab
  getVarDiag (Trans lab) = getVarDiag lab
  getVarDiag (Answer)    = []
  getVarDiag other = error (show other)

instance HasVar Diagram where
 getVarDiag (a :~~> b)   = nub $ (concatMap getVarDiag a) ++ (concatMap getVarDiag b)

instance HasVar Diagrams where
 getVarDiag (D _ xs) = nub $ (concatMap getVarDiag xs)


-- | Apply a substitiution 

class HasSubst a where
 applySubst :: [(String,Label)] -> a -> a 

instance HasSubst Item where
 applySubst s (SR lab)    = SR    (applySubst s lab)
 applySubst s (Trans lab) = Trans (applySubst s lab)
 applySubst _ Answer = Answer
 applySubst _ _ = undefined
instance HasSubst Label where
 applySubst s (Var v)  =
  case lookup v s of
     Nothing -> error $ "in applyS: variable " ++ v ++ " not found in " ++ show s
     Just sth -> sth
 applySubst s (Plus l) = Plus (applySubst s l)
 applySubst s (Star l) = Star (applySubst s l)
 applySubst _ a = a

instance HasSubst Diagram where
 applySubst s (a :~~> b) = map (applySubst s) a :~~> map (applySubst s) b

-- | convert diagrams to textual representation
printDiagrams :: Diagrams -> String
printDiagrams (D _ ds) =  unlines $ map printDiagram ds

printDiagram :: Diagram -> String
printDiagram (l :~~> r) = printItemList l ++  " ~~> " ++ printItemList r

printItemList :: [Item] -> String
printItemList xs = concat $ intersperse " . " $ map printItem xs

printItem :: Item -> String
printItem (SR lab) = "SR," ++ printLabel lab
printItem (Trans lab) = printLabel lab
printItem Answer = "Answer"
printItem (SingleName str) = "<" ++ str ++ ">"
printItem (DoubleName str intsym) = "<" ++ str ++ "," ++ printIntSym intsym ++ ">"
printItem _ = undefined

printLabel :: Label -> String
printLabel (Rule str) = str
printLabel Tau        = "tau"
printLabel (Var str)  = str
printLabel (Plus lab) =  printLabel lab ++ ",+"
printLabel (Star lab) = printLabel lab ++ ",*" 

printIntSym :: IntSym -> String
printIntSym VarK = "k"
printIntSym KPlusOne = "k+1"
printIntSym One = "1"


printDiagramWithDir :: Bool -> Diagram -> String
printDiagramWithDir b (l :~~> r) = printItemListWithDir b l ++  " ~~> " ++ printItemListWithDir b r

printItemListWithDir :: Bool -> [Item] -> String
printItemListWithDir b xs = concat $ intersperse " . " $ map (printItemWithDir b) xs

printItemWithDir :: Bool -> Item -> String
printItemWithDir _ (SR lab) = "<-SR," ++ printLabel lab ++ "-"
printItemWithDir b (Trans lab) = if b then 
                                   "<-" ++ printLabel lab ++ "-" 
                                      else 
                                   "-" ++ printLabel lab ++ "->"                                       
printItemWithDir _ Answer = "Answer"
printItemWithDir _ (Exception str) = error str 
printItemWithDir _ (SingleName str) = "<" ++ str ++ ">"
printItemWithDir _ (DoubleName str intsym) = "<" ++ str ++ "," ++ printIntSym intsym ++ ">"



removeDir :: String -> String
removeDir ('-':'>':xs) = removeDir xs
removeDir ('<':'-':xs) = removeDir xs

removeDir (a:'-':b:xs) 
    | isLetter a && isLetter b =  a:'-':b:(removeDir (xs))
removeDir ('-':xs)  = removeDir xs
    
    
removeDir (x:xs) = x:(removeDir xs)
removeDir [] = []

-- ===================================================================================
-- Calculating variable interpretations
--  Input: Diagrams with variables
--  Output: Expanded diagrams without variables:
--            - all occuring SR-labels replace the variables
--            - non-occurring SR-labels are replaced by tau
-- ===================================================================================

allVariableInterpretations :: Diagrams -> Diagrams
allVariableInterpretations dia@(D b ds) = 
      D b (concatMap allVariableInterpretationsOneDiagram ds)
 where  
    allVariableInterpretationsOneDiagram diagram = 
          let occuringVariables = getVarDiag diagram
              variableInterpretations = makeVariableInterpretations occuringVariables labels                
          in map (\s -> applySubst s diagram) variableInterpretations
    labels = getLabel dia ++ [Tau]
    -- makeVariableInterpretations receives the variables and the labels and 
    -- generates all possible substitutions
    makeVariableInterpretations vars ls =
     let numberOfVars = (genericLength vars)::Integer
         combs        = combinations numberOfVars ls
         interpretations = map (\comb -> zip vars comb) combs
     in interpretations
    -- combinations calculuates for a set of elements and a number i all combinations of elements of length i
    combinations 0 _  = [[]]
    combinations i elems  = [l:xs | l <- elems, xs <- (combinations (i-1) elems)]


-- ===================================================================================
-- Removing transitive closures
--  Input: Diagrams with + 
--  Output: Expanded diagrams without + but with number variables, i.e. ERSARS
-- ===================================================================================

removePlusses :: Diagrams     -- Diagrams with +
                 -> [String]  -- fresh names 
                 -> Diagrams  -- diagrams as NARS
removePlusses (D b xs) names = D b (go xs names)
 where 
  -- go goes through the diagrams
  go  []           _ = []
  go  (d:ds) ns =  let (rules,names') = removePlussesOneDiagram d ns
                            in  rules ++ (go ds names')


removePlussesOneDiagram :: Diagram                  -- Diagram with +
                           -> [String]              -- fresh names
                           -> ([Diagram], [String]) -- pair consisting of the diagrams without + and the non-used fresh names

removePlussesOneDiagram (lhs :~~> rhs) freshnames = 
 let (lhs',rules,n')   = removePlusLeft lhs freshnames  -- remove + on lhs
     (rhs',rules',n'') = removePlusRight rhs n'         -- remove + on rhs 
 in ((lhs' :~~> rhs'):(rules ++ rules'),n'')            -- results in a new rule  lhs' :~~> rhs' and additional rules

-- Removing + on left hand side
removePlusLeft :: [Item] -> [String] -> ([Item], [Diagram], [String])
removePlusLeft lhs freshnames =
  let l = genericLength lhs                      -- l = Anzahl der Symbole in der lhs
      (names,unusednames) = splitAt l freshnames -- wir ber"ucksichtigen l viele neue Namen 
      old_and_new =  (zip lhs names)             -- Abbildung: altes Symbol auf neuen Namen
      newlhs = map (\(a,b) -> if isPlus a then SingleName b else a) old_and_new -- Neue lhs: Ersetze + durch neuen Namen, ansonsten unver"andert
      -- erzeugen der zus"atzlichen Regeln: 
      -- genKLists rennt durch die alte lhs und gleichzeitig durch die neue lhs
      genKLists [] [] = [] 
      genKLists (a:as) ls@(_:lefths)
        -- wenn ein + gefunden wird, m"ussen Regeln erzeugt werden
        | isPlus a = (genLRules a ls) ++ genKLists as lefths
        -- anderenfalls mache weiter
        | otherwise = genKLists as lefths
      genKLists _ _ = undefined
      genLRules (SR (Plus lab)) klist = 
        -- I_j ist von der Form <SR,+,l
        -- klist ist gerade K_jK_j-1...K_1-T->
        -- erzeuge zwei Regeln (vgl. Contraction Rules, IJCAR Papier):
        [((SR lab):(tail klist)) :~~> klist  -- <-SR,l- . K_j-1 . ... K_1 ~~> K_j K_j-1 . ... K_1-T->
        , ((SR lab):klist) :~~> klist        -- <-SR,l- . K_j K_j-1 . ... K_1 ~~> K_j K_j-1 . ... K_1-T->
        ]
      genLRules _ _ = []
 in (newlhs, genKLists lhs newlhs,unusednames)
      

-- removing + on right hand side
removePlusRight :: [Item]  -> [String] -> ([Item], [Diagram], [String])
removePlusRight rhs freshnames =
  let l = length rhs                                  -- l = Anzahl der Symbole in der rhs
      (names,unusednames) = splitAt l freshnames      -- wir ber"ucksichtigen l viele neue Namen
      old_and_new =  (zip rhs names)                  -- Abbildung: altes Symbol auf neuer Name
      -- Verdopple: Wenn <sr,+,l-Symbol dann erzeuge (w(K),<-sr,l) ansonsten: Wenn I dann erzeuge (I,I)
      doubly = map (\(a,b) -> if isPlus a then (DoubleName b VarK,removePlus a) else (a,a)) old_and_new
      revs = reverse doubly -- drehe die Liste um
      -- gehe durch die revs liste zum Erzeugen der Expansion-Rules, vgl. IJCAR-Paper
      rgo xs = let (_,rs) = span (\x -> not (isDouble (fst x))) xs -- finde erste DoubleName-Symbol (ist das rightmost I_j, IJCAR-Paper)
               in if null rs then [] -- wenn es keins gibt, dann fertig
                  -- sonst
                  else let wj = (fst $ head rs) -- w(K)
                           lj = (snd $ head rs) -- <-sr,l- = L_j
                           lntolj = reverse (map snd rs) -- Ln ... L_j
                           rule1 =[(toKPlusOne wj)] :~~> [wj,lj] -- Regel: w(K+1) ~~> w(K)L_j
                           rule2 = [(toOne wj)] :~~> lntolj      -- Regel: w(1)   ~~> Ln ... Lj
                           wmrule = if null (filter (isDouble) (map fst (tail rs))) then [] -- gibt es noch ein Plus-Symbol davor
                                    else  let (lis',wmr:_) =span (\x -> not (isDouble (fst x))) (tail rs) -- finde das Plus
                                          in [[((toKPlusOne wj))] :~~> (((fst wmr):reverse (map snd lis'))++[lj])]
                                            -- erzeuge Regel w_j(k+1) ~~> w_m(k)L_m-1...L_j
                       in (rule1:[rule2]) ++ wmrule ++ (rgo $ tail rs)
      -- Berechnen der neuen rechten Seite
      newrhs = go revs       
      go xs = let (lis,rs) = span (\x -> not (isDouble (fst x))) xs  -- suche rechtestes Plus
              in if null rs then (map fst $ reverse lis)
                 else (fst $ head rs):(map fst $ reverse lis)
  in (newrhs, rgo revs ,unusednames)
 where
  -- einige kleine Hilfsfunktionen
  -- isDouble pr"uft ob ein DoubleName-Label vorliegt
   isDouble (DoubleName _  _) = True
   isDouble _ = False
  -- Konvertiere in k+1 Symbol
   toKPlusOne (DoubleName a _) = (DoubleName a KPlusOne)
   toKPlusOne _ = undefined
  -- Konvertiere 1
   toOne (DoubleName a _) = (DoubleName a One)
   toOne _  = undefined
  -- Entferne Plus aus dem Item
   removePlus (SR (Plus l)) = SR l
   removePlus (Trans (Plus l)) = Trans l
   removePlus x = x 

-- ===================================================================================
-- Convert diagrams to ERSARS
-- first remove the variables, then the transitive closures
-- ===================================================================================

toERSARS :: Diagrams -> Diagrams
toERSARS ds =
 let names = ["W" ++ show i | i <- ([1..] :: [Integer])]
     diagrams1 = allVariableInterpretations ds
     diagrams2 = removePlusses diagrams1 names
 in diagrams2
 
-- Data type for terms and term rewrite systems and integer term rewrite systems 

data Term = Fn String [Term] -- function symbol
          | TVar String      -- Term variable 
          | IVar String      -- Integer variable
 deriving(Show)

-- Term rewrite rule

data TRSRule = Term :--> Term
 deriving(Show)

type ITRS = [ITRSRule]

-- Interger term rewrite rule
type ITRSRule = (TRSRule,Constraint)
type Constraint = String 

-- Hilfsfunktionen zum Ausdrucken

printTerm :: Term -> String
printTerm (Fn str []) = str 
printTerm (Fn str term) = str ++ "(" ++ (concat . intersperse "," . map printTerm $ term) ++ ")"
printTerm (TVar v) = v
printTerm (IVar v) = v

printTRSRule :: ITRSRule -> String
printTRSRule ((a :--> b ),[]) = printTerm a ++ " -> " ++ printTerm b
printTRSRule ((a :--> b ),c)  = printTerm a ++ " -> " ++ printTerm b  ++ " :|: " ++ c

printITRS :: ITRS -> String
printITRS trs = unlines $ map printTRSRule trs


-- conversion: ERSARS to ITRS
diagramsToITRS (D _ ds) tvar = 
  -- uebersetze jedes Diagramm einzeln (Achtung: Die Eingabe muss schon in NARS vorliegen)
  map (\d -> diagramToITRSRule d tvar) ds

diagramToITRSRule (a :~~> b) tvar = ((itemsToTerm (reverse a) tvar) :--> (itemsToTerm (reverse b) tvar),constraint)
 where 
  -- Erzeuge Constraint: Wenn die Regel eine Zahlvariable enth"alt:
  constraint = if containsNumberVar  a || containsNumberVar b then "k > 1" else ""
      where
       containsNumberVar [] = False
       containsNumberVar ((DoubleName _ (VarK)):xs) = True
       containsNumberVar ((DoubleName _ (KPlusOne)):xs) = True
       containsNumberVar (x:xs) = containsNumberVar xs
  -- Erzeuge Term aus Liste von Items
  itemsToTerm [] tvar = tvar
  itemsToTerm ((SingleName s):xs) tvar = Fn s [(itemsToTerm xs tvar)    ]
  itemsToTerm ((DoubleName s (VarK)):xs) tvar = Fn (s) [IVar "k - 1",itemsToTerm xs tvar]
  itemsToTerm ((DoubleName s One):xs) tvar = Fn (s) [IVar "1",itemsToTerm xs tvar]
  itemsToTerm ((DoubleName s (KPlusOne)):xs) tvar = Fn (s) [IVar "k",itemsToTerm xs tvar]
  itemsToTerm ((SR r):xs) tvar = Fn (mkString r "SR") [itemsToTerm xs tvar]
  itemsToTerm ((Trans r):xs) tvar = Fn (mkString r "T" ) [itemsToTerm xs tvar]
  itemsToTerm (Answer:[]) tvar = Fn ("Answer") []
  -- String erzeugen (wird als Funktionssname im ITRS verwendet)
  mkString (Rule str) = \x -> (x ++ (replMinus str))
  mkString (Tau) = \x ->  x ++ "TAU"
  mkString x = error (show x)
  replMinus ('-':xs) = "Minus" ++ (replMinus xs)
  replMinus (x:xs) = x:(replMinus xs)
  replMinus [] = []



-- =============================================
-- conversion: ERSARS to TRS with free variables

diagramsToTRS :: Diagrams    -- Diagramme (ERSARS)
                 -> Term     -- Termvariable, die die TRS-Regeln eingef"ugt wird
                 -> ITRS
diagramsToTRS (D _ ds) tvar = 
  -- uebersetze jedes Diagramm einzeln (Achtung: Die Eingabe muss schon in NARS vorliegen)
  map (\d -> diagramToTRSRule d tvar) ds

-- einzelnes Diagramm
diagramToTRSRule :: Diagram -> Term -> (TRSRule, String)
diagramToTRSRule (a :~~> b) tvar = ((itemsToTerm (reverse a) tvar) :--> (itemsToTerm (reverse b) tvar),constraint)
 where 
  -- Erzeuge Constraint: Wenn die Regel eine Zahlvariable enth"alt:
  constraint = if containsNumberVar  a || containsNumberVar b then "" else ""
      where
       containsNumberVar [] = False
       containsNumberVar ((DoubleName _ (VarK)):_) = True
       containsNumberVar ((DoubleName _ (KPlusOne)):_) = True
       containsNumberVar (_:xs) = containsNumberVar xs
  -- Erzeuge Term aus Liste von Items
  itemsToTerm [] tv = tv
  itemsToTerm ((SingleName s):xs) tv = Fn s [(itemsToTerm xs tv)    ]
  itemsToTerm ((DoubleName s (VarK)):xs) tv = Fn (s) [IVar "k",itemsToTerm xs tv]
  itemsToTerm ((DoubleName s One):xs) tv = Fn (s) [IVar "s(k)",itemsToTerm xs tv]
  itemsToTerm ((DoubleName s (KPlusOne)):xs) tv = Fn (s) [IVar "s(k)",itemsToTerm xs tv]
  itemsToTerm ((SR r):xs) tv = Fn (mkString r "SR") [itemsToTerm xs tv]
  itemsToTerm ((Trans r):xs) tv = Fn (mkString r "T" ) [itemsToTerm xs tv]
  itemsToTerm (Answer:[]) _ = Fn ("Answer") []
  itemsToTerm _ _ = undefined
  -- String erzeugen (wird als Funktionssname im ITRS verwendet)
  mkString (Rule str) = \x -> (x ++ str)
  mkString (Tau) = \x ->  x ++ "TAU"
  mkString x = error (show x)


  

-- -------------------------------------------
-- VI c) Eingabediagramme in ITRS-Output 
--       umwandeln
-- ------------------------------------------

diagramsToFreeVarsTerminationProblem :: Diagrams -> String
diagramsToFreeVarsTerminationProblem  ds =
 -- Header
  "(STRATEGY INNERMOST)\n" ++
  "(VAR x k)\n" ++
  "(RULES\n" ++
 -- Regeln drucken
  (printITRS  itrs ++ ")")
  where 
   -- Diagramme in ERSARS umwandeln
   (D b diagrams') = toERSARS $ ds
   -- doppelte l"oschen
   unfolded_diagrams = D b (nub $ diagrams')
   -- ITRS erzeugen
   itrs = diagramsToTRS unfolded_diagrams (TVar "x")


diagramsToITRSTerminationProblem :: Diagrams -> String
diagramsToITRSTerminationProblem diagrams =
 -- Header
  "(VAR x k)\n" ++
  "(RULES\n" ++
 -- Regeln drucken
   (printITRS  itrs ++ ")")
   where 
    -- Diagramme in ERSARS umwandeln
    (D b diagrams') = toERSARS $ diagrams
    -- doppelte l"oschen
    unfolded_diagrams = D b (nub $ diagrams')
    -- ITRS erzeugen
    itrs = diagramsToITRS unfolded_diagrams (TVar "x")

 
-- ===================================================================================
-- VII. E/A-Schnittstelle
-- ===================================================================================
-- 
-- 
-- 
-- -------------------------------------------
-- VI a) Hauptfunktion withAproveFork
-- ------------------------------------------
-- 
-- withAproveFork liest eine Datei von Forking-Diagrammen
-- und erzeugt das AProVE ITRS 
-- Anschlie"ssen wird AProVE auf dem ITRS aufgerufen
-- und das Ergebnis in einer Datei gespeichert und
-- in Kurzform auf dem Bildschirm ausgedruckt
withAproveFork :: FilePath -> IO ()
withAproveFork file = 
 do
  startfork file
  path <- (canonicalizePath $ file ++ ".trs") 
  _ <- system $ "cd " ++ pathToAProvE ++ " && ./call_aproveN.sh " ++ path ++ " > " ++ file ++ ".aprove.out" ++ " 2> " ++ file ++ ".aprove.err"
  res1a <- readFile (file ++ ".aprove.out")
  _ <- readFile (file ++ ".aprove.err")
  putStrLn "======================================================="
  putStrLn "AProVE answers:"
  putStrLn (res1a)
  _ <- system $ "mv t.proof.xml " ++ file ++ ".proof.xml"
  _ <- system $ "mv t.proof.html " ++ file ++ ".proof.html"
  putStrLn "======================================================="
  _ <- system $ "./ceta " ++  file ++ ".proof.xml > " ++ file ++ ".ceta.result" ++ " 2> " ++ file ++ ".ceta.err"
  res1 <- readFile (file ++ ".ceta.result")
  res2 <- readFile (file ++ ".ceta.err")
  putStrLn "CeTA answers:"
  putStrLn res2
  putStrLn res1


-- Hilfsfunktion startfork:
startfork :: FilePath -> IO ()
startfork file = go  file
 where
    go fs = 
       do
        content <- readFile fs
        let p = parseDiagrams content
        putStrLn $ "FILE: " ++  fs
        putStrLn "======================================================="
        putStrLn "Parsed Diagrams"
        putStrLn "======================================================="
        pprintDiagrams p
        putStrLn "======================================================="
        putStrLn "ERSARS"
        putStrLn "======================================================="
        let n = toERSARS (p)
        pprintDiagrams n
        let trs = (diagramsToFreeVarsTerminationProblem (p))
        putStrLn "======================================================="
        putStrLn "Calculated TRS:"
        putStrLn "======================================================="
        putStrLn trs
        writeFile (fs ++ ".trs") trs

-- Diagramme ausdrucken
pprintDiagrams :: Diagrams -> IO ()
pprintDiagrams d = putStrLn (printDiagrams d)



-- -------------------------------------------
-- VII b) Eingabe parsen (rudiment"ar)
-- ------------------------------------------
-- 
-- So sieht die Eingabe normalerweise aus: (Achtung: Gro"ss / Kleinschreibung!)
-- 
-- SR,x . SR,LLET . SR,+,LLET  ~~> LLET . SR,a
-- 
-- Bedingungen:
-- ============
-- * Eine Regel pro Zeile
-- * Leerzeilen und Zeilen beginnend mit # werden ignoriert
-- * Gro"ss / Kleinschreibung: 
--   - SR immer gro"ss
--   - Gro"sbuchstaben = Transformation oder SR-Label
--   - Kleinbuchstaben = Variablen
-- 
-- Hauptfunktion parseDiagrams, parst den Eingabestring in eine Menge von Diagrammen
parseDiagramsOnlyConstants :: String -> Diagrams
parseDiagramsOnlyConstants str = 
  let ls = lines str -- aufbrechen in Zeilen
  in D True $ map parseOneRuleOnlyConstants $ filter (\x -> head x /= '#') $ filter (not . null) ls

-- Einzelne Regel parsen
parseOneRuleOnlyConstants :: String -> Diagram
parseOneRuleOnlyConstants str = 
              let (left,right) = breakStr "~~>" str
                  leftitems = map (map removeBlanks .  breakAll (== ',')) (breakAll (== '.') left)
                  rightitems = map (map removeBlanks .  breakAll (== ',')) (breakAll (== '.') right)
              in
                --  trace (show leftitems) $ 
                  (map parseItemOnlyConstants leftitems) :~~> (map parseItemOnlyConstants rightitems)

-- Einzelne Items parsen
parseItemOnlyConstants :: [String] -> Item
parseItemOnlyConstants ("SR":xs) = SR (parseLabelOnlyConstants xs)
parseItemOnlyConstants ("Answer":_) = Answer
parseItemOnlyConstants xs = Trans (parseLabelOnlyConstants xs)

-- Label Parsen
parseLabelOnlyConstants :: [String] -> Label
parseLabelOnlyConstants [] = error "parse error"
parseLabelOnlyConstants (ys:"+":xs) = Plus (parseLabelOnlyConstants (ys:xs))
parseLabelOnlyConstants (xs) = Rule  (intercalate "," xs)

parseDiagrams :: String -> Diagrams
parseDiagrams str = 
  let ls = lines str -- aufbrechen in Zeilen
  in D True $ map parseOneRule $ filter (\x -> head x /= '#') $ filter (not . null) ls

-- Einzelne Regel parsen
parseOneRule :: String -> Diagram
parseOneRule str = 
              let (left,right) = breakStr "~~>" str
                  leftitems = map (map removeBlanks .  breakAll (== ',')) (breakAll (== '.') left)
                  rightitems = map (map removeBlanks .  breakAll (== ',')) (breakAll (== '.') right)
              in (map parseItem leftitems) :~~> (map parseItem rightitems)

-- Einzelne Items parsen
parseItem :: [String] -> Item
parseItem ("SR":xs) = SR (parseLabel xs)
parseItem ("Answer":_) = Answer
parseItem xs = Trans (parseLabel xs)

-- Label Parsen
parseLabel :: [String] -> Label
parseLabel [] = error "parse error"
parseLabel ("+":xs) = Plus (parseLabel xs)
parseLabel (x:_)
 | isLower (head x) = Var x
 | otherwise        = Rule x

-- Hilfsfunktionen
 
-- Leerzeichen entfernen
removeBlanks :: String -> String
removeBlanks = filter (not . isSpace)
 
-- an allen erf"ullenden Positionen eine Liste aufbrechen 
breakAll :: (a -> Bool) -> [a] -> [[a]] 
breakAll _ [] = [] 
breakAll p xs = let (success,rest) = break p xs
                in if null rest
                   then [success]
                   else success:(breakAll p (tail rest))

-- String anhand eines Patterns in 2 St"ucke aufbrechen 
breakStr :: Eq t => [t] -> [t] -> ([t],[t])
breakStr _ [] = ([],[])
breakStr pat str@(x:xs)
 | pat `isPrefixOf` str = ([],drop (length pat) str)
 | otherwise            = let (s,rest)  = breakStr pat xs
                          in (x:s,rest)


{-
===========================================================================================
VIII. Main-Funktion-}
main :: IO ()
main = 
  do
   args <- getArgs
   if null args then 
    do
     putStrLn "error: missing input file name"
     putStrLn helptxt
    else
     do
      let (a:_) = args
      if a == "-h" || a == "--help" then
        putStrLn helptxt
       else
        withAproveFork a
helptxt :: String
helptxt = "usage: chkdiagrams <filename>\n for help: chkdiagrams -h\n or chkdiagram --help\n"

-- ===========================================================================================
-- ENDE
-- ===========================================================================================
-- 
-- Beispiele
d1,d2,d3,d4,d5,allet :: Diagram
d1 = [SR (Var "x"),Trans (Rule "iSllet")] :~~> [Trans (Rule "iSllet"),SR (Var "x")]

d2 = [SR (Var "x"),Trans (Rule "iSllet")] :~~> [SR (Var "x")]
d3 = [(SR (Plus (Rule "lll"))),Trans (Rule "iSllet")] :~~> [(SR (Plus (Rule "lll")))]
d4 = [SR (Plus ((Rule "lll"))),Trans (Rule "iSllet")] :~~> [Trans (Rule "iSllet"), SR (Plus (Rule "lll"))]
d5 = [SR (Rule "llet"), SR (Var "x"),Trans (Rule "iSllet")] :~~> [SR (Var "x")]
allet = [Answer,Trans (Rule "iSllet")] :~~> [Answer]
dllet :: [Diagram]
dllet = [d1,d2,d3,d4,d5,allet]

test :: Diagram
test = [(SR (Plus (Rule "p2"))),(SR (Plus (Rule "p1"))),Trans (Rule "iSllet")] :~~> 
       [Trans (Plus (Rule "t")), Trans (Rule "a"),(SR (Plus (Rule "pr2"))),(SR (Rule "n1")),(SR (Plus (Rule "pr1"))),(SR ((Rule "n0")))]
diagram1,diagram2,diagram3,diagram4,answerd :: Diagram 
diagram1 = [SR (Var "x"),Trans (Rule "iSSeq")] :~~> [Trans (Rule "iSSeq"),SR (Var "x")]
diagram2 = [SR (Var "x"),Trans (Rule "iSSeq")] :~~> [SR (Var "x")]
diagram3 = [SR (Rule "seq"), SR (Var "x"), Trans (Rule "iSSeq")] :~~> [SR (Var "x")]
diagram4 = [SR (Rule "cp"), Trans (Rule "iSSeq")] :~~> [Trans (Rule "iSSeq"),Trans (Rule "iSSeq"),SR (Rule "cp")]
answerd =  [Answer, Trans (Rule "isSeq")] :~~> [Answer]
diagrams :: [Diagram]
diagrams = [diagram1,diagram2,diagram3,diagram4, answerd]



-- Global parameters
pathToAProvE :: FilePath
pathToAProvE = "/local/sabel/aprove_ceta/aprove_free_var_rhs/"
