module LRSX.Diagrams.AbstractDiagrams.Types where

-- =============================================================================
-- Representation of diagrams

-- Set of abstract diagrams
data DiagSet = DiagSet 
                Item      -- Name der Transformation des Diagramssatzes 
                [Item]    -- Dependencies
                Diagrams  -- Diagrammmenge
              | EmptyDiagSet
               deriving(Show,Eq)

-- Diagrams is a list of abstract diagrams
data Diagrams = D Bool [Diagram] deriving(Show,Eq,Ord)


-- =============================================================================

-- | fromD  converts 'Diagrams' into a list of 'Diagram' by removing the outer constructor
fromD :: Diagrams -> [Diagram]
fromD (D _ xs) = xs
data Diagram =  [Item] :~~> [Item]
  deriving(Show,Eq,Ord)

-- Example
-- [SR (Rule "no-lcase4"),Trans (Rule "cp-in-ch")] :~~> [Trans (Rule "cp-in-ch"),SR (Rule "no-lcase4")]

data Direction = LEFT | RIGHT
 deriving(Eq,Show,Ord)

data Item     = SR    Direction Label    -- standard reduction with label
              | Trans Direction Label    -- transformation with label
              | Answer                   -- abstract symbol for answers
              | SingleName String        -- a 0-ary name (w-symbols, IJCAR-paper)
              | DoubleName String IntSym -- a unary name (w'-symbols, IJCAR-paper)
                                         -- argument is an int symbol
              | Exception String         -- if a diagram is not closed
  deriving(Show,Eq,Ord)

-- | check whether an 'Item' is an 'SR'-item
isSR :: Item -> Bool  
isSR (SR _ _) = True
isSR _ = False

-- | check whether an 'Item' is a 'Trans'-item
isTrans :: Item -> Bool
isTrans (Trans _ _) = True
isTrans _ = False


-- labels
data Label    = Rule String Int -- rule with its name and variant
              | Tau           -- \tau (abstract value)
              | Var String    -- variable
              | Plus Label    -- transitiv closure
              | Star Label    -- reflexive-transitive closure
 deriving(Show,Eq,Ord)


-- number symbols
data IntSym = VarK      -- Number variable K
            | KPlusOne  -- K+1
            | One       -- 1
 deriving(Show,Eq,Ord)

class HasPlus a where
  isPlus :: a -> Bool

instance HasPlus Item where
  isPlus (SR dir l)    = isPlus l
  isPlus (Trans dir l) = isPlus l
  isPlus _         = False

instance HasPlus Label where
  isPlus (Plus _)  = True
  isPlus _         = False
  