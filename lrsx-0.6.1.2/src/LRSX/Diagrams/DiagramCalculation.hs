-----------------------------------------------------------------------------
-- |
-- Module      :  LRSX.Diagrams.DiagramCalculation
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--  
-- Computing the diagrams by first calling the unification algorithm to 
-- compute the critical pairs and then joining them by iterative search
-- and the matching algorithm
--
-----------------------------------------------------------------------------
--
module LRSX.Diagrams.DiagramCalculation where
import Control.Parallel.Strategies
import Control.Parallel
import Data.List
import Data.Maybe
import Data.Either
import Data.Char
import Data.Function
import Debug.Trace 
import LRSX.Matching.Matching hiding(trace)
import LRSX.Reduction.Rules
import LRSX.Language.Syntax
import LRSX.Language.ContextDefinition
import LRSX.Language.Constraints
import LRSX.Language.Alpha hiding(trace)
import LRSX.Util.Util
import LRSX.Interface.Parser
import LRSX.Interface.Lexer
import LRSX.Unification.UnificationProblem
import LRSX.Matching.ConstraintInclusion hiding(trace)
import LRSX.Reduction.Reduction hiding(trace)
import qualified LRSX.Unification.Unification as U
import qualified LRSX.Diagrams.AbstractDiagrams.Types as ADT
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

-- data types for diagrams
-- A path is a sequence of reduction problems
type Path = [ReductionProblem]             

-- A join consists of two pathes
-- joining the left and the right expression 
-- of the critical pair
--                .
--              /   \
--             v     v
--             .     .
--              \   /
--               \ /
--                u

data Join = Join {
         pathFromLeft :: Path,
         pathFromRight :: Path
     }

-- A diagram consists of a critical pair,
-- a list of joins (since there maybe several possible joins)
-- and an attribute diagLog used for logging
data Diagram =
     Diagram {
         overlap :: CriticalPair,
         joins   :: [Join],
         isCommutingDiagram :: Bool,
         isAnswerDiagram :: Bool,
         diagLog :: String,
         isExpansion :: Maybe (RuleName,RuleName),
         outfile :: Maybe String
     }

processExpands xs = concatMap processExpand xs     
processExpand (EXPAND r rs f) = [Diagram {overlap = undefined
                                         ,joins = undefined
                                         ,isCommutingDiagram = False
                                         ,isAnswerDiagram = False
                                         ,diagLog = []
                                         ,isExpansion = Just (r,r')
                                         ,outfile = getOutFile (EXPAND r rs f)} | r' <- rs]
     
-- ======================================================================================================================== 
-- Computing overlappings and joining them
-- ============================================================================================================================
runDiagramCalculation ::
        Map.Map RuleName RuleName
        -> Int       -- ^ bound for context expansion
        -> Int       -- ^ bound for search depth
        -> Bool      -- ^ True if standard reduction extensions are forbidden
        -> Bool      -- ^ True if standard reduction can be extended
        -> Bool      -- ^ True if alphaRenaming should be always applied
        -> Bool      -- ^ True if noAlpha is set
        -> Bool      -- ^ True if fullContextGuess is used
        -> Bool      -- ^ True if fullEnvGuess is used
        -> ([ConstrainedRule],[ConstrainedRule],[ConstrainedRule],FunInfo,CtxtDef)  -- ^ (standard reductions, transformations, funinfo)
        -> [Command] -- ^ list of overlap commands
        -> [Diagram] -- ^ the output is a list of diagrams

runDiagramCalculation unionMap expBound bound noExtendStd extendStd alwaysAlpha noAlpha contextguess envguess (stdRules,transRules,answerRules,finfo,cdef) commands =
  let 
      -- set the information for standard reductions
      stdRulesWithInfo                           = [r{standardReduction=True} | r <- stdRules]
      answerRulesWithInfo                        = [r{standardReduction=True} | r <- answerRules]
      transRulesWithInfo                         = [r{standardReduction=False} | r <- transRules]
      -- globally fresh names (cannot occur in the input due to '#' as prefix)
      fresh                                      = ["#"++ show i | i <- [1..]::[Integer]]
      -- compute all overlappings from the given commands in the input:
      overlapProblems = 
        [OverlapProblem { rules         = (stdRulesWithInfo++answerRulesWithInfo++transRulesWithInfo)
                        , command       = cmd
                        , freshNames    = fresh
                        , funInfo       = finfo
                        , criticalPairs = [] -- will be computed
                        , isCommuting   = False -- will be computed
                        , opLogging     = []
                        , answerOverlap = isAnswerRuleCommand answerRulesWithInfo cmd
                        , fullContextGuess = contextguess
                        , fullEnvironmentGuess = envguess
                        , restrictedRules = [] -- will be computed
                        , ruleSubsumption = unionMap
                        , ctxtInfo = cdef -- will be computed

                        }
                    | cmd@(Single _ _ _ _ _) <- commands]
      allOverlappings                            = executeCommands overlapProblems
      -- flatten the overlappings, s.t. there is one critical pair per problem
      flattenedOverlappings                      = [overlapPB{criticalPairs = [cp]}  | overlapPB <- allOverlappings, cp <- criticalPairs overlapPB]      
     
  in 
      -- call the overlap procedure
     concatMap (closeOverlap bound expBound noExtendStd extendStd alwaysAlpha noAlpha) flattenedOverlappings

-- ======================================================================================================================== 
-- close the overlappings
-- ======================================================================================================================== 
closeOverlap ::
        Int              -- ^ bound for the search depth
        -> Int              -- ^ bound for context expansion
        -> Bool          -- ^ extendStd forbidden
        -> Bool          -- ^ extendStd allowed?
        -> Bool          -- ^ alwaysAlphaRename allowed?
        -> Bool          -- ^ noAlpha allowed?
        -> OverlapProblem   -- ^ the overlap problem
        -> [Diagram]        -- ^ the output is a list of diagrams, since in some cases one overlap problem is split into several
                            --   several ones
    
closeOverlap bound expBound noExtendStd extendStd alwaysAlpha noAlpha overlapProblem
    | isCommuting overlapProblem = 
--
-- commuting case
--   lExpr -T-> . -StdRule-> rExpr 
--
-- try to expand lExpr first by standard reduction then by transformation rules
-- if this does not lead to a join, then also search for standard reduction for rExpr
 let answerFork   = answerRule (leftRuleOrig cp) || answerRule (rightRuleOrig cp)
     stdRules     = [r | r <- rules overlapProblem, standardReduction r, if answerFork then True else (not $ answerRule r)]
     transRules   = [r | r <- rules overlapProblem, not (standardReduction r)]
     restrictions = restrictedRules overlapProblem
     fresh        = freshNames overlapProblem
     finfo        = funInfo overlapProblem
     [cp]         = criticalPairs overlapProblem
     lExpr        = leftExpression cp
     rExpr        = rightExpression cp
     pb           = unifier cp   
     deltaBase    = delta pb
     ruleSubsum   = ruleSubsumption overlapProblem
     
     lReductionProblem = ReductionProblem {
                            rpRedex = constantify lExpr
                           ,rpConstraintBase = constantify deltaBase
                           ,rpGlobalNCCs = []
                           ,rpAlphaBase = []
                           ,rpRule = identity
                           ,rpFreshNames = ["#?" ++ show i | i <- [1..]]
                           ,rpFunInfo = finfo
                           ,rpLogging = []
                           ,rpCDef = ctxtInfo overlapProblem
                           ,rpAlphaOn = False
                         }
     rReductionProblem = ReductionProblem {
                            rpRedex = constantify rExpr
                           ,rpConstraintBase = constantify deltaBase
                           ,rpGlobalNCCs = []
                           ,rpAlphaBase = []
                           ,rpRule = identity
                           ,rpFreshNames = ["#!" ++ show i | i <- [1..]]
                           ,rpFunInfo = finfo                           
                           ,rpLogging = []
                           ,rpCDef = ctxtInfo overlapProblem
                           ,rpAlphaOn = False
                         }
     applyRestrictions path =
       let 
         go [] = []
         go (restr:xs) = 
                         let (resRule,num) = restr
                             uses          = length (filter (\rp -> ruleSubsumedByRule ruleSubsum ((ruleName . rpRule) rp) resRule) path)
                         in 
                             
                             if uses >= num then (resRule:(go xs)) else (go xs)
         forbidden = fastLazyNub (go restrictions)
       in  (\x ->  not (or [ruleSubsumedByRule ruleSubsum (ruleName x) f | f <- forbidden]))
     -- successors w.r.t. standard reductions:
     succStdOnly          f = (\rp -> [[rp'] | rp' <- tryReduceSetOfRules rp (filter f stdRules)])
     -- successors w.r.t. first applying delta4 constraints and then a standard reduction
     succConstraintsStd   f = (\rp -> [[rp'',rp'] | rp' <- applyConstraintEquation rp, rp'' <- tryReduceSetOfRules rp' (filter f stdRules)])     
     -- combining succStdOnly and succConstraintsStd
     succStd              f = (\rp -> (succStdOnly f rp) ++ (succConstraintsStd f rp))
     -- applying a standard reduction and a alpha repairing subsequently
     succAlphaRepairStd   f = (\rp -> [[rp'',rp'] | rp' <- tryReduceSetOfRules rp (filter f stdRules),let rp'' = alphaRepairProblem rp'])     
     -- same as succAlphaRepairStd                                                                                             
     succStdAlpha         f = (\rp ->  (succAlphaRepairStd f rp)) --  ++ (succStd f rp))
     -- computing successors w.r.t. transformation rules
     succTransOnly        f = (\rp -> [[rp'] | rp' <- tryReduceSetOfRules rp (filter f transRules)])
     -- successor w.r.t. transformation rules after applying a delta4-constraint
     succConstraintsTrans f = (\rp -> [[rp'',rp'] | rp' <- applyConstraintEquation rp, rp'' <- tryReduceSetOfRules rp' (filter f transRules)])     
     -- combination of succTransOnly and succConstraintsTrans
     succTrans            f = (\rp -> (succTransOnly f rp) ++ (succConstraintsTrans f rp))
     -- applying a transformation rule, followed by an alpha repair step
     succAlphaRepairTrans f = (\rp -> [[rp'',rp'] | rp' <- tryReduceSetOfRules rp (filter f transRules),let rp'' = alphaRepairProblem rp'])     
     succTransAlpha       f = (\rp ->  (succAlphaRepairTrans f rp)) -- ++ (succTrans f rp))
     -- alphaRenaming
     alpha rp   = [[alphaRenameProblem rp]]
     -- compute the maximal sequence of standard reductions for the lExpr, either with or without alpha alphaRenaming
     -- ensures that at least one standard reduction is applied
     stdMax      = 
         unionSetN [maxPathSearch (bound-1) [p] ((repeat (succStd (applyRestrictions p))))
                | p@(x:y:_) <- maxPathSearch 1 [[lReductionProblem]] ((repeat (succStd (applyRestrictions []))))] 
     stdMaxAlpha = 
         unionSetN [maxPathSearch (bound-1) [p] ((repeat (succStdAlpha (applyRestrictions p))))
                | p@(x:y:z:_) <- (maxPathSearch 2 [[lReductionProblem]] (alpha:(repeat (succStdAlpha  (applyRestrictions [])))))
                ] 
     
     -- extend the standard reduction pathes by transformations (iteratively by bound b), without alpha renaming
     extendPathesLeft ps b     = 
        (if extendStd then (\x -> ps `unionN` x) else id)
                    (unionSetN [pathSearchOne  [p] (repeat (succTrans (applyRestrictions p)))        
                      | p <- ps
                      , length (removeTrivNames p) >= (b-1)
                      ])   
     extendPathesLeftWithAlpha ps b     =   
         (if extendStd then (\x -> ps `unionN` x) else id)
                    (unionSetN [pathSearchOne  [p] (repeat (succTransAlpha (applyRestrictions p)))          
                      | p <- ps
                      , length (removeTrivNames p) >= (b-1)
                      ] ) 
                      
     extendPathesRight ps b    =  
        ps `unionN` (unionSetN [pathSearchOne [p] (repeat (succStd (applyRestrictions p)))
                      | p <- ps
                      , length (removeTrivNames p) >= (b-1)
                      ] )
     
     extendPathesRightWithAlpha ps b    =  
        ps `unionN`  (unionSetN [pathSearchOne [p] (repeat (succStdAlpha (applyRestrictions p))) 
                      | p <- ps
                      , length (removeTrivNames p) >= (b-1)
                      ] )
                      
     initPathesRightWithOutAlpha =  [[rReductionProblem]] 
                                   
     initPathesRightWithAlpha    = [[alphaRenameProblem rReductionProblem,rReductionProblem]] 
     -- in exceptional cases, also standard reductions are applied to the rightExpression:
     pathesRightWithOutAlphaAndWithStdReductions b = pathSearch b [[rReductionProblem]] ((repeat (succStd (applyRestrictions []))))
     pathesRightWithAlphaAndWithStdReductions b    = pathSearch b [[rReductionProblem]] (alpha:(repeat (succStdAlpha (applyRestrictions []))))
     
     joinStart  =  
      let 
       lWithoutAlpha =  
                       -- (if extendStd then (\x -> stdMax `unionN` x) else id) (pathSearchOne stdMax (repeat (succTrans (\x -> True))))
                       (\x -> stdMax `unionN` x) (pathSearchOne stdMax (repeat (succTrans (applyRestrictions []))))                       
       rWithoutAlpha =  initPathesRightWithOutAlpha
       lAlpha =   -- (if extendStd then (\x -> stdMaxAlpha `unionN` x) else id) (pathSearchOne stdMaxAlpha (repeat (succTransAlpha (\x -> True))))
                  (\x -> stdMaxAlpha `unionN` x)  (pathSearchOne stdMaxAlpha (repeat (succTransAlpha (applyRestrictions []))))
       rAlpha =   initPathesRightWithAlpha
      in  
          (lAlpha,rAlpha,lWithoutAlpha,rWithoutAlpha,
           (if (not alwaysAlpha) then 
             -- first search without alpha-renaming
             [  (r1,r2,res) | r1@(p1:ps1) <- lWithoutAlpha
                            , r2@(p2:ps2) <- rWithoutAlpha
                            , let (tr,res) = checkSimLetEquivalence (rpFreshNames p1) p1 p2
             ]
              else [])
            ++ if noAlpha then [] else
            -- search with alpha-renaming
            [  (r1,r2,res) | r1@(p1:ps1) <- lAlpha
                           , r2@(p2:ps2) <- rAlpha
                           , let (tr,res) = checkAlphaEquivalenceWithTrace (rpFreshNames p1) p1 p2
            ]
           )
            
         -----                
     join (lps,rps,lls,rrs) currentBound  =  
      let 
       lWithoutAlpha = extendPathesLeft lls currentBound
       rWithoutAlpha = if extendStd && currentBound == bound  then extendPathesRight rrs currentBound else rrs
       lAlpha        = extendPathesLeftWithAlpha lps currentBound
       rAlpha        = if extendStd && currentBound == bound   then extendPathesRightWithAlpha rps currentBound else rps
      in 
          (lAlpha,rAlpha,lWithoutAlpha,rWithoutAlpha,
           (if (not (alwaysAlpha)) then 
            -- search without alpha renaming:
            [ (r1,r2,b) |    r1@(p1:ps1) <- lWithoutAlpha
                           , r2@(p2:ps2) <- rWithoutAlpha
                           , let (tr,b) = checkSimLetEquivalence (rpFreshNames p1) p1 p2
            ]
             else [])
           ++ if noAlpha then [] else
             -- search with alpha renaming:
           [ (r1,r2,b) | r1@(p1:ps1) <- lAlpha 
                          , r2@(p2:ps2) <- rAlpha
                          , let (tr,b) = checkAlphaEquivalenceWithTrace (rpFreshNames p1) p1 p2
                          
           ]
{-           ++ -- search without alpha but use alpha for checking equivalence:
            (if (not (alwaysAlpha)) then 
             [ (u1:r1,u2:r2,b) |   r1@(p1:ps1) <- lWithoutAlpha
                                 , r2@(p2:ps2) <- rWithoutAlpha
                                 , let u1=alphaRenameProblem p1
                                 , let u2=alphaRenameProblem p2
                           , let (tr,b) = checkAlphaEquivalenceWithTrace (rpFreshNames u1) u1 u2
             ]
                                    else [])
                                    -}                                    
           
           )
     joinWithRPathes (lps,rps,lls,rrs) currentBound  =  
      let 
       lWithoutAlpha = lls
       rWithoutAlpha = extendPathesRight rrs currentBound
       lAlpha        = lps
       rAlpha        = extendPathesRightWithAlpha rps currentBound
      in 
          (lAlpha,rAlpha,lWithoutAlpha,rWithoutAlpha,
           (if (not (alwaysAlpha)) then 
            -- search without alpha renaming:
            [ (r1,r2,b) |    r1@(p1:ps1) <- lWithoutAlpha
                           , r2@(p2:ps2) <- rWithoutAlpha
                           , let (tr,b) = checkSimLetEquivalence (rpFreshNames p1) p1 p2
            ]
             else [])
           ++  if noAlpha then [] else
             -- search with alpha renaming:
           [ (r1,r2,b) | r1@(p1:ps1) <- lAlpha 
                          , r2@(p2:ps2) <- rAlpha
                          , let (tr,b) = checkAlphaEquivalenceWithTrace (rpFreshNames p1) p1 p2
                          
           ]
{-           
           ++ -- search without alpha but use alpha for checking equivalence:
            (if (not (alwaysAlpha)) then 
             [ (u1:r1,u2:r2,b) |   r1@(p1:ps1) <- lWithoutAlpha
                                 , r2@(p2:ps2) <- rWithoutAlpha
                                 , let u1=alphaRenameProblem p1
                                 , let u2=alphaRenameProblem p2
                           , let (tr,b) = checkAlphaEquivalenceWithTrace (rpFreshNames u1) u1 u2
             ]
                                    else [])
                                    -}           
           )
     -- iterative search
     rIterate ps i 
       | i > bound = []

     rIterate ps i =  let (lA,rA,l,r,pathes) = joinWithRPathes ps i 
                      in [(a,b) | (a,b,d) <- pathes, d] ++ (rIterate (lA,rA,l,r) (i+1))


--      iterate _  i 
--        | trace ("\n ITERATE BOUND:" ++ show i) False = undefined
     iterate ps i 
       | i == bound+1 
         && extendStd = rIterate ps 1 
       
       | i > bound = []
     iterate ps 1 =  let (lA,rA,l,r,pathes) = joinStart 
                     in [(a,b) | (a,b,d) <- pathes, d] ++ (iterate (lA,rA,l,r) 2)
                    
     iterate ps i =  let (lA,rA,l,r,pathes) = join ps i 
                     in [(a,b) | (a,b,d) <- pathes, d] ++ (iterate (lA,rA,l,r) (i+1))
     
     joined = iterate undefined 1
     
     (pathesLeftWithAlpha,pathesRightWithAlpha,pathesLeftWithOutAlpha,pathesRightWithOutAlpha) = 
         let (lA,rA,l,r,_) = joinStart
             resWithoutR = foldr (\i (lA,rA,l,r)  -> let (a,b,c,d,e) = join (lA,rA,l,r) i in (a,b,c,d)) (lA,rA,l,r) [2..bound]
             resWithR = foldr (\i (lA,rA,l,r)  -> let (a,b,c,d,e) = joinWithRPathes (lA,rA,l,r) i in (a,b,c,d)) resWithoutR [1..bound]
         in if extendStd then resWithR else resWithoutR
     pathesLeft =    (if noAlpha then [] else (alpha lReductionProblem) )
                  ++ (if noAlpha then pathesLeftWithOutAlpha else if alwaysAlpha then pathesLeftWithAlpha else pathesLeftWithAlpha ++ pathesLeftWithOutAlpha)
     pathesRight = if noAlpha then pathesRightWithOutAlpha else if alwaysAlpha then pathesRightWithAlpha else pathesRightWithAlpha ++ pathesRightWithOutAlpha
                              
 in if null joined 
     then 
      if (not extendStd) && (not noExtendStd)
       then (closeOverlap bound expBound noExtendStd True alwaysAlpha noAlpha overlapProblem )
       else
       -- no join found, try to expand context variables, by guessing them as empty /non-empty
       -- this may increase the number of overlap problems, but it helps to find joins
       -- TODO: maybe a smarter strategy, to expand specific variables instead of all would
       -- be helpful
          case expandAllContextsOverlapProblem (overlapProblem{fullContextGuess=True}) of
               -- expand if there at least two new subproblems, otherwise this does not help
               newOverlapProblems@(_:_:_) ->  trace "\n*EXPANDING CONTEXTS NOW" (concatMap (closeOverlap bound expBound noExtendStd extendStd alwaysAlpha noAlpha ) newOverlapProblems) 
               -- otherwise no join could be found
               -- thus we try to expand the environment variables
               _ ->  case expandAllEnvsOverlapProblem (overlapProblem{fullEnvironmentGuess=True}) of
                        newOverlapProblems@(_:_:_) ->  trace "\n*EXPANDING ENVIRONMENTS NOW" (concatMap (closeOverlap bound expBound noExtendStd extendStd alwaysAlpha noAlpha) newOverlapProblems) 
                        _ -> case (expBound > 0,applyCtxtUnfoldingOverlapProblem overlapProblem) of
                              (True,newOverlapProblems@(_:_:_)) -> trace "\n*UNFOLDING CONTEXTS NOW" 
                                                                     (concatMap (closeOverlap bound (expBound-1) noExtendStd extendStd alwaysAlpha noAlpha ) newOverlapProblems)
                              _ ->  -- give up:
                                    [Diagram {isAnswerDiagram=answerOverlap overlapProblem
                                      ,outfile = (getOutFile (command overlapProblem))
                                      ,overlap = cp       
                                      ,isCommutingDiagram = True
                                      ,joins = []
                                      ,isExpansion = Nothing
                                      ,diagLog = "\nCommuting Overlap: " ++ ((showEither2 $ ruleNameMiddleToLeft cp) ++ " . " ++ (showEither $ ruleNameMiddleToRight cp)) 
                                                  ++
                                                  "\nNO JOIN FOUND for commuting case:"
                                                  ++ 
                                                  "\n " ++ extend 30 "" ++ (show lExpr) ++ " = left expression"
                                                  ++
                                                  "\n " ++ extend 30 (showEither2 $ ruleNameMiddleToLeft cp)  ++ show (middleExpression cp)
                                                  ++ 
                                                  "\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
                                                  ++ 
                                                  "\n " ++ extend 30 "" ++ show (middleExpression cp)
                                                  ++
                                                  "\n " ++ extend 30 (showEither $ ruleNameMiddleToRight cp) ++ (show rExpr) ++ " = right expression"
                                                  ++
                                                  "\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
                                                  ++
                                                  "\n Nabla was:"
                                                  ++
                                                  "\n Nabla1:" ++ show (delta1 (delta $ unifier cp))
                                                  ++
                                                  "\n Nabla2:" ++ show (delta2 (delta $ unifier cp))
                                                  ++
                                                  "\n Nabla3:" ++ showNCCs (delta3 (delta $ unifier cp))
                                                  ++
                                                  "\n Nabla4:" ++ show (delta4 (delta $ unifier cp))
                                                  ++
                                                  "\n SplitNCCs: " ++ show [(s,Set.elems dset)| (s,dset) <- Map.assocs $ splitCapCon (delta3 (delta $ unifier cp))]
                                                  ++                                                   
                                                  "\n Left Expression:"
                                                  ++
                                                  show (rpRedex lReductionProblem)
                                                  ++
                                                  "\n ApplyDelta4? "
                                                  ++
                                                  unlines (map (show . rpRedex) (applyConstraintEquation lReductionProblem))
                                                  ++
                                                  "\n Reductions for left expression: "
                                                  ++
                                                  intercalate "\n=============================================================\n" (map showPathLong (pathesLeft))
                                                  ++
                                                  "\n Reductions for right expression: "
                                                  ++
                                                  intercalate "\n=============================================================\n" (map showPathLong (pathesRight))
                                      }
                                    ]
        else
        -- at least one join was found, so the diagram can be closed:
        [Diagram {    overlap = cp
                    , isAnswerDiagram=answerOverlap overlapProblem
                    , isCommutingDiagram = True        
                    , outfile = (getOutFile (command overlapProblem))
                    , isExpansion = Nothing                    
                    , joins = [Join { pathFromLeft  = reverse leftPath
                                    , pathFromRight = rightPath
                                    } 
                              | (leftPath,rightPath) <- joined     -- keep all joins, even only one is needed
                              ]
                    , diagLog = ""
             }]
 where
    showEither (Right str)  = "<=" ++ showRuleName str ++ "="       
    showEither (Left str)   = "="  ++ showRuleName str ++ "=>"       
    showEither2 (Right str) = "="  ++ showRuleName str ++ "=>"       
    showEither2 (Left str)  = "<=" ++ showRuleName str ++ "="       
    extend i str            = take i (str ++ (repeat ' '))
    showPathLong p = (intercalate "\n " (map showRLong $ reverse p)) ++ ""     
    showRLong rp = 
        let n = "\n"
                ++
                (extend 30  (if (snd4 $ ruleName $ rpRule rp) == "Identity" then "" else (" ==" ++ (showRuleName $ ruleName $ rpRule rp) ++ "==> ")))
                ++ (show (rpRedex rp) ++ "\nDEL3:" ++ (showSplitted $ splitCapCon $ delta3 (rpConstraintBase rp)))
                              ++ "\nALPHABASE" ++ (show $ rpAlphaBase rp)
  
        in n 
             

closeOverlap bound expBound noExtendStd extendStd alwaysAlpha noAlpha overlapProblem 
        | not (isCommuting overlapProblem) = 
--
-- forking case
--   lExpr  <--T-- . --StdRule--> rExpr
-- try to join the diagram by bidirectional search:
-- apply standard reductions to lExpr and transformations to rExpr, 
--    

 let answerFork   = answerRule (leftRuleOrig cp) || answerRule (rightRuleOrig cp)
     stdRules     = [r | r <- rules overlapProblem, standardReduction r, if answerFork then True else (not $ answerRule r)] -- exclude Answer-Rules for closing, if this isn't an an
     transRules   = [r | r <- rules overlapProblem, not (standardReduction r)]
     restrictions = restrictedRules overlapProblem
     fresh        = freshNames overlapProblem
     finfo        = funInfo overlapProblem
     [cp]         = criticalPairs overlapProblem
     lExpr        = leftExpression cp
     rExpr        = rightExpression cp
     pb           = unifier cp   
     deltaBase    = delta pb
     ruleSubsum   = ruleSubsumption overlapProblem
     lReductionProblem = ReductionProblem {
                            rpRedex = constantify lExpr
                           ,rpConstraintBase = constantify deltaBase
                           ,rpAlphaBase = []
                           ,rpRule = identity
                           ,rpFreshNames = ["#?" ++ show i | i <- [1..]]
                           ,rpFunInfo = finfo
                           ,rpLogging = []
                           ,rpCDef = ctxtInfo overlapProblem
                           ,rpAlphaOn = False
                           ,rpGlobalNCCs = []
                           }
     rReductionProblem = ReductionProblem {
                            rpRedex = constantify rExpr
                           ,rpConstraintBase = constantify deltaBase
                           ,rpAlphaBase = []
                           ,rpRule = identity
                           ,rpFreshNames = ["#!" ++ show i | i <- [1..]]
                           ,rpFunInfo = finfo                           
                           ,rpLogging = []
                           ,rpCDef = ctxtInfo overlapProblem
                           ,rpAlphaOn = False
                           ,rpGlobalNCCs = []
                           }
     
     succStdOnly        f  = (\rp -> [[rp'] | rp' <- tryReduceSetOfRules rp (filter f stdRules)])
     succConstraintsStd f  = (\rp -> [[rp'',rp'] | rp' <- applyConstraintEquation rp, rp'' <- tryReduceSetOfRules rp' (filter f stdRules)])     
     succStd            f  = (\rp -> (succStdOnly f rp) ++ (succConstraintsStd f rp))
     
     succAlphaRepairStd  f = (\rp -> [[rp'',rp'] | rp' <- tryReduceSetOfRules rp (filter f stdRules),let rp'' = alphaRepairProblem rp'])     
     succStdAlpha        f = (\rp ->  (succAlphaRepairStd f rp))
     
     succTransOnly f        = (\rp -> [[rp'] | rp' <- tryReduceSetOfRules rp (filter f transRules)])
     succConstraintsTrans f = (\rp -> [[rp'',rp'] | rp' <- applyConstraintEquation rp, rp'' <- tryReduceSetOfRules rp' (filter f transRules)])     
     succTrans f            = (\rp -> (succTransOnly f rp) ++ (succConstraintsTrans f rp))
     
     succAlphaRepairTrans f  = (\rp -> [[rp'',rp'] | rp' <- tryReduceSetOfRules rp (filter f (transRules))
                                                   , let rp'' = alphaRepairProblem rp'])     
     succTransAlpha       f  = (\rp ->  (succAlphaRepairTrans f rp))

     finalAlphaRepair pathes = concat [ [path,((alphaRepairProblem p):path)]  | path@(p:ps) <- pathes]
     
     alpha rp = [[alphaRenameProblem rp]]

     
     pathesLeftWithAlphaInit   =  let initps = [[alphaRenameProblem lReductionProblem,lReductionProblem]] 
                                  in initps ++ 
                                     (concat [pathSearchOne [p] ((repeat (succStdAlpha (applyRestrictions p)))) | p <- initps] )
     pathesRightWithAlphaInit =   let initps =  [[alphaRenameProblem rReductionProblem,rReductionProblem]] 
                                  in initps ++ 
                                     (concat [pathSearchOne [p] ((repeat (succTransAlpha (applyRestrictions p)))) | p <- initps])
                                  
     pathesLeftInit   =  let initps = [[lReductionProblem]] 
                         in  initps ++ 
                             (concat [pathSearchOne [p]  ((repeat (succStd (applyRestrictions p)))) | p <- initps])
     pathesRightInit =   let initps =  [[rReductionProblem]] 
                         in initps ++ 
                             (concat [pathSearchOne [p] ((repeat (succTrans (applyRestrictions p)))) | p <- initps] )
     
     applyRestrictions path =
       let 
         go [] = []
         go (restr:xs) = 
                         let (resRule,num) = restr
                             uses          = length (filter (\rp -> ruleSubsumedByRule ruleSubsum ((ruleName . rpRule) rp) resRule) path)
                             
                         in if uses >= num then (resRule:(go xs)) else (go xs)
         forbidden = fastLazyNub (go restrictions)
       in 
                 (\x ->  not (or [ruleSubsumedByRule ruleSubsum (ruleName x) f | f <- forbidden]))
                 
                 -- different successor functions for the reduction problem:
             
             
         
     
     extendPathesLeft ps b     = 
        ps `unionN` (unionSetN [pathSearchOne  [p] (repeat (succStd (applyRestrictions p)))        
                      | p <- ps
                      , length (removeTrivNames p) >= ((b-1))  
                      ])   
        
     extendPathesRight ps b    =
        ps `unionN` (unionSetN [pathSearchOne [p] (repeat (succTrans (applyRestrictions p)))
                      | p <- ps
                      , length (removeTrivNames p) >= ((b-1)) 
                      ] )
     
     extendPathesLeftWithAlpha ps b     = 
        ps `unionN` (unionSetN [pathSearchOne  [p] (repeat (succStdAlpha (applyRestrictions p)))          
                      | p <- ps
                      , length (removeTrivNames p) >= ((b-1)) 
                      ] ) 
     extendPathesRightWithAlpha ps b    =
        ps `unionN` (unionSetN [pathSearchOne [p] (repeat (succTransAlpha (applyRestrictions p))) 
                      | p <- ps
                      , length (removeTrivNames p) >= ((b-1)) 
                      
                      ] )
     
     joinStart  =  
      let 
       lWithoutAlpha = pathesLeftInit
       rWithoutAlpha = pathesRightInit
       lAlpha =  pathesLeftWithAlphaInit
       rAlpha =  pathesRightWithAlphaInit
      in -- trace ("\n length lAlpha: " ++ (show $ length lAlpha) ++ ", length rAlpha: " ++ (show $ length rAlpha))
          (lAlpha,rAlpha,lWithoutAlpha,rWithoutAlpha,
           (
           (if (not (alwaysAlpha)) then 
            -- search without alpha renaming:
            [ (r1,r2,tr,b) | r1@(p1:ps1) <- lWithoutAlpha
                           , r2@(p2:ps2) <- rWithoutAlpha
                           , let (tr,b) = checkSimLetEquivalence (rpFreshNames p1) p1 p2
            ]
             else [])
           ++ if noAlpha then [] else
             -- search with alpha renaming:
           [ (r1,r2,tr,b) | r1@(p1:ps1) <- lAlpha 
                          , r2@(p2:ps2) <- rAlpha
                          , let (tr,b) = checkAlphaEquivalenceWithTrace (rpFreshNames p1) p1 p2
                          
           ]
{-           
           ++ -- search without alpha but use alpha for checking equivalence:
            (if (not (alwaysAlpha)) then 
             [ (u1:r1,u2:r2,tr,b) |   r1@(p1:ps1) <- lWithoutAlpha
                                 , r2@(p2:ps2) <- rWithoutAlpha
                                 , let u1=alphaRenameProblem p1
                                 , let u2=alphaRenameProblem p2
                           , let (tr,b) = checkAlphaEquivalenceWithTrace (rpFreshNames u1) u1 u2
             ]
                                    else [])
                                    -}                                    
           
           ))
     join (lps,rps,lls,rrs) currentBound  =  
      let 
       lWithoutAlpha = extendPathesLeft lls currentBound
       rWithoutAlpha = extendPathesRight rrs currentBound
       lAlpha =  extendPathesLeftWithAlpha lps currentBound
       rAlpha =  extendPathesRightWithAlpha rps currentBound
      in --trace ("\n length lAlpha: " ++ (show $ length lAlpha) ++ ", length rAlpha: " ++ (show $ length rAlpha))
          (lAlpha,rAlpha,lWithoutAlpha,rWithoutAlpha,
           (if (not (alwaysAlpha)) then 
            -- search without alpha renaming:
            [ (r1,r2,tr,b) | r1@(p1:ps1) <- lWithoutAlpha
                           , r2@(p2:ps2) <- rWithoutAlpha
                           , let (tr,b) = checkSimLetEquivalence (rpFreshNames p1) p1 p2
            ]
             else [])
           ++ if noAlpha then [] else
             -- search with alpha renaming:
           [ (r1,r2,tr,b) | r1@(p1:ps1) <- lAlpha 
                          , r2@(p2:ps2) <- rAlpha
                          , let (tr,b) = checkAlphaEquivalenceWithTrace (rpFreshNames p1) p1 p2
                          
           ]
 {-           
           ++ -- search without alpha but use alpha for checking equivalence:
            (if (not (alwaysAlpha)) then 
             [ (u1:r1,u2:r2,tr,b) |   r1@(p1:ps1) <- lWithoutAlpha
                                 , r2@(p2:ps2) <- rWithoutAlpha
                                 , let u1=alphaRenameProblem p1
                                 , let u2=alphaRenameProblem p2
                           , let (tr,b) = checkAlphaEquivalenceWithTrace (rpFreshNames u1) u1 u2
             ]
                                    else [])
                                    -}           
           )
     -- iterative search
--      iterate ps  i 
--        | trace ("\n ITERATE BOUND:" ++ show i) False = undefined
     iterate ps i 
       | i > bound = []
     iterate ps 1 =  let (lA,rA,l,r,pathes) = joinStart 
                     in [(a,b,c) | (a,b,c,d) <- pathes, d] ++ (iterate (lA,rA,l,r) 2)
                    
     iterate ps i =  let (lA,rA,l,r,pathes) = join ps i 
                     in [(a,b,c) | (a,b,c,d) <- pathes, d] ++ (iterate (lA,rA,l,r) (i+1))
     
     
     joined = iterate undefined 1
     
     (pathesLeftWithAlpha,xpathesRightWithAlpha,pathesLeftWithOutAlpha,pathesRightWithOutAlpha) = 
         let (lA,rA,l,r,_) = joinStart
         in  foldr (\i (lA,rA,l,r)  -> let (a,b,c,d,e) = join (lA,rA,l,r) i in (a,b,c,d)) (lA,rA,l,r) [2..bound]
                                 
     pathesRightWithAlpha     = 
            let
               go ps b 
                  | b > bound = ps
               go ps b = go ( (
                                ps `unionN`
                                  (unionSetN [pathSearchOne [p] (repeat (succTransAlpha (applyRestrictions p)))
                                               | p <- ps
                                               , length (removeTrivNames p) >= ((b-1))

                                              ] ))) (b+1)

            in go [[alphaRenameProblem rReductionProblem,rReductionProblem]] 1
                                
 in 
     if null joined 
        then case expandAllContextsOverlapProblem overlapProblem{fullContextGuess=True} of
             -- expand contexts, if no join was found
               newOverlapProblems@(_:_:_) -> trace "\n*EXPANDING CONTEXTS NOW" (concatMap (closeOverlap bound expBound noExtendStd extendStd alwaysAlpha noAlpha) newOverlapProblems ) -- expand if there at least two new subproblems
               _ ->  case expandAllEnvsOverlapProblem (overlapProblem{fullEnvironmentGuess=True}) of
                      newOverlapProblems@(_:_:_) -> trace "\n*EXPANDING ENVIRONMENTS NOW" (concatMap (closeOverlap bound expBound noExtendStd extendStd alwaysAlpha noAlpha) newOverlapProblems) 
                      _ -> case (expBound > 0, applyCtxtUnfoldingOverlapProblem overlapProblem) of
                              (True,newOverlapProblems@(_:_:_)) -> trace "\n*UNFOLDING CONTEXTS NOW" 
                                                                      (concatMap (closeOverlap bound (expBound-1) noExtendStd extendStd alwaysAlpha noAlpha) newOverlapProblems)
                              _ -> [Diagram {isAnswerDiagram=answerOverlap overlapProblem
                                    ,overlap = cp                           -- only one subproblem, no join found
                                    ,outfile = (getOutFile (command overlapProblem))
                                    ,isCommutingDiagram = False               
                                    ,joins = []
                                    ,isExpansion = Nothing                             
                                    ,diagLog = "\nForking Overlap: " ++ ((showEither2 $ ruleNameMiddleToLeft cp) ++ " . " ++ (showEither $ ruleNameMiddleToRight cp)) 
                                                ++
                                                "\nNO JOIN FOUND for forking case:"
                                                ++
                                                "\n " ++ extend 30 "" ++ show (middleExpression cp)
                                                ++
                                                "\n " ++ extend 30 (showEither $ ruleNameMiddleToLeft cp) ++ (show lExpr)  ++ " = left expression"
                                                ++ 
                                                "\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
                                                ++ 
                                                "\n " ++ extend 30 "" ++ show (middleExpression cp)
                                                ++
                                                "\n " ++ extend 30 (showEither $ ruleNameMiddleToRight cp) ++ (show rExpr) ++ " = right expression"
                                                ++ 
                                                "\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
                                                ++
                                                "\n Nabla was:"
                                              ++
                                                    "\n Nabla1:" ++ show (delta1 (delta $ unifier cp))
                                                    ++
                                                    "\n Nabla2:" ++ show (delta2 (delta $ unifier cp))
                                                    ++
                                                    "\n Nabla3:" ++ showNCCs (delta3 (delta $ unifier cp))
                                                    ++
                                                    "\n Nabla4:" ++ show (delta4 (delta $ unifier cp))
                                                    ++
                                                    "\n SplitNCCs: " ++ show [(s,Set.elems dset)| (s,dset) <- Map.assocs $ splitCapCon (delta3 (delta $ unifier cp))]
                                                    ++                                    
                                                "\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
                                                ++ 
                                                "\n Reductions for left expression: "
                                                ++
                                                intercalate "\n=============================================================\n" (map showPathLong (if noAlpha then pathesLeftWithOutAlpha else if alwaysAlpha then pathesLeftWithAlpha else pathesLeftWithOutAlpha ++ pathesLeftWithAlpha))
                                                ++
                                                "\n Reductions for right expression: "
                                                ++
                                                intercalate "\n=============================================================\n" (map showPathLong (if noAlpha then pathesRightWithOutAlpha else if alwaysAlpha then pathesRightWithAlpha else pathesRightWithOutAlpha  ++ pathesRightWithAlpha))
                                                }
                                  ]
       else
        [Diagram {    isAnswerDiagram=answerOverlap overlapProblem
                    , overlap = cp
                    ,outfile = (getOutFile (command overlapProblem))
                     , isCommutingDiagram = False        
                    ,isExpansion = Nothing                    
                    , joins = [Join { pathFromLeft  = reverse leftPath
                                    , pathFromRight = rightPath
                                    } 
                              | (leftPath,rightPath,_) <- joined -- keep all joins, even only one is needed
                              ]
                    , diagLog = ""
                  }
         ]
                                    
 where
    showEither (Right str)  = "<=" ++ showRuleName str ++ "="       
    showEither (Left str)   = "="  ++ showRuleName str ++ "=>"       
    showEither2 (Right str) = "="  ++ showRuleName str ++ "=>"       
    showEither2 (Left str)  = "<=" ++ showRuleName str ++ "="       
    extend i str            = take i (str ++ (repeat ' '))
    showPathLong p = (intercalate "\n " (map showRLong $ reverse p)) ++ ""     
    showRLong rp = 
        let n = "\n"
                ++
                (extend 30  (if (snd4 $ ruleName $ rpRule rp) == "Identity" then "" else (" ==" ++ (showRuleName $ ruleName $ rpRule rp) ++ "==> ")))
                ++ (show (rpRedex rp))
                ++ "\nDEL3:" ++ (showSplitted $ splitCapCon $ delta3 (rpConstraintBase rp))
                ++ "\nALPHABASE" ++ (show $ rpAlphaBase rp)
                
        in n 

 
-- ===================================================================================================================    
-- search algorithms
-- ===================================================================================================================
-- iterative depth first search, where the successor function may append pathes (instead of single successors)
  
pathSearch :: Int -> [Path] -> [ReductionProblem -> [Path]] -> [Path]  
pathSearch bound reversedPathes (nf:nfrest)
    | bound == 0 = reversedPathes
    | otherwise = 
  let reversedPathesNew =
        [ resultPathes ++ (rp:rest)
            | rp:rest <- reversedPathes
            , resultPathes <- nf rp 
        ]
  in 
      
      reversedPathes ++ (pathSearch (bound-1) reversedPathesNew nfrest)
  
pathSearchOne :: [Path] -> [ReductionProblem -> [Path]] -> [Path]  
-- pathSearchOne reversedPathes (nf:nfrest) 
--  | trace ("pOne:\n" ++ (intercalate "\n\t" (map (intercalate ";" . (map (showRuleName . ruleName . rpRule))) reversedPathes))) False = undefined
pathSearchOne reversedPathes (nf:nfrest) =
  let reversedPathesNew =
        [ resultPathes ++ (rp:rest)
            | p@(rp:rest) <- reversedPathes
            , resultPathes <- nf rp
        ]
  in 
      reversedPathesNew

-- iterative depth first search, where the successor function may append pathes (instead of single successors),
-- pathes generated in reversed order, i.e. longest pathes first
maxPathSearch :: Int -> [Path] -> [ReductionProblem -> [Path]] -> [Path]  
-- maxPathSearch bound reversedPathes (nf:nfrest)
--  | trace ("MPS:\n" ++ (intercalate "\n\t" (map (intercalate ";" . (map (showRuleName . ruleName . rpRule))) reversedPathes))) False = undefined
maxPathSearch bound reversedPathes (nf:nfrest)
    | bound == 0 = reversedPathes
    | otherwise = 
  let reversedPathesNew =
        [ resultPathes ++ (rp:rest)
            | rp:rest <- reversedPathes
            , resultPathes <- nf rp 
        ]
  in 
   (maxPathSearch (bound-1) reversedPathesNew nfrest) ++ reversedPathes
   
 
 
 
-- =======================================
-- convert to abstract diagrams

{-  


convert unionMap diagram =
 let 
    cp = overlap diagram
    join = case joins diagram of
             (x:xs) -> Just x
             []     -> Nothing
    fromEither (Left a) = a
    fromEither (Right a) = a
    commutingOrForking = isCommutingDiagram diagram
    answerDiagram = isAnswerDiagram diagram
    fork =     [if answerRule (rightRuleOrig cp) then ADT.Answer else
                   if standardReduction (rightRuleOrig cp) 
                    then let (sr,name,var,pl) = fromEither $ ruleNameMiddleToRight cp
                             (_,name',var',_) = weaken unionMap (sr,name,var,False)
                         in if pl 
                                then  ADT.SR ADT.LEFT (ADT.Plus (ADT.Rule name' var'))
                                else  ADT.SR ADT.LEFT (ADT.Rule name' var')
                    else let (sr,name,var,pl) = fromEither $ ruleNameMiddleToRight cp
                             (_,name',var',_) = weaken unionMap (sr,name,var,False)
                         in if pl 
                                then  ADT.Trans ADT.LEFT (ADT.Plus (ADT.Rule name' var'))
                                else  ADT.Trans ADT.LEFT (ADT.Rule name' var')
               ,if answerRule (leftRuleOrig cp) then ADT.Answer else
                  if standardReduction (leftRuleOrig cp) 
                   then let (sr,name,var,pl) = fromEither $ ruleNameMiddleToLeft cp
                            (_,name',var',_) = weaken unionMap (sr,name,var,False)
                        in if pl 
                                then  ADT.SR (if commutingOrForking then ADT.LEFT else ADT.RIGHT) (ADT.Plus (ADT.Rule name' var'))
                                else  ADT.SR (if commutingOrForking then ADT.LEFT else ADT.RIGHT) (ADT.Rule name' var')
                   else let (sr,name,var,pl) = fromEither $ ruleNameMiddleToLeft cp
                            (_,name',var',_) = weaken unionMap (sr,name,var,False)
                        in if pl 
                                then  ADT.Trans (if commutingOrForking then ADT.LEFT else ADT.RIGHT) (ADT.Plus (ADT.Rule name' var'))
                                else  ADT.Trans (if commutingOrForking then ADT.LEFT else ADT.RIGHT) (ADT.Rule name' var')
               ]
               
    result = ADT.D commutingOrForking  $
     case join of 
      Nothing   -> [fork ADT.:~~> [(ADT.Exception (diagLog diagram))]]           
      Just join ->
        let closing =
                     [if answerRule r then ADT.Answer else
                       if standardReduction r 
                        then let (sr,name,var,pl) = ruleName r
                                 (_,name',var',_) = weaken unionMap (sr,name,var,False)
                             in if pl 
                                    then ADT.SR ADT.LEFT (ADT.Plus (ADT.Rule name' var'))
                                    else ADT.SR ADT.LEFT (ADT.Rule name' var')
                        else let (sr,name,var,pl) = ruleName r
                                 (_,name',var',_) = weaken unionMap (sr,name,var,False)
                             in if pl 
                                    then ADT.Trans ADT.LEFT (ADT.Plus (ADT.Rule name' var'))
                                    else ADT.Trans ADT.LEFT (ADT.Rule name' var')
                       | rp <- (if commutingOrForking 
                                 then [] 
                                 else reverse $ pathFromRight join)
                       , let r = rpRule rp
                       , (snd4 $ ruleName r) `notElem` ["Identity","alpha","alphaRepair"]
                       ]
                      ++
                    [if answerRule r then ADT.Answer else
                      if standardReduction r 
                        then let (sr,name,var,pl) = ruleName r
                                 (_,name',var',_) = weaken unionMap (sr,name,var,False)
                             in if pl 
                                    then ADT.SR ADT.LEFT (ADT.Plus (ADT.Rule name' var'))
                                    else ADT.SR ADT.LEFT (ADT.Rule name' var')
                        else let (sr,name,var,pl) = ruleName r
                                 (_,name',var',_) = weaken unionMap (sr,name,var,False)
                             in if pl 
                                    then ADT.Trans ADT.LEFT (ADT.Plus (ADT.Rule name' var'))
                                    else ADT.Trans ADT.LEFT (ADT.Rule name' var') 
                      | rp <- reverse $ pathFromLeft join
                      , let r = rpRule rp
                      , (snd4 $ ruleName r) `notElem` ["Identity","alpha","alphaRepair"]]
            extendedFork = [if answerRule r 
                             then ADT.Answer 
                             else if standardReduction r 
                                   then let (sr,name,var,pl) = ruleName r
                                            (_,name',var',_) = weaken unionMap (sr,name,var,False)
                                        in if pl 
                                            then ADT.SR ADT.LEFT (ADT.Plus (ADT.Rule name' var'))
                                            else ADT.SR ADT.LEFT (ADT.Rule name' var')
                                   else let (sr,name,var,pl) = ruleName r
                                            (_,name',var',_) = weaken unionMap (sr,name,var,False)
                                        in if pl 
                                            then ADT.Trans ADT.LEFT (ADT.Plus (ADT.Rule name' var'))
                                            else ADT.Trans ADT.LEFT (ADT.Rule name' var')
                           | rp <- (if commutingOrForking 
                                     then reverse $ pathFromRight join 
                                     else [])
                           , let r = rpRule rp
                           , (snd4 $ ruleName r) `notElem` ["Identity","alpha","alphaRepair"]
                           ]
        in if null closing 
            then [] 
            else [  (extendedFork ++ fork) 
                     ADT.:~~> 
                     closing
                 ]
 in  result -}
                
fastLazyNubOnWithTrace ys = 
--   xs
  let go set (x:xs) 
       | Set.member x set  = go set xs
       | otherwise = x:(go (Set.insert x set ) xs)
      go set [] = []
      res = map (\(APath p) -> p) (go (Set.empty) (map APath ys)) 
      test = nubBy (\x y -> snd $ fastCheckAlphaEquivalence ["_AEQCHECK" ++ show i | i <- [1..]] (head x) (head y)) (ys)
 
  in 
--          trace ("\n NUB1 no nub: " ++ show (length ys)) $
--            trace ("\n NUB3 snub: " ++ show (length res)) $ 
--             trace ("\n NUB3 renub: " ++ show (length test)) $ 
--              trace ("\n NUB3 snub: " ++ unlines (map (show . dropAllSubsExpr . rpRedex . head) res)) $
             test -- res
             
    

unionSetN xxs = 
   let sets = map (Set.fromList . map APath) xxs          
       res = Set.elems $ Set.unions sets
       test = nubBy (\x y -> snd $ fastCheckAlphaEquivalence ["_AEQCHECK" ++ show i | i <- [1..]] (head x) (head y)) (concat xxs)
   in
--     trace ("\n USET: no nub: " ++ show (sum $ map length xxs)) $
--      trace ("\n USET: snub:" ++ show (length res)) $
--       trace ("\n NUB3 renub: " ++ show (length test)) $      
--          map (\(APath p) -> p) res
        test
unionN xs ys =
   let xmap =  Set.fromList ([APath p | p <- xs])
       ymap =  Set.fromList ([APath p | p <- ys])
       res  = Set.elems $ Set.union xmap ymap
       test = nubBy (\x y -> snd $ fastCheckAlphaEquivalence ["_AEQCHECK" ++ show i | i <- [1..]] (head x) (head y)) (xs ++ ys)
   in 
--      trace ("\n un: no nub: " ++ show (length xs + length ys)) $
--       trace ("\n un: snub: " ++ show (length res)) $
--        trace ("\n NUB3 renub: " ++ show (length test)) $       
--          map (\(APath p) -> p) res
           test

newtype APath = APath Path
instance Eq APath where
 (APath (p:ps)) == (APath (q:qs)) 
   | (rpRedex p) == (rpRedex q) = True
   | otherwise =  snd (fastCheckAlphaEquivalence ["_AEQCHECK" ++ show i | i <- [1..]] p q)
 (APath _) == (APath _) = False
instance Ord APath where
 compare (APath []) (APath []) = EQ
 compare (APath []) (APath _) = LT
 compare (APath (_:_))   (APath []) = GT
 compare p1@(APath (p:ps)) p2@(APath (q:qs)) =
   if p1 == p2 then EQ
    else compare (rpRedex p) (rpRedex q)
       
    
                    
{-
          
  
fromSolved (Solved p) = p
fromSolved other = error ("(fromSolved): " ++ show other)


                  
                                     
-- ================================================================
-- Pretty Printing of the obtained results 
   
showDiagramCalculationResult fork flag [] = []
showDiagramCalculationResult fork flag (xs:xxs) =  
  "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" ++ "\n" ++
  "~~~~~~~      collect diagrams           ~~~~~~~~" ++ "\n\n" ++
  showBlock fork flag xs ++ "\n\n" ++
  "~~~~~~~~~~~~ End of this collection ~~~~~~~~~~~~" ++ "\n" ++
  "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" ++ "\n" ++
  showDiagramCalculationResult fork flag xxs

showBlock fork flag xs = intercalate "\n\n" (map (showSingle fork flag) xs)
getIt (a,b,c,d) = c
getIt' (a,b,c,d) = d

-- showSingle :: Bool -> Bool -> Either a b -> String

showSingle :: Bool -> Bool ->
         Either
            [((Bool, String, ExprRule),
              (Bool, String, ExprRule),
              Delta,
              Solution,
              (ExprRule, ExprRule),
              Problem,
              ((Bool, String, ExprRule),
               (Bool, String, ExprRule),
               Maybe [[(Expression, String, Problem)]],
               Maybe [[(Expression, String, Problem)]]))]
            [(Bool,String,Bool,String,Problem,(ExprRule, ExprRule),
              Solution,
              (([(Expression, String)], [(Expression, String)]),
               Maybe [[(Expression, String, Problem)]],
               Maybe [[(Expression, String, Problem)]]))]
        -> String
showSingle fork flag (Left list) = 
  "List of unsolvable problems:\n"   
    ++ 
   (intercalate 
     "\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n" 
    [ rule1 ++ "  vs.  " ++ rule2 ++ "\n" ++
     "   " ++ show r1 ++ "\n   " ++ show r2 
        ++ "\nDelta1 was:" ++ show (delta1 x)
        ++ "\nDelta2 was:" ++ show (delta2 x)
        ++ "\nDelta3 was:" ++ show (delta3 x)
        ++ "\nDelta4 was:" ++ show (delta4 x)       
        ++ "\n" ++   
        if flag then (
         "Found reduction sequences for " ++ show r1 ++ ":\n"
       ++ (showSeq r1 $ nub (case getIt a of -- Nothing -> Nothing))
                              Nothing -> []
                              Just xs ->   map (map (\(a,b,c) -> (deconstantify a,b))) xs))
        ++ "Found reduction sequences for " ++ show r2 ++ ":\n"
        ++ ((take 20000000000 (
           (showSeq r2 $ nub (case getIt' a of -- Nothing -> Nothing))
                              Nothing -> []
                              Just xs ->   map (map (\(a,b,c) -> (deconstantify a,b))) xs))
                              )) ++ "...(stopped)\n\n\n\n\n\n\n\n\n\n\n\n"))  -- DEBUG, remove STOPNOW again
                              
                                         
          else ""
        | ((w1,rule1,l1 :==>: r1),(w2,rule2,l2 :==>: r2),x,y,z,pb,a) <- list])
        
showSingle fork flag (Right list) = 

   (intercalate
     "\n+++++++++++++++++++++++++++++++\n"
     ["Diagram:\n\n   "++

      (if w2 then "<==" ++ rule2 ++ "==" else  "==" ++ rule2 ++ "==>") ++
            " . " ++
      (if w1 then "==" ++ rule1 ++ "==>" else  "<==" ++ rule1 ++ "==") ++
      " ~~~> " ++
      rule2' ++ " . " ++ rule1' 
      ++ 
       (if not flag then "" else  
          "\n\nProof: The overlap \n\n" ++
       
       (if w2 then (show $ applySolution sol r2) 
                    ++ "\n <==" ++ rule2 ++ "==" 
                    ++ "\n  " ++ (show $ applySolution sol l2) 
              else  (show $ applySolution sol l2) 
                    ++ "\n ==" ++ rule2 ++ "==>" 
                    ++ "\n  " ++(show $ applySolution sol r2))
       ++ "\n" ++ 
       (if w1 then     "   =?=" ++
                       "\n  " ++(show $ applySolution sol l1) 
                    ++ "\n   ==" ++ rule1 ++ "==>" 
                    ++ "\n    " ++ (show $ applySolution sol r1) 
              else     "   =?=" ++
                       "\n  " ++ (show $ applySolution sol l1) 
                    ++ "\n   <==" ++ rule1 ++ "==" 
                    ++ "\n    " ++ (show $ applySolution sol r1))
       ++ "\n\n with" 
       ++ "\n\n  Delta1 =" ++ show (delta1 $ delta pb)
       ++ "\n  Delta2 =" ++ show (delta2 $ delta pb)
       ++ "\n  Delta3 =" ++ intercalate ",\n           " (map showNonCapConstraint (delta3 $ delta pb))
       ++ "\n  Delta4 =" ++ show (delta4 $ delta pb)
       ++ "\n\n can be closed as follows:\n" 
   --    ++
 --      "Transform " ++ show (applySolutionWeak sol r2) ++ ":\n"
        ++ (let l = 6+ maximum (map (\(a,b) -> (length $ show b)) (rules2X++[(undefined,"")]))
            in (showSeqW l (applySolution sol r2) fork ((map (\(a,b) -> (deconstantify a,b)) (rules2X)))))
        ++ "\n" 
--       "Transform " ++ show (applySolutionWeak sol r1) ++ ":\n"
        ++ (let l = 6+ maximum (map (\(a,b) -> (length $ show b)) (reverse rules1X++[(undefined,"")]))
            in (showSeqW2 l (applySolution sol r1) False ((map (\(a,b) -> (deconstantify a,b)) (reverse $ rules1X)))))
        )   
          
       
       
       | (w1,rule1,w2,rule2,pb,rpair@((l1 :==>: r1),(l2 :==>: r2)),sol,(res@(rules1X,rules2X),a,b)) <- list
      , let rule1' = intercalate " . " (map (\r -> ("<==" ++ r ++ "==")) (reverse $ map snd rules1X))
      , let rule2' = intercalate " . " (map (\r -> (if (if fork then not w2 else w2) then "<==" ++ r ++ "==" else  "==" ++ r ++ "==>")) (map snd rules2X)            )
      ]  
     )
   
showSeq l [] = []   
showSeq l (xs:xxs) =  "         " ++ (show l) ++"\n" ++ showSeq' xs ++ "\n\n" ++ showSeq l xxs      

showSeq' [] = ""
showSeq' ((a,b):xs) = "<==" ++ b ++ "==> " ++ show a ++ "\n" ++ showSeq' xs

showSeqW  i l f xs = (replicate i ' ') ++ show l ++ "\n" ++ showSeq'' i f xs
showSeqW2 i l f xs =  (replicate i ' ') ++ showSeq''' i f xs  ++ show l ++ "\n"
showSeq'' l f [] = ""
showSeq'' l f ((a,b):xs) = (if f then take l (("==" ++ b ++ "==> ") ++ repeat ' ')
                                 else take l (("<==" ++ b ++ "== ") ++ repeat ' '))
                                 ++ show a ++ "\n" ++ showSeq'' l f xs

showSeq''' l f [] = ""
showSeq''' l f ((a,b):xs) =      show a ++ "\n"
                              ++ (if f then take l (("==" ++ b ++ "==> ") ++ repeat ' ')
                                       else take l (("<==" ++ b ++ "== ") ++ repeat ' '))
                              ++ showSeq''' l f xs
   
-- Representing the diagrams internally   
-- corresponding to the IJCAR'12 paper to use it for automated induction

data DiagSet = DiagSet
               Item      -- Name of the transformation
               Diagrams  -- Set of Diagrams
             | EmptyDiagSet
               deriving(Show,Eq)

newtype Diagrams = D [Diagram] deriving(Show,Eq)

data Diagram =  [Item] :~~> [Item] | Exception String
  deriving(Show,Eq,Ord)

-- [SR (Rule "no-lcase4"),Trans (Rule "cp-in-ch")] :~~> [Trans (Rule "cp-in-ch"),SR (Rule "no-lcase4")]

data Item     = SR (Either Label Label)                 -- standard reduction, Either to represent the direction
              | Trans (Either Label Label)              -- transformation step, Either to represent the direction
              | Answer                   -- answer 
              | SingleName String        -- variable 
              | DoubleName String IntSym -- variable 
  deriving(Show,Eq,Ord)

data Label    = Rule String  -- named rule
               | Tau         -- \tau (abstract label)
               | Var String  -- variable
               | Plus Label  -- transitive closure
               | Star Label  -- reflexiv-transitive closure
  deriving(Show,Eq,Ord)

data IntSym = VarK      -- Variable for a number 
            | KPlusOne  -- K + 1
            | One       -- 1
 deriving(Show,Eq,Ord)

showDiagSet (DiagSet trans (D diagrams)) =
 unlines $
 ["Set of Diagrams for transformation " ++ (showItem trans)
 ,"-------------------------------------------------------"
 ]
 ++
 [ (intercalate " . " (map showItem lhs)) ++ " ~~~> "  ++ (intercalate " . " (map showItem rhs)) 
  | (lhs :~~> rhs) <- diagrams 
 ]
 ++
 [ "exceptions (unjoinable critical pairs):\n" ++ show s | (Exception s) <- diagrams]

showItem (SR (Left (Var a)))  = "<=" ++ "no," ++ a ++ "=="  
showItem (SR (Right (Var a))) = "==" ++ "no," ++ a ++ "=>"  
showItem (Trans (Left (Var a)))  = "<-" ++ "T," ++ a ++ "--"  
showItem (Trans (Right (Var a))) = "--" ++ "T," ++ a ++ "->"  
showItem (SR (Left lab))  = "<=" ++ showLabel lab ++ "=="  
showItem (SR (Right lab)) = "==" ++ showLabel lab ++ "=>"  
showItem (Trans (Left lab))  = "<-" ++ showLabel lab ++ "--"  
showItem (Trans (Right lab)) = "--" ++ showLabel lab ++ "->"  
showItem (Answer) = "A"

showLabel (Rule n) = clear n 
showLabel (Tau)    = "tau"
showLabel (Plus l) = showLabel l ++ ",+"
showLabel (Star l) = showLabel l ++ ",*"
showLabel (Var a) = a

clear n 
 | "rule_" `isPrefixOf` n = drop 5 n 
 | otherwise = n
 
-- ---------------------------------------------------------------------------- 
outputToDiagramsCompress fork flag l = 
 recompress
 $ toDiagSet 
 $ groupBy groupFn
 $ fastLazyNub (outputToDiagrams fork flag l)
 
groupFn (l1 :~~> r1) (l2 :~~> r2) = last l1 == last l2
groupFn _ _ = False
 
toDiagSetSingle list@((l :~~> r):_) = DiagSet (last l) (D list)
toDiagSetSingle list@(Exception str:_) = error $ "non-closable case:" ++ str

toDiagSet = map toDiagSetSingle
 
outputToDiagrams fork flag [] = []
outputToDiagrams fork flag (xs:xxs) =  
  (blockToDiagrams fork flag xs)++(outputToDiagrams fork flag xxs)

blockToDiagrams fork flag xs = 
    (concatMap (singleToDiagram fork flag) xs)
getTransname  (Right ((w1,rule1,w2,rule2,_,_,_,_):list)) = 
      (if w1 then getRuleType (Right  (Rule rule1)) else (getRuleType $ Left $ Rule rule1))
getTransname  (Left (((w1,rule1,_),(w2,rule2,_),x,y,z,pb,a):_)) =
      (if w1 then (getRuleType $ Right  $ Rule rule1) else (getRuleType $ Left $ Rule rule1))
  
singleToDiagram fork flag (Right list) = 
     [
     let 
        lhs = (if w2 then [getRuleType $ Left  $ Rule rule2] else [getRuleType $ Right $ Rule rule2])
              ++
              (if w1 then [getRuleType $ Right $ Rule rule1] else [getRuleType $ Left $ Rule rule1])
        rhs = rule2' ++ rule1' 
     in lhs :~~> rhs
       | (w1,rule1,w2,rule2,pb,rpair@((l1 :==>: r1),(l2 :==>: r2)),sol,(res@(rules1X,rules2X),a,b)) <- list
      , let rule1' = (map (\r -> (getRuleType $ Left $ Rule r)) (removeCstr $ reverse $ map snd rules1X))
      , let rule2' = (map (\r -> (if (if fork then not w2 else w2) then (getRuleType $ Left $ Rule r) else (getRuleType $ Right $ Rule r))) (removeCstr $ map snd rules2X)            )
      ]  
    
singleToDiagram fork flag (Left list) = [Exception $ (showSingle fork flag (Left list))]
removeCstr xs =  [x | x <- xs, not ("applyConstraintsFrom" `isInfixOf` x)] 
getRuleType rs@(Left (Rule r))
 | "_NO" `isInfixOf` r && "plus" `isInfixOf` r= SR (Left $ Plus $ Rule r)
 | "_NO" `isInfixOf` r = SR rs
 | "plus" `isInfixOf` r = Trans (Left $ Plus $ Rule r)
 | otherwise = Trans rs
getRuleType rs@(Right (Rule r))
 | "_NO" `isInfixOf` r && "plus" `isInfixOf` r= SR (Right $ Plus $ Rule r)
 | "_NO" `isInfixOf` r = SR rs
 | "plus" `isInfixOf` r = Trans (Left $ Plus $ Rule r)
 | otherwise = Trans rs

offset = 0.1
 
showXL (x,y) = show (x+offset,y)
showXR (x,y) = show (x-offset,y)
showYD (x,y) = show (x,y+offset)
showYU (x,y) = show (x,y-offset)

showXYUR (x,y) = show (x-offset,y-offset)
showXYDL (x,y) = show (x+offset,y+offset)


-- 

simplify = map simplifyDiagram
simplifyDiagram (DiagSet t1 (D xs)) = (DiagSet (simplifyItem t1) (D (map simplifyD xs)))
simplifyD (l :~~> r) = (map simplifyItem l) :~~> (map simplifyItem r)

simplifyM n@(x:xs)
 | "plus" `isInfixOf` n = simplifyM (removeSequence "plus" n)
 | "-Chain" `isSuffixOf` n = simplifyM (reverse (drop 6 (reverse n)))
 | "rule_" `isPrefixOf` n = simplifyM (drop 5 n)
 | "NO_" `isPrefixOf` n = ("no," ++ simplifyM (drop 2 n))
 | "T_"  `isPrefixOf` n = ("T," ++ simplifyM (drop 1 n))
 | "-in" `isSuffixOf` n = simplifyM (reverse (drop 3 (reverse n)))
 | "-e" `isSuffixOf` n = simplifyM (reverse (drop 2 (reverse n)))
simplifyM [] = []
simplifyM xs
    | (i:ys) <- reverse xs
    , isDigit i = simplifyM (reverse ys)
simplifyM n@('_':xs)  = simplifyM xs
simplifyM n@(x:xs)    = (x:simplifyM xs)

simplifyItem (SR (Left lab)) = (SR (Left (simplifyLab lab)))
simplifyItem (SR (Right lab)) = (SR (Right (simplifyLab lab)))
simplifyItem (Trans (Left lab)) = (Trans (Left (simplifyLab lab)))
simplifyItem (Trans (Right lab)) = (Trans (Right (simplifyLab lab)))

simplifyLab (Rule n) = Rule (simplifyM n)
simplifyLab (Tau)    = Tau
simplifyLab (Plus l) = Plus (simplifyLab l)
simplifyLab (Star l) = Star (simplifyLab l)
simplifyLab (Var a)  = Var a




recompressIt set = 
 [DiagSet lab (D set') | DiagSet lab (D s) <- 
 -- union the diagrams
    [unionS g  | g <- (groupBy (\(DiagSet t1 _) (DiagSet t2 _) -> t1 == t2) set)]
    , let set' = combineSquares s
    ]
    
isSquare ([SR (Left (Rule l1)), Trans (Right (Rule l2))] :~~> [Trans (Right (Rule r1)), SR (Left (Rule r2))])
  | l1 == r2  && l2 == r1 = True
isSquare ([SR (Right (Rule l1)), Trans (Left (Rule l2))] :~~> [Trans (Left (Rule r1)), SR (Right (Rule r2))])
  | l1 == r2  && l2 == r1 = True  
isSquare ([SR (Left (Rule l1)), Trans (Left (Rule l2))] :~~> [Trans (Left (Rule r1)), SR (Left (Rule r2))])
  | l1 == r2  && l2 == r1 = True
isSquare ([SR (Right (Rule l1)), Trans (Right (Rule l2))] :~~> [Trans (Right (Rule r1)), SR (Right (Rule r2))])
  | l1 == r2  && l2 == r1 = True  
isSquare inp = False


modifySquare ([SR (Left (Rule l1)), Trans (Right (Rule l2))] :~~> [Trans (Right (Rule r1)), SR (Left (Rule r2))])
 = ([SR (Left $ Var "a"), Trans (Right $ Rule l2)] :~~> [Trans (Right $ Rule r1), SR (Left $ Var "a")], l1)
modifySquare ([SR (Left (Rule l1)), Trans (Left (Rule l2))] :~~> [Trans (Left (Rule r1)), SR (Left (Rule r2))])
 = ([SR (Left $ Var "a"), Trans (Left $ Rule l2)] :~~> [Trans (Left $ Rule r1), SR (Left $ Var "a")], l1)


combineSquares set = 
  let
   (squares,rest) = partition isSquare set
   compressed     = fastLazyNub [square | (square,labs) <- map modifySquare squares]
  in compressed++rest
   
  
 
unionS l@((DiagSet t1 _):rest) = DiagSet t1  (D (fastLazyNub $ concatMap (\(DiagSet _ (D y)) -> y) l))
 
recompress  set = recompressIt (simplify set)

showAllDiagramsLaTeX sets = map (showDiagSetLaTeX) (recompress sets)
showDiagSetLaTeX (DiagSet trans (D diagrams)) =

 "\\begin{figure}[tpb]\n"++
 (
 unlines $
 [ 
  if isRectangle then
    "%" ++ show (lhs :~~> rhs)
    ++  
    "%\n\\begin{tikzpicture}[scale=1.3,baseline=-5ex]\n"
    ++  -- given normal order rules
    (concatMap (\x -> x++ ";\n") $ 
        [showItemLaTeX (showYU a) (showYD b) "left" "solid" item 
            |  ((a,b),item) <- zip  [((x,y),(x,(-1)*(y+stepWidthNoLHS))) | let x=0, y <-[0,stepWidthNoLHS..maxY]] noLHS])
    ++  -- given transformations
    (concatMap (\x -> x++ ";\n") $ 
        [showItemLaTeX (showXL a) (showXR b) "above" "solid" item 
            |  ((a,b),item) <- zip  [((x,y),(x+stepWidthTransLHS,y)) | let y=0, x <-[0,stepWidthTransLHS..maxX]] transLHS])
    ++  -- exist. normal order rules
    (concatMap (\x -> x++ ";\n") $ 
        [showItemLaTeX (showYU a) (showYD b) "right" "dotted" item 
            |  ((a,b),item) <- zip  [((x,((-1)*y)),(x,(-1)*(y+stepWidthNoRHS))) | let x=maxX, y <-[0,stepWidthNoRHS..maxY]] (reverse noRHS)])
    ++  -- exists. transformations
    (concatMap (\x -> x++ ";\n") $ 
        [showItemLaTeX (showXL a) (showXR b) "below" "dotted" item 
            |  ((a,b),item) <- zip  [((x,y),(x+stepWidthTransRHS,y)) | let y=(-1)*maxY, x <-[0,stepWidthTransRHS..maxX]] transRHS])
    ++                                               
    "\\end{tikzpicture}"                                                                                                               
   else if isId then
       "%" ++ show (lhs :~~> rhs)
    ++  
    "%\n\\begin{tikzpicture}[scale=1.3,baseline=-5ex]\n"
    ++  -- given normal order rules
    (concatMap (\x -> x++ ";\n") $ 
        [showItemLaTeX (showYU a) (showYD b) "left" "solid" item 
            |  ((a,b),item) <- zip  [((x,y),(x,(-1)*(y+stepWidthNoLHS))) | let x=0, y <-[0,stepWidthNoLHS..maxY]] noLHS])
    ++  -- given transformations
    (concatMap (\x -> x++ ";\n") $ 
        [showItemLaTeX (showXL a) (showXR b) "above" "solid" item 
            |  ((a,b),item) <- zip  [((x,y),(x+stepWidthTransLHS,y)) | let y=0, x <-[0,stepWidthTransLHS..maxX]] transLHS])
    ++  -- identity
    "\\draw[-, double," ++ "dotted" ++ "] " ++ (showXYUR (maxX,0))++ " to node[" ++ "right" ++ "] {" ++ "id" ++ "} " ++ (showXYDL (0,(-1)*(maxY))) ++ ";"
    ++                                               
    "\\end{tikzpicture}"                                                                                                               
   else   -- 
      "%" ++ show (lhs :~~> rhs)
    ++  
    "%\n\\begin{tikzpicture}[scale=1.3,baseline=-5ex]\n"
    ++  -- given normal order rules
    (concatMap (\x -> x++ ";\n") $ 
        [showItemLaTeX (showYU a) (showYD b) "left" "solid" item 
            |  ((a,b),item) <- zip  [((x,y),(x,(-1)*(y+triangleStepWidthNoLHS))) | let x=0, y <-[0,triangleStepWidthNoLHS..maxY]] noLHS])
    ++  -- given transformations
    (concatMap (\x -> x++ ";\n") $ 
        [showItemLaTeX (showXL a) (showXR b) "above" "solid" item 
            |  ((a,b),item) <- zip  [((x,y),(x+triangleStepWidthTransLHS,y)) | let y=0, x <-[0,triangleStepWidthTransLHS..maxX]] transLHS])
    ++  -- exist. normal order rules
    (concatMap (\x -> x++ ";\n") $ 
        [showItemLaTeXP (showXYUR a) (showXYDL b) "right" "dotted" item 
            |  ((a,b),item) <- zip  [((x,((-1)*y)),(x-stepWidthTriangle,(-1)*(y+triangleStepWidthNoRHS))) | 
                                        (x,y) <-(zip [triangleMax,triangleMax-stepWidthTriangle..0] [0,triangleStepWidthNoRHS..maxY])] ((reverse transRHS)++(reverse noRHS))])
      -- exists. transformations
--     (concatMap (\x -> x++ ";\n") $ 
--         [showItemLaTeX (showXL a) (showXR b) "below" "dotted" item 
--             |  ((a,b),item) <- zip  [((x,y),(x+stepWidthTransRHS,y)) | let y=(-1)*maxY, x <-[0,stepWidthTransRHS..maxX]] transRHS])
    ++                                               
    "\\end{tikzpicture}"    

--     else 
--     (show  (lhs :~~> rhs))
  | (lhs :~~> rhs) <- diagrams,
    let noLHS = filter isSR lhs,
    let transLHS = filter isTrans lhs,
    let noRHS = filter isSR rhs,
    let transRHS = filter isTrans rhs,
    let maxX = max (genericLength transLHS) (genericLength transRHS),
    let maxY = max (genericLength noLHS) (genericLength noRHS),
    let stepWidthNoLHS = (maxY / (max 1.0 (genericLength noLHS))),
    let stepWidthTransLHS = (maxX / (max 1.0 (genericLength transLHS))),
    let stepWidthNoRHS = (maxY / (max 1.0 (genericLength noRHS))),
    let stepWidthTransRHS = (maxX / (max 1.0 (genericLength transRHS))),
    let isRectangle = length noLHS > 0 && length noRHS > 0 && length transLHS > 0 && length transRHS > 0,
    let isId = length noRHS == 0 && length transRHS == 0,
    let isTriangle = length noRHS /= length noLHS && length transRHS == 0,
--     let isTriangleNO = length noRHS >= 0 && length transRHS == 0,        
    let triangleMax = max (genericLength noLHS) (genericLength noRHS),        
    let stepWidthTriangle = (triangleMax / (max 1 (genericLength noRHS))),
    let triangleStepWidthNoLHS = (triangleMax / (max 1 (genericLength noLHS))),
    let triangleStepWidthTransLHS = (triangleMax / (max 1 (genericLength transLHS))),
    let triangleStepWidthNoRHS = (triangleMax / (max 1 (genericLength noRHS)))
    
  ]) ++ (unlines ["unsolvalbe critical pairs: " ++ s | (Exception s) <- diagrams])
    ++ "\n\\caption{A complete set of diagrams for " ++ (showItemL trans) ++ "}\n" 
    ++ "\\end{figure}"
isTrans (Trans _) = True
isTrans _ = False

isSR (SR _) = True
isSR _ = False

showItemLaTeX start end lpos style (SR (Left (Var a)))  =  "\\draw[-latex,line width=.5pt," ++ style ++ "] " ++ start ++ " to node[" ++ lpos ++ "] {" ++ mathL ("no," ++ a ) ++ "} " ++ end
showItemLaTeX start end lpos style (SR (Right (Var a)))  =  "\\draw[-latex,line width=.5pt," ++ style ++ "] " ++ start ++ " to node[" ++ lpos ++ "] {" ++ mathL ("no," ++ a ) ++ "} " ++ end
showItemLaTeX start end lpos style (Trans (Left (Var a)))  =  "\\draw[-latex,line width=.5pt," ++ style ++ "] " ++ start ++ " to node[" ++ lpos ++ "] {" ++ mathL ("T," ++ a ) ++ "} " ++ end
showItemLaTeX start end lpos style (Trans (Right (Var a)))  =  "\\draw[-latex,line width=.5pt," ++ style ++ "] " ++ start ++ " to node[" ++ lpos ++ "] {" ++ mathL ("T," ++ a ) ++ "} " ++ end
showItemLaTeX start end lpos style (SR (Left lab))  =  "\\draw[-latex,line width=.5pt," ++ style ++ "] " ++ start ++ " to node[" ++ lpos ++ "] {" ++ mathL (showLabelLaTeX lab) ++ "} " ++ end
showItemLaTeX start end lpos style (SR (Right lab))  = "\\draw[-latex,line width=.5pt," ++ style ++ "] " ++ start ++ " to node[" ++ lpos ++ "] {" ++ mathL (showLabelLaTeX lab) ++ "} " ++ end
showItemLaTeX start end lpos style (Trans (Left lab))  =  "\\draw[latex-,line width=.5pt," ++ style ++ "] " ++ start ++ " to node[" ++ lpos ++ "] {" ++ mathL (showLabelLaTeX lab) ++ "} " ++ end
showItemLaTeX start end lpos style (Trans (Right lab))  = "\\draw[-latex,line width=.5pt," ++ style ++ "] " ++ start ++ " to node[" ++ lpos ++ "] {" ++ mathL (showLabelLaTeX lab) ++ "} " ++ end
showItemLaTeXP start end lpos style (SR (Left (Var a)))  =  "\\draw[latex-,line width=.5pt," ++ style ++ "] " ++ start ++ " to node[" ++ lpos ++ "] {" ++ mathL ("no," ++ a ) ++ "} " ++ end
showItemLaTeXP start end lpos style (SR (Right (Var a)))  =  "\\draw[latex-,line width=.5pt," ++ style ++ "] " ++ start ++ " to node[" ++ lpos ++ "] {" ++ mathL ("no," ++ a ) ++ "} " ++ end
showItemLaTeXP start end lpos style (Trans (Left (Var a)))  =  "\\draw[-latex,line width=.5pt," ++ style ++ "] " ++ start ++ " to node[" ++ lpos ++ "] {" ++ mathL ("T," ++ a ) ++ "} " ++ end
showItemLaTeXP start end lpos style (Trans (Right (Var a)))  =  "\\draw[-latex,line width=.5pt," ++ style ++ "] " ++ start ++ " to node[" ++ lpos ++ "] {" ++ mathL ("T," ++ a ) ++ "} " ++ end
showItemLaTeXP start end lpos style (SR (Left lab))  =  "\\draw[-latex,line width=.5pt," ++ style ++ "] " ++ start ++ " to node[" ++ lpos ++ "] {" ++ mathL (showLabelLaTeX lab) ++ "} " ++ end
showItemLaTeXP start end lpos style (SR (Right lab))  = "\\draw[-latex,line width=.5pt," ++ style ++ "] " ++ start ++ " to node[" ++ lpos ++ "] {" ++ mathL (showLabelLaTeX lab) ++ "} " ++ end
showItemLaTeXP start end lpos style (Trans (Left lab))  =  "\\draw[-latex,line width=.5pt," ++ style ++ "] " ++ start ++ " to node[" ++ lpos ++ "] {" ++ mathL (showLabelLaTeX lab) ++ "} " ++ end
showItemLaTeXP start end lpos style (Trans (Right lab))  = "\\draw[latex-,line width=.5pt," ++ style ++ "] " ++ start ++ " to node[" ++ lpos ++ "] {" ++ mathL (showLabelLaTeX lab) ++ "} " ++ end
showLabelLaTeX (Rule n) = clearForLaTeX n 
showLabelLaTeX (Tau)    = "\tau"
showLabelLaTeX (Plus l) = showLabel l ++ ",+"
showLabelLaTeX (Star l) = showLabel l ++ ",*"
mathL txt = "\\ensuremath{" ++ txt ++ "}"
showItemL (SR (Left (Var a)))  =  "$\\xrightarrow{no," ++ a ++ "}$"
showItemL (SR (Right (Var a)))  =  "$\\xrightarrow{no," ++ a ++ "}$"
showItemL (Trans (Left (Var a)))  =  "$\\xrightarrow{T," ++ a ++ "}$"
showItemL (Trans (Right (Var a)))  =  "$\\xleftarrow{T," ++ a ++ "}$"
showItemL (SR (Left lab))  =  "$\\xleftarrow{no," ++ showLabelLaTeX lab ++ "}$"
showItemL (SR (Right lab))  =  "$\\xrightarrow{no," ++ showLabelLaTeX lab ++ "}$"
showItemL (Trans (Left lab))  =  "$\\xleftarrow{" ++ showLabelLaTeX lab ++ "}$"
showItemL (Trans (Right lab))  =  "$\\xrightarrow{" ++ showLabelLaTeX lab ++ "}$"



clearForLaTeX [] = []
clearForLaTeX n@(x:xs) 
 | "plus" `isInfixOf` n = clearForLaTeX (removeSequence "plus" n)
 | "-Chain" `isSuffixOf` n = clearForLaTeX (reverse (drop 6 (reverse n)))
 | "rule_" `isPrefixOf` n = clearForLaTeX (drop 5 n)
 | "NO_" `isPrefixOf` n = ("no," ++ clearForLaTeX (drop 2 n))
 | "T_"  `isPrefixOf` n = ("T," ++ clearForLaTeX (drop 1 n))
clearForLaTeX n@('_':xs)  = clearForLaTeX xs
clearForLaTeX n@(x:xs)  = (x:clearForLaTeX xs) 
 
removeSequence ys zs@(x:xs)
  | ys `isPrefixOf` zs = drop (length ys) zs
  | otherwise = (x:removeSequence ys xs)
 
 -}
-- -------------- DEBUG ---------------------------------------------------------------------
-- only for Debug
{-
getOneStepSuccessorsDebug pb expr delta rules fresh =
  let inputContextContraints      = delta1 delta
      inputEnvironmentConstraints = delta2 delta
      inputCaptureConstraints     = delta3 delta
      (freshRules,fresh')         = makeAllFresh  rules fresh
      solutions       = ((concat 
                        [let 
                             solsOneRule = --map fromSolved 
                                            --(solvedAndOpenLeaves 
                                              (runMatching 
                                                (functions pb) 
                                                fresh' 
                                                ([leftRule :=?=: expr],contextConstraints,environmentConstraints,captureConstr)
                                                
                                               )
                                             --)
                             
                         in  [(rulename,solsOneRule)
                         --
                         --      trace (
                         --       (show rightRule) ++ "\n" ++ 
                         --       (show (applySolutionWeak sigma rightRule))++
                         --       (intercalate "  \n" (map show sigma))
                         --       )
--                              ((applySolutionWeak sigma rightRule), rulename,pb) | (unifExpr,sigma,problem) <- (solsOneRule)
                              
                              ]
                            | crule <- freshRules
                            , let rulename = ruleName crule
                            , let leftRule :==>: rightRule = rule crule 
                            , let contextConstraintsRule = nonEmptyCtxts crule
                            , let environmentConstraintsRule = nonEmptyEnvs crule
                            , let capcons = nonCapCon crule
                            , let contextConstraints     = contextConstraintsRule ++ inputContextContraints
                            , let environmentConstraints = environmentConstraintsRule ++ inputEnvironmentConstraints
                            , let contextConstraints     = contextConstraintsRule ++ inputContextContraints
                            , let environmentConstraints = environmentConstraintsRule ++ inputEnvironmentConstraints
                            , let capConLeft = computeConstraints leftRule 
                            , let capConRight = computeConstraints rightRule                                  
                            , let captureConstr = capcons ++ inputCaptureConstraints  ++ capConLeft -- todo, add further constraints?
                            , rulename == "rule_NO_llet-e2"
                                  
                        ]))
                        
   in solutions
   -}   
-- -------------- DEBUG ---------------------------------------------------------------------


-- UNUSED:

-- closeOverlap flag bound stdRules transRules (overlap@((w1,rule1,l1 :==>: r1),(w2,rule2,l2 :==>: r2),delta,sol,rpair,pb)) =
--  closeOverlapNew flag bound stdRules transRules (overlap)

   {-
   
   the given overlap is
   
   .-----T-----> r1
   |
   |
   no
   |
   |
   V
   r2
   
   Note that in the commuting case T is already a reverse transformation, but this does not
   make a difference.
   
   Strategies for joining the overlap
   ==================================
   * compute all -no-> successors of r1   r1 = r1_0 -no-> r1_1 -no-> r1_2 -no-> r1_3 -no-> ... 
     compute all -T->  successors of r2   r2 = r2_0 -T->  r2_1 -T--> r2_2 -T->  r2_3 -T-> ...
     check whether there exists r1_i and r2_j s.t. r1_i and r2_j are equal (or alpha-equivalent)
   * compute all -no-> successors of r1   r1 = r1_0 -no-> r1_1 -no-> r1_2 -no-> r1_3 -no-> ... 
     for each r1_i  compute its successors   r1_i <-T- r1_i_1 <-T- r1_i_2 <-T- ...
     check whether there exists r1_i_j  s.t. r1_i_j equals r_2 (or is alpha-equivalent)
   * compute all <-T- successors or r2    r2 = r2_0 <-T- r2_1 <-T- r2_2 <-T- ...
     for each r2_i compute its sucessesor w.r.t. <-no-:  r2_j <-no- r2_j_1 <-no- r2_j_2 <-no- ...   
     check whether there exists r2_j_i s.t. r2_j_i equals r1 (or is alpha-equivalent)
     
   All strategies may be helpful to apply the transformation T in a forward manner (w.r.t. their original formulation),
   it may also be helpful to apply normal order reductions in reversed form, e.g. when the rule introduces fresh variables,
   like e.g.
   
    letrec xs = cons r1 r2 in case xs s y.ys.t --> letrec xs = cons z zs; z=r; zs=rs in letrec y=z; ys=zs in t
    
   for this normal-order case rule, it makes sense to apply them backwards, since the variable z zs are free in the rhs 
   
   However, the proposed 3 strategies do not allow mixtures of the strategies, and thus we use the following 
   search:
    start with the triple (ALL, r1,r2) and use 
      * the successor function for (STATE,r1,r2) 
            (a) (STATE,r1',r2) if r1 -no-> r1'  if A in STATE
            (b) (STATE,r1,r2') if r2 -T->  r2'  if B in STATE
            (c) (AC,r1,r2') if r2 <-no- r2'     if C in STATE
            (d) (BD,r1',r2) if r1 <-T-  r1'     if D in STATE 
        the flags ALL,AC,BD ensure that no mixtures of no and T are generated
        in all states also a constraint-equation may be applied (is slso built in getOneStepSuccessors and mangleE, but should be removed from there)
      * Goal test: (_,r1,r2) r1 == r2 (or also alpha-equivalence)        
      
      
   getOneStepSuccessors pb expr rules fresh =
   
   -}
   

   
removeTrivNames path = 
  [el |  (_,el,_,_) <- map (ruleName . rpRule) path, el `notElem` ["Identity","alpha","alphaRepair"]]
  
  
