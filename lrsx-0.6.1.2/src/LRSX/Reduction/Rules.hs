-----------------------------------------------------------------------------
-- |
-- Module      :  LRSX.Reduction.Rules
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--  
-- Data types for reduction and transformation rules, and 
-- several functions and instances for the type
--
--
-----------------------------------------------------------------------------

module LRSX.Reduction.Rules(
     Rule(..)
    ,ExprRule(..)
    ,ConstrainedRule(..)
    ,Command(..)
    ,RuleName(..)
    ,CriticalPair(..)
    ,OverlapProblem(..)
    ,IgnoredRulesForClosing
    ,IgnoredRulesForOverlap
    ,RestrictedRulesForClosing
    ,SetOfRules
    ,weaken
    ,ruleSubsumedByRule
    ,buildUnionMap
    ,identity
    ,showRuleName
    ,makeFresh
    ,isAnswerRuleCommand
    ,expandAllContextsOverlapProblem
    ,executeCommands
    ,expandAllEnvsOverlapProblem
    ,unfoldAll
    ,getOutFile
    ,showConstrainedRule
    ,showAnswer
    ,applyCtxtUnfoldingOverlapProblem
  ) where
import Data.List
import Data.Maybe
import Control.Parallel.Strategies
import LRSX.Language.Syntax
import LRSX.Language.ContextDefinition
import LRSX.Unification.UnificationProblem
import LRSX.Unification.Unification
import LRSX.Language.Constraints
import LRSX.Language.Fresh
import LRSX.Util.Util
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set


-- ==============================================
-- Data structures
--
-- Rules
--
-- | Reduction / Transformation rules are represented by the type @Rule@, by  @lhs :==>: rhs@
data Rule a = a :==>: a
 deriving(Eq,Ord)

-- | @RuleName@ is a type synonym for a 4-tuple (/sr/,/name/,/variant/,/closure/) were
-- 
--         * /sr/ is a 'Bool' s.t. sr is 'True' iff the rule is a standard reduction, and 'False' otherwise
--         * /name/ is a 'String' which represents the name of the rule
--         * /variant/ is an 'Int' representing the i'th variant of the rule
--         * /closure/ is a 'Bool' which is 'True' iff the rule is a transitive closure of another rule, and 'False' otherwise
type RuleName = (Bool,String,Int,Bool)  -- (StandardReduction,Name,Variantnumber,TransitiveClosure?)

-- | 'ExprRule' is a 'Rule' where the lhs and rhs are of type 'Expression'
type ExprRule = Rule Expression

-- | a 'ConstrainedRule' represents a letrec rewrite rule together with constraints
data ConstrainedRule  = 
    ConstrainedRule {
        rule              :: ExprRule                -- ^ a rule where left and right hand side are expressions
       ,nonEmptyCtxts     :: [Expression]            -- ^ a list of context variables, which are non-empty
       ,nonEmptyEnvs      :: [EnvVar]                -- ^ a list of Environment-Variables which are non-empty
       ,nonCapCon         :: NonCaptureConstraints   -- ^ non-capture constraints
       ,priority          :: Int                     -- ^ a priority of the rules (for later use while closing diagrams)
       ,answerRule        :: Bool                    -- ^ a flag whether the rule is an answer rule
       ,standardReduction :: Bool                    -- ^ a flag whether the rule is a standard reduction of the program calculus
       ,ruleName          :: RuleName                -- ^ the name of the rule as a tuple 
    }
  deriving(Show)


getOutFile (Single _ _ _ _ f) = f
getOutFile (All _ _ _ _ f) = f
getOutFile (EXPAND _ _ f) = f
getOutFile _ = Nothing
-- | The 'Command' type represents overlap commands
data Command = 
        Single
         (Either RuleName RuleName) 
         (Either RuleName RuleName) 
         IgnoredRulesForClosing   
         RestrictedRulesForClosing   
         (Maybe String)
          -- ^ a single overlap of two rules, the arguments are
          --
          --  (1) the name of the first rule encapsulated in an 'Either' expression where 'Left' or 'Right' denotes the side of the first rule to be overlapped
          --  (2) the name of the sencod rule encapsulated in an 'Either' expression where 'Left' or 'Right' denotes the side of the second rule to be overlapped
          --  (3) a list of 'RuleName', containing the rule names which shall not be used for closing diagrams (to speed up search, and to find /better/ diagrams
          --  (4) a list of pairs ('RuleName','Int') which restrict the number of occurrences of the rule while closing diagrams
          --  (5) an optional file name for the output
      | All                         
         (Either RuleName RuleName) 
         IgnoredRulesForOverlap     
         IgnoredRulesForClosing     
         RestrictedRulesForClosing  
         (Maybe String)         
          -- ^ overlap a rule with all left hand sides of all standard reductions except for those rules which are included in the second argument. The arguments are
          -- 
          --  (1) the to-be-overlapped rule, either 'Left' rule or 'Right' rule which denotes whether the rule should be overlapped with the lhs or the rhs 
          --  (2) a list of 'RuleName', containing the rule names which shall not overlapped with the given rule
          --  (3) a list of 'RuleName', containing the rule names which shall not be used for closing diagrams (to speed up search, and to find /better/ diagrams
          --  (4) a list of pairs ('RuleName','Int') which restrict the number of occurrences of the rule while closing diagrams
          --  (5) an optional file name for the ouput
      | UNION 
         RuleName
         SetOfRules
          -- ^ a 'UNION' command expresses that a set of rules is joined to a new rule. The arguments are:
          -- 
          --  (1) the rule name of the new union
          --  (2) a list rule names which should be joined
      | EXPAND 
         RuleName
         SetOfRules
         (Maybe String)
         -- ^ an 'EXPAND' command expresses that a rule should be expanded into several rules. The arguments are
         --
         --  (1) the to be expanded rule name
         --  (2) the rule name where it should be expanded to
         --
         --  An example is @EXPAND [(True,"lll",0,False)]  [(True,"lapp",0,False),(True,"lseq",0,False),(True,"lcase",0,False),(True,"llet",0,False)]@
         --  Which means that (SR,lll)-reductions shall be expanded to (SR,lapp)-, (SR,lseq), (SR,lcase)-, and (SR,llet)-reductions
         --  and thus there will be abstract rewrite rules for the diagrams
         --
         --  \<-SR,lll- \~\~\> \<-SR,lapp-
         --
         --  \<-SR,lll- \~\~\> \<-SR,lcase-
         --
         --  \<-SR,lll- \~\~\> \<-SR,lseq-
         --
         --  \<-SR,lll- \~\~\> \<-SR,llet-
         --
         -- to branch into all cases
 deriving(Show)
 
-- | a type synomym for a list of rule names, represents this rules  which should not be used during the search for closing diagrams
type IgnoredRulesForClosing = [RuleName] 
-- | a type synomym for a list of rule names, represents this rules  which should not be overlapped, when using the 'All'-commands
type IgnoredRulesForOverlap = [RuleName]  
-- | a type synomym for a list of pairs of (rule name,number), represent the occurrence restrictions of the rule when using them for closing an overlap
type RestrictedRulesForClosing = [(RuleName,Int)] 
-- | a set of rule names, represented as a list
type SetOfRules = [RuleName]                                                              

-- some helper functions to get the rule of an Either String String-rulename 
getRule (Left rule) = rule
getRule (Right rule) = rule
getSide (Left  _) (l,r)  = l
getSide (Right _) (l,r) = r
getOtherSide (Left  _) (l,r)  = r
getOtherSide (Right _) (l,r) = l

-- lookup a rule from a list of rules by its rulename
lookupRule key list = lookup key [(ruleName r,r) | r <- list] 
-- rule1 is subsumbed by rule2 iff rule2 is a superset of rule1 
 
ruleSubsumedByRule mp rule1 rule2  = 
    let res1 = (weakenAllAncestors mp rule1)
        res2 = Set.member rule2 res1
    in  res2
 
weakenAllAncestors mp t  =
 case Map.lookup t mp of 
   Nothing -> Set.singleton t
   Just x -> if x == t then Set.singleton x else Set.union (Set.singleton t) (weakenAllAncestors mp x)
    
weaken mp t =
 case Map.lookup t mp of 
   Nothing -> t
   Just x -> if x == t then x else weaken mp x   
buildUnionMap us = 
  foldr (\u mpN -> buildUnionMapSingle mpN u) (Map.empty) us  
buildUnionMapSingle mp (UNION trans set) =
  foldr (\(s,trans) mpN -> Map.insert s trans mpN) mp (zip set (repeat trans))
  
   
 
-- reverse a rule by swapping its lhs and rhs
reverseRule :: Rule a -> Rule a
reverseRule (a :==>: b) = (b :==>: a)
 
instance Show a => Show (Rule a) where
 show (a :==>: b) = show a ++ " ==> " ++ show b
 
instance HasFnSymbol a => HasFnSymbol (Rule a) where
 getFnSymbols (a :==>: b) = nubFnSyms $ getFnSymbols a ++ getFnSymbols b 
 
instance ApplySolution a => ApplySolution (Rule a) where
 applySolution s (a :==>: b) = (applySolution s a) :==>: (applySolution s b) 
 -- applySolutionWeaken s (a :==>: b) = (applySolutionWeaken s a) :==>: (applySolutionWeaken s b) 

instance HasMetaVars a => HasMetaVars (Rule a) where
 computeMetaVars (a :==>: b) = addMetaVars b (computeMetaVars a)
 
instance HasMetaContexts a => HasMetaContexts (Rule a) where
 computeMetaContexts (a :==>: b) = (computeMetaContexts a) ++ (computeMetaContexts b)

 
 

showRuleName (b,str,i,False)
 | b = "SR,"++ str ++ "," ++(show i) 
 | otherwise = str ++ "," ++(show i) 
showRuleName (b,str,i,True)
 | b = "SR,"++ str ++ ",+" ++ "," ++(show i) 
 | otherwise = str ++ ",+" ++ "," ++(show i) 


identity = 
    ConstrainedRule {
        rule              = (MetaExpression (emptySubstitution) "@@ID") :==>:  (MetaExpression (emptySubstitution) "@@ID")
       ,nonEmptyCtxts     = []
       ,nonEmptyEnvs      = []
       ,nonCapCon         = []
       ,priority          = 0
       ,answerRule        = False
       ,standardReduction = False
       ,ruleName          = (False,"Identity",1,False)
    }

showConstrainedRule indent cr = 
 let (delta1,delta2,delta3) = (nonEmptyCtxts cr,nonEmptyEnvs cr,nonCapCon cr)
     showConst ([],[],[]) = ""
     showConst (delta1,delta2,delta3) = "\n" ++ (replicate indent ' ') ++ "where " ++ intercalate ", " (filter (not . null) [showDelta1 delta1,showDelta2 delta2,showDelta3 delta3])
     pfx = "{" ++ showRuleName (ruleName cr) ++ "} "
     l   = length pfx
     prefix = take (max indent l) (pfx ++ (replicate l ' '))
 in
   prefix ++ show (rule cr) ++ showConst (delta1,delta2,delta3)
  
showAnswer indent cr = 
 let (delta1,delta2,delta3) = (nonEmptyCtxts cr,nonEmptyEnvs cr,nonCapCon cr)
     showConst ([],[],[]) = ""
     showConst (delta1,delta2,delta3) = "\n" ++ (replicate indent ' ') ++ "where " ++ intercalate ", " (filter (not . null) [showDelta1 delta1,showDelta2 delta2,showDelta3 delta3])
     pfx = "{" ++ showRuleName (ruleName cr) ++ "} "
     l   = length pfx
     prefix = take (max indent l) (pfx ++ (replicate l ' '))
 in
   prefix ++ show (let (a :==>: b) = rule cr in a) ++ showConst (delta1,delta2,delta3)

  
standardReductions :: [ConstrainedRule] -> [ConstrainedRule]
standardReductions = filter  standardReduction
answerReductions :: [ConstrainedRule] -> [ConstrainedRule]
answerReductions = filter  answerRule
  
-- reverse a constrainted rule by swapping its lhs and rhs
reverseConstrainedRule :: ConstrainedRule -> ConstrainedRule  
reverseConstrainedRule crule = crule{rule= reverseRule (rule crule)}


isAnswerRuleCommand answerRules (Single leftrule rightrule _ _ _) =
 case (leftrule) of
   Left (sr,name,num,p) -> case 
                            lookupRule (True,name,num,p) answerRules of
                             Nothing -> case lookupRule (False,name,num,p) answerRules of 
                                           Nothing -> False
                                           Just _ -> True
                             Just _ -> True
   _ -> False                             
   
   
-- unfoldAll eliminates the  All commands from a list of commands by replacing it 
-- with the corresponding Single-Commands
unfoldAll :: [ConstrainedRule] -> [ConstrainedRule] -> [Command] -> [Command]
unfoldAll stdRules answerRules ((EXPAND a b f):xs) =(EXPAND a b f):(unfoldAll stdRules answerRules xs)
unfoldAll stdRules answerRules ((UNION a b):xs) =(UNION a b):(unfoldAll stdRules answerRules xs)
unfoldAll stdRules answerRules ((Single x y ignoreForClosing restricted fname):xs) =(Single x y ignoreForClosing restricted fname):(unfoldAll stdRules answerRules xs)
unfoldAll stdRules answerRules ((All x ys ignoreForClosing restricted fname):xs)   
        = [(Single x r ignoreForClosing restricted fname) | r' <- stdRules, (ruleName r') `notElem` ys, let r = Left (ruleName r') ] 
        ++ [(Single x r ignoreForClosing restricted fname) | r' <- answerRules, let r = Left (ruleName r')] ++ (unfoldAll stdRules answerRules xs)
        
        
unfoldAll stdRules _ [] = []

-- ============================================================================================================
-- Interface to compute critical pairs for rule overlaps, given as overlap commands
-- 
--
-- | An OverlapProblem represents an overlap
data OverlapProblem = 
     OverlapProblem {
          rules         :: [ConstrainedRule]       -- ^ all given rules
         ,command       :: Command                 -- ^ the overlap command
         ,freshNames    :: [String]                -- ^ fresh names
         ,funInfo       :: FunInfo                 -- ^ 'FunInfo' for the function symbols occuring in the problem
         ,criticalPairs :: [CriticalPair]          -- ^ the output of overlapping by executing the command, will fill the list of critical pairs
         ,isCommuting   :: Bool                    -- ^ determines whether the overlap is a commuting or a forking case
         ,opLogging            :: String                  -- ^ used for logging
         ,answerOverlap        :: Bool                    -- ^ overlapping for answer diagram, 
         ,fullContextGuess     :: Bool                  -- ^ True if all contexts should be guessed as empty/non-empty
         ,fullEnvironmentGuess :: Bool              -- ^ True if all contexts should be guessed as empty/non-empty
         ,restrictedRules      :: RestrictedRulesForClosing -- ^ rule restrictions for closing
         ,ruleSubsumption      :: Map.Map RuleName RuleName -- ^ a map containing the hierarchy of rules
         ,ctxtInfo             :: CtxtDef
     }
     
-- | A critical pair represent one overlap   'leftExpression' \<- 'middleExpression -\> rightExpression
data CriticalPair =
     CriticalPair {
         middleExpression :: Expression,        -- ^ the unified expression 
         leftExpression :: Expression,          -- ^ the left expression of the critical pair
         rightExpression :: Expression,         -- ^ the right expression of the critical pair
         ruleNameMiddleToLeft :: Either RuleName RuleName,  --  ^ the rule from middle to left given as Dir rulename, where Dir is 'Left'/'Right'
         ruleNameMiddleToRight :: Either RuleName RuleName, --  ^ the rule from middle to right given as Dir rulename, where Dir is 'Left'/'Right'
         unifier :: Problem,                                --  ^ the unifier, it is necessary to keep it, since it contains the Delta-Constraints
                                                            --    which are necessary for closing overlaps, i.e. the NCCs in Delta3 and the constraint equations in Delta4
         leftRuleOrig :: ConstrainedRule    , -- ^ the original but renamed and oriented rule 
         rightRuleOrig :: ConstrainedRule,    -- ^ the original but renamed and oriented rule 
         cpLogging     :: String -- ^ used for logging
         
     }

-- | executes the commands as given in the overlap problems,
--   and thus fills the critical pairs in the overlap problem
--   and may split a single overlap problem into multiple new 
--   overlap problems, depending on the command given.
--   (The function is already parallelized by 'parMap')
executeCommands :: [OverlapProblem] -> [OverlapProblem]
executeCommands overlapProblems = concat $ parMap rseq executeCommand overlapProblems

-- | execute a single overlap command:
executeCommand :: OverlapProblem -> [OverlapProblem]

-- | translate All-commands into a sequence of Single-commands 
executeCommand pb@(OverlapProblem{command = All rule exrules exrulesClosing restrictedRulesForClosing fname})  =
    executeCommands [pb{command = cmd} 
                    | cmd <- (unfoldAll (standardReductions (rules pb)) (answerReductions (rules pb)) [All rule exrules exrulesClosing restrictedRulesForClosing fname])]

-- | Single-command, first lookup the corresponding rules:
executeCommand pb@(OverlapProblem{command = Single rule1 rule2 ignoreForClosing restrictedRulesForClosing fname, freshNames = fresh, funInfo = fnSyms,ctxtInfo=ctxtinfo})
 | Just crule1  <- lookupRule (getRule rule1) (rules pb)
 , Just crule2  <- lookupRule (getRule rule2) (rules pb) = 
  let       
        -- make both rules fresh
        ([freshcrule1,freshcrule2],fresh') = makeAllFresh [crule1,crule2] fresh
        (l  :==>: r)         = rule freshcrule1
        (l' :==>: r')        = rule freshcrule2
        leftruleRenamedAndOriented = case rule1 of
                                      Left _ -> freshcrule1
                                      Right _ -> reverseConstrainedRule freshcrule1
        rightruleRenamedAndOriented = case rule2 of
                                       Left _ -> freshcrule2
                                       Right _ -> reverseConstrainedRule freshcrule2
        -- generate expression equations
        equations            = Gamma { varLiftEq = []
                                     , expressionEq = [getSide rule1 (l,r) :=?=: getSide rule2 (l',r')]
                                     , environmentEq = []
                                     , bindingEq = []
                               } 
        -- generate constraints:                               
        contextConstraints     = nonEmptyCtxts freshcrule1 ++ nonEmptyCtxts freshcrule2
        environmentConstraints = nonEmptyEnvs freshcrule1 ++ nonEmptyEnvs freshcrule2
        captureConstraints     = nonCapCon freshcrule1 ++ nonCapCon freshcrule2 ++ computeConstraints l ++ computeConstraints r ++ computeConstraints l' ++ computeConstraints r'
        -- built the unification problem
        problem                = Problem {
                                     solution       = []
                                    ,gamma          = equations
                                    ,delta          = Delta {delta1=contextConstraints,delta2=environmentConstraints,delta3=captureConstraints,delta4=emptyGamma}
                                    ,fresh          = fresh'
                                    ,functions      = fnSyms
                                    ,inputGamma     = equations
                                    ,inputDelta     = Delta {delta1=contextConstraints,delta2=environmentConstraints,delta3=captureConstraints,delta4=emptyGamma}
                                    ,nabla  = emptyDelta
                                    ,alphaBase      = []
                                    ,mpLogging = []
                                    ,expandAllEnvs = (fullEnvironmentGuess pb)
                                    ,expandAllContexts = (fullContextGuess pb)
                                    ,expandDelta4Constraints = True
                                    ,contextDefinitions = ctxtinfo
                                }
        -- unify the problem, and single out only successful unifiers
        unificationResultTree =  unification problem
        unificationResult = solvedAndOpenLeaves  unificationResultTree
        -- components of the critical pairs:
        left                 = getOtherSide rule1 (l,r)
        right                = getOtherSide rule2 (l',r')
        middle               = getSide rule1 (l,r)        
        computedCriticalPairs = map (computeCriticalPair middle left right rule1 rule2 leftruleRenamedAndOriented rightruleRenamedAndOriented) unificationResult
        rulesAfterRemoveIgnored = [r | r <- (rules pb)
                                     , let umap = (ruleSubsumption pb) 
                                     
                                     , let rx = [(ruleName r) | ignored  <- ignoreForClosing, ruleSubsumedByRule umap (ruleName r) ignored]
                                     , (null rx)
                                  ]
                                       

        isCommutingOrForking = case (rule1,rule2) of
                                 (Left _, Left _) -> False -- Forking
                                 (Right _, Left _) -> True -- Commuting
   in -- check whether the problem fulfills the LVC
      case check problem of 
        Nothing ->  [pb{rules = rulesAfterRemoveIgnored, restrictedRules = restrictedRulesForClosing, criticalPairs = computedCriticalPairs,isCommuting=isCommutingOrForking}]
        Just err -> error err
        

              
  
 
-- Otherwise, if rules are not found, stop with an error    
executeCommand pb@(OverlapProblem{command = Single rule1 rule2 ignoreForClosing restricted fname, freshNames = fresh, funInfo = fnSyms})
 | otherwise = error $ "unknown rules" ++ show rule1 ++ " " ++ show rule2 
    
    
-- a simple helper function to generate the critical pair    
computeCriticalPair middle left right ruleMtoL ruleMtoR leftOrig rightOrig (Node p _) = error $ "PANIC: found open leaf " ++ show p       
computeCriticalPair middle left right ruleMtoL ruleMtoR leftOrig rightOrig (Solved pb) = 
 computeCriticalPairFromProblem middle left right ruleMtoL ruleMtoR leftOrig rightOrig pb

computeCriticalPairFromProblem middle left right ruleMtoL ruleMtoR leftOrig rightOrig pb =     
     CriticalPair {
          middleExpression     = applySolution (solution pb) middle
         ,leftExpression        = applySolution (solution pb) left
         ,rightExpression        = applySolution (solution pb) right
         ,ruleNameMiddleToLeft  = ruleMtoL
         ,ruleNameMiddleToRight = ruleMtoR
         ,unifier = pb
         ,leftRuleOrig = leftOrig 
         ,rightRuleOrig = rightOrig
         ,cpLogging = let showEither (Left str)  = ".<=" ++ showRuleName str ++ "="
                          showEither (Right str) = "=" ++ showRuleName str ++ "=>."
                      in
                       unlines ["Overlapping:"  ++ showEither ruleMtoL ++ " with " ++ showEither ruleMtoR
                               , "Rule: " ++  show leftOrig
                               , "with"
                               , "Rule: " ++ show rightOrig
                               , "yields the critical pair triple:"
                               , "left:\t"   ++ (show $ applySolution (solution pb) left)
                               , "middle:\t" ++ (show $ applySolution (solution pb) middle)
                               , "right:\t"  ++ (show $ applySolution (solution pb) right)
                               , "and delta:" ++ (show (nabla pb))
                              ]
     }
     
expandAllContextsCriticalPair  cp =
    let oldPB = unifier cp
        unificationResultTree = unification oldPB{expandAllContexts=True}
        unificationResult = solvedAndOpenLeaves  unificationResultTree
        -- components of the critical pairs:
        computedCriticalPairs = map (computeCriticalPair
                                        (middleExpression cp) 
                                        (leftExpression cp) 
                                        (rightExpression cp) 
                                        (ruleNameMiddleToLeft cp) 
                                        (ruleNameMiddleToRight cp) 
                                        (leftRuleOrig cp) 
                                        (rightRuleOrig cp))
                                    unificationResult 
                              
    in computedCriticalPairs

    
expandAllEnvsOverlapProblem op =
    let [cp] = criticalPairs op
        newCps = expandAllEnvsCriticalPair cp
        flattened  = [op{criticalPairs = [cp]}  | cp <- newCps]      
    in flattened        
    
expandAllEnvsCriticalPair  cp =
    let oldPB = unifier cp
        unificationResultTree = unification oldPB{expandAllEnvs=True}
        unificationResult = solvedAndOpenLeaves  unificationResultTree
        -- components of the critical pairs:
        computedCriticalPairs = map (computeCriticalPair
                                        (middleExpression cp) 
                                        (leftExpression cp) 
                                        (rightExpression cp) 
                                        (ruleNameMiddleToLeft cp) 
                                        (ruleNameMiddleToRight cp) 
                                        (leftRuleOrig cp) 
                                        (rightRuleOrig cp))
                                    unificationResult 
                              
    in computedCriticalPairs

    
expandAllContextsOverlapProblem op =
    let [cp] = criticalPairs op
        newCps = expandAllContextsCriticalPair cp
        flattened  = [op{criticalPairs = [cp]}  | cp <- newCps]      
    in flattened        

     
-- 
-- given a list of constrained rules and a list of fresh names,
-- compute renamed rules, and the remaining fresh names
makeAllFresh :: [ConstrainedRule] -> [String] -> ([ConstrainedRule],[String])
makeAllFresh [] fresh = ([],fresh)
makeAllFresh (r:rs) fresh =
 let (r',fresh') = makeFresh r fresh
     (rs',fresh'') = makeAllFresh rs fresh'
 in (r':rs',fresh'')
 
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
-- =====================================================================================================================
-- =====================================================================================================================
-- =====================================================================================================================
-- =====================================================================================================================
-- =====================================================================================================================


       
       
applySol _ ((Node p _)) = error $ "PANIC: found open leaf " ++ show p       
-- for DEBUG only
applySol ((w1,n1,l1 :==>: r1), (w2,n2,l2 :==>: r2)) (Solved pb)
 | uexpr <- applySolution (solution pb) (inputGamma pb)
 , sigma1 <- solution pb
 , constraints <- delta pb
 , (any (\(x :=?=: y) -> x /= y) (expressionEq uexpr)) && nullGamma (delta4 constraints) = error "PANIC1a(applySol)"
 
applySol z@((w1,n1,l1 :==>: r1), (w2,n2,l2 :==>: r2)) xx@(Solved pb) 
 | uexpr <- applySolution (solution pb) (inputGamma pb)
 , sigma1 <- solution pb
 , constraints <- delta pb
 , ((applySolution sigma1 l1) /= (applySolution sigma1 l2)) && nullGamma (delta4 constraints) = 
 error $ "PANIC2(applySol)" ++  show z ++ "\n" ++ show xx
 ++ "\n" ++ show (applySolution sigma1 l1) ++ "\n" ++  show (applySolution sigma1 l2)
-- end of DEBUG 
applySol ((w1,n1,l1 :==>: r1), (w2,n2,l2 :==>: r2)) (Solved pb)
 | uexpr <- applySolution (solution pb) (inputGamma pb)
 , sigma1 <- solution pb
 , constraints <- delta pb
 , finalproblem <- pb 
 =
  let  sigma = sigma1 
       l1'   = applySolution sigma1 l1
       l2'   = applySolution sigma1 l2
       r1'   = applySolution sigma1 r1
       r2'   = applySolution sigma1 r2
       res   = ((w1,n1,l1' :==>: r1'), (w2,n2,l2' :==>: r2'), constraints,sigma1,(l1 :==>: r1,l2 :==>: r2),finalproblem)
  in 
--   DEBUG:  
--    case test of
--     Nothing -> 
     -- does not really work since lbeta can destroy the LVC, for instance:
     --
     --
    -- DVCFail r2: 
    -- (letrec {E21;X12=A22[A18[(letrec {X1=S16} in S7)]];X3=(\X1.S7)} in A19[(vlet X12)])
    -- Details:
    -- ((False,"rule_T_cp-e",
    -- (letrec {E21;X12=A22[A18[(app (\X1.S7) S16)]];X3=(\X1.S7)} in A19[(vlet X12)]) 
    -- ==> (letrec {E21;X12=A22[A18[(app (vlet X3) S16)]];X3=(\X1.S7)} in A19[(vlet X12)])),
    -- (True,"rule_NO_lbeta3",
    -- (letrec {E21;X12=A22[A18[(app (\X1.S7) S16)]];X3=(\X1.S7)} in A19[(vlet X12)])
    -- ==> (letrec {E21;X12=A22[A18[(letrec {X1=S16} in S7)]];X3=(\X1.S7)} in A19[(vlet X12)])),
    --
    --
    --     
     
     -- if  l1' /= l2'  && null (delta4 constraints) then error "PANIC4"
      -- else if null (delta4 constraints) then 
           -- if not $ checkLVC l1' then error  ("DVCFail l1: " ++ show l1' ++"\n Details:\n" ++ show res ++ "\nsigma\n" ++ show sigma) else
           -- if not $ checkLVC l2' then trace  ("DVCFail l2: " ++ show l2' ++"\n Details:\n" ++ show res ++ "\nsigma\n" ++ show sigma) else
           -- if not $ checkLVC r1' then Nothing else
           -- if not $ checkLVC r2' then Nothing else
           --                           Just res 
           -- else
--   END OF DEBUG    
         Just res
--     Just fail -> error $ "DOES THIS HAPPEN??? CAPCONFail" ++ show fail
-- --  where weaken sol = concat [  wk s    |  s <- sol]
-- --        wk     m@(MapVarLift ((MetaVariableLift x) :|-> r)) = [m, (MapVarLet ((MetaVariableLet x) :|-> wk1 r))]
-- --        wk     m@(MapVarLet ((MetaVariableLet x) :|-> r)) = [m, (MapVarLam ((MetaVarLift x) :|-> wk2 r))]
-- --        wk     r                      = [r]
-- --        wk1 (MetaVarLift x) = MetaVariableLet x
-- --        wk1 (VariableLam x)     = VariableLet x
-- --        wk2 (MetaVariableLet x) = MetaVarLift x
-- --        wk2 (VariableLet x)     = VariableLam x


       -- let (rules',fresh') = makeAllFresh rules fresh 
     -- problems        = map (\x -> execCommand x  rules') commands
     

-- =====================================================================================================================
-- =====================================================================================================================
-- =====================================================================================================================
 

makeFresh :: ConstrainedRule -> [String] -> (ConstrainedRule,[String]) 
-- some sanity checks
makeFresh crule fresh 
  | (l :==>: _) <- rule crule
  , Just err <- checkLVC l   = error $ "makeFresh: LVC does not hold (1):" ++ show l ++ err
  | (r :==>: _) <- rule crule
  , Just err <- checkLVC r   = error $ "makeFresh: LVC does not hold (2):" ++ show r ++ err  
makeFresh ConstrainedRule{ruleName=name,rule=theRule,nonEmptyCtxts=ctxts,nonEmptyEnvs=envs,nonCapCon=capcon,priority=prio,standardReduction=st,answerRule=ar} fresh =
 let metavars          = (computeMetaVars theRule)
                            `combineMetaVars` (computeMetaVars (Delta {delta1=ctxts,delta2 =envs, delta3 =capcon,delta4=emptyGamma}))
     alllamlet         = nub (liftvars metavars)
     (mLamLetVars1,f2) = zipWithFresh (\x y -> (MapVarLift $ MetaVarLift emptySubstitution x :|-> MetaVarLift emptySubstitution ("X" ++ y))) alllamlet fresh
     (mEnvVars,f3)     = zipWithFresh (\x y -> MapEnv $  x :|-> metaEnv (Env emptySubstitution ("E" ++ y))) (map (Env emptySubstitution) $ nub $ envvars metavars) f2
     mEnvVarsShrink    = zip (map (Env emptySubstitution) $ nub $ envvars metavars) (map (\x -> (Env emptySubstitution ("E" ++ x))) f2)
     (mExprVars,f4)    = zipWithFresh (\x y -> MapExpr $ MetaExpression emptySubstitution x :|-> MetaExpression emptySubstitution ("S" ++ y)) (nub $ exprvars metavars) f3
     (mCtxtVars,f5)    = zipWithFresh (\(MetaCtxt sub srt x) y -> MapExpr $ MetaCtxt sub srt x :|-> MetaCtxt sub srt ((ctxtToString srt) ++y)) (nub $ computeMetaContexts theRule) f4
     (mChainVars,f6)   = zipWithFresh (\x y -> (x,y)) (nub $ chainvars metavars) f5
     substitution = (mLamLetVars1 ++ mExprVars ++ mEnvVars ++ mCtxtVars)
     substituteEnvs subs []      = []
     substituteEnvs subs (z:zs)
        | Just y <- lookup z subs = y:(substituteEnvs subs zs)
        | otherwise               = z:(substituteEnvs subs zs)
 in 
    ((ConstrainedRule {ruleName=name
                      ,priority=prio
                      ,standardReduction=st
                      ,answerRule=ar
                      ,rule = subsEE mChainVars (applySolution substitution theRule)
                      ,nonEmptyCtxts = map (subsEE mChainVars) $ applySolution substitution ctxts
                      ,nonEmptyEnvs = substituteEnvs mEnvVarsShrink envs
                      ,nonCapCon = map (subsEE mChainVars) $ applySolution substitution capcon
                      }),f6)
   
  
instance SubsEE a => SubsEE (Rule a) where
 subsEE subs (a :==>: b) = (subsEE subs a) :==>: (subsEE subs b)
  
 

applyCtxtUnfoldingOverlapProblem overlapProblem = 
  let newCPs = 
         [let 
              pb     = unifier cp
              middle = middleExpression cp
              left   = leftExpression cp
              right  = rightExpression cp
              lg     = cpLogging cp
          in cp{middleExpression = applySolution (solution newPB) middle
               ,leftExpression   = applySolution (solution newPB) left 
               ,rightExpression  = applySolution (solution newPB) right
               ,cpLogging = lg ++ "applied context split"
               ,unifier = newPB}
                 | cp <- criticalPairs overlapProblem, newPB <-applyCtxtUnfolding (unifier cp)]
   in 
     case [overlapProblem{criticalPairs=[c]} | c <- newCPs] of
        [] -> [overlapProblem]
        other -> other
