-----------------------------------------------------------------------------
-- |
-- Module      :  LRSX.Reduction.Reduction      
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--  
--
-----------------------------------------------------------------------------
-- The Reduction Engine

module LRSX.Reduction.ReductionProblem(
   ReductionProblem(..)
  )
  where
import LRSX.Language.Syntax
import LRSX.Language.ContextDefinition
import LRSX.Language.Constraints
import LRSX.Unification.UnificationProblem
import LRSX.Reduction.Rules

-- ==========================================================================================
-- Data type for a reduction problem
data ReductionProblem = 
 ReductionProblem {
   rpRedex          :: Expression       -- the expression to be reduced / the result
  ,rpConstraintBase :: Delta            -- the constraints on the redex
  ,rpGlobalNCCs     :: NonCaptureConstraints      -- the constraints which can be transferred to other problems, since they involve fresh variables
  ,rpAlphaBase      :: [(SyntaxComponent,SubstItem)]         -- an alpha renaming with has been applied to redex
  ,rpRule           :: ConstrainedRule  -- the reduction rule to be applied
  ,rpFunInfo        :: FunInfo          -- info of function symbols
  ,rpFreshNames     :: [String]         -- fresh names
  ,rpLogging        :: String           -- Used for logging
  ,rpCDef           :: CtxtDef          -- Context Definitions
  ,rpAlphaOn        :: Bool             -- Flag whether alpha renaming was applied
 }
 deriving(Show)

 