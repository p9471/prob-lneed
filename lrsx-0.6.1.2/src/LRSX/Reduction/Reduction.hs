-----------------------------------------------------------------------------
-- |
-- Module      :  LRSX.Reduction.Reduction      
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--  
--
-----------------------------------------------------------------------------
-- The Reduction Engine

module LRSX.Reduction.Reduction(
   ReductionProblem(..)
  ,checkAlphaEquivalenceWithTrace
  ,checkAlphaEquivalence
  ,fastCheckAlphaEquivalence
  ,alphaRenameProblem
  ,tryReduce
  ,tryReduceWithFailure  
  ,tryReduceSetOfRules
  ,applyConstraintEquation
  ,alphaRepairProblem
  ,checkSimLetEquivalence
  )
  where

import Data.List
import Data.Maybe
import Data.Either
import Data.Char
import Data.Function
import qualified Data.Map.Strict as Map  
import qualified Data.Set as Set
-- import Debug.Trace
import LRSX.Matching.Matching hiding(trace)
import LRSX.Reduction.Rules
import LRSX.Language.Syntax
import LRSX.Language.ContextDefinition
import LRSX.Language.Constraints
import LRSX.Util.Util
import LRSX.Language.Pretty
import LRSX.Interface.Parser
import LRSX.Interface.Lexer
import LRSX.Unification.UnificationProblem
import LRSX.Matching.ConstraintInclusion hiding(trace)
import qualified LRSX.Unification.Unification as U
import LRSX.Language.Alpha       hiding(trace)
import LRSX.Language.Alpha.Rename 
import LRSX.Language.Alpha.Simplify hiding(trace) 
import LRSX.Language.Alpha.Similar  hiding(trace)
import LRSX.Language.Alpha.Equivalence hiding(trace)
import LRSX.Language.Alpha.Refresh hiding(trace)
import Control.Parallel.Strategies
import LRSX.Language.Fresh
import LRSX.Reduction.ReductionProblem
trace a b = b

-- ========================================================================
-- exported functions
-- ========================================================================
-- | checkAlphaEquivalence checks wether to reduction problems are alpha-equivalent,
--   it first searches for a common alpha-renaming of both expressions
--   and then also checks whether the constraint bases are equivalent
checkAlphaEquivalence :: [String]            -- ^ fresh names
                         -> ReductionProblem    -- ^ reduction problem 1
                         -> ReductionProblem    -- ^ reduction problem 2
                         -> (String,Bool)       -- ^ (log messages,result) delivers true, if the rpRedexes of reduction problems 1 and 2 can be made alpha equivalent
                                                                                             
-- | fastCheckAlphaEquivalence does a fast (and incomplete) check of alpha-equivalence,
--   it only searches for a common renaming, but does not check whether the
--   constraint bases are equivalent. Thus it should only be used to check alpha-in-equivalence
fastCheckAlphaEquivalence :: [String]            -- ^ fresh names
                         -> ReductionProblem    -- ^ reduction problem 1
                         -> ReductionProblem    -- ^ reduction problem 2
                         -> (String,Bool)       -- ^ (log messages,result) delivers true, if the rpRedexes of reduction problems 1 and 2 can be made alpha equivalent                                                                                            
-- | alphaRenameProblem applies a meta alpha-renaming to a reduction problem,
--   it should only be used if the problem has not be alpha-renamed before, otherwise
--   alphaRepair should be used
alphaRenameProblem :: ReductionProblem -> ReductionProblem
-- | tryReduce uses the matching algorithm to check whether the reduction or transformation given by rpRule 
--   is applicable to rpRedex and returns a list of new reduction problems
tryReduce          :: ReductionProblem ->  [ReductionProblem]
-- | tryReduceSetOfRules tries to apply the reduction or transformations to the reduction problem
--   it returns a list of new reduction problems (i.e. the results of applying a rule)
tryReduceSetOfRules :: ReductionProblem -> [ConstrainedRule] ->  [ReductionProblem]
-- | applyConstraintEquation tries to apply the constraint equations given in delta4 of the contraintBase
--   to the reduction problem. Note that this only works if the problem was not renamed before (by alphaRenameProblem),
--   since the constraint base is not renamed by alpha renaming
applyConstraintEquation :: ReductionProblem -> [ReductionProblem]
-- | alphaRepair repairs the meta alpha renaming of a problem. A typical situation is that a problem
--   was alpha-renamed by alphaRenameProblem, and then it was transformed by tryReduce or tryReduceSetOfRules
--   which may destroy the DVC property of alpha renaming. Now alpha repair repairs the alpha renaming.
alphaRepairProblem :: ReductionProblem -> ReductionProblem
-- | checkSimLetEquivalence gets to reduction problems (and a list of fresh names) and checks
--   whether the problem are equivalent w.r.t. simlet
checkSimLetEquivalence :: [String] -> ReductionProblem -> ReductionProblem -> (String,Bool)
-- ========================================================================
 
-- | shrinkNCC takes a reduction problem and simplifies the NCCs in the constraint base
--   by removing all constraints on variables which do not occur in the expression
-- do not use at the momement
-- shrinkNCC :: ReductionProblem -> ReductionProblem
-- shrinkNCC rp =
--  let 
--    expr = rpRedex rp
--    occVar = getAllVarsFromExpr expr
--    nabla_rp = rpConstraintBase rp
--    shrink = 
--                  splitToNCC $
--                   Map.fromList 
--                    [(s',dset') | (s',dset) <- [ (s,dset) | (s,dset) <- Map.assocs (splitCapCon $ delta3 $ nabla_rp), 
--                      s `Set.member` occVar  || any (\scomp -> scomp `Set.member` occVar) (getAllSCVarsFromSubstitution (getSubstFromSC s))
--                      ]  
--                    , let dset' = Set.fromList [d | d <- Set.elems dset
--                                  , d `Set.member` occVar 
--                                    || any (\dcomp -> dcomp `Set.member` occVar) (getAllSCVarsFromSubstitution (getSubstFromSC d))]  
--                    , not (Set.null dset')]
--  in rp{rpConstraintBase = nabla_rp{delta3 = shrink}          }

-- ==========================================================================================
-- Checking AlphaEquivalence finally
-- the final check for alpha equivalence:       
fastCheckAlphaEquivalence fresh rp1 rp2 
        -- do a quick check whether the expressions (without alpha renamings) are simlet 
        -- if this does not hold, they cannot be alpha equivalent
        | not ((dropAllSubsExpr $ rpRedex rp1) `similar` (dropAllSubsExpr $ rpRedex rp2)) = ("",False)
        
fastCheckAlphaEquivalence fresh rp1 rp2 
  | (rpAlphaOn rp1) && (rpAlphaOn rp2) =
    case alphaEquiv (map ("AEQC_" ++) fresh)  (rpRedex rp1) (rpRedex rp2) of
        (b,fullrenaming) -> ("",b)
  | otherwise = checkSimLetEquivalence fresh rp1 rp2
       

checkAlphaEquivalenceWithTrace fresh rp1 rp2 
--        | ((dropAllSubsExpr $ rpRedex rp1) `similar` (dropAllSubsExpr $ rpRedex rp2))
--          && (trace ("similar expressions:" ++ "\n" ++ pretty (sortSubstExpr $ rpRedex rp1) ++ "\n\n" ++ pretty (sortSubstExpr $ rpRedex rp2)) False) = undefined
        | otherwise = checkAlphaEquivalence fresh rp1 rp2
        
checkAlphaEquivalence fresh rp1 rp2 
        -- do a quick check whether the expressions (without alpha renamings) are simlet 
        -- if this does not hold, they cannot be alpha equivalent
        | not ((dropAllSubsExpr $ rpRedex rp1) `similar` (dropAllSubsExpr $ rpRedex rp2)) =  ("",False)
        
checkAlphaEquivalence fresh rp1 rp2 
 | otherwise =
    case alphaEquiv (map ("AEQC_" ++) fresh)  (rpRedex rp1) (rpRedex rp2) of
        (b,fullrenaming)
          | trace (show b) ((b == True) || (b == False)) ->
          if b && not ((dropAllSubsExpr $ rpRedex rp1) `similar` (dropAllSubsExpr $ rpRedex rp2))
           then error $ show (dropAllSubsExpr $ rpRedex rp1,dropAllSubsExpr $ rpRedex rp2)
           else  
            if b then 
             -- =================
             -- stuff for check equivalence of NCCs of both Reduction Problems
             let 
              envVariables1 = sort $                map fromMetaEnv $ filter isMetaEnv $ metaVars $ rpRedex rp1
              envVariables2 = sort $                map fromMetaEnv $ filter isMetaEnv $ metaVars $ rpRedex rp2
              ctxtVariables1 = sort $                map fromCtxtVar $ filter isCtxtVar $ metaVars $ rpRedex rp1
              ctxtVariables2 = sort $                map fromCtxtVar $ filter isCtxtVar $ metaVars $ rpRedex rp2              
              isCtxtVar (SCCtxt _) = True
              isCtxtVar _ = False
              fromCtxtVar (SCCtxt c) = c
              isMetaEnv (SCMetaEnv e) = True
              isMetaEnv (SCConstantEnv e) = True
              isMetaEnv _ = False
              fromMetaEnv (SCMetaEnv e) = e
              fromMetaEnv (SCConstantEnv e) = e
              
              d1  = [ c | c <- delta1 $  rpConstraintBase rp1 , c `elem` ctxtVariables1]
              d1'  = [ c | c <- delta1 $  rpConstraintBase rp2 , c `elem` ctxtVariables2]
              d2  = [ e | e <- delta2 $  rpConstraintBase rp1, e `elem` envVariables1]
              d2'  = [ e | e <- delta2 $  rpConstraintBase rp2, e `elem` envVariables2]
                 
              (fullrenaming1orig,fullrenaming2orig) = fullrenaming
              fullrenaming1 = fullrenaming1orig 
              
                           -- `Map.union`
                           --     Map.fromList [(old,news) | (old,news) <- Map.assocs fullrenaming2orig , isNothing $ Map.lookup old fullrenaming1orig]
              fullrenaming2 = fullrenaming2orig 
                           -- `Map.union`
                           --     Map.fromList [(old,news) | (old,news) <- Map.assocs fullrenaming1orig , isNothing  $ Map.lookup old fullrenaming2orig]
                                
              nabla_rp1 = rpConstraintBase rp1
              nabla_rp2 = rpConstraintBase rp2
              expr1 = sortSubstExpr $ rpRedex rp1
              expr2 = sortSubstExpr $ rpRedex rp2
              occVar1 = getAllVarsFromExpr expr1
              occVar2 = getAllVarsFromExpr expr2
              shrink1 = 
                 splitToNCC $
                  Map.fromList 
                   [(s',dset') | (s',dset) <- [ (s,dset) | (s,dset) <- Map.assocs (splitCapCon $ delta3 $ nabla_rp1)
                               , s `Set.member` occVar1  || any (\scomp -> scomp `Set.member` occVar1) (getAllSCVarsFromSubstitution (getSubstFromSC s))
                   ]  
                   , let dset' = Set.fromList [d | d <- Set.elems dset
                                 , d `Set.member` occVar1 
                                   || any (\dcomp -> dcomp `Set.member` occVar1) (getAllSCVarsFromSubstitution (getSubstFromSC d))]  
                   , not (Set.null dset')]
              shrink2 = 
                 splitToNCC $
                  Map.fromList 
                   [(s',dset') | (s',dset) <- [ (s,dset) | (s,dset) <- Map.assocs (splitCapCon $ delta3 $ nabla_rp2)
                                              , s `Set.member` occVar2  || any (\scomp -> scomp `Set.member` occVar2) (getAllSCVarsFromSubstitution (getSubstFromSC s))
                                              ]  
                   , let dset' = Set.fromList [d | d <- Set.elems dset
                                 , d `Set.member` occVar2 
                                   || any (\dcomp -> dcomp `Set.member` occVar2) (getAllSCVarsFromSubstitution (getSubstFromSC d))]  
                   , not (Set.null dset')]
              cc1 = (splitFlatToNCC (concatMap getNCCsEnvLVC $  getEnvironments $ rpRedex rp1)) ++ shrink1 -- (delta3 $ nabla_rp1)
              cc2 = (splitFlatToNCC (concatMap getNCCsEnvLVC $  getEnvironments $ rpRedex rp2)) ++ shrink2 -- (delta3 $ nabla_rp2)
              givenNCC1 = applySingleFullRenamingNCC fullrenaming1 $ cc1 ++ (rpGlobalNCCs rp2) ++ (delta3 $ nabla_rp1)
              givenNCC2 = applySingleFullRenamingNCC fullrenaming2 $ cc2 ++ (rpGlobalNCCs rp1) ++ (delta3 $  nabla_rp2)
              neededNCC1 = applySingleFullRenamingNCC fullrenaming1 shrink1 -- (delta3 $ nabla_rp1)
              neededNCC2 = applySingleFullRenamingNCC fullrenaming2 shrink2 -- (delta3 $ nabla_rp2)
              --renamedAlphaBase1 = applySingleFullRenamingAlphaBase fullrenaming1 (rpAlphaBase rp1)
              -- renamedAlphaBase2 = applySingleFullRenamingAlphaBase fullrenaming2 (rpAlphaBase rp2)
              -- jointAlphaBase  = renamedAlphaBase1 ++ renamedAlphaBase2
              givenDelta2_1 = [ (Env sub' e)
                              |
                                (Env sub e) <- (delta2 $ nabla_rp1)
                                ,sub' <- applySingleFullRenamingSubst fullrenaming1 sub
                              ]  
              givenDelta2_2 = [ (Env sub' e)
                              |
                                (Env sub e) <- (delta2 $ nabla_rp2)
                                ,sub' <- applySingleFullRenamingSubst fullrenaming2 sub
                              ]
              (test1,r1,_) =    nccImplies givenDelta2_1 givenNCC2  neededNCC1 -- jointAlphaBase -- renamedAlphaBase1
              (test2,r2,_) =    nccImplies givenDelta2_2 givenNCC1  neededNCC2 -- jointAlphaBase -- renamedAlphaBase2 
             in                                    -- ============================
                                                   -- DS: Okt 16: This check should be done, i.e.
                                                   -- nabla1 ==> nabla2 
                                                   -- and
                                                   -- nabla2 ==> nabla1
                                                   -- however, we first need the common alpha renaming and apply it also to
                                                   -- the constraints, at the moment the alpha-renaming is not really output of the 
                                                   -- alphaEquivalence check
                                                   --
                                                   -- problems:
                                                   --  * the renaming is not a function, but only a function is the position 
                                                   --    is included
                                                   --    however: if alpha-renaming and alpha repair is used before then it is a function
                                                   --  * how to apply alpha-renaming to NCCs (this has been avoided so far)
                                                  
                                                      if isJust r1 then 
                                                        trace ("TEST1 EX2=>EX1\n" ++ intercalate "\n\n" 
                                                        ["TESTOUTPUT",test1,
                                                         "GLOBAL1", (show $ rpGlobalNCCs rp1),
                                                         "GLOBAL2", (show $ rpGlobalNCCs rp2),
                                                         "REDEX1", show (rpRedex rp1),
                                                         "OCCVAR1", show occVar1,
                                                         "neededNCC1", (show $ splitCapCon $ neededNCC1),
                                                         "fullrenaming1", show fullrenaming1,
                                                         "REDEX2", (show (rpRedex rp2)),
                                                         "givenNCC2", (show $ splitCapCon $ givenNCC2),
                                                         "fullrenaming2", show fullrenaming2 -- ,
                                                         ]) 
                                                           ("",False)
                                                       else if isJust r2 then
                                                        trace ("TEST2 EX1=>EX2\n" ++ intercalate "\n\n" 
                                                         ["TESTOUTPUT",test2,
                                                         "GLOBAL1", (show $ rpGlobalNCCs rp1),
                                                         "GLOBAL2", (show $ rpGlobalNCCs rp2),
                                                         "REDEX2", show (rpRedex rp2),
                                                         "OCCVAR2", show occVar2,
                                                         "neededNCC2", (show $ splitCapCon $ neededNCC2),
                                                         "fullrenaming2", show fullrenaming2,
                                                         "REDEX1", (show (rpRedex rp1)),
                                                         "givenNCC1", (show $ splitCapCon $ givenNCC1),
                                                         "fullrenaming1", show fullrenaming1 -- ,
                                                         ]) 
                                                           ("",False)
                                                            else 
                                                              if d1 == d1' && d2 == d2' then ("",b) else error (show (rpRedex rp1, rpRedex rp2, b,d1,d1',d2,d2'))
            else ("",b) 
                                          
checkSimLetEquivalence fresh rp1 rp2 =
  if  (rpRedex rp1) `simlet` (rpRedex rp2) 
     then 
             -- ============================
             -- stuff for check equivalence of NCCs of both Reduction Problems
             let 
              nabla_rp1 = rpConstraintBase rp1
              nabla_rp2 = rpConstraintBase rp2
              expr1 = rpRedex rp1
              expr2 = rpRedex rp2
              occVar1 = getAllVarsFromExpr expr1
              occVar2 = getAllVarsFromExpr expr2
              shrink1 = 
                 splitToNCC $
                  Map.fromList 
                   [(s',dset') | (s',dset) <- [ (s,dset) | (s,dset) <- Map.assocs (splitCapCon $ delta3 $ nabla_rp1), 
                     s `Set.member` occVar1  || any (\scomp -> scomp `Set.member` occVar1) (getAllSCVarsFromSubstitution (getSubstFromSC s))
                     ]  
                   , let dset' = Set.fromList [d | d <- Set.elems dset
                                 , d `Set.member` occVar1 
                                   || any (\dcomp -> dcomp `Set.member` occVar1) (getAllSCVarsFromSubstitution (getSubstFromSC d))]  
                   , not (Set.null dset')]
              shrink2 = 
                 splitToNCC $
                  Map.fromList 
                   [(s',dset') | (s',dset) <- [ (s,dset) | (s,dset) <- Map.assocs (splitCapCon $ delta3 $ nabla_rp2), 
                                s `Set.member` occVar2  || any (\scomp -> scomp `Set.member` occVar2) (getAllSCVarsFromSubstitution (getSubstFromSC s))
                                ]  
                   , let dset' = Set.fromList [d | d <- Set.elems dset
                                 , d `Set.member` occVar2 
                                   || any (\dcomp -> dcomp `Set.member` occVar2) (getAllSCVarsFromSubstitution (getSubstFromSC d))]  
                   , not (Set.null dset')]
                   
              cc1 = (splitFlatToNCC (concatMap getNCCsEnvLVC $  getEnvironments $ rpRedex rp1)) ++ shrink1  -- (delta3 $ nabla_rp1)
              cc2 = (splitFlatToNCC (concatMap getNCCsEnvLVC $  getEnvironments $ rpRedex rp2)) ++ shrink2  -- (delta3 $ nabla_rp2)
              givenNCC1 = cc1 ++ (rpGlobalNCCs rp2) ++ (delta3 $ nabla_rp1)
              givenNCC2 = cc2 ++ (rpGlobalNCCs rp1) ++ (delta3 $ nabla_rp2)
              neededNCC1 = cc1 
              neededNCC2 = cc2
              renamedAlphaBase1 =  (rpAlphaBase rp1)
              renamedAlphaBase2 =  (rpAlphaBase rp2)
              jointAlphaBase  = renamedAlphaBase1 ++ renamedAlphaBase2
              givenDelta2_1 =  (delta2 $ nabla_rp1)
              givenDelta2_2 = (delta2 $ nabla_rp2)
              (test1,r1,_) =    nccImplies givenDelta2_1 givenNCC2  neededNCC1  -- renamedAlphaBase1
              (test2,r2,_) =    nccImplies givenDelta2_2 givenNCC1  neededNCC2  -- renamedAlphaBase2 
             in                                    -- ============================
                                                   -- DS: Okt 16: This check should be done, i.e.
                                                   -- nabla1 ==> nabla2 
                                                   -- and
                                                   -- nabla2 ==> nabla1
                                                   -- however, we first need the common alpha renaming and apply it also to
                                                   -- the constraints, at the moment the alpha-renaming is not really output of the 
                                                   -- alphaEquivalence check
                                                   --
                                                   -- problems:
                                                   --  * the renaming is not a function, but only a function is the position 
                                                   --    is included
                                                   --    however: if alpha-renaming and alpha repair is used before then it is a function
                                                   --  * how to apply alpha-renaming to NCCs (this has been avoided so far)
                                                  
                                                      if isJust r1 then 
--                                                         trace ("TEST1 EX2=>EX1\n" ++ intercalate "\n\n" 
--                                                         ["TESTOUTPUT",test1,
--                                                          "REDEX1", show (rpRedex rp1),
--                                                          "OCCVAR1", show occVar1,
--                                                          "neededNCC1", (show $ splitCapCon $ neededNCC1),
--                                                          "REDEX2", (show (rpRedex rp2)),
--                                                          "givenNCC2", (show $ splitCapCon $ givenNCC2),
--                                                          "alphaBase", show jointAlphaBase
--                                                          ]) 
                                                         ("",False)
                                                       else if isJust r2 then
--                                                         trace ("TEST2 EX1=>EX2\n" ++ intercalate "\n\n" 
--                                                          ["TESTOUTPUT",test2,
--                                                          "REDEX2", show (rpRedex rp2),
--                                                          "OCCVAR2", show occVar2,
--                                                          "neededNCC2", (show $ splitCapCon $ neededNCC2),
--                                                          "REDEX1", (show (rpRedex rp1)),
--                                                          "givenNCC1", (show $ splitCapCon $ givenNCC1),
--                                                          "alphaBase", show jointAlphaBase
--                                                          ]) 
                                                         ("",False)
                                                            else 
                                                              ("",True)
            else ("",False) 
                               
-- helper functions
-- zipGroups [] []  = []
-- zipGroups [] zs2 = [(k,[],g2) | (k,g2)  <- zs2]
-- zipGroups zs1 [] = [(k,g1,[]) | (k,g1)  <- zs1]
-- zipGroups zs1@((k1,g1):xs1) zs2@((k2,g2):xs2) 
--     | k1 == k2 = (k1,g1,g2):(zipGroups xs1 xs2)
--     | k1 <  k2 = (k1,g1,[]):(zipGroups xs1 zs2)
--     | k1 >  k2 = (k2,[],g2):(zipGroups zs1 xs2)
-- zipGroups zs1 zs2 = error ("IN zipGroups" ++ "\n" ++ show zs1 ++ "\n" ++ show zs2)
{-    
groupNames base = [(a,map snd g) | g@((a,b):_)  <- groupBy (on (==) fst) $ sortBy (on compare fst) [(a,extractName b) | (a,b) <- base ]]
extractName (SubstName _ name)                      = name
extractName (SubstVar _ (VarLift sub name))         = name 
extractName (SubstVar _ (MetaVarLift sub name))     = name
extractName (SubstVar _ (ConstantVarLift sub name)) = name-}

-- compute possible name - name substitutions 
--
-- getTwoNameSubsts  :: [String] -> [(t, [String], [String])] -> [([String], Map.Map String String, Map.Map String String)]

getTwoNameSubsts fresh []                                 = [(fresh,Map.empty,Map.empty)]
-- easy case: both groups consist of exactly one name:
getTwoNameSubsts (fresh) ((k,[name1],[name2]):rest) = 
    let rek = getTwoNameSubsts fresh rest
    in [ (f, Map.insert name1 f1 m1, Map.insert name2 f1 m2) | (f1:f,m1,m2) <- rek ]

-- easy cases: one group is empty
getTwoNameSubsts fresh ((k,names1,[]):rest)               =
    let 
        rek = getTwoNameSubsts fresh rest
    in [ let (new,fresh') = splitAt (length names1) f
             m1' = Map.union m1  (Map.fromList (zip names1 new))
         in (fresh',m1',m2)  
       | (f,m1,m2) <- rek 
       ]

getTwoNameSubsts fresh ((k,[],names2):rest)               =
    let 
        rek = getTwoNameSubsts fresh rest
    in [ let (new,fresh') = splitAt (length names2) f
             m2' = Map.union m2  (Map.fromList (zip names2 new))
         in (fresh',m1,m2')  
       | (f,m1,m2) <- rek 
       ]

-- difficult case: both are non-empty and contain more than one name    
getTwoNameSubsts fresh ((k,names1,names2):rest)           =
    let 
       rek         = getTwoNameSubsts fresh rest 
    in
     concat 
      [ let 
            l1 = length names1
            l2 = length names2
            (new,fresh') = splitAt (max l1 l2) f
            m1' = if l1 >= l2 then
                    [Map.union m1 (Map.fromList (zip names1 new))]
                   else
                    [Map.union m1 (Map.fromList (zip names1 x)) | x <- nub $ map (take l1) $ permutations new]
            m2' = if l1 >= l2 then
                    [Map.union m2 (Map.fromList (zip names2 x)) | x <- nub $ map (take l2) $ permutations new]
                    else
                    [Map.union m2 (Map.fromList (zip names2 new))]
        in      
                
         [(fresh',m1new,m2new) | m1new <- m1', m2new <- m2']      
        | (f,m1,m2) <- rek
      ]    
       
-- changeNamesRP mp rp =
--     rp {rpRedex     = changeNamesExpr mp (rpRedex rp)
--        ,rpAlphaBase = map (\(a,b) -> (changeNamesSyntaxComponent mp a, changeNamesSubstItem mp b)) (rpAlphaBase rp)
--        -- todo: change names somewhere else?
--        }
    
    
-- ==========================================================================================
-- perform alpha renaming of a reduction problem 
-- TODO: Alpha-Renaming of Delta4?
alphaRenameProblem rp = 
    let frsh = rpFreshNames rp
        -- perform the renaming
        (frsh',expr',renaming) = renameExpr  frsh (rpRedex rp)
        -- append the renaming to the alphaBase
        -- alpha = (rpAlphaBase rp) ++ renaming        
        -- adjust the contraintBase by computing the renamed environment and contexts and
        -- add them to Delta1, Delta2 if the non-renamed variables are in these sets:
        environmentVars        = nub $ environmentVarsInGamma (emptyGamma{expressionEq=[expr' :=?=: expr']})
        contextVars            = nub $ contextVarsInGamma (emptyGamma{expressionEq=[expr' :=?=: expr']})
        constraintBase = (rpConstraintBase rp)-- {--delta1=(updateDelta1 contextVars (delta1 $ rpConstraintBase rp))
                                              -- ,delta2=(updateDelta2 environmentVars (delta2 $ rpConstraintBase rp))}
                                              
    in 
      let res = simplifyReductionProblem $ rp{rpAlphaOn=True,rpFreshNames=frsh',rpRedex=expr',rpConstraintBase=constraintBase, rpRule=(rpRule rp){ruleName=(False,"alpha",1,False)}}
      in res
-- ==========================================================================================    


-- ============================
-- REDUCTION 
-- ============================

-- ==========================================================================================
-- try to perform the reduction of a reduction problem for a set rules
-- already ready for parallelization

tryReduceSetOfRules reductionProblem rulesSet =
 let 
   res =
    concat (
         parMap 
           rseq 
           (\r -> tryReduce reductionProblem{rpRule = r})
           rulesSet
          )
 in  -- trace ("\n\n reducing:\n\t" ++ show (rpRedex reductionProblem) ++ "\n\t" ++ show (rpConstraintBase reductionProblem)) $
--      trace (show (rpConstraintBase reductionProblem)) $
--       trace ("\n Computed Successors: Rules: " ++ (intercalate "," $ map (showRuleName . ruleName . rpRule) res)
--             ++ " found " ++ show (length res) ++ " successors\n" 
      --      ++ (intercalate "\n * " $ map (show . rpRedex) res
--             )
      --     )
      res  

      
-- ==========================================================================================
-- try to perform the reduction of a reduction problem:
-- tryReduce rp
--    | trace ("start tryReduce" ++ (showRuleName. ruleName . rpRule $ rp)) False = undefined
tryReduce rp@(ReductionProblem{rpRedex=expr,rpCDef=ctxtdefs, rpFreshNames=frsh,rpRule=crule,rpConstraintBase=cdel,rpAlphaBase=alp,rpFunInfo=finfo,rpLogging=rplog}) = 
  let  
   (freshRule,fresh') = makeFresh crule frsh
   (lhs :==>: rhs)    = rule freshRule
   igamma             = Gamma {varLiftEq     = []
                                         ,expressionEq  = [lhs :=?=: expr]
                                         ,environmentEq = []
                                         ,bindingEq     = []}
   del                = Delta {delta1=nonEmptyCtxts freshRule
                                         ,delta2=nonEmptyEnvs freshRule
                                         ,delta3=nonCapCon freshRule ++ computeConstraints lhs 
                                         ,delta4=emptyGamma
                                         }                              
   matchingProblem    = Problem
                        {solution      = []
                        ,gamma         = igamma
                        ,delta         = del
                        ,nabla = cdel
                        ,functions     = finfo
                        ,fresh         = fresh'
                        ,inputGamma    = igamma
                        ,inputDelta    = del
                        ,alphaBase     = alp
                        ,mpLogging     = []
                        ,expandAllEnvs = True
                        ,expandAllContexts = True -- irrelevant for matching at the moment
                        ,expandDelta4Constraints=True
                        ,contextDefinitions=ctxtdefs
                        }
   tableau            = matching matchingProblem
   solutions          = map fromSolved $ solvedAndOpenLeaves tableau
   failures           = failLeaves   $ tableau
   results            = 
                          [
                            let n =
                                 rp{ rpRule=freshRule
                                   , rpRedex=(constantify $ applySolution (solution sol) rhs)
                                   , rpConstraintBase=cdel{ delta1=(delta1 cdel) ++ (constantify $ delta1 (delta sol))
                                                          , delta2=(delta2 cdel) ++ (delta2 (delta sol))
                                                          , delta3=fastLazyNub ((delta3 cdel) ++ (constantify $ applySolution (solution sol) (delta3 (delta sol))))}
                                   , rpGlobalNCCs =  let   unfoldedDelta3 = unfoldSplittedNCCs (splitCapCon (delta3 (delta sol)))
                                                           go ((s,d):xs)
                                                             | isFreshSym s || isFreshSym d
                                                            , s /= d = (s,d):(go xs)
                                                           go (_:xs) = go xs
                                                           go [] = []
                                                           res = constantify $ splitToNCC $ reFoldUnfoldedNCCs (go unfoldedDelta3)
                                                     in ((rpGlobalNCCs rp) ++ res)
                                                     
                                   , rpFreshNames=(fresh sol)
                                   , rpLogging = rplog ++ (mpLogging sol) ++
                                                    (unlines ["REDUCTIONLOG: START"
                                                            , "REDUCTIONLOG: successfully applied reduction " ++ showRuleName (ruleName crule) ++ "  to expression "
                                                            , "REDUCTIONLOG: " ++ show expr
                                                            , "REDUCTIONLOG: resulting in"
                                                            , "REDUCTIONLOG: " ++ show (constantify $ applySolution (solution sol) rhs)
                                                    ])
                                    } 
                            in n
                                    | sol <- solutions
                                ]
   noDuplicates = fastLazyNubOn rpRedex results
   -- noDuplicates = nubBy (\x y -> snd $ checkAlphaEquivalence ["_AEQCHECK" ++ show i | i <- [1..]] x y) results
  in  -- trace ("TRY:" ++((showRuleName . ruleName . rpRule $ rp) ++ ":" ++  (show $ length noDuplicates))) $
 -- (trace ((show $ length results) ++ " > " ++ (show $ length noDuplicates))) $
--        if ("lbeta" `isInfixOf` (showRuleName. ruleName . rpRule $ rp)) && null (noDuplicates) then 
--          trace ("\nREDEX:\t" ++ show (rpRedex rp) ++ "\n" ++ (unlines $ map  show $ allLeaves tableau)) noDuplicates
--         else 
        -- if (not . null $ noDuplicates) then trace ("success" ++ (showRuleName. ruleName . rpRule $ rp)) noDuplicates else
    -- map shrinkNCC     
     results -- noDuplicates


    
tryReduceWithFailure rp@(ReductionProblem{rpRedex=expr,rpCDef=ctxtdefs, rpFreshNames=frsh,rpRule=crule,rpConstraintBase=cdel,rpAlphaBase=alp,rpFunInfo=finfo,rpLogging=rplog}) = 
  let  
   (freshRule,fresh') = makeFresh crule frsh
   (lhs :==>: rhs)    = rule freshRule
   igamma             = Gamma {varLiftEq     = []
                                         ,expressionEq  = [lhs :=?=: expr]
                                         ,environmentEq = []
                                         ,bindingEq     = []}
   del                = Delta {delta1=nonEmptyCtxts freshRule
                                         ,delta2=nonEmptyEnvs freshRule
                                         ,delta3=nonCapCon freshRule ++ computeConstraints lhs 
                                         ,delta4=emptyGamma
                                         }                              
   matchingProblem    = Problem
                        {solution      = []
                        ,gamma         = igamma
                        ,delta         = del
                        ,nabla = cdel
                        ,functions     = finfo
                        ,fresh         = fresh'
                        ,inputGamma    = igamma
                        ,inputDelta    = del
                        ,alphaBase     = alp
                        ,mpLogging     = []
                        ,expandAllEnvs = True
                        ,expandAllContexts = True -- irrelevant for matching at the moment
                        ,expandDelta4Constraints=True
                        ,contextDefinitions=ctxtdefs
                        }
   tableau            = matching matchingProblem
   solutions          = map fromSolved $ solvedAndOpenLeaves tableau
   failures           = unlines ["\n==================\nBranch" ++ show num ++  "\n------------------\n" ++ lg |  (lg,num) <- zip (failLeaves   $ tableau) [1..]]
   results            = 
                          [
                            let n =
                                 rp{ rpRule=freshRule
                                   , rpRedex=(constantify $ applySolution (solution sol) rhs)
                                   , rpConstraintBase=cdel{ delta1=(delta1 cdel) ++ (constantify $ delta1 (delta sol))
                                                          , delta2=(delta2 cdel) ++ (delta2 (delta sol))
                                                          , delta3=fastLazyNub ((delta3 cdel) ++ (constantify $ applySolution (solution sol) (delta3 (delta sol))))}
                                   , rpGlobalNCCs =  let   unfoldedDelta3 = unfoldSplittedNCCs (splitCapCon (delta3 (delta sol)))
                                                           go ((s,d):xs)
                                                             | isFreshSym s || isFreshSym d
                                                            , s /= d = (s,d):(go xs)
                                                           go (_:xs) = go xs
                                                           go [] = []
                                                           res = constantify $ splitToNCC $ reFoldUnfoldedNCCs (go unfoldedDelta3)
                                                     in ((rpGlobalNCCs rp) ++ res)
                                                     
                                   , rpFreshNames=(fresh sol)
                                   , rpLogging = rplog ++ (mpLogging sol) ++
                                                    (unlines ["REDUCTIONLOG: START"
                                                            , "REDUCTIONLOG: successfully applied reduction " ++ showRuleName (ruleName crule) ++ "  to expression "
                                                            , "REDUCTIONLOG: " ++ show expr
                                                            , "REDUCTIONLOG: resulting in"
                                                            , "REDUCTIONLOG: " ++ show (constantify $ applySolution (solution sol) rhs)
                                                    ])
                                    } 
                            in n
                                    | sol <- solutions
                                ]
   in (   results, failures)

    
    
-- ========================================          
-- apply simplification of alpha-renamings
          
simplifyReductionProblem rp = 
   let 
      nccLVC = splitFlatToNCC (concatMap getNCCsEnvLVC $   getEnvironments (rpRedex rp))
      nccsx   = splitCapCon $ delta3 $ rpConstraintBase rp
      info = (Info{nccs=nccsx,nablaSets=computeNablaSets nccsx})
      nccsSimpl = [(s',d') | (s,d) <- (delta3 $ rpConstraintBase rp), let (s',d') = case s of 
                                                                                       Left expr -> (Left $ simplifyExpr info expr, simplifyExpr info d)
                                                                                       Right env -> (Right $ simplifyEnv  info env, simplifyExpr info d)]
      nccsAll = splitCapCon ( (delta3 $ rpConstraintBase rp) ++ nccsSimpl ) 
      infoAll = Info{nccs=nccsAll,nablaSets=computeNablaSets nccsAll}
   in   rp {rpRedex = simplifyExpr infoAll (rpRedex rp), rpConstraintBase = (rpConstraintBase rp){delta3=nccsSimpl}}

-- =========================================
-- some helper functions                                     
fromSolved (Solved p) = p
fromSolved other = error ("(fromSolved): " ++ show other)

                                     
-- applyConstraintEquation tries to apply equations from the  environment constraints in Delta4             
applyConstraintEquation reductionProblem =
  let     
   solConstr1 = [(constantify l,constantify r) | (r :=?=: l) <- environmentEq $ delta4 $ rpConstraintBase reductionProblem ]
   solConstr2 = [(constantify l,constantify r) | (l :=?=: r) <- environmentEq $ delta4 $ rpConstraintBase reductionProblem ]
 
   expr1      = constantify $ foldr (\(l,r) e -> substEnvConstraint r l e) (rpRedex reductionProblem)  solConstr1
   expr2      = constantify $ foldr (\(l,r) e -> substEnvConstraint r l e) (rpRedex reductionProblem)  solConstr2
   res1       =   (if constantify (rpRedex reductionProblem) == expr1 then [] else  [reductionProblem{rpRedex=expr1,rpRule=identity{ruleName=(False,"D4ConstraintLToR",1,False)}}])
   res2       = (if constantify (rpRedex reductionProblem) == expr2 then [] else  [reductionProblem{rpRedex=expr2,rpRule=identity{ruleName=(False,"D4ConstraintRToL",1,False)}}])
  in res1 ++ res2
  

-- interface for starting the repair procedure for the alpha renaming
--
  
alphaRepairProblem rp = 
    let (f,e,renaming) = startAlphaRepair (rpFreshNames rp)  (splitCapCon $ delta3 $ rpConstraintBase rp) (rpRedex rp)  
--         oldAlphaBase = rpAlphaBase rp
--         getFirsts [] _ = []
--         getFirsts ((comp,ren):rest) have
--           | Just _ <- lookup comp have = getFirsts rest have
--           | otherwise = (comp,ren):(getFirsts rest ((comp,ren):have))
--         subs = [(comp,ren,lookupAll comp newAlphaBase)  | (comp,ren) <- oldAlphaBase']
--         splitMulti [] = [[]] 
--         splitMulti (el@(comp,ren,[]):rest)    = splitMulti rest
--         splitMulti (el@(comp,ren,[new]):rest) = [(toSubs comp ren new):recurse | recurse <- splitMulti rest] 
--         splitMulti (el@(comp,ren,new):rest)   = [(toSubs comp ren n):recurse |  recurse <- splitMulti rest,n <- new] 
--         allSubs = splitMulti subs  
--         lookupAll key list = [v | (k,v) <- list, k == key]
--         cdel = (rpConstraintBase rp)
--         cdel3 = delta3 cdel
--         newcdel3 =
--            splitToNCC $ Map.fromList
--              [
--               (fst (head gs), Set.fromList (map snd gs))
--               |
--                gs <-
--                groupBy ((==) `on` fst)        
--                ( sortBy (compare `on` fst)
--                 [(applySubsSCList s t,applySubsSCList s d) 
--                     | (t,dset) <- Map.assocs (splitCapCon cdel3)
--                     , d <- Set.elems dset, s <- allSubs
--                 ]
--                 )
--               ]
--         newcdel = cdel{delta3=cdel3} ++ newcdel3} -- okt 16 cdel3 ++ 
--     in  (if  not (null ([() | (a,b) <- unfoldSplittedNCCs (splitCapCon (delta3 newcdel)), a==b]))
--          then     
--           trace ("\nAP: BASE BEFORE: " ++ show (oldAlphaBase)) $
--             trace ("\nAP: BASE AFTER: " ++ show newAlphaBase) $
--               trace ("\nAP: ConstraintBase before:" ++ show (splitCapCon $ cdel3)) $
--                trace ("\nAP: Subs:" ++ show subs) $
--                   trace ("\nAP: ConstraintBase after:" ++ show (splitCapCon $ newcdel3)) $ id
--            else id)     
    in           
                rp{rpFreshNames=f--,rpAlphaBase=newAlphaBase++oldAlphaBase
                                 ,rpRedex=e
                                 --,rpConstraintBase=newcdel
                                 ,rpRule=identity{ruleName=(False,"alphaRepair",1,False)}}
      

-- applySubSC (Left (v0,v1,v2)) (SCVar v) = SCVar (replaceTriplesVar v0 v1 v2 v)
-- applySubSC (Right (sc,n1,n2)) (SCVar v) = SCVar (replaceInVarBy sc n2 n1 v)
-- 
-- applySubSC (Left (v0,v1,v2)) (SCExpr e) = SCExpr (replaceTriples v0 v1 v2 e)
-- applySubSC (Right (sc,n1,n2)) (SCExpr e) = SCExpr (replaceInExprBy sc n2 n1 e)
-- 
-- applySubSC (Left (v0,v1,v2)) (SCCtxt e) = SCCtxt (replaceTriples v0 v1 v2 e)
-- applySubSC (Right (sc,n1,n2)) (SCCtxt e) = SCCtxt (replaceInExprBy sc n2 n1 e)
-- 
-- applySubSC (Left (v0,v1,v2)) (SCMetaEnv e) = 
--  let env=empty{metaEnvVars=[e]}
--      env'=replaceTriplesEnv v0 v1 v2 env
--      [e'] = metaEnvVars env'
--  in (SCMetaEnv e')
-- applySubSC (Right (sc,n1,n2)) (SCMetaEnv e) = 
--  let env=empty{metaEnvVars=[e]}
--      env'=replaceInEnvBy sc n2 n1 env
--      [e'] = metaEnvVars env'
--  in (SCMetaEnv e')
-- 
-- applySubSC (Left (v0,v1,v2)) (SCConstantEnv e) = 
--  let env=empty{constantEnvVars=[e]}
--      env'=replaceTriplesEnv v0 v1 v2 env
--      [e'] = constantEnvVars env'
--  in (SCConstantEnv e')
--  
-- applySubSC (Right (sc,n1,n2)) (SCConstantEnv e) = 
--  let env=empty{constantEnvVars=[e]}
--      env'=replaceInEnvBy sc n2 n1 env
--      [e'] = constantEnvVars env'
--  in (SCConstantEnv e')
-- 
-- applySubSC (Left (v0,v1,v2)) (SCMetaChain sub typ name) = 
--    let env= empty{metaChains=[MetaChain 
--                             sub 
--                             typ 
--                             name 
--                             (ConstantVarLift (Substitution []) ("__internalvar_" ++ name))
--                             (Fn "__internalc" [])]}
--        env' = replaceTriplesEnv v0 v1 v2 env
--        [MetaChain sub1 typ1 name1 _ _] = metaChains env' 
--     in SCMetaChain sub1 typ1 name1
--    
-- applySubSC (Right (sc,n1,n2))  (SCMetaChain sub typ name) = 
--    let env= empty{metaChains=[MetaChain 
--                             sub 
--                             typ 
--                             name 
--                             (ConstantVarLift (Substitution []) ("__internalvar_" ++ name))
--                             (Fn "__internalc" [])]}
--        env' = replaceInEnvBy sc n2 n1 env
--        [MetaChain sub1 typ1 name1 _ _] = metaChains env' 
--     in SCMetaChain sub1 typ1 name1
--       
--       
-- applySubSC (Left (v0,v1,v2)) (SCConstantChain sub typ name) = 
--    let env= empty{constantChains=[ConstantChain 
--                             sub 
--                             typ 
--                             name 
--                             (ConstantVarLift (Substitution []) ("__internalvar_" ++ name))
--                             (Fn "__internalc" [])]}
--        env' = replaceTriplesEnv v0 v1 v2 env
--        [ConstantChain sub1 typ1 name1 _ _] = constantChains env' 
--     in SCConstantChain sub1 typ1 name1
--    
-- applySubSC (Right (sc,n1,n2))  (SCConstantChain sub typ name) = 
--    let env= empty{constantChains=[ConstantChain 
--                             sub 
--                             typ 
--                             name 
--                             (ConstantVarLift (Substitution []) ("__internalvar_" ++ name))
--                             (Fn "__internalc" [])]}
--        env' = replaceInEnvBy sc n2 n1 env
--        [ConstantChain sub1 typ1 name1 _ _] = constantChains env' 
--     in SCConstantChain sub1 typ1 name1
--       
-- applySubSC x y = error $ "in applySubSC:" ++ show (x,y)      
-- applySubsSCList [] sc = sc
-- applySubsSCList (x:xs) sc = applySubsSCList xs (applySubSC x sc)      
--       
{-
applySubLR (Left (v0,v1,v2)) e = replaceTriples v0 v1 v2 e
applySubLR (Right (sc,n1,n2)) e = replaceInExprBy sc n2 n1 e
applySubLRList (x:xs) e 
 | trace (show ("RP:",x,e)) False = undefined
applySubLRList (x:xs) e = applySubLRList xs (applySubLR x e)      
applySubLRList [] e = e 
applySubLREnv (Left (v0,v1,v2)) e = replaceTriplesEnv v0 v1 v2 e
applySubLREnv (Right (sc,n1,n2)) e = replaceInEnvBy sc n2 n1  e
applySubLRListEnv (x:xs) e 
 | trace (show ("RP:",x,e)) False = undefined
applySubLRListEnv (x:xs) e = applySubLRListEnv xs (applySubLREnv x e)      
applySubLRListEnv [] e = e 
-}

-- toSubs sc@(SCVar v0)  (SubstVar v1 v1') (SubstVar v2 v2')
--    | v0 == v1 && v0 == v2 = Left (v0,v1',v2')
   
-- toSubs sc@(SCVar (VarLift sub v))  (SubstName (None comp1) name1) (SubstName (None comp2) name2)
--   | sc == comp1 && sc == comp2 = (sc,name1,name2)

-- toSubs sc@(SCExpr e0)  (SubstName (None comp1) name1) (SubstName (None comp2) name2)
--    | sc == comp1 && sc == comp2 = Right (sc,name1,name2)
-- toSubs sc@(SCCtxt e0)  (SubstName (None comp1) name1) (SubstName (None comp2) name2)
--    | sc == comp1 && sc == comp2 = Right (sc,name1,name2)
-- toSubs sc@(SCMetaEnv e0)  (SubstName (None comp1) name1) (SubstName (None comp2) name2)
--    | sc == comp1 && sc == comp2 = Right (sc,name1,name2)
-- toSubs sc@(SCMetaChain sub t s)  (SubstName (None comp1) name1) (SubstName (None comp2) name2)
--    | sc == comp1 && sc == comp2 = Right (sc,name1,name2)
-- toSubs sc@(SCConstantEnv e0)  (SubstName (None comp1) name1) (SubstName (None comp2) name2)
--    | sc == comp1 && sc == comp2 = Right (sc,name1,name2)
-- toSubs sc@(SCConstantChain sub t s)  (SubstName (None comp1) name1) (SubstName (None comp2) name2)
--    | sc == comp1 && sc == comp2 = Right (sc,name1,name2)
-- 
-- toSubs a b c = error ("IN toSubs:" ++ show (a,b,c))   
--    
-- 
