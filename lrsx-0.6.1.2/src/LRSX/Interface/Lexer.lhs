
> -- DONE: SYNTAX CHANGE ?
> -----------------------------------------------------------------------------
> -- |
> -- Module      :  LRSX.Interface.Lexer
> -- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
> -- License     :  BSD-style 
> -- 
> -- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
> --
> --  
> -- Lexer for meta-lambda expressions with recursive bindings
> --
> -----------------------------------------------------------------------------
> 
> --
> -- todo: make importing more smart, by locating the imported files automatically
> --

> module LRSX.Interface.Lexer where
> import Data.List
> import Data.Char
> import Data.Maybe
> import LRSX.Language.Syntax
> import LRSX.Util.Util
> import System.Directory
> import System.FilePath


> -- | the type of the tokens
> type TokenType = Token SourceMark
> -- | SourceMark is a pair (line,column)
> type SourceMark = (String,Int,Int) -- (FilePath,Line,Column)

> -- the token type parametrized of a label 
> data Token label =
>    TokenNumber   Int  label         -- ^ numbers
>  | TokenKeyword  String label         -- ^ keywords
>  | TokenSymbol   String label         -- ^ symbols
>  | TokenVar      String label         -- ^ variables (beginning with a lower-case letter)
>  | TokenMetaVar  String label         -- ^ meta-variables (beginning with an upper-case letter)
>  | TokenString   String label
>  | TokenUnknown  Char label           -- ^ token for non-recognized symbols, error handling will be done by the parser
>  | TokenMetaVarCtxt CtxtSort String label -- ^ meta-variables for contexts (beginning with A,T,C)
>  deriving(Eq,Show)


> fromTokenVar (TokenVar s _) = s
> fromTokenMetaVar (TokenMetaVar s _) = s
> fromTokenString (TokenString s _) = s
> fromTokenNumber (TokenNumber i _) = i


> -- | show a list of tokens (for error handling)
> printMark :: [TokenType] -> String
> printMark []     = "At EOF"
> printMark (x:xs) =  let (file,r,c) = getLabel x 
>                     in "\nFile: " ++ file ++ "Line: " ++ show r ++ " " ++ "Column: " ++ show c

> -- | extract the label of token
> getLabel (TokenNumber  _ label) = label
> getLabel (TokenKeyword  _ label) = label
> getLabel (TokenSymbol   _ label) = label
> getLabel (TokenVar      _ label) = label
> getLabel (TokenMetaVar  _ label) = label
> getLabel (TokenMetaVarCtxt _ _ label) = label
> getLabel (TokenString  _ label) = label
> getLabel (TokenUnknown  _ label) = label

> setLabel label (TokenNumber num _) = TokenNumber num label
> setLabel label (TokenKeyword  kw _) =TokenKeyword kw label
> setLabel label (TokenSymbol sym _) = TokenSymbol sym label
> setLabel label (TokenVar  var _) = TokenVar var label
> setLabel label (TokenString  var _) = TokenString var label
> setLabel label (TokenMetaVar  var _) = TokenMetaVar var label
> setLabel label (TokenMetaVarCtxt s n _) = TokenMetaVarCtxt s n label
> setLabel label (TokenUnknown  u _) = TokenUnknown u label

> -- textual representation of tokens
> showToken (TokenNumber  s _) = show s
> showToken (TokenKeyword  s _) = s
> showToken (TokenSymbol   s _) = s
> showToken (TokenVar  s _)     = s
> showToken (TokenMetaVar  s _) = s
> showToken (TokenMetaVarCtxt _ s _) = s
> showToken (TokenUnknown  c _) = [c]
> showToken (TokenString string _) = string
> showToken x = error $ show x

> -- association of symbols, and keywords with their tokens
> keywordsAndToken =
>      [(x,TokenSymbol x)    | x <- ["[.]", "[.", "<-","#","^","*",".l",".r","=?=","::=", "==>","\\","/=",";","=",".","|","(",")","{","}","[","]",",","+",":","/"]]
>   ++ [(x,TokenKeyword x)   | x <- keywords ]

> -- | list of keywords
> keywords = 
>   [
>    "induct"
>   ,"unions-from"
>   ,"atp-path"
>   ,"overlap"
>   ,"and"
>   ,"all"
>   ,"letrec"
>   ,"flatrec"
>   ,"in"
>   ,"var"
>   ,"vlam"
>   ,"vlet"
>   ,"vlift"
>   ,"where"
>   ,"except"
>   ,"ignore"
>   ,"HOLE"
>   ,"SR"
>   ,"union"
>   ,"define"
>   ,"declare"
>   ,"prefix"
>   ,"fork"
>   ,"ANSWER"
>   ,"restrict"
>   ,"expand"
>   ]

> -- | list of keywords and symbols
> allkeys = map fst keywordsAndToken

> -- | get comment parses @{- -}@ comments
> getComment [] z s = error "unterminated {-"
> getComment ('-':'}':xs) z s = (xs,z,(s+2))
> getComment input@(i:xs) zeile spalte 
>   | i == '\n' = getComment xs (zeile +1) 1
>   | i == '\t' = getComment xs (zeile) (spalte+8)
> getComment (i:xs) z s = getComment xs (z) (s+1)

> lexInputCR col row input = lexer input row col
> -- | the main function of the lexer, gets a string and outputs a list of tokens
> lexInput :: String -> [TokenType]
> lexInput input = lexer "" input 1 1
> lexer file []   _ _ = []
> lexer file inp@('{':'-':is) z s = 
>  let (rest,z',s') = getComment inp z s
>  in lexer file rest z' s'
> lexer file ('-':'-':is) z s = lexer file (dropWhile (/= '\n') is) (z) 1
> lexer file input@('-':i:is) zeile spalte 
>   | isDigit i = 
>     let (num,rest) = span isDigit (i:is)
>         offset = length num
>     in (TokenNumber ((-1)*(read num)::Int) (file,zeile,spalte)):(lexer file rest zeile (spalte+offset))
> lexer file input@(i:is) zeile spalte 
>   | isDigit i = 
>     let (num,rest) = span isDigit input
>         offset = length num
>     in (TokenNumber ((read num)::Int) (file,zeile,spalte)):(lexer file rest zeile (spalte+offset))
>   | i == '\"' = 
>         let (a,bs) =  break (\x -> x `elem` ['\"','\n','\r','\t'])  is
>         in case () of 
>             _ |  null a ->  error $ "while lexing input: \" on line " ++ show zeile ++ "column " ++ show spalte ++ " is unterminated."
>               |  otherwise ->
>                   case bs of 
>                    '\n':bs' -> error $ "while lexing input: \" on line " ++ show zeile ++ "column " ++ show spalte ++ " line break found."
>                    '\r':bs' -> error $ "while lexing input: \" on line " ++ show zeile ++ "column " ++ show spalte ++ " line break found."
>                    '\t':bs' -> error $ "while lexing input: \" on line " ++ show zeile ++ "column " ++ show spalte ++ " tab found."
>                    '\"':bs' -> (TokenString a (file,zeile,spalte)):(lexer file bs' zeile (spalte + 2 + (length $ a)))
>                     
>   | isJust lk = case lk of
>                  Just (token, offset, rest) -> (let (x,y)=getLabel token in setLabel (file,x,y) token):(lexer file rest zeile (spalte+offset))
>                  Nothing -> error "impossible" 
>   | isUpper i = 
>       let (tk@(b:bs),rest) = break (not.isIdentifier) input
>           offset   = length tk
>       in case b of
>           _   -> TokenMetaVar tk (file,zeile,spalte):(lexer file rest zeile (spalte+offset))
>   | isLower i = 
>       let (tk,rest) = break (not.isIdentifier) input
>           offset   = length tk
>       in TokenVar tk (file,zeile,spalte):(lexer file rest zeile (spalte+offset))
>   | i == '\n' = lexer file is (zeile +1) 1
>   | i == '\t' = lexer file is (zeile) (spalte+8)
>   | isSpace i = lexer file is zeile (spalte + 1)
>   | otherwise = (TokenUnknown i (file,zeile,spalte)):lexer file is (zeile) (spalte +1)
>  where lk = (lookupPrefix input keywordsAndToken zeile spalte) 

> -- | checks whether a Char is an alphaNum or is '_' or is '-'
> isIdentifier x = isAlphaNum x || x `elem` "_-"

> importPrePocessor file =
>   do content <- readFile (file)
>      process content
>  where 
>  process [] = return []
>  process xs 
>   | "import" `isPrefixOf` xs =
>        let rest1 = drop 6 xs
>            rest2 = dropWhile isSpace rest1
>            (filename,rest3) = (span (not . isSpace)) rest2
>            rest4 = dropWhile isSpace rest3
>        in do
>         content <- readFile (joinPath [importdir,filename])
>         process (unlines [content,rest4])
>  process xs = do
>         let (l:rest) = lines xs
>         let rest' = unlines rest
>         ys <- process rest'
>         return (unlines[l,ys])
>  importdir = takeDirectory file

> lexWithPreProcessor file = undefined
>   

