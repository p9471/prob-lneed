-----------------------------------------------------------------------------
-- |
-- Module      :  LRSX.Interface.Pretty
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--  
-- Pretty printing of unifcation problems, overlaps and diagrams

-----------------------------------------------------------------------------
--
module LRSX.Interface.Pretty where
import LRSX.Diagrams.DiagramCalculation
import Data.List
import Data.Maybe
import Data.Either
import Data.Char
import Data.Function
import LRSX.Reduction.Rules
import LRSX.Language.Syntax
import LRSX.Language.Pretty
import LRSX.Language.ContextDefinition
import LRSX.Language.Constraints
import LRSX.Language.Alpha
import LRSX.Util.Util
import LRSX.Interface.Parser
import LRSX.Interface.Lexer
import LRSX.Unification.UnificationProblem
import LRSX.Matching.ConstraintInclusion
import LRSX.Reduction.Reduction
import qualified LRSX.Unification.Unification as UN
import qualified LRSX.Matching.Matching as MA
import qualified Data.Map.Strict as Map
import Debug.Trace
import LRSX.Language.Fresh

-- ========================================================================================================================                                    
-- pretty printing of diagrams: Closed Diagrams in short text-notation, unjoinable cases with more details
    
showDiagramWU stopOnErr umap diagram =       
    let cp = overlap diagram 
        commuting = isCommutingDiagram diagram
    in case (joins diagram) of
        (j:_) -> "\n" ++ (if commuting then "Commuting diagram: "  else "Forking diagram: ")
                      ++ ((showEither2 $ ruleNameMiddleToRight  cp) ++ " . " ++ (showEither $ ruleNameMiddleToLeft  cp)) 
                      ++ (showJoin j) 
        []   -> if stopOnErr then (error $ (diagLog diagram)) else let x = (diagLog diagram) in Debug.Trace.trace x x
        
showDiagram stopOnErr diagram =       
    let cp = overlap diagram 
        commuting = isCommutingDiagram diagram
    in case (joins diagram) of
        (j:_) -> "\n" ++ (if commuting then "Commuting diagram: "  else "Forking diagram: ")
                      ++ ((showEither2 $ ruleNameMiddleToRight  cp) ++ " . " ++ (showEither $ ruleNameMiddleToLeft  cp)) 
                      ++ (showJoin j) 
        []   -> if stopOnErr then (error $ (diagLog diagram)) else let x = (diagLog diagram) in Debug.Trace.trace x x
                                                                       
                
showJoin join =
          " ~~> "    ++ (intercalate " . " $ filter (not . null) 
                         (((map (\p -> (showRuleNameWithIdentity $ ruleName $ rpRule p)) ( reverse $ pathFromRight join)))
                         ++  (map (\p -> showRuleNameReversed $ ruleName $ rpRule p) (reverse $ pathFromLeft join)))) 
                     
showDiagramProof stopOnErr umap diagram =
    let cp = overlap diagram 
        commuting = isCommutingDiagram diagram
    in ("TRY TO JOIN:" ++ ppCriticalPairShort cp) ++ (
       case (joins diagram) of
        (j:_) -> 
         unlines 
          [
          "----------------------------------------------------------------------"
          ,showDiagramWU stopOnErr umap diagram
          ,"----------------------------------------------------------------------"
          ,"PROOF: "
          ,(ppCriticalPairShort cp)
          ,showJoinProof j
          ]           
        []   -> if stopOnErr then error $ (diagLog diagram) else let x = (diagLog diagram) in Debug.Trace.trace x x
                                                                    
     )   
   

    
showJoinProof join =
 unlines 
  ["can be joined as follows:"
  ,unlines (map (\p -> ("   " ++ fill 30 (showRuleNameWithIdentity $ ruleName (rpRule p)))  ++ (intercalate ("\n   " ++ fill 30 " ") .  lines) (pretty (rpRedex p))) (pathFromLeft join)) 
  ,"   ~_{let,alpha}"
  ,("   " ++ fill 30 " ") ++ (concatMap (\p -> ((intercalate ("\n   " ++ fill 30 " ") .  lines) (pretty (rpRedex p)) ++ "\n   " ++ (fill 30 (showRuleNameReversed $ ruleName (rpRule p))))) (pathFromRight join)) 
   ]

   
                     
-- printing of the function symbol statistic                  
showStatisticFunInfo finfo =
    unlines ["-- ========================================================="
            ,"-- Recognized function symbols in input:"
            ,"-- "
            ,"--   " ++ (fill 20 "name") ++ (fill 8 "arity")  ++ (fill 20 "binder-arity")
            ,"--   " ++ (fill 20 "----") ++ (fill 8 "-----")++  (fill 20 "-----")
            , unlines $ map (\(f,ar,bari) -> "--   " ++ (fill 20 f) ++ (fill 8 (show ar))  ++ (fill 20 (showB bari))) finfo
            ,"-- ========================================================="
            ]
  where            
    showB xs   = "[" ++ (intercalate "," (map showBB xs)) ++ "]"
    showBB (-1) = "V"
    showBB i    = show i   
    
showReduction [] = []       
showReduction (p:path) = 
    "==" ++ showRuleName (ruleName (rpRule p)) ++ "-> " ++ pretty (rpRedex p)  ++ "\n" ++ showReduction path
    
showRuleNameWithIdentity (_,"Identity",_,_) = ""
showRuleNameWithIdentity other = "-" ++ showRuleName other ++ "->"    
    
showRuleNameReversed (_,"Identity",_,_) = ""
showRuleNameReversed other = "<-" ++ showRuleName other ++ "-"    
    
    
showRuleIt (Left str) = " <-"++ showRuleName str ++ "- "
showRuleIt (Right str) = " -"++ showRuleName str ++ "-> "      
showRuleIt2 (Left str) = " -"++ showRuleName str ++ "-> "
showRuleIt2 (Right str) = " <--"++ showRuleName str ++ "- "      
showPathShort p = (intercalate " " (map showRShort $ reverse p)) ++ ""     
showPathShort2 p = (intercalate " " (map showRShort2 p)) ++ ""     

showPathLong p = (intercalate "\n " (map showRLong $ reverse p)) ++ ""     
showPathLong2 p = (intercalate "\n " (map showRLong2 p)) ++ ""     

showRLong rp = 
    let n = "\n"
            ++
            (extend 30  (if (snd4 $ ruleName $ rpRule rp) == "Identity" then "" else (" -" ++ (showRuleName $ ruleName $ rpRule rp) ++ "-> ")))
            ++ (pretty (rpRedex rp))
    in n 

showRLong2 rp = 
    let n = "\n" 
            ++ 
            (extend 30 (if (snd4 $ ruleName $ rpRule rp) == "Identity" then "" else ((" <-" ++ (showRuleName $ ruleName $ rpRule rp) ++ "- "))))
            ++ 
            (pretty (rpRedex rp))
    in n 

showR rp = 
    let n = if  (snd4 $ ruleName $ rpRule rp) == "Identity" then "" else (showRuleName $ ruleName $ rpRule rp) ++ "-> "
    in n  ++ pretty (rpRedex rp)
 
showRShort rp = 
    let n = if  (snd4 $ ruleName $ rpRule rp) == "Identity" then "" else "-" ++ (showRuleName $ ruleName $ rpRule rp) ++ "-> "
    in n 

showRShort2 rp = 
    let n = if  (snd4 $ ruleName $ rpRule rp) == "Identity" then "" else " <-" ++ (showRuleName $ ruleName $ rpRule rp) ++ "- "
    in n 
    
showEither (Right str)  = "<-" ++ showRuleName str ++ "-"       
showEither (Left str)   = "-"  ++ showRuleName str ++ "->"       
showEither2 (Right str) = "-"  ++ showRuleName str ++ "->"       
showEither2 (Left str)  = "<-" ++ showRuleName str ++ "-"       
extend i str            = take i (str ++ (repeat ' '))
    


         
-- pretty printing of computing a critical pair    
ppCriticalPair cp = 
   let finalproblem         = unifier cp
       leftExpr             = leftExpression cp
       middleExpr           = middleExpression cp             
       rightExpr            = rightExpression cp
       constrainedRuleLeft  = leftRuleOrig cp
       constrainedRuleRight = rightRuleOrig cp            
       (origl1 :==>: origr1) = rule constrainedRuleLeft
       (origl2 :==>: origr2) = rule constrainedRuleRight
       str1 =  case ruleNameMiddleToLeft cp of
                 (Left name) ->   "<-" ++ showRuleName name ++ "-"
                 (Right name) ->  "-"  ++ showRuleName name ++ "->"
       rstr1 =  case ruleNameMiddleToLeft cp of
                 (Left name) ->   "-" ++ showRuleName name ++ "->"
                 (Right name) ->  "<-"  ++ showRuleName name ++ "-"
       str2 =  case ruleNameMiddleToRight  cp of
                 (Left name) ->   "-" ++ showRuleName name ++ "->"
                 (Right name) ->  "<-"  ++ showRuleName name ++ "-"
       overlapstr = case (ruleNameMiddleToLeft cp, ruleNameMiddleToRight cp) of
                     (Left name1, Left name2) -> ("<-" ++ showRuleName name2 ++ "- . -" ++ showRuleName name1 ++ "->")
                     (Right name1, Left name2) -> ("-" ++ showRuleName name1 ++ "-> . -" ++ showRuleName name2 ++ "->")
                     
                     
       del4 = delta4 (delta finalproblem)
   in 
    unlines 
     [replicate 80 '='
     ,"Critical Pair:"
     ,""     
     ,"  ("
     ,"     " ++ (intercalate "\n     " . lines ) (pretty leftExpr)
     ,"    ,"
     ,"     " ++ (intercalate "\n     " . lines ) (pretty rightExpr)
     ,"  )"
     ,""
     ,"where"
     ,""     
     ,"   Delta1 = {" ++ (intercalate "," (map show (delta1 $ delta finalproblem)))  ++ "}"
     ,"   Delta2 = {" ++ (intercalate "," ((map show $ delta2 $ delta finalproblem)))  ++ "}"
     ,"   Delta3 = {" ++ (intercalate 
                                ",\n             " 
                                (map showNCC (delta3 $ delta finalproblem))
                          )  ++ "}"
     ,(if nullGamma (delta4 $ delta finalproblem) 
        then "   Delta4=  {}"
        else "   Delta4=  {\n" 
              ++ (intercalate 
                     ",\n"
                     [ let oneLine = "             " ++ lhs ++ " =?= " ++ rhs                        
                       in if (length $ oneLine ) <= 80 
                            then oneLine 
                            else "             " ++ lhs ++ "\n             =?=\n             " ++ rhs
                     | (lhs :=?=: rhs) 
                           <-    ([(show l :=?=: show r) | (l :=?=: r) <- environmentEq (delta4 $ delta finalproblem)]
                                  ++ [(show l :=?=: show r) | (l :=?=: r) <- expressionEq (delta4 $ delta finalproblem)]
                                  ++ [(show l :=?=: show r) | (l :=?=: r) <- bindingEq (delta4 $ delta finalproblem)]
                                  ++ [(show l :=?=: show r) | (l :=?=: r) <- varLiftEq (delta4 $ delta finalproblem)] 
                                  )]) ++ "}")
    ,""
    ,"from overlapping " ++ overlapstr ++ " with input rules:"
    ,""
    ,"    " ++ fill 30 " "  ++ show origr1
    ,"    " ++ fill 30 str1 ++ show origl1
    ,"    " ++ "and constraints: "  ++ (intercalate 
                                            ", "  
                                            (   (map (\e -> show e ++ "/= [.]") (nonEmptyCtxts constrainedRuleLeft)) 
                                             ++ (map (\e -> show e ++ "/= {}") (nonEmptyEnvs constrainedRuleLeft))
                                             ++ (map (\(t,d) -> "(" ++ 
                                                                (case t of 
                                                                    Left e -> show e
                                                                    Right e -> show e)
                                                                 ++ "," ++ show d ++ ")") (nonCapCon constrainedRuleLeft)))
                                            )
    ,"    " ++ (replicate 80 '.') 
    ,"    " ++ fill 30 " "  ++ show origl2
    ,"    " ++ fill 30 str2 ++ show origr2
    ,"    " ++ "and constraints: "  ++ (intercalate 
                                            ", "  
                                            (
                                                (map (\e -> show e ++ "/= [.]") (nonEmptyCtxts constrainedRuleRight)) 
                                             ++ (map (\e -> show e ++ "/= {}") (nonEmptyEnvs constrainedRuleRight))
                                             ++ (map (\(t,d) -> "(" ++ 
                                                                (case t of 
                                                                    Left e -> show e
                                                                    Right e -> show e)
                                                                 ++ "," ++ show d ++ ")") (nonCapCon constrainedRuleRight)))
                                            )
    ,""
    ,"and letrec unification problem:"
    ,""
    ,(unlines . map (\l -> "      " ++ l) . lines)  (showInputUnificationProblem (finalproblem))
    ,""
    ,"and found solution:"
    ,""
    ,"    Sol = {"
            ++ intercalate 
                    ",\n           " (lines $ ppSolution (solution finalproblem)) ++ "}"
    ,""
    ,"and instantiated rules:"
    ,""
    ,"    " ++ fill 30 " " ++ show (applySolution (solution finalproblem) origr1)
    ,"    " ++ fill 30 str1 ++ show (applySolution (solution finalproblem) origl1)
    ,"    " ++ (replicate 80 '.') 
    ,"    " ++ fill 30 " " ++ show (applySolution (solution finalproblem) origl2)
    ,"    " ++ fill 30 str2 ++ show (applySolution (solution finalproblem) origr2)
    ,"    " ++ (if nullGamma (del4) 
                    then ""   
                    else 
                   "    ............................\n" 
                ++ "    Delta4:\n" ++ (intercalate 
                                            ",\n"
                                            [ let oneLine = "    " ++ lhs ++ " =?= " ++ rhs                        
                                              in if (length $ oneLine ) <= 80 
                                                    then oneLine 
                                                    else "  " ++ lhs ++ "\n  =?=\n  " ++ rhs
                                            | (lhs :=?=: rhs) <- ([(show l :=?=: show r) | (l :=?=: r) <- environmentEq del4]
                                                                ++
                                                                [(show l :=?=: show r) | (l :=?=: r) <- expressionEq del4]
                                                                ++
                                                                [(show l :=?=: show r) | (l :=?=: r) <- bindingEq del4]
                                                                ++
                                                                [(show l :=?=: show r) | (l :=?=: r) <- varLiftEq del4])
                                            ]))
    ]                                            
  where
    showB xs   = "[" ++ (intercalate "," (map showBB xs)) ++ "]"
    showBB (-1) = "V"
    showBB i    = show i   

ppCriticalPairShort cp = 
   let finalproblem         = unifier cp
       leftExpr             = leftExpression cp
       middleExpr           = middleExpression cp             
       rightExpr            = rightExpression cp
       constrainedRuleLeft  = leftRuleOrig cp
       constrainedRuleRight = rightRuleOrig cp            
       (origl1 :==>: origr1) = rule constrainedRuleLeft
       (origl2 :==>: origr2) = rule constrainedRuleRight
       str1 =  case ruleNameMiddleToLeft cp of
                 (Left name) ->   "<-" ++ showRuleName name ++ "-"
                 (Right name) ->  "-"  ++ showRuleName name ++ "->"
       rstr1 =  case ruleNameMiddleToLeft cp of
                 (Left name) ->   "-" ++ showRuleName name ++ "->"
                 (Right name) ->  "<-" ++ showRuleName name ++ "-"
       str2 =  case ruleNameMiddleToRight  cp of
                 (Left name) ->   "-" ++ showRuleName name ++ "->"
                 (Right name) ->  "<-"  ++ showRuleName name ++ "-"
       overlapstr = case (ruleNameMiddleToLeft cp, ruleNameMiddleToRight cp) of
                     (Left name1, Left name2) -> ("<-" ++ showRuleName name2 ++ "- . -" ++ showRuleName name1 ++ "->")
                     (Right name1, Left name2) -> ("-" ++ showRuleName name1 ++ "-> . -" ++ showRuleName name2 ++ "->")
                     
                     
       del4 = delta4 (delta finalproblem)
   in 
    unlines 
     [replicate 80 '='
     ,"Critical Pair:"
     ,""     
     ,"  ("
     ,"     " ++ (intercalate "\n     " . lines ) (pretty leftExpr)
     ,"    ,"
     ,"     " ++ (intercalate "\n     " . lines ) (pretty rightExpr)
     ,"  )"
     ,""
     ,"where"
     ,""     
     ,"   Delta1 = {" ++ (intercalate "," (map show (delta1 $ delta finalproblem)))  ++ "}"
     ,"   Delta2 = {" ++ (intercalate "," ((map show $ delta2 $ delta finalproblem)))  ++ "}"
     ,"   Delta3 = {" ++ (intercalate 
                                ",\n             " 
                                (map showNCC (delta3 $ delta finalproblem))
                          )  ++ "}"
     ,(if nullGamma (delta4 $ delta finalproblem) 
        then "   Delta4=  {}"
        else "   Delta4=  {\n" 
              ++ (intercalate 
                     ",\n"
                     [ let oneLine = "             " ++ lhs ++ " =?= " ++ rhs                        
                       in if (length $ oneLine ) <= 80 
                            then oneLine 
                            else "             " ++ lhs ++ "\n             =?=\n             " ++ rhs
                     | (lhs :=?=: rhs) 
                           <-    ([(show l :=?=: show r) | (l :=?=: r) <- environmentEq (delta4 $ delta finalproblem)]
                                  ++ [(show l :=?=: show r) | (l :=?=: r) <- expressionEq (delta4 $ delta finalproblem)]
                                  ++ [(show l :=?=: show r) | (l :=?=: r) <- bindingEq (delta4 $ delta finalproblem)]
                                  ++ [(show l :=?=: show r) | (l :=?=: r) <- varLiftEq (delta4 $ delta finalproblem)] 
                                  )]) ++ "}")
    ]

ppSolution sol =
    let splitFine =
            [ head l | s <- sol
                , let l = [(b,c) | (b,'|','-':'>':c) <-  splits2 (show s)]
               ]
        m = maximum $ map (length . fst) splitFine
    in unlines [(fill m a) ++ " |-> " ++ b | (a,b) <- splitFine]
    
    
fill n str = let l = length str in 
                    if l <= n then take n (str ++ repeat ' ')
                        else str   
    
-- | show Non-Capture-Constraint as 'String'
showNCC :: NonCaptureConstraint -> String
showNCC (Left a,b) = "(" ++ show a ++ "," ++ show b ++ ")"     
showNCC (Right a,b) = "[" ++ show a ++ "," ++ show b ++ "]"      
    
showInputUnificationProblem :: Problem -> String
showInputUnificationProblem pb = 
 let a = (inputGamma pb)
     c = inputDelta pb
     d = pb
 in
   unlines 
        ["Gamma ={"
        , (intercalate ",\n" 
                        [   let lhs = show l
                                rhs = show r
                                oneLine = "  " ++ lhs ++ " =?= " ++ rhs                        
                            in if (length $ oneLine ) <= 80 then oneLine 
                                                            else "  " ++ show l ++ "\n  =?=\n  " ++ show r | l :=?=: r <- (expressionEq a) ]) 
                ++ "}"
        , "Delta1=  {" ++ (intercalate "," (map show (delta1 c)))  ++ "}"
        , "Delta2=  {" ++ (intercalate "," (map show (delta2 c)))  ++ "}"
        , "Delta3=  {" ++ (intercalate "," (map showNCC (delta3 c)))  ++ "}"
        ]
    
            
showUnificationResult [] = "NO SOLUTION!"
showUnificationResult res = 
 unlines 
   [ unlines 
     [
        "Solution " ++ show i ++ ":"
        , "------------------"
        , "Instantiated Expressions:"
        , (intercalate ",\n" 
                            [ let   lhs = show l
                                    rhs = show r
                                    oneLine = "  " ++ lhs ++ " =?= " ++ rhs                        
                              in if (length $ oneLine ) <= 80 then oneLine 
                                                                else "  " ++ show l ++ "\n  =?=\n  " ++ show r | l :=?=: r <- (expressionEq a) ])
        ,"Sol = {"
        ,(intercalate ",\n" ["  " ++ show m | m <- b ])  ++ "}"
        , "Delta1  = \n  {" ++ (intercalate "," (map show (delta1 c)))  ++ "}"
        , "Delta2 = \n  {" ++ (intercalate "," (map show (delta2 c)))  ++ "}"
        , "Delta3 = \n  {" ++ (intercalate "," (map showNCC (delta3 c)))  ++ "}"
        , (if nullGamma (delta4 c) 
            then "" 
            else "Delta4 = {\n" ++ (intercalate 
                                            ",\n"
                                            [ let oneLine = "  " ++ lhs ++ " =?= " ++ rhs                        
                                            in if (length $ oneLine ) <= 80 
                                                    then oneLine 
                                                    else "  " ++ lhs ++ "\n  =?=\n  " ++ rhs
                                            | (lhs :=?=: rhs) <- (  [ (show l :=?=: show r) 
                                                                    | (l :=?=: r) <- environmentEq (delta4 c)
                                                                    ]
                                                                    ++
                                                                    [(show l :=?=: show r) 
                                                                    | (l :=?=: r) <- expressionEq (delta4 c)
                                                                    ]
                                                                    ++
                                                                    [(show l :=?=: show r) 
                                                            | (l :=?=: r) <- bindingEq (delta4 c)
                                                            ]
                                                            ++
                                                            [(show l :=?=: show r) 
                                                            | (l :=?=: r) <- varLiftEq (delta4 c)
                                                            ] 
                                                         )
                                                         ])++ " }") 
            ]
        | (i,UN.Solved resPB) <- zip [1..] res
        , let a = applySolution (solution resPB) (inputGamma resPB)
        , let b = solution resPB, let c = (delta resPB) 
        ]
        ++
        (unlines [ "PANIC: There are open problems:" ++ (show p) | UN.Node p [] <- res])      
        

        
showInputMatchingProblem pb = 
 let a = (inputGamma pb)
     c = inputDelta pb
     d = pb 
 in unlines
      [ "Gamma ={"
      ,  (intercalate ",\n" 
            [ let lhs = show l
                  rhs = show r
                  oneLine = "  " ++ lhs ++ " =?= " ++ rhs                        
              in if (length $ oneLine ) <= 80 
                    then oneLine 
                    else "  " ++ show l ++ "\n  =?=\n  " ++ show r 
            | l :=?=: r <- (expressionEq a) 
            ]
        ) 
     ,"}"
     ,"Constraints on variables (needed)"
     ,"Delta1  =  {" ++ (intercalate "," (map show (delta1 c)))  ++ "}"
     ,"Delta2  =  {" ++ (intercalate "," (map show (delta2 c)))  ++ "}"
     ,"Delta3  =  {" ++ (intercalate "," (map showNCC (delta3 c)))  ++ "}"
     ,"Constraints on constants (given)"
     ,"Nabla1 =  {" ++ (intercalate "," (map show (delta1 (nabla d))))  ++ "}"
     ,"Nabla2 =  {" ++ (intercalate "," (map show (delta2 (nabla d))))  ++ "}"
     ,"Nabla3 =  {" ++ (intercalate "," (map showNCC (delta3 (nabla d))))  ++ "}"            
     ]
   
showMatchingResult [] = "NO SOLUTION!"
showMatchingResult res = 
    unlines 
     [ 
      unlines 
       [ "Solution " ++ show i ++ ":"
       , "------------------"
       , "Instantiated Expressions:" 
       , (intercalate ",\n" 
            [ let lhs = show l
                  rhs = show r
                  oneLine = "  " ++ lhs ++ " =?= " ++ rhs                        
              in if (length $ oneLine ) <= 80 
                    then oneLine
                    else "  " ++ show l ++ "\n  =?=\n  " ++ show r 
            | l :=?=: r <- (expressionEq a) 
            ]
         )
       , "Sol     = {" 
       , (intercalate ",\n" ["  " ++ show m | m <- b ])  ++ "}" 
       , "Delta1  = {" ++ (intercalate "," (map show (delta1 c)))  ++ "}"
       , "Delta2  = {" ++ (intercalate "," (map show (delta2 c)))  ++ "}"
       , "Delta3  = {" ++ (intercalate "," (map showNCC (delta3 c)))  ++ "}"
       , "Nabla1 = {" ++ (intercalate "," (map show (delta1 (nabla d)))) ++ "}"
       , "Nabla2 = \n  {" ++ (intercalate "," (map show (delta2 (nabla d))))  ++ "}"
       , "Nabla3 = \n  {" ++ (intercalate "," (map showNCC (delta3 (nabla d))))  ++ "}"
       , (if nullGamma (delta4 c) then ""   else "Delta4 = {\n" ++ (intercalate ",\n" 
                                                                                        [ let oneLine = "  " ++ lhs ++ " =?= " ++ rhs                        
                                                                                          in if (length $ oneLine ) <= 80 then oneLine 
                                                                                              else "  " ++ lhs ++ "\n  =?=\n  " ++ rhs
                                                                                          | (lhs :=?=: rhs) <- ([(show l :=?=: show r) | (l :=?=: r) <- environmentEq (delta4 c)]
                                                                                                              ++
                                                                                                              [(show l :=?=: show r) | (l :=?=: r) <- expressionEq (delta4 c)]
                                                                                                              ++
                                                                                                              [(show l :=?=: show r) | (l :=?=: r) <- bindingEq (delta4 c)]
                                                                                                              ++
                                                                                                              [(show l :=?=: show r) | (l :=?=: r) <- varLiftEq (delta4 c)] 
                                                                                                                  )
                                                                                        ])++ " }\n") 
   ]   
   | (i,MA.Solved pb) <- zip [1..] res, let b = solution pb, let a = applySolution b (inputGamma pb), let c=delta pb, let d=pb 
   ]
            ++
            (unlines [ "PANIC: There are open problems:" ++ (show p) | MA.Node p [] <- res]        )      
        
    

-- | header for LaTeX-file     
latexheader :: String
latexheader = "\\documentclass[a4paper]{article}\n\\usepackage[margin=5mm]{geometry}\n\\usepackage{tikz}\n\\usepackage{amsmath}\n\\begin{document}\n"
-- | footer for LaTeX-file 
latexfooter :: String
latexfooter = "\\end{document}\n% This file was produced automatically by lrsx (see http://www.ki.cs.uni-frankfurt.de/research/lrsx ) "
    
getLong content = unlines $ (filter fn) (lines content)
  where
    fn l  
      | "Forking diagram:" `isPrefixOf` l = True
      | "Commuting diagram:" `isPrefixOf` l = True
      | otherwise = False
      
