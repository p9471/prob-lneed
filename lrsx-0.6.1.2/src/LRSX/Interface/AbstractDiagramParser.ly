> {
> -----------------------------------------------------------------------------
> -- |
> -- Module      :  LRSX.Interface.AbstractDiagramParser
> -- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
> -- License     :  BSD-style 
> -- 
> -- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
> --
> --  
> -- The parser for the backend processing of diagrams
> --
> -----------------------------------------------------------------------------
> -- 
> --
> --
> module LRSX.Interface.AbstractDiagramParser 
> where
> import LRSX.Diagrams.AbstractDiagrams.Types
> import Data.Char
> import Data.List
> import Data.Maybe
> import LRSX.Util.Util

> }

> %name parseDiagrams Diagrams
> %tokentype { TokenType }

> %token 
> var               { TokenVar   $$ _   }
> num               { TokenNumber $$ _  }
> '~~>'             { TokenSymbol "~~>" _}
> '<-'              { TokenSymbol "<-"  _}
> '->'              { TokenSymbol "->" _}
> '+'               { TokenSymbol "+" _}
> '-'               { TokenSymbol "-" _}
> '.'               { TokenSymbol "." _}
> ','               { TokenSymbol "," _}
> ':'               { TokenSymbol ":" _}
> 'CommutingDiagram' {TokenKeyword "Commuting diagram" _}
> 'ForkingDiagram' {TokenKeyword "Forking diagram" _}
> 'SR'             {TokenKeyword "SR" _}
> 'ANSWER'         {TokenKeyword "ANSWER" _}
  
> %%


> Diagrams :: {[Diagram]}
> Diagrams : Diagram           { [$1] }
>          | Diagram Diagrams  { $1:$2 }
> 
> Diagram : 'ForkingDiagram'   ':' RealForkingDiagram { $3 }
> Diagram : 'CommutingDiagram'   ':' RealCommutingDiagram { $3 }

> RealForkingDiagram : LeftForkingDiagram '~~>' RightForkingDiagram  {$1 :~~> $3}
> LeftForkingDiagram : Lefts '.' Rights          {$1 ++ $3}
>                    | Left                      {[$1]}
> RightForkingDiagram : {- epsilon -} { [] }
>                     | SequenceRightsThenLefts {$1}

> RealCommutingDiagram : LeftCommutingDiagram '~~>' RightCommutingDiagram  {let (move,keep) = $3 in ((map directionToLeft $ reverse move) ++ $1) :~~> keep}
> LeftCommutingDiagram : Lefts    {$1}
> RightCommutingDiagram : {- epsilon -} { ([],[]) }
>                     | Lefts  {([],$1)}
>                     | Rights '.' Lefts {($1,$3)}


> SequenceRightsThenLefts :  Right '.' SequenceRightsThenLefts  {$1 : $3}
>      |  Right  {[$1]}
>      |  Lefts           {$1}


> Left : '<-' LABEL '-'         { $2 LEFT }
>      | Answer                 { $1 }

> Right : '-' LABEL '->'        {$2 RIGHT}

> Lefts : Left           { [$1] }
>       | Lefts '.' Left { $1 ++ [$3]}


> Rights  : Right {[$1]}
>         | Rights '.' Right  {$1 ++ [$3]}


> LABEL :   'SR' ',' var ',' num            { \dir -> (SR dir (Rule $3 $5)) }
>         | 'SR' ',' var ',' '+' ',' num    { \dir -> (SR dir (Plus (Rule $3 $7))) }
>         | 'SR' ',' var ',' '+'            { \dir -> (SR dir (Plus (Rule $3 (-1)))) }
>         | 'SR' ',' var                    { \dir -> (SR dir (Rule $3 (-1))) }

>         |  var ',' num            { \dir -> (Trans dir (Rule $1 $3)) }
>         |  var ',' '+' ',' num    { \dir -> (Trans  dir (Plus (Rule $1 $5))) }
>         |  var ',' '+'     { \dir -> (Trans  dir (Plus (Rule $1 (-1)))) }
>         |  var    { \dir -> (Trans  dir (Rule $1 (-1))) }

> Answer : '<-' 'ANSWER' '-' { Answer  }
>        |'<-' 'ANSWER' ',' num '-' { Answer  }



> {

  
> directionToLeft (SR dir x) = SR LEFT x
> directionToLeft (Trans dir x) = Trans LEFT x
> directionToLeft x = x

  
====================================================================================================================================
The lexer
> happyError :: [TokenType] -> a
> happyError []     = error $ "parse error: unexpected end of file "
> happyError (x:xs) = error $ "parse error before: "
>                           ++ (take 200 $ concat ( intersperse " " ( map showToken xs)))  ++ (printMark xs)



> -- | the type of the tokens
> type TokenType = Token SourceMark
> -- | SourceMark is a pair (line,column)
> type SourceMark = (Int,Int) -- (Line,Column)

> -- the token type parametrized of a label 
> data Token label =
>    TokenNumber   Int  label         -- ^ numbers
>  | TokenKeyword  String label         -- ^ keywords
>  | TokenSymbol   String label         -- ^ symbols
>  | TokenVar      String label         -- ^ variables (beginning with a lower-case letter)
>  | TokenUnknown  Char label           -- ^ token for non-recognized symbols, error handling will be done by the parser
>  deriving(Eq,Show)


> -- | show a list of tokens (for error handling)
> printMark :: [TokenType] -> String
> printMark []     = "At EOF"
> printMark (x:xs) =  let (r,c) = getLab x 
>                     in "\nLine: " ++ show r ++ " " ++ "Column: " ++ show c

> -- | extract the label of token
> getLab (TokenNumber  _ label) = label
> getLab (TokenKeyword  _ label) = label
> getLab (TokenSymbol   _ label) = label
> getLab (TokenVar      _ label) = label
> getLab (TokenUnknown  _ label) = label

> -- textual representation of tokens
> showToken (TokenNumber  s _) = show s
> showToken (TokenKeyword  s _) = s
> showToken (TokenSymbol   s _) = s
> showToken (TokenVar  s _)     = s
> showToken (TokenUnknown  c _) = [c]
> showToken x = error $ show x

> -- association of symbols, and keywords with their tokens
> keywordsAndToken =
>      [(x,TokenSymbol x)    | x <- [ "~~>"
>                                   , "<-"
>                                   , "->"
>                                   , "+"
>                                   , "-"
>                                   , ","
>                                   , "."
>                                   , ":"
>                                   ]]
>   ++ [(x,TokenKeyword x)   | x <- keywords ]

> -- | list of keywords
> keywords = 
>   [
>    "Commuting diagram"
>   ,"Forking diagram"
>   ,"SR"
>   ,"ANSWER"
>   ]

> -- | list of keywords and symbols
> allkeys = map fst keywordsAndToken

> -- | get comment parses @{- -}@ comments
> getComment [] z s = error "unterminated {-"
> getComment ('-':'}':xs) z s = (xs,z,(s+2))
> getComment input@(i:xs) zeile spalte 
>   | i == '\n' = getComment xs (zeile +1) 1
>   | i == '\t' = getComment xs (zeile) (spalte+8)
> getComment (i:xs) z s = getComment xs (z) (s+1)

> -- | the main function of the lexer, gets a string and outputs a list of tokens
> lexInput :: String -> [TokenType]
> lexInput input = lexer input 1 1
> lexer []   _ _ = []
> lexer inp@('{':'-':is) z s = 
>  let (rest,z',s') = getComment inp z s
>  in lexer rest z' s'
> lexer ('-':'-':is) z s = lexer (dropWhile (/= '\n') is) (z) 1
> lexer input@('-':i:is) zeile spalte 
>   | isDigit i = 
>     let (num,rest) = span isDigit (i:is)
>         offset = length num
>     in (TokenNumber ((-1)*(read num)::Int) (zeile,spalte)):(lexer rest zeile (spalte+offset))
> lexer input@(i:is) zeile spalte 
>   | isDigit i = 
>     let (num,rest) = span isDigit input
>         offset = length num
>     in (TokenNumber ((read num)::Int) (zeile,spalte)):(lexer rest zeile (spalte+offset))
>   | isJust lk = case lk of
>                  Just (token, offset, rest) -> token:(lexer rest zeile (spalte+offset))
>                  Nothing -> error "impossible" 
>   | isLower i = 
>       let (tk1,rest1) = break (not.isIdentifier) input
>           (tk,rest) = if length tk1 > 1 then  
>                       case (last tk1) of 
>                         '-' -> case rest1 of 
>                                   (a:as) -> if isAlphaNum a then (tk1,rest1) 
>                                               else (init tk1, (last tk1):rest1)
>                                   []     ->  (init tk1, (last tk1):rest1)
>                         _  -> (tk1,rest1)
>                       else (tk1,rest1)
>           offset   = length tk
>       in TokenVar tk (zeile,spalte):(lexer rest zeile (spalte+offset))
>   | i == '\n' = lexer is (zeile +1) 1
>   | i == '\t' = lexer is (zeile) (spalte+8)
>   | isSpace i = lexer is zeile (spalte + 1)
>   | otherwise = (TokenUnknown i (zeile,spalte)):lexer is (zeile) (spalte +1)
>  where lk = (lookupPrefix input keywordsAndToken zeile spalte) 

> -- | checks whether a Char is an alphaNum or is '_' or is '-'
> isIdentifier x = isAlphaNum x || x `elem` "_-"



> }


