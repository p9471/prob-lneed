-----------------------------------------------------------------------------
-- |
-- Module      :  LRSX.Interface.Interactive
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--  
-- The Interactive Environment of the LRSX Tool
--
-----------------------------------------------------------------------------
module LRSX.Interface.Interactive where
import Data.List
import System.Environment
import Data.Maybe
import Data.Either
import System.Process
import Text.Read
import System.FilePath
import System.IO
-- import the stuff of the LRSX libraries
import LRSX.Diagrams.DiagramCalculation
import LRSX.Interface.Pretty
import LRSX.Language.Syntax
import LRSX.Language.Alpha
import LRSX.Reduction.Reduction
import LRSX.Unification.UnificationProblem
import qualified LRSX.Unification.Unification as UN
import qualified LRSX.Matching.Matching as MA
import LRSX.Language.Constraints
import LRSX.Language.ContextDefinition
import LRSX.Interface.Parser
import LRSX.Interface.DocStrings
import LRSX.Interface.Lexer
import LRSX.Util.Util
import LRSX.Reduction.Rules
import qualified LRSX.Diagrams.AbstractDiagrams.Types as ADT
import qualified LRSX.Diagrams.AbstractDiagrams as AD
import qualified Data.Map.Strict as Map
import Data.IORef
import System.Console.Haskeline
import Control.Monad.Catch
import Control.Monad.IO.Class
import System.Directory
import System.FilePath
import Control.Exception (evaluate, SomeException(..))
-- import LRSX.Interface.CommandLine
interactive_main :: IO ()
interactive_main = 
  do 
    homedir <- getHomeDirectory
    let history_file = joinPath [homedir,".lrsxi_history"]
    state <- newIORef (initInteractiveState,initState)
    runInputT defaultSettings{historyFile=Just history_file} (outputStrLn greeting >> loop state)
   where 
       loop :: IORef (InteractiveState,ParseState) -> InputT IO ()
       loop state = do
           minput <- getInputLine "LRSXi> "
           case minput of
               Nothing -> return ()
               Just str -> case words str of 
                             ("quit":[]) -> return ()
                             ("help":[]) -> outputStrLn helpText >>  loop state
                                           
                             ("load":file:[]) -> do 
                                                    (st,pst) <- liftIO $ readIORef state
                                                    (c,pst') <- liftIO $ readInputFile st file
                                                    liftIO $ writeIORef state (c,pst')
                                                    loop state
                             ("show":"funinfo":[]) -> do 
                                                       (isState,pst) <- liftIO $ readIORef state
                                                       outputStrLn (showStatisticFunInfo $ isFunInfo isState)
                                                       loop state
                             ("show":"context":"definitions":[]) -> do 
                                                       (isState,pst) <- liftIO $ readIORef state
                                                       outputStrLn (showGrammar $ grammar $ isCtxtDef isState)
                                                       loop state
                             ("show":"prefix":"table":[]) -> do 
                                                       (isState,pst) <- liftIO $ readIORef state
                                                       outputStrLn (showPfxTable $ pfxTable $ isCtxtDef isState)
                                                       loop state
                             ("show":"fork":"table":[]) -> do 
                                                       (isState,pst) <- liftIO $ readIORef state
                                                       outputStrLn (showForkTable $ frkTable $ isCtxtDef isState)
                                                       loop state
                             ("show":"standard":"reductions":[]) -> do 
                                                       (isState,pst) <- liftIO $ readIORef state
                                                       outputStrLn (unlines $ intersperse "" $  map (showConstrainedRule 15) $ isStdRules isState)
                                                       loop state
                             ("show":"transformations":[]) -> do 
                                                       (isState,pst) <- liftIO $ readIORef state
                                                       outputStrLn (unlines $ intersperse "" $ map (showConstrainedRule 15) $ isTransRules isState)
                                                       loop state                                                       
                             ("show":"answers":[]) -> do 
                                                       (isState,pst) <- liftIO $ readIORef state
                                                       outputStrLn (unlines $ intersperse "" $ map (showAnswer 15) $ isAnswerRules isState)
                                                       loop state      
                             ("show":"expression":[]) -> do 
                                                         (isState,pst) <- liftIO $ readIORef state
                                                         case isReductionProblem isState of 
                                                              Nothing -> outputStrLn "[NONE]" >> loop state
                                                              Just rp -> outputStrLn (show (rpRedex rp)) >> loop state
                             ("show":"expression":"drop":"subs":[]) -> do 
                                                         (isState,pst) <- liftIO $ readIORef state
                                                         case isReductionProblem isState of 
                                                              Nothing -> outputStrLn "[NONE]" >> loop state
                                                              Just rp -> outputStrLn (show (dropAllSubsExpr $ rpRedex rp)) >> loop state                                                         
                             ("show":"constraints":[]) -> do                                                              
                                                           (isState,pst) <- liftIO $ readIORef state

                                                           case isReductionProblem isState of 
                                                              Nothing -> outputStrLn "[NONE]" >> loop state
                                                              Just rp -> do 
                                                                           let d1 = delta1 (rpConstraintBase rp)
                                                                           let d2 = delta2 (rpConstraintBase rp)
                                                                           let d3 = delta3 (rpConstraintBase rp)                                                                           
                                                                           let d4 = delta4 (rpConstraintBase rp)                                                                           
                                                                           outputStrLn ("Non-empty context constraints    : " ++ show d1)
                                                                           outputStrLn ("Non-empty environment constraints: " ++ show d2)
                                                                           outputStrLn ("Non-capture constraints          : " ++ displayNCCs d3)
                                                                           outputStrLn ("Constraint equations             : " ++ show d4)                                                                           
                                                                           loop state
                               
                             ("show":"delta1":[]) -> do 
                                                         (isState,pst) <- liftIO $ readIORef state
                                                         case isReductionProblem isState of 
                                                              Nothing -> outputStrLn "[NONE]" >> loop state
                                                              Just rp -> outputStrLn (show (delta1 $ rpConstraintBase rp)) >> loop state
                                                         
                             ("show":"delta2":[]) -> do 
                                                         (isState,pst) <- liftIO $ readIORef state
                                                         case isReductionProblem isState of 
                                                              Nothing -> outputStrLn "[NONE]" >> loop state
                                                              Just rp -> outputStrLn (show (delta2 $ rpConstraintBase rp)) >> loop state

                             ("show":"delta3":[]) -> do 
                                                         (isState,pst) <- liftIO $ readIORef state
                                                         case isReductionProblem isState of 
                                                              Nothing -> outputStrLn "[NONE]" >> loop state
                                                              Just rp -> outputStrLn ((displayNCCs $ delta3 $ rpConstraintBase rp)) >> loop state
                             ("show":"delta4":[]) -> do 
                                                         (isState,pst) <- liftIO $ readIORef state
                                                         case isReductionProblem isState of 
                                                              Nothing -> outputStrLn "[NONE]" >> loop state
                                                              Just rp -> outputStrLn (show (delta4 $ rpConstraintBase rp)) >> loop state
                                                              
                             ("show":"alphabase":[]) -> do 
                                                         (isState,pst) <- liftIO $ readIORef state
                                                         case isReductionProblem isState of 
                                                              Nothing -> outputStrLn "[NONE]" >> loop state
                                                              Just rp -> outputStrLn (show (rpAlphaBase rp)) >> loop state
                             ("set":"delta":rest) ->    do
                                                           (isState,pst) <- liftIO $ readIORef state
                                                           catch 
                                                              ( do
                                                                 (d_1,d_2,d_3,pst1) <- liftIO $ evaluate $ onlyConstraints pst (lexInput $ (unwords rest)) 
                                                                 let d1=constantify d_1
                                                                 let d2=d_2
                                                                 let d3=constantify d_3
                                                                 outputStrLn ("Non-empty context constraints: " ++ show d1)
                                                                 outputStrLn ("Non-empty environment constraints: " ++ show d2)
                                                                 outputStrLn ("Non-capture constraints: " ++ displayNCCs d3)
                                                                 let isState1 = refresh isState pst1
                                                                 case isReductionProblem isState1 of 
                                                                      Nothing -> outputStrLn "[Fail, no expression set]" >> loop state
                                                                      Just rp -> let del=rpConstraintBase rp
                                                                                     delNew = del{delta1=d1,delta2=d2,delta3=d3}
                                                                                     rpNew = rp{rpConstraintBase=delNew}
                                                                                     isState2 = isState1{isReductionProblem=Just rpNew}
                                                                                 in do
                                                                                     liftIO $ writeIORef state (isState1{isReductionProblem = Just rpNew},pst1)
                                                                                     loop state
                                                               )
                                                              (\x -> case (x :: SomeException) of _ -> outputStrLn (show x) >> loop state)
                                                           
                             ("set":"expression":rest) -> do 
                                                             (isState,pst) <- liftIO $ readIORef state
                                                             catch 
                                                              (do 
                                                                (r0,pst1) <- liftIO $ evaluate $ runStatePST pst . parseExpr  . lexInput $ (unwords rest)
                                                                let isState1 = refresh isState pst1
                                                                let r = constantify $ r0
                                                                let rpFresh =  case isReductionProblem isState1 of 
                                                                                  Nothing -> ReductionProblem {
                                                                                                    rpRedex = r,
                                                                                                    rpConstraintBase = emptyDelta,
                                                                                                    rpAlphaBase = [],
                                                                                                    rpRule = identity,
                                                                                                    rpFunInfo = isFunInfo isState1,
                                                                                                    rpFreshNames = ["#" ++ show i | i <- [1..]],
                                                                                                    rpLogging =[],
                                                                                                    rpGlobalNCCs = [],
                                                                                                    rpCDef = isCtxtDef isState1,
                                                                                                    rpAlphaOn=False}
                                                                                  Just rp -> rp {
                                                                                                    rpRedex = r,
                                                                                                    rpFunInfo = isFunInfo isState1,
                                                                                                    rpCDef = isCtxtDef isState1
                                                                                                }
                                                                outputStrLn (show r)
                                                                liftIO $ writeIORef state (isState1{isReductionProblem = Just rpFresh},pst1)
                                                                loop state
                                                               )
                                                              (\x -> case (x :: SomeException) of _ -> outputStrLn (show x) >> loop state)
                                                                
                             ("apply":rule:[]) -> do 
                                                       (isState,pst) <- liftIO $ readIORef state
                                                       let (a,b,c,d,e) = (runP . parseRuleName . lexInput $ rule)
                                                       let rulename =(b,c,d,e)
                                                       outputStrLn (show rulename)
                                                       case isReductionProblem isState of 
                                                          Nothing -> outputStrLn "[NO EXPRESSION SET]" >> loop state
                                                          Just rp0 -> 
                                                            case lookup rulename ([(ruleName r,r) | r <- (isStdRules isState) ++ (isTransRules isState)]) of
                                                               Just cr -> let rp = rp0 { rpRule = cr }
                                                                          in do
                                                                               case tryReduceWithFailure rp of 
                                                                                 ([],failstring) 
                                                                                       -> do outputStrLn failstring
                                                                                             outputStrLn "NOT APPLICABLE" 
                                                                                             loop state
                                                                                 ([rp'],failstring) -> do 
                                                                                            outputStrLn "unique successor"
                                                                                            let isState' = isState{isReductionProblem=Just rp'}
                                                                                            outputStrLn (show (rpRedex rp'))
                                                                                            liftIO $ writeIORef state (isState',pst)
                                                                                            loop state
                                                                                 (rps,failstring)   -> do 
                                                                                            let l = length rps
                                                                                            outputStrLn ( (show $ l)  ++ " successors" )
                                                                                            sequence_ [outputStrLn ("(" ++ show num ++ ") " ++ show (rpRedex rp')) | (num,rp') <- zip [1..] rps]
                                                                                            num <- let rek = do 
                                                                                                               line <- getInputLine ("LRSXi> " ++ " choose 1 - " ++ show l ++ ": ")
                                                                                                               case line of Nothing -> rek
                                                                                                                            Just line' -> case readMaybe line' of 
                                                                                                                                            Nothing -> rek
                                                                                                                                            Just n -> return n
                                                                                                   in rek
                                                                                            let isState' = isState{isReductionProblem=Just (rps!!(num-1))}
                                                                                            outputStrLn (show (rpRedex $ rps!!(num-1)))
                                                                                            liftIO $ writeIORef state (isState',pst)
                                                                                            loop state
                                                               Nothing -> outputStrLn "rule not found" >> loop state
                             ("standard":"reduce":[]) -> do 
                                                       (isState,pst) <- liftIO $ readIORef state
                                                       case isReductionProblem isState of 
                                                          Nothing -> outputStrLn "[NO EXPRESSION SET]" >> loop state
                                                          Just rp0 -> 
                                                            let rules = [r | r <- (isStdRules isState)]
                                                            in
                                                                               case tryReduceSetOfRules rp0 rules of 
                                                                                 [] -> outputStrLn "NOT APPLICABLE" >> loop state
                                                                                 [rp'] -> do 
                                                                                            outputStrLn $ "unique successor with rule " ++ "{"  ++ showRuleName (ruleName $ rpRule rp') ++ "}"
                                                                                            let isState' = isState{isReductionProblem=Just rp'}
                                                                                            outputStrLn (show (rpRedex rp'))
                                                                                            liftIO $ writeIORef state (isState',pst)
                                                                                            loop state
                                                                                 rps   -> do 
                                                                                            let l = length rps
                                                                                            outputStrLn ( (show $ l)  ++ " successors" )
                                                                                            outputStrLn ("(0)  keep expression")
                                                                                            sequence_ [outputStrLn ("(" ++ show num ++ ") " ++ " {" ++ showRuleName (ruleName (rpRule rp')) ++ "} " ++ show (rpRedex rp')) | (num,rp') <- zip [1..] rps]
                                                                                            num <- let rek = do 
                                                                                                               line <- getInputLine ("LRSXi> " ++ " choose 0 - " ++ show l ++ ": ")
                                                                                                               case line of Nothing -> rek
                                                                                                                            Just line' -> case readMaybe line' of 
                                                                                                                                            Nothing -> rek
                                                                                                                                            Just n -> if n <= l && l >= 0 then return n else rek
                                                                                                   in rek
                                                                                            let isState' = if num == 0 then isState else isState{isReductionProblem=Just (rps!!(num-1))}
                                                                                            outputStrLn (show (rpRedex $ rps!!(num-1)))
                                                                                            liftIO $ writeIORef state (isState',pst)
                                                                                            loop state
                             ("all":"applicable":"rules":[]) -> do 
                                                       (isState,pst) <- liftIO $ readIORef state
                                                       case isReductionProblem isState of 
                                                          Nothing -> outputStrLn "[NO EXPRESSION SET]" >> loop state
                                                          Just rp0 -> 
                                                            let rules = [r | r <- (isStdRules isState) ++ (isTransRules isState) ++ (isAnswerRules isState)]
                                                            in
                                                                               case tryReduceSetOfRules rp0 rules of 
                                                                                 [] -> outputStrLn "NO RULE APPLICABLE" >> loop state
                                                                                 rps   -> do 
                                                                                            let l = length rps
                                                                                            outputStrLn ( (show $ l)  ++ " successors" )
                                                                                            outputStrLn ("(0)  keep expression")                                                                                            
                                                                                            sequence_ [outputStrLn ("(" ++ show num ++ ") " ++ " {" ++ showRuleName (ruleName (rpRule rp')) ++ "} " ++ show (rpRedex rp')) | (num,rp') <- zip [1..] rps]
                                                                                            num <- let rek = do 
                                                                                                               line <- getInputLine ("LRSXi>" ++ " choose 0 - " ++ show l ++ ": ")
                                                                                                               case line of Nothing -> rek
                                                                                                                            Just line' -> case readMaybe line' of 
                                                                                                                                            Nothing -> rek
                                                                                                                                            Just n -> if n <= l && l >= 0 then return n else rek
                                                                                                   in rek
                                                                                            let isState' = if num == 0 then isState else isState{isReductionProblem=Just (rps!!(num-1))}
                                                                                            liftIO $ writeIORef state (isState',pst)
                                                                                            loop state                                                                                            
 
                             ("alpha":"rename":[]) -> do 
                                                       (isState,pst) <- liftIO $ readIORef state
                                                       case isReductionProblem isState of 
                                                          Nothing -> outputStrLn "[NO EXPRESSION SET]" >> loop state
                                                          Just rp ->   do let rp' = alphaRenameProblem rp
                                                                          let isState' = isState{isReductionProblem = Just rp'}
                                                                          outputStrLn (show (rpRedex rp'))
                                                                          liftIO $ writeIORef state (isState',pst)
                                                                          loop state

                             ("alpha":"repair":[]) -> do 
                                                       (isState,pst) <- liftIO $ readIORef state
                                                       case isReductionProblem isState of 
                                                          Nothing -> outputStrLn "[NO EXPRESSION SET]" >> loop state
                                                          Just rp ->   do let rp' = alphaRepairProblem rp
                                                                          let isState' = isState{isReductionProblem = Just rp'}
                                                                          outputStrLn (show (rpRedex rp'))
                                                                          liftIO $ writeIORef state (isState',pst)
                                                                          loop state                                                                          
                             [] -> loop state                                                                          
                             other -> do 
                                       outputStrLn $ "unknown command: " ++ show (unwords other) 
                                       outputStrLn "type 'help' to display list of commands"
                                       loop state

                             

                             
                             
displayNCCs xs = 
    intercalate "," $ map displayNCC xs
  where   
    displayNCC (Left expr,ctxt)  = show (expr,ctxt)
    displayNCC (Right env,ctxt)  =  "[" ++  show env ++ "," ++ show ctxt ++ "]"
                             
                             
                             

greeting = unlines $ 
 [
   "  _________________________________________________________________________  "
  ," /    /   /   /  /  /  / / / / /////     \\\\\\\\\\ \\ \\ \\ \\  \\  \\  \\   \\   \\    \\ "
  ,"<    <   <   <  <  <  < < < < <<<<< LRSXi >>>>> > > > >  >  >  >   >   >    >"
  ," \\    \\   \\   \\  \\  \\  \\ \\ \\ \\ \\\\\\\\\\     ///// / / / /  /  /  /   /   /    / "
  ,"  -------------------------------------------------------------------------  "
  ,"                  >>> Interactive mode of the LRSX Tool <<<                  "
  ,"==========================================================================="
  ," type 'help' to display an overview of commands                            "
  ]
                             
helpText =  unlines $
  [
   "  _________________________________________________________________________  "
  ," /    /   /   /  /  /  / / / / /////     \\\\\\\\\\ \\ \\ \\ \\  \\  \\  \\   \\   \\    \\ "
  ,"<    <   <   <  <  <  < < < < <<<<< LRSXi >>>>> > > > >  >  >  >   >   >    >"
  ," \\    \\   \\   \\  \\  \\  \\ \\ \\ \\ \\\\\\\\\\     ///// / / / /  /  /  /   /   /    / "
  ,"  -------------------------------------------------------------------------  "
  ,"                  >>> Interactive mode of the LRSX Tool <<<                  "
  ,"==========================================================================="
  ,""
  ,"available commands:"
  ,"---------------------------------------------------------------------------"
  ,"GLOBAL COMMANDS"
  ,"                                                                           "  
  ,"  quit                      exit LRSXi                                     "
  ,"  help                      display help                                   "
  ,"                                                                           "
  ,"SET ENVIRONMENT"
  ,"                                                                           "  
  ,"  load FILEPATH             load calculus description from FILEPATH        "
  ,"  set expression EXPRESSION set the current expression as EXPRESSION       "
  ,"  set delta CONSTRAINTS     set the current non-empty context, non-empty   "
  ,"                            environment and non-capture constraints        "
  ,"                                                                           "  
  ,"SHOW INFORMATION ABOUT THE CURRENT STATE"
  ,"                                                                           "  
  ,"  show funinfo              display collected properties of function       "
  ,"                            symbols (arity and operator arity)             "
  ,"  show context definitions  display grammars for defined context classes   "
  ,"  show fork table           display fork table for context classes         "
  ,"  show prefix table         display prefix table for context classes       "
  ,"  show standard reductions  display available standard reduction rules     "
  ,"  show transformations      display available transformation rules         "
  ,"  show answers              display available answer descriptions          "
  ,"  show expression           display current expression                     "
  ,"  show constraints          display current non-empty context, non-empty   "
  ,"                            environment, and non-capture constraints       "
  ,"  show delta1               display current non-empty context constraints  "
  ,"  show delta2               display current non-empty environment constraints"  
  ,"  show delta3               display current non-capture constraints        "
  ,"  show alphabase            display current alpha renaming                 "
  ,"                                                                           "  
  ,"APPLYING RULES TO THE CURRENT EXPRESSION"
  ,"                                                                           "  
  ,"  standard reduce           apply all standard reduction rules to the      "
  ,"                            current expression                             "
  ,"  apply RULE                apply rule named RULE to the current expression"
  ,"  all applicable rules      list all rules that are applicable to the      "
  ,"                            current expression                             "
  ,"  alpha rename              apply alpha renaming to the current expression "
  ,"  alpha repair              apply alpha repairing to the current expression"
  ]
  

readInputFile stateOld file = do    
   content <- importPrePocessor file
   let (r,st) = parseSRulesAndCommands (lexInput content)
   let stdRules = psStandardReductions st
   let transRules = psTransformations st
   let answerRules = psAnswerRules st
   let commands = psOverlapCommands st
   let unions = psUnionCommands st
   let expands = psExpandCommands st
   let ctxtdef = psCtxtDefn st
   let finfo0 = toFunInfo (psFunInfo st)
   let finfo = compactFunInfo finfo0
   let fresh = ["#"++ show i | i <- [1..]]
   let state = stateOld
                 {
                   isStdRules = stdRules
                  ,isTransRules = transRules
                  ,isAnswerRules = answerRules
                  ,isCtxtDef    = ctxtdef
                  ,isFunInfo    = finfo
               }
   return (state,st)
   
initInteractiveState = 
   InteractiveState {
     isStdRules = [],
     isTransRules = [],
     isAnswerRules = [],
     isCtxtDef = initCtxtDef,
     isFunInfo = [],
     isReductionProblem = Nothing  
   }
    
   
data InteractiveState = 
      InteractiveState {
        isStdRules    :: [ConstrainedRule]
       ,isTransRules  :: [ConstrainedRule]
       ,isAnswerRules :: [ConstrainedRule]
       ,isCtxtDef     :: CtxtDef
       ,isFunInfo     :: FunInfo
       ,isReductionProblem :: Maybe ReductionProblem
      }
 deriving(Show)   
   
refresh isState parserState =
   let state = isState
                 {
                  isCtxtDef    = (psCtxtDefn parserState),
                  isFunInfo    = (compactFunInfo $ toFunInfo $ psFunInfo parserState)
               }
   in state
