-----------------------------------------------------------------------------
-- |
-- Module      :  LRSX.Interface.DocStrings
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--  
-- option parsing and execution of commands
--
-----------------------------------------------------------------------------
module LRSX.Interface.CommandLine where


import Data.List
import System.Environment
import Data.Maybe
import Data.Either
import System.Process
import Text.Read
import System.FilePath
import System.IO
-- import the stuff of the LRSX libraries
import LRSX.Diagrams.DiagramCalculation
import LRSX.Interface.Pretty
import LRSX.Language.Syntax
import LRSX.Unification.UnificationProblem
import qualified LRSX.Unification.Unification as UN
import qualified LRSX.Matching.Matching as MA
import LRSX.Language.Constraints
import LRSX.Interface.Parser
import LRSX.Interface.DocStrings
import LRSX.Interface.Lexer
import LRSX.Util.Util
import LRSX.Reduction.Rules
import qualified LRSX.Diagrams.AbstractDiagrams.Types as ADT
import qualified LRSX.Diagrams.AbstractDiagrams as AD
import qualified Data.Map.Strict as Map
import qualified LRSX.Interface.Interactive as LII
-- * Handling of Options
-- Several data types and functions to parse and process the options as given by the input
    
-- | Data type for the options of the main module    
data OptCommand = Match       [SimpleOpt]            -- ^ match equations
                | Unify       [SimpleOpt]            -- ^ unify equations
                | Overlap     [SimpleOpt]            -- ^ compute critical pairs 
                | Help String                        -- ^ print help text, the addtional 'String' argument is for an error message
                | OverlapAndJoin [OptDiagram]        -- ^ compute forking / commuting diagrams
                | Convert        [OptDiagram]        -- ^ convert outputfiles
                | Induct         [OptDiagram]        -- ^ Use diagrams as input and perform the automated induction by using AProVE and CeTA
                | Interactive [SimpleOpt]
 deriving(Eq,Show)
                
-- 
-- ** Parsing options                

-- | 'parseArgs' parses the command line arguments into 'OptCommand'                

parseArgs []               = Help    "No command given"
parseArgs ("join":rest)    = parseOpt (OverlapAndJoin []) rest []
parseArgs ("induct":rest)  = parseOpt (Induct []) rest []
parseArgs ("convert":rest) = parseOpt (Convert []) rest []
parseArgs ("unify":rest)   = parseSOpt (Unify []) rest []
parseArgs ("match":rest)   = parseSOpt (Match []) rest []
parseArgs ("overlap":rest) = parseSOpt (Overlap []) rest []
parseArgs ("interactive":rest) = parseSOpt (Interactive []) rest []
parseArgs ("help":rest)    = Help ""
parseArgs (a:xs)           = Help ("Unknown command: " ++ a)
    
-- | parse sub-options for join mode 
parseOpt (OverlapAndJoin _) [] parsed = 
    let singleOpts    = filter isSingleOpt parsed
        addSingleOpts = if null singleOpts 
                            then Long:parsed    -- mode 'Long' by default
                            else parsed
        addBound      = if null [() | Bound n <- addSingleOpts] 
                           then (Bound 10):addSingleOpts 
                           else addSingleOpts
        addExpBound   = if null [() | ExpBound n <- addBound] 
                           then (ExpBound 0):addBound 
                           else addBound
        noInfile      = null [() | (Infile _) <- addExpBound]
        extendStdAndNoExtendStd = (not (null [() | ExtendStd <- parsed])) &&
                                  (not (null [() | NoExtendStd <- parsed]))
    in 
      case () of 
        _ | length singleOpts > 1       -> OverlapAndJoin [HelpOpt ("Option clash for 'lrsx join': only one option in {"  ++ intercalate "," (map showOptDiagram singleOpts)  ++ "} is allowed")]
          | extendStdAndNoExtendStd     -> OverlapAndJoin [HelpOpt ("Option clash for 'lrsx join': extend-standard-reduction and no-extend-standard-reduction cannot be used together")]
          | Short `elem` singleOpts     -> OverlapAndJoin [HelpOpt "Output-format 'short' for 'lrsx join' not supported\n Use output format 'long' or 'proofs' and as a second run  usw'lrsx convert' to convert the obtained output."]
          | TRS `elem` singleOpts       -> OverlapAndJoin [HelpOpt "Output-format 'trs' for 'lrsx join' not supported\n Use output format 'long' or 'proofs' and as a second run  usw'lrsx convert' to convert the obtained output."]
          | noInfile                    -> OverlapAndJoin [HelpOpt ("No input given")]
          | otherwise                   -> OverlapAndJoin addExpBound
              
              
parseOpt (Convert _) [] parsed = 
    let singleOpts    = filter isSingleOpt parsed
        noInfile      = null [() | (Infile _) <- parsed]
    in 
      case () of 
       _ |  length singleOpts > 2       ->  Convert [HelpOpt ("Option clash for 'lrsx convert': exactly two formats in {" ++ intercalate "," (map showOptDiagram [Short,TRS,Long,Proofs])  ++ "} are allowed")]
         |  noInfile                    -> Convert [HelpOpt ("No input given")]
         |  otherwise                   -> Convert parsed
              
parseOpt (Induct _) [] parsed = 
    let singleOpts    = filter isSingleOpt parsed
        noInfile      = null [() | (Infile _) <- parsed]
    in 
      case () of 
       _ | length singleOpts > 1        -> Induct [HelpOpt ("Option clash for 'lrsx induct': exactly one input format from {" 
                                                  ++ intercalate "," (map showOptDiagram [Short,Long,Proofs]) 
                                                  ++ "} is allowed")]
         | noInfile                     -> Induct [HelpOpt ("No input given")]
         | otherwise                    -> Induct parsed
              
parseOpt a  ("short":rest) parsed                         = parseOpt a rest (parsed ++ [Short])
parseOpt a  ("trs":rest) parsed                           = parseOpt a rest (parsed ++ [TRS])
parseOpt a  ("itrs":rest) parsed                          = parseOpt a rest (parsed ++ [ITRS])
parseOpt a  ("long":rest) parsed                          = parseOpt a rest (parsed ++ [Long]  )
parseOpt a  ("proofs":rest) parsed                        = parseOpt a rest (parsed ++ [Proofs] ) 
parseOpt a  ("latex":rest) parsed                         = parseOpt a rest (parsed ++ [LaTeX] ) 
parseOpt a  ("pdf":rest) parsed                           = parseOpt a rest (parsed ++ [PDF]  )
parseOpt a  ("debug":rest) parsed                         = parseOpt a rest (parsed ++ [DebugOpt]  )
parseOpt a  ("no-extend-standard-reduction":rest) parsed  = parseOpt a rest (parsed ++ [NoExtendStd]  )
parseOpt a  ("extend-standard-reduction":rest) parsed     = parseOpt a rest (parsed ++ [ExtendStd]  )
parseOpt a  ("no-alpha-rename":rest) parsed               = parseOpt a rest (parsed ++ [NoAlpha]  )
parseOpt a  ("no-stop-on-error":rest) parsed              = parseOpt a rest (parsed ++ [NoStop]  )
parseOpt a  ("always-alpha-rename":rest) parsed           = parseOpt a rest (parsed ++ [AlwaysAlpha]  )
parseOpt a  ("full-context-guess":rest) parsed            = parseOpt a rest (parsed ++ [FullContextGuess]  )
parseOpt a  ("full-environment-guess":rest) parsed        = parseOpt a rest (parsed ++ [FullEnvironmentGuess]  )
parseOpt a  ("use-itrs":rest) parsed                      = parseOpt a rest (parsed ++ [UseITRS]  )
parseOpt a  ("help":rest) parsed                          = parseOpt a rest (parsed ++ [HelpOpt ""])
parseOpt a  (('b':'o':'u':'n':'d':'=':n):rest) parsed
  | Just num <- readMaybe n =  parseOpt a rest (parsed ++ [Bound num])
  | otherwise               = parseOpt a rest (parsed ++ [HelpOpt ("cannot parse number: " ++ n)])
parseOpt a  (('e':'x':'p':'a':'n':'s':'i':'o':'n':'-':'b':'o':'u':'n':'d':'=':n):rest) parsed
  | Just num <- readMaybe n =  parseOpt a rest (parsed ++ [ExpBound num])
  | otherwise               = parseOpt a rest (parsed ++ [HelpOpt ("cannot parse number: " ++ n)])
parseOpt a  (('u':'n':'i':'o':'n':'s':'-':'f':'r':'o':'m':'=':n):rest) parsed =
    parseOpt a rest (parsed ++ [UnionBy n])
parseOpt a  (('a':'t':'p':'-':'p':'a':'t':'h':'=':n):rest) parsed     =
    parseOpt a rest (parsed ++ [ATPPath n])
parseOpt a  (('o':'u':'t':'p':'u':'t':'-':'t':'o':'=':n):rest) parsed =
    parseOpt a rest (parsed ++ [OutPath n])
parseOpt a  [infile]     parsed =  parseOpt a []   (parsed ++ [Infile infile])
parseOpt a  (other:rest) parsed = parseOpt a rest (parsed ++ [HelpOpt ("unknow flag: " ++ other)])

-- | parse sub-options for unify, match, and overlap mode
parseSOpt (Unify _) [] parsed    = 
    let noInfile = null [() | (SInfile _) <- parsed]
    in  if noInfile 
          then Unify [SHelpOpt ("No input given")]
          else Unify parsed
parseSOpt (Match _) [] parsed    = 
    let noInfile = null [() | (SInfile _) <- parsed]
    in  if noInfile 
          then Match [SHelpOpt ("No input given")]
          else Match parsed
parseSOpt (Overlap _) [] parsed    = 
    let noInfile = null [() | (SInfile _) <- parsed]
    in   if noInfile 
            then Overlap [SHelpOpt ("No input given")]
            else Overlap parsed
            
parseSOpt (Interactive _) [] parsed =
  let guessContextOpt = not (null [() | (SFullContextGuess) <- parsed])
      guessEnvOpt     = not (null [() | (SFullEnvironmentGuess) <- parsed])
      infile          = not (null [() | (SInfile _) <- parsed])
      helpOpt         = not (null [() | (SHelpOpt _) <- parsed])
  in case () of 
       _ | guessContextOpt -> Interactive [SHelpOpt "flag full-context-guess not available for 'interactive mode'"]
         | guessEnvOpt -> Interactive [SHelpOpt "flag full-environment-guess not available for 'interactive mode'"]  
         | infile      -> Interactive [SHelpOpt "input file not available for 'interactive mode'"]  
         | otherwise   -> Interactive parsed         
  
parseSOpt a ("help":rest) parsed                      = parseSOpt a rest (parsed ++ [SHelpOpt ""])
parseSOpt a ("debug":rest) parsed                     = parseSOpt a rest (parsed ++ [SDebug])
parseSOpt a  ("full-context-guess":rest) parsed       = parseSOpt a rest (parsed ++ [SFullContextGuess])
parseSOpt a  ("full-environment-guess":rest) parsed   = parseSOpt a rest (parsed ++ [SFullEnvironmentGuess])
parseSOpt a [infile] parsed                           = parseSOpt a [] (parsed ++ [SInfile infile])
parseSOpt a (other:rest) parsed                       = parseSOpt a rest (parsed ++ [SHelpOpt ("unknow flag: " ++ other)])
              

-- | The data type 'OptDiagram' represents sub-options for diagram computation
data OptDiagram = 
      Short            -- ^ short ASCII-representation of the diagrams, diagrams are compressed, does not work well, if diagrams aren't closed.
    | TRS              -- ^ print the TRS corresponding to the diagrams
    | ITRS             -- ^ print the Integer TRS corresponding to the diagrams
    | Long             -- ^ print all diagrams in ASCII-representation, no compression, works also well if diagrams cannot be closed.
    | Proofs           -- ^ print diagrams and proofs which show the critical pairs and the joining, no compression, works well if diagrams cannot be closed
    | LaTeX            -- ^ generate the compressed diagrams in graphical notation using LaTeX and the Tikz package, does not work well, if diagrams aren't closed
    | PDF              -- ^ similar to LaTeX, but generates the PDF file in out.pdf, requires pdflatex in the PATH
    | Bound Int        -- ^ add an explicit search bound 
    | ExpBound Int     -- ^ bound for context expansion (default=0)
    | Infile String    -- ^ the input file name
    | HelpOpt String   -- ^ help for the specific command
    | DebugOpt         -- ^ option for turning on the debug mode
    | ExtendStd        -- ^ option for allowing to extend standard reductions pathes in diagrams (calculus should be determinstic)
    | NoExtendStd      -- ^ option to forbid to extend standard reductions pathes in diagrams 
    | AlwaysAlpha      -- ^ option for using always alpha-renaming
    | NoAlpha          -- ^ option to turn off alpha-renaming
    | NoStop           -- ^ option to turn off stopping on non closable cases
    | FullContextGuess  -- ^ guess all context variables as empty/non-empty (default = False)           
    | FullEnvironmentGuess -- ^ guess all context variables as empty/non-empty (default = False)           
    | UseITRS           -- ^ use integer term rewrite system for induction
    | UnionBy FilePath  -- ^  file from which unions are read for conversion, switches on ignore-unions
    | ATPPath FilePath  -- ^  path where aprove.jar and CeTA can be found, note that java must be installed
    | OutPath FilePath  -- ^  path where output-files will be stored
    
 deriving(Eq,Show)
 
-- | show an 'OptDiagram'-values as 'String' 
showOptDiagram :: OptDiagram -> String
showOptDiagram Short       = "short"
showOptDiagram TRS         = "trs"
showOptDiagram ITRS         = "itrs" 
showOptDiagram Long        = "long" 
showOptDiagram Proofs      = "proofs" 
showOptDiagram PDF         = "pdf" 
showOptDiagram LaTeX       = "latex" 
showOptDiagram DebugOpt    = "debug" 
showOptDiagram ExtendStd   = "extend-standard-reduction" 
showOptDiagram NoExtendStd   = "no-extend-standard-reduction" 
showOptDiagram FullContextGuess = "full-context-guess"
showOptDiagram FullEnvironmentGuess = "full-environment-guess"
showOptDiagram UseITRS = "use-itrs"
showOptDiagram AlwaysAlpha = "always-alpha-rename"
showOptDiagram NoAlpha = "no-alpha-rename"
showOptDiagram NoStop = "no-stop-on-error"
showOptDiagram (Bound n)   = "bound=" ++ show n
showOptDiagram (ExpBound n)   = "expansions-bound=" ++ show n
showOptDiagram (UnionBy file)   = "unions-from=" ++ file
showOptDiagram (ATPPath path)   = "atp-path=" ++ path
showOptDiagram (OutPath path)   = "output-to=" ++ path
showOptDiagram (Infile f)  = f 
showOptDiagram (HelpOpt s) = s 

-- | Type for simple options
data SimpleOpt = SInfile String  -- ^ the input file name
               | SHelpOpt String -- ^ help for the specific command
               | SDebug          -- ^ noisy output
               | SFullContextGuess -- ^ guess all context variables as empty/non-empty (default = False)               
               | SFullEnvironmentGuess -- ^ guess all context variables as empty/non-empty (default = False)               
 deriving(Eq,Show)

-- | decides whether the option is exclusive and cannot be used
--   with other options (except for input and bound)
isSingleOpt:: OptDiagram -> Bool
isSingleOpt LaTeX  = True
isSingleOpt Short  = True
isSingleOpt TRS    = True
isSingleOpt ITRS   = True
isSingleOpt Long   = True
isSingleOpt PDF    = True
isSingleOpt Proofs = True
isSingleOpt _      = False

-- | decides whether the option is the help option and returns the 'String' within the help-option
isHelpOpt :: OptDiagram -> Maybe String
isHelpOpt (HelpOpt s) = Just s
isHelpOpt _ = Nothing
-- | decides whether the option is the help option and returns the 'String' within the help-option
isSHelpOpt :: SimpleOpt -> Maybe String
isSHelpOpt (SHelpOpt s) = Just s
isSHelpOpt _ = Nothing


-- ** Processing Options
-- | process the options, i.e. execute the corresponding parts of the LRSX Tool
processOpts :: OptCommand -> IO ()
-- Conversion of diagrams
processOpts (Convert opts)
    | (Just str):_ <- filter isJust (map isHelpOpt opts) =
        do 
         error (str   ++ "\n" ++  (commandHelp "convert"))
    | otherwise                      = 
        let bound = null [() | Bound num <- opts]
            infile = head [file | Infile file <- opts]
            unionfile =  case [file | UnionBy file <- opts] of
                           [] -> Nothing
                           (x:xs) -> Just x
            
            fromFormat =  case [o | o <- opts, o `elem` [Short,Long,Proofs,TRS,ITRS]] of
                            (h:t) -> h
                            [] -> error ("could not parse options: from-format must be in {" 
                                         ++ 
                                         (intercalate "," (map showOptDiagram [Short,Long,Proofs,TRS,ITRS]))
                                         ++ "}")
            toFormat   =  case [o | o <- opts, o `elem` [Short,Long,Proofs,TRS,ITRS]] of
                            (_:h:t) -> h
                            _ -> error ("could not parse options: to-format must be in {" 
                                         ++ 
                                         (intercalate "," (map showOptDiagram [Short,Long,Proofs,TRS,ITRS]))
                                         ++"}")
            
        in 
         do
            content <- readFile infile
            case (fromFormat, toFormat) of 
             (Proofs,Long)   ->  putStrLn $ getLong content
             (Long,Short)    ->   AD.convertLongToShort unionfile content
             (Proofs,Short)  -> AD.convertLongToShort unionfile (getLong content)
             (Long,TRS)      ->  AD.convertLongToTRSs unionfile (getLong content)
             (Short,TRS)      ->  AD.convertLongToTRSs unionfile (getLong content)             
             (Proofs,TRS)    ->  AD.convertLongToTRSs unionfile (getLong content)   
             (Long,ITRS)      ->  AD.convertLongToITRSs unionfile (getLong content)
             (Short,ITRS)      ->  AD.convertLongToITRSs unionfile (getLong content)             
             (Proofs,ITRS)    ->  AD.convertLongToITRSs unionfile (getLong content)               
             (x,y)       -> error $ "Conversion from " ++ showOptDiagram x ++ " into " ++ showOptDiagram y ++ " not available."
             
             
-- Induction             
processOpts (Induct opts)
    | (Just str):_ <- filter isJust (map isHelpOpt opts) =
        do 
         error (str   ++ "\n" ++  (commandHelp "induct"))
    | otherwise                      = 
        let infile = head [file | Infile file <- opts]
            unionfile =  case [file | UnionBy file <- opts] of
                           [] -> Nothing
                           (x:xs) -> Just x
            fromFormat =  case [o | o <- opts, o `elem` [Short,Long,Proofs]] of
                            (h:t) -> h
                            [] -> case [o | o <- opts, o `elem` [TRS,ITRS]] of 
                                      (h:t) -> error "Input format trs or itrs are not supported for 'lrsx induct'"
                                      [] -> Long
            itrs = not (null [() | UseITRS <- opts])
            atpPath =  case [path | ATPPath path <- opts] of
                           [] -> "."
                           (x:xs) -> x
            outPath =  case [path | OutPath path <- opts] of
                           [] -> "."
                           (x:xs) -> x
            prefix = joinPath [outPath,takeFileName infile ++ ".out"]                           
                           
        in do 
             content <- readFile infile
             case fromFormat of
                Long ->   (if itrs then AD.automatedInductionITRS else AD.automatedInduction) atpPath unionfile (getLong content) prefix
                Short ->  (if itrs then AD.automatedInductionITRS else AD.automatedInduction) atpPath unionfile (getLong content) prefix
                Proofs -> (if itrs then AD.automatedInductionITRS else AD.automatedInduction) atpPath unionfile (getLong content) prefix

-- ---------------------------
-- computing  diagrams:
-- ---------------------------
processOpts (OverlapAndJoin opts)
    | (Just str):_ <- filter isJust (map isHelpOpt opts) =
        do 
         putStrLn str  
         putStrLn (commandHelp "join")
    | otherwise                      =
        let bound = last [num | Bound num <- opts]
            expBound = last [num | ExpBound num <- opts]
            mode  = head (filter isSingleOpt opts)
            debug = not (null [() | DebugOpt <- opts])
            contextguess = not (null [() | FullContextGuess <- opts])
            envguess = not (null [() | FullEnvironmentGuess <- opts])
            extendStd = not (null [() | ExtendStd <- opts])
            noExtendStd = not (null [() | NoExtendStd <- opts])
            alwaysAlpha = not (null [() | AlwaysAlpha <- opts])
            noAlpha = not (null [() | NoAlpha <- opts])
            stopOnErr =  (null [() | NoStop <- opts])
            
            infile =head [file | Infile file <- opts]
        in do 
            content <- importPrePocessor infile
            let directory = takeDirectory infile
            --
            let (r,st) = parseSRulesAndCommands (lexInput content)
            let stdRules = psStandardReductions st
            let transRules = psTransformations st
            let answerRules = psAnswerRules st
            let commands = psOverlapCommands st
            let unions = psUnionCommands st
            let expands = psExpandCommands st
            let ctxtdefs = psCtxtDefn st
            let finfo0 = toFunInfo (psFunInfo st)
            let inducts = psInductCommands st
            --
            --let (((stdRules,transRules,answerRules),commands,unions,expands,ctxtdefs),finfo0) = parseSRulesAndCommands $ lexInput content
            let finfo                                            = compactFunInfo finfo0
            let commands'                                        = unfoldAll stdRules answerRules commands
            let unionMap = buildUnionMap unions
            let diagramsFromExpand = processExpands expands
            let res = (runDiagramCalculation unionMap expBound bound noExtendStd extendStd alwaysAlpha noAlpha contextguess envguess (stdRules,transRules,answerRules,finfo,ctxtdefs) commands') ++ diagramsFromExpand
            case mode of
             Long ->
               do
                putStrLn (showStatisticFunInfo finfo)
                sequence_ [  case outfile r of 
                               Nothing -> putStrLn ( showDiagramWU stopOnErr unionMap r)  >> hFlush stdout
                               Just file -> appendFile file ( showDiagramWU stopOnErr unionMap r)
                           | r <- res]            
                sequence_ [processOpts (Induct  [TRS,Long,Infile infile,ATPPath atppath,UnionBy (joinPath [directory,unionsfrom])])  | (infile,atppath,unionsfrom) <- inducts]

                
             Proofs ->             
                do
                putStrLn (showStatisticFunInfo finfo)
                sequence_ [  case outfile r of 
                               Nothing -> sequence_ [putStrLn l >> hFlush stdout | l <- lines ( showDiagramProof stopOnErr unionMap r)]
                               Just file -> sequence_ [appendFile file (l ++ "\n") | l <- lines ( showDiagramProof stopOnErr unionMap r)]
                           | r <- res]       
                sequence_ [processOpts (Induct  [TRS,Proofs,Infile infile,ATPPath atppath,UnionBy unionsfrom])  | (infile,atppath,unionsfrom) <- inducts]
                
                
                
processOpts (Interactive opts)
    | (Just str):_ <- filter isJust (map isSHelpOpt opts) =
        do 
         putStrLn str  
         putStrLn (commandHelp "interactive")
    |  otherwise = LII.interactive_main
         
                
 
-- ---------------------------
-- unification of expressions
-- ---------------------------
processOpts (Unify opts)
    | (Just str):_ <- filter isJust (map isSHelpOpt opts) =
        do 
         putStrLn str  
         putStrLn (commandHelp "unify")
    | otherwise                      =
        let infile = head [file | SInfile file <- opts]
            debug  = not (null [() | SDebug <- opts])
            fullcontextguess = not (null [() | SFullContextGuess <- opts])
            fullenvguess = not (null [() | SFullEnvironmentGuess <- opts])
        in unifyInputEquations debug fullcontextguess fullenvguess infile
        
-- processOpts (Match opts) = putStrLn "Not supported yet"        
processOpts (Match opts)
    | (Just str):_ <- filter isJust (map isSHelpOpt opts) =
        do 
         putStrLn str  
         putStrLn (commandHelp "match")
    | otherwise                      =
        let infile = head [file | SInfile file <- opts]
            debug  = not (null [() | SDebug <- opts])
        in matchInputEquations debug infile
        
-- ---------------------------
-- computing critical pairs
-- ---------------------------
processOpts (Overlap opts)
    | (Just str):_ <- filter isJust (map isSHelpOpt opts) =
        do 
         putStrLn str  
         putStrLn (commandHelp "overlap")
    | otherwise                      =
        let infile =head [file | SInfile file <- opts]
            debug  = not (null [() | SDebug <- opts])            
            fullcontextguess = not (null [() | SFullContextGuess <- opts])
            fullenvguess = not (null [() | SFullEnvironmentGuess <- opts])
        in overlapAndComputeCriticalPairs debug fullcontextguess fullenvguess (infile)
-- ---------------------------
-- help mode:
-- ---------------------------
processOpts (Help str) = 
    do
         putStrLn str  
         putStrLn (globalHelp)
        
-- * Several "main"-functions for the different modi        

-- ** Unifying equations with constraints
unifyInputEquations :: Bool         -- ^ flag whether debug mode is turned on (True) or not (False)
                        -> Bool     -- ^ flag whether all contexts should be guessed as empty/non-empty
                        -> Bool     -- ^ flag whether all contexts should be guessed as empty/non-empty
                        -> FilePath -- ^ path to input file
                        -> IO ()
unifyInputEquations debug contextguess envguess infile = 
  do
    inp <- importPrePocessor infile
    let ((eqs,ctxtcon,envcon,capcon),finfo0,ctxtdef) = parser inp
    print ctxtdef
    let finfo = compactFunInfo finfo0
    let capconstr = (capcon) ++ concat [computeConstraints l ++ computeConstraints r     | (l:=?=:r) <-  eqs]
    let sol =[]
    let gam =(Gamma {varLiftEq = []
                    , expressionEq =  eqs
                   , environmentEq = []
                   , bindingEq = []
                   }) 
    let del = (Delta {delta1=ctxtcon,delta2=envcon,delta3=capconstr,delta4=emptyGamma}) 
    let frsh = ["_#" ++ show i | i <- [1..]]     
    let fn = nubFnSyms (getFnSymbols eqs)
    let prob = Problem {  inputGamma=gam
                        , inputDelta=del
                        , solution=sol
                        , gamma=gam
                        , delta=del
                        , fresh=frsh
                        , functions=finfo
                        , nabla=emptyDelta
                        , expandAllEnvs=envguess
                        , expandAllContexts=contextguess
                        , expandDelta4Constraints = False
                        , alphaBase = []
                        , mpLogging = []
                        , contextDefinitions = ctxtdef
                        }
    case check prob of 
       Just err -> error err -- "LVC failure in input"
       Nothing  ->
         do 
           let res = UN.solvedAndOpenLeaves $ UN.unification prob
           -- pretty print the recognized function symbols
           putStrLn (showStatisticFunInfo finfo)
           -- pretty print the input problem
           putStrLn "==============================="
           putStrLn "Input Problem:"
           putStrLn "==============================="
           putStrLn (showInputUnificationProblem prob)
           putStrLn "==============================="
           -- pretty print the unifiers
           putStrLn (showUnificationResult res)
           if debug 
              then do
                    let fulltree = UN.unification prob 
                    putStrLn "Unification Tableau (debug):"    
                    putStrLn "============================"                       
                    print fulltree
              else return ()
    
-- ** Match equations with constraints    
matchInputEquations :: Bool         -- ^ flag whether debug mode is turned on (True) or not (False)
                        -> FilePath -- ^ path to input file
                        -> IO ()
matchInputEquations debug infile = 
    do
        inp <- importPrePocessor infile
        let ((eqs,ctxtcon,envcon,capcon,ctxtconK,envconK,capconK),finfo0,ctxtdef) = parserMatching inp
        let finfo = compactFunInfo finfo0
        let capconstr = (capcon) ++ concat [computeConstraints l   | (l:=?=:r) <-  eqs]
        let sol =[]
        let gam = Gamma {expressionEq  = map (\(a:=?=:b) -> (a:=?=:(constantify b))) (eqs)
                        ,varLiftEq = []
                        ,bindingEq = []
                        ,environmentEq = []
                        }
        let del = (Delta {delta1=ctxtcon,delta2=envcon,delta3=capconstr,delta4=emptyGamma})
        let cdel = constantify (Delta {delta1=ctxtconK,delta2=envconK,delta3=capconK,delta4=emptyGamma})                    
        let frsh = ["_#" ++ show i | i <- [1..]]
        let fn = nubFnSyms (getFnSymbols eqs)
        let prob = Problem {  inputGamma=gam
                            , inputDelta=del
                            , solution=sol
                            , gamma=gam
                            , delta=del
                            , fresh=frsh
                            , functions=finfo
                            , nabla=cdel
                            , expandAllContexts=False
                            , expandAllEnvs=False
                            , expandDelta4Constraints = False
                            , alphaBase = []
                            , mpLogging = []
                            , contextDefinitions = ctxtdef
                          }
        let res = MA.solvedAndOpenLeaves $ MA.matching prob
        putStrLn "==============================="
        putStrLn "Input Problem:"
        putStrLn "==============================="
        putStrLn (showInputMatchingProblem prob)
        putStrLn "==============================="
        putStrLn (showMatchingResult res)
        if debug
           then do
                let fulltree = MA.matching prob 
                putStrLn "Matching Tableau (debug):"    
                putStrLn "============================"                       
                print fulltree
           else return ()
       
    
--         
-- ** Compute critical pairs, and display them on stdout
overlapAndComputeCriticalPairs :: Bool      -- ^ flag whether debug mode is turned on (True) or not (False)
                                -> Bool     -- ^ flag whether all contexts should be guessed as empty/non-empty
                                -> Bool     -- ^ flag whether all envs should be guessed as empty/non-empty
                                -> FilePath -- ^ path to input file
                                -> IO ()
overlapAndComputeCriticalPairs debug contextguess envguess file =
  do
   content <- importPrePocessor file
   let (r,st) = parseSRulesAndCommands (lexInput content)
   let stdRules = psStandardReductions st
   let transRules = psTransformations st
   let answerRules = psAnswerRules st
   let commands = psOverlapCommands st
   let unions = psUnionCommands st
   let expands = psExpandCommands st
   let ctxtdef = psCtxtDefn st
   let finfo0 = toFunInfo (psFunInfo st)
   -- let (((stdRules,transRules,answerRules),commands,unions,expands,ctxtdef),finfo0) =  parseSRulesAndCommands $ lexInput content
   let finfo = compactFunInfo finfo0
   let fresh = ["#"++ show i | i <- [1..]]
   print ctxtdef
   let commands'                                        = unfoldAll stdRules answerRules commands
   let overlapProblems = 
        [OverlapProblem { rules = (stdRules++[r{standardReduction=True} | r <- answerRules]++transRules)
                        , command = cmd
                        , freshNames = fresh
                        , funInfo = finfo
                        , criticalPairs = [] -- will be computed
                        , isCommuting=False
                        , fullContextGuess=contextguess
                        , fullEnvironmentGuess=envguess
                        , opLogging=[]
                        , answerOverlap = isAnswerRuleCommand answerRules cmd
                        , restrictedRules = []
                        , ruleSubsumption = buildUnionMap unions
                        , ctxtInfo = ctxtdef
                        }
                    | cmd <- commands']
   let result = executeCommands overlapProblems
   -- one critical pair per problem
   let flattenedResult = [overlapPB{criticalPairs = [cp]}  | overlapPB <- result, cp <- criticalPairs overlapPB]

   let groupedResult = [(gp!!0){criticalPairs = concatMap criticalPairs gp}
                        | gp <- 
                            groupBy 
                                (\overlapPB1 overlapPB2 -> extractTrans (command overlapPB1) == extractTrans (command overlapPB2)) 
                                $ sortBy (\overlapPB1 overlapPB2 -> (extractTrans (command overlapPB1)) `compare` (extractTrans (command overlapPB2)))
                                    flattenedResult]
   putStrLn "========================================================="
   putStrLn $ "Computing overlaps from file " ++  file
   putStrLn "---------------------------------------------------------"
   putStrLn $ (showStatisticFunInfo finfo)
   putStrLn "---------------------------------------------------------"
   putStrLn $ unlines $ map (ppCriticalPair . head . criticalPairs) flattenedResult
   putStrLn "========================================================="
   putStrLn "Statistics: "
   putStrLn "---------------------------------------------------------"
   putStrLn $ (fill 50 " ") ++ "#critical pairs"
   putStrLn $ unlines $ map (\overlapProblem -> (fill 50 
                                                 (case command overlapProblem of
                                                    Single (Left r1) (Left r2)  _ _  _ ->  "<==" ++ showRuleName r1 ++ "== . ==" ++ showRuleName r2 ++ "==>"
                                                    Single (Right r1) (Left r2) _ _ _ ->  "==" ++ showRuleName r1 ++ "==> . ==" ++ showRuleName r2 ++ "==>"                                                 
                                                    Single (Left r1) (Right r2) _ _ _ ->  "<==" ++ showRuleName r1 ++ "== . <==" ++ showRuleName r2 ++ "=="
                                                    Single (Right r1) (Right r2) _ _ _ ->  "==" ++ showRuleName r1 ++ "==> . <==" ++ showRuleName r2 ++ "=="                                                 
                                                 )
                                                )
                                                ++ (": " ++ (show $ length $ criticalPairs overlapProblem)))
                            result
   putStrLn "---------------------------------------------------------"
   putStrLn "Overlaps per transformation"
   putStrLn $ (fill 50 " ") ++ "#critical pairs"
   putStrLn $ unlines $ map (\overlapProblem -> (fill 50 (extractTrans (command overlapProblem))) ++ (": " ++ (show $ length $ criticalPairs overlapProblem)))  groupedResult
   putStrLn "---------------------------------------------------------"
   putStrLn "========================================================="
   putStrLn $ (fill 50 " Total number of critical pairs: ") ++ (show $  length flattenedResult)
   putStrLn "========================================================="   
 where 
    extractTrans (Single (Left r)  _ _ _ _) = showRuleName r
    extractTrans (Single (Right r) _ _ _ _) = showRuleName r    
