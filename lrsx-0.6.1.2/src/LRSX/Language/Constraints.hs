-----------------------------------------------------------------------------
-- |
-- Module      :  LRSX.Syntax.Constraints
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--  
--  Compute NonCaptureConstraints automatically
--
-----------------------------------------------------------------------------

module LRSX.Language.Constraints where
import Data.List
import Data.Function
import LRSX.Language.Syntax
import LRSX.Util.Util



-- computing constraints for bound variables,
-- \X.C[X] ---> X,C
-- \x.C[x] ---> x,C
-- ... 
-- f(X1.X2.C[X1]) --> X1, f'(X2.C[X1])?
-- f(X1.X2.C[X2]) --> 

computeConstraintsArgPos (VarPos v) = []
computeConstraintsArgPos (Binder v e) = go v e 
  where go (var:vs) e = [(Left (VarL var), c') | c <- getContexts (VarL var) e, let c' = Fn "_internal_f" [Binder vs c]] 
                        ++
                        go vs e
        go [] e = computeConstraints e
    
-- computeConstraints (Lam var body) = [(Left (VarLam var),c) | c <- getContexts (VarLam var) body, c /= Hole] ++ (computeConstraints body)
computeConstraints (VarL _) = []
computeConstraints (Fn f args) = concatMap computeConstraintsArgPos args
computeConstraints (Letrec env e) =
  (computeConstraints e)
  ++
  [(Left (VarL x),c) | (before,x:=rhs,after) <- splits2 (bindings env), c <- getContexts (VarL x) rhs]
  ++
  [(Left (VarL x),c) | (before,x:=rhs,after) <- splits2 (bindings env), c <- getContexts (VarL x) e]
  ++
  [(Left (VarL x),c) | (before,x:=rhs,after) <- splits2 (bindings env), (MetaChain s0 t ee z s) <- metaChains env, c <- getContexts (VarL x) s]
  ++
  [(Left (VarL x),c) | (before,x:=_,after) <- splits2 (bindings env), (y:=rhs) <- (before ++ after), c <- getContexts (VarL x) rhs]
  ++
  [(Left (VarL x),c) | (before,(MetaChain s0 _ ee x s),after) <- splits2 (metaChains env), 
                          (y:=rhs) <- bindings env, 
                          c <- getContexts (VarL x) rhs ]
  ++
  [(Left (VarL x),c) | (before,(MetaChain s0 _ ee x s),after) <- splits2 (metaChains env), 
                          c <- getContexts (VarL x) e ]
  ++                         
  [(Left (VarL x),c) | (before,(MetaChain s0 _ ee x s),after) <- splits2 (metaChains env)
                       ,  (MetaChain s0 _ ee' x' s') <- metaChains env, 
                          c <- getContexts (VarL x) s' ]
  ++
  (concatMap (\(_ := rhs) -> computeConstraints rhs) (bindings env))
  ++
  (concatMap (\(MetaChain _ _ _ _ s) -> computeConstraints s) (metaChains env))
  
computeConstraints (FlatLetrec env e) =
  (computeConstraints e)
  ++
  [(Left (VarL x),c) | (before,x:=rhs,after) <- splits2 (bindings env), c <- getContexts (VarL x) rhs]
  ++
  [(Left (VarL x),c) | (before,x:=rhs,after) <- splits2 (bindings env), c <- getContexts (VarL x) e]
  ++
  [(Left (VarL x),c) | (before,x:=rhs,after) <- splits2 (bindings env), (MetaChain s0 t ee z s) <- metaChains env, c <- getContexts (VarL x) s]
  ++
  [(Left (VarL x),c) | (before,x:=_,after) <- splits2 (bindings env), (y:=rhs) <- (before ++ after), c <- getContexts (VarL x) rhs]
  ++
  [(Left (VarL x),c) | (before,(MetaChain s0 _ ee x s),after) <- splits2 (metaChains env), 
                          (y:=rhs) <- bindings env, 
                          c <- getContexts (VarL x) rhs ]
  ++
  [(Left (VarL x),c) | (before,(MetaChain s0 _ ee x s),after) <- splits2 (metaChains env), 
                          c <- getContexts (VarL x) e ]
  ++                         
  [(Left (VarL x),c) | (before,(MetaChain s0 _ ee x s),after) <- splits2 (metaChains env)
                       ,  (MetaChain s0 _ ee' x' s') <- metaChains env, 
                          c <- getContexts (VarL x) s' ]
  ++
  (concatMap (\(_ := rhs) -> computeConstraints rhs) (bindings env))
  ++
  (concatMap (\(MetaChain _ _ _ _ s) -> computeConstraints s) (metaChains env))
computeConstraints (MetaCtxtSubst _ srt ee s) = computeConstraints s
computeConstraints _ = []

getContextsArgPos _ (VarPos _) = []
getContextsArgPos (VarL v1) (Binder vs e)
 | v1 `elem` vs = []
getContextsArgPos e1 (Binder vs e) = [Binder vs c | c <- getContexts e1 e]

getContexts e1 e2
 | e1 == e2 = [hole]
getContexts e1 (Fn f args) =  [Fn f (before  ++ [c]  ++ after)    | (before,h,after) <- splits2 args, c <- getContextsArgPos e1 h]
getContexts e1 (VarL _) = []
getContexts e1 (MetaCtxtSubst s0 srt c e) = [MetaCtxtSubst s0 srt c c' | c' <- getContexts e1 e]
getContexts e1 (Letrec env e) = 
 [Letrec env c | c <- getContexts e1 e]
 ++
 [Letrec env{bindings=before ++ [x:= c] ++ after} e| (before,(x:=r),after) <- splits2 (bindings env), c <- getContexts e1 r]
 ++
 [Letrec env{metaChains = before ++ [MetaChain s0 typ ee x c] ++ after} e |  
    (before,MetaChain s0 typ ee x s,after) <- splits2 (metaChains env), c <- getContexts e1 s]
getContexts e1 (FlatLetrec env e) = 
 [FlatLetrec env c | c <- getContexts e1 e]
 ++
 [FlatLetrec env{bindings=before ++ [x:= c] ++ after} e| (before,(x:=r),after) <- splits2 (bindings env), c <- getContexts e1 r]
 ++
 [FlatLetrec env{metaChains = before ++ [MetaChain s0 typ ee x c] ++ after} e |  
    (before,MetaChain s0 typ ee x s,after) <- splits2 (metaChains env), c <- getContexts e1 s]
getContexts e1 (MetaCtxt s0 _ _) = []
getContexts e1 (MetaExpression s0 _) = []
getContexts e1 (Constant s0 _) = []
getContexts e1 (ConstantCtxt s0 _ _) = []
getContexts e1 (ConstantCtxtSubst s0 srt c e)= [ConstantCtxtSubst s0 srt c c' | c' <- getContexts e1 e]
getContexts e1 e = error $"(in getContexts)" ++  show e



