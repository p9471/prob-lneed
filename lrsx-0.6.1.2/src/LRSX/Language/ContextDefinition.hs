-----------------------------------------------------------------------------
-- | 
-- Module      :  LRSX.Language.ContextDefinition
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--
--  Data types for the internal representation 
--  of unification expressions
--
-----------------------------------------------------------------------------
module LRSX.Language.ContextDefinition where
import Data.List
import Data.Maybe
import Data.Either
import LRSX.Util.Util
import LRSX.Language.Syntax
import qualified Data.Set as Set
import qualified Data.Map as Map

data CtxtDef = CtxtDef {grammar :: Map.Map String [Either ((Expression,SetOfCtxtVars,SetOfEnvironmentVars,NonCaptureConstraints),(Expression,SetOfCtxtVars,SetOfEnvironmentVars,NonCaptureConstraints))
                                                          (Expression,SetOfCtxtVars,SetOfEnvironmentVars,NonCaptureConstraints)
                                                  ]
                       ,pfxTable :: Map.Map (CtxtSort,CtxtSort) [(CtxtSort,CtxtSort)]
                       ,frkTable :: Map.Map (CtxtSort,CtxtSort) [(CtxtSort,CtxtSort,CtxtSort,Context)]
                       }
 deriving(Show)
 
isTriv (UserDefinedSort _ "TRIV") = True
isTriv _ = False

showForkTable mp =  unlines $ 
  [  show srt1 ++ "1[S1] =?= " ++ show srt2 ++ "2[S2] branches to\n" ++ 
     "   " ++ show srt1 ++ "1 |-> " ++ show 
     
                (subst 
                  (MetaCtxt emptySubstitution srt4 ((show srt4)++"4"))
                  (MultiHole 1)
                 (subst 
                  (MetaCtxtSubst emptySubstitution srt5 ((show srt5)++"5") ( MetaExpression emptySubstitution "S2"))
                  (MultiHole 2)
                      (subst
                          ctxt
                          (Hole emptySubstitution)
                          (MetaCtxt emptySubstitution srt3 (show srt3 ++ "3")))
                  ))
     ++ ",\n" ++ "   " ++ 
          show srt2 ++ "2 |-> " ++ show 
     
                (subst 
                  (MetaCtxtSubst emptySubstitution srt4 ((show srt4)++"4") (MetaExpression emptySubstitution "S1"))
                  (MultiHole 1)
                 (subst 
                  (MetaCtxt emptySubstitution srt5 ((show srt5)++"5") )
                  (MultiHole 2)
                      (subst
                          ctxt
                          (Hole emptySubstitution)
                          (MetaCtxt emptySubstitution srt3 (show srt3 ++ "3")))
                  ))
                      
  | ((srt1,srt2),list) <- Map.assocs mp, (srt3,srt4,srt5,ctxt) <- list ]
  
showPfxTable mp =
  unlines $ [ show srt1  ++ "1[S1] =?= " ++ show srt2 ++ "2[S2] branches to " ++ show srt1 ++ "1 |-> " ++ show srt3 ++"3, " ++ show srt2 ++ "2 |-> " ++ show srt3 ++ "3 " ++ show srt4 ++ "4, " ++ "S1 =?= " ++ show srt4 ++ "4[S2]"
  | ((srt1,srt2),list) <- Map.assocs mp, (srt3,srt4) <- list ]
  
  


showGrammar g =
 unlines $ intersperse "" [concat (s:" ::= ":intersperse ("\n" ++ (replicate (length s) ' ') ++ "  |  ") (map showItem gr))  | (s,gr) <- Map.assocs g]

  where    
  showItem (Left ((e,delta1,delta2,delta3),(e',delta1',delta2',delta3'))) = 
      show e ++ showConst (delta1,delta2,delta3)
      ++ "/" 
      ++ show e' ++ showConst (delta1',delta2',delta3')
      
  showItem (Right (e,delta1,delta2,delta3)) =
      show e ++ showConst (delta1,delta2,delta3)
  showConst ([],[],[]) = ""
  showConst (delta1,delta2,delta3) = " where " ++ (showDelta1 delta1) ++ (showDelta2 delta2) ++ (showDelta3 delta3)

                 
mp  !!!! v = case Map.lookup v mp of 
                 Just a -> a
                 Nothing -> error $ (show (v,mp))
                 
initCtxtDef = CtxtDef {grammar=Map.insert "TRIV" [Right (Hole emptySubstitution,[],[],[]) ] Map.empty,pfxTable=Map.empty,frkTable=Map.empty}
isCapturingDef srt cdef =
  any (isCapturingCtxt [srt]) (concat [fromEither a | a <- (grammar cdef) !!!! srt ])
  where 
      isCapturingCtxt lk (VarL _) = False
      isCapturingCtxt lk (Fn _ args) = any (isCapturingCtxtArgPos lk)  args
      isCapturingCtxt lk (Letrec env e) = isCapturingCtxtEnv lk  env || hasHole e
      isCapturingCtxt lk (FlatLetrec env e) = isCapturingCtxtEnv lk  env || hasHole e
      isCapturingCtxt lk (MetaExpression _ _) = False
      isCapturingCtxt lk (Constant _  _) = False
      isCapturingCtxt lk (Hole _)= False

      isCapturingCtxt lk (MetaCtxt sub srt1 name) 
        | (ctxtToString srt1) `elem` lk  = False
        | otherwise              = any (isCapturingCtxt (ctxtToString srt1:lk)) (concat [fromEither a | a <- (grammar cdef) Map.! (ctxtToString srt1) ])
      isCapturingCtxt lk (ConstantCtxt sub srt1 name) 
        | (ctxtToString srt1) `elem` lk  = False 
        | otherwise              = any (isCapturingCtxt (ctxtToString srt1:lk)) (concat [fromEither a | a <- (grammar cdef) Map.! (ctxtToString srt1) ])
      isCapturingCtxt lk (MetaCtxtSubst sub srt1 name expr)   
        | (ctxtToString srt1) `elem` lk  = isCapturingCtxt lk expr
        | otherwise              = (any (isCapturingCtxt ((ctxtToString srt1):lk)) (concat [fromEither a | a <- (grammar cdef) Map.! (ctxtToString srt1) ])) || (isCapturingCtxt ((ctxtToString srt1):lk) expr)
        
      isCapturingCtxt lk (ConstantCtxtSubst sub srt1 name expr)
         | (ctxtToString srt1) `elem` lk  = isCapturingCtxt lk expr
         | otherwise = (any (isCapturingCtxt ((ctxtToString srt1):lk)) (concat [fromEither a | a  <- (grammar cdef) Map.! (ctxtToString srt1) ] )) || (isCapturingCtxt ((ctxtToString srt1):lk) expr)

      isCapturingCtxtEnv lk env =
            (any (\(MetaChain _ _ _ _ e) -> hasHole e || isCapturingCtxt lk e)  $ metaChains env)
        ||  (any (\(ConstantChain _ _ _ _ e) -> hasHole e || isCapturingCtxt lk e)  $ constantChains env)
        ||  (any (\(_ := e) -> hasHole e || isCapturingCtxt lk e) $ bindings env)
      isCapturingCtxtArgPos lk (VarPos _) = False
      isCapturingCtxtArgPos lk (Binder [] e) = isCapturingCtxt lk e
      isCapturingCtxtArgPos lk (Binder vs e) = hasHole e || isCapturingCtxt lk e




  
  
fromEither (Left ((a,_,_,_),(b,_,_,_))) = [a,b]
fromEither (Right (a,_,_,_)) = [a]


setCapturingCtxtSort ctxtsort capturing s@(UserDefinedSort b srt)
  | ctxtsort == srt = (UserDefinedSort capturing srt)
  | otherwise = s
  

setCapturingFrkTable ctxtsort capturing fTable  = Map.fromAscList
  [
  ((setCapturingCtxtSort ctxtsort capturing u,
    setCapturingCtxtSort ctxtsort capturing v),
      map (\(s1,s2,s3,c) -> (setCapturingCtxtSort ctxtsort capturing s1
                            ,setCapturingCtxtSort ctxtsort capturing s2
                            ,setCapturingCtxtSort ctxtsort capturing s3
                            ,setCapExpr ctxtsort capturing c
                            )) list)
  |
    ((u,v),list) <- Map.assocs fTable
  ]
   
setCapturingPfxTable ctxtsort capturing pTable   = Map.fromAscList
  [
  ((setCapturingCtxtSort ctxtsort capturing u,
    setCapturingCtxtSort ctxtsort capturing v),map (\(a,b) -> (setCapturingCtxtSort ctxtsort capturing a,setCapturingCtxtSort ctxtsort capturing b)) list)
  |
     ((u,v),list) <- Map.assocs pTable
  ]



setCapturing ctxtsort grams iscap = 
  map (setCapturingEither ctxtsort iscap) grams
 
setCapturingEither ctxtsort iscap (Left ((a,b,c,d),(a2,b2,c2,d2))) = Left (((setCapExpr ctxtsort iscap a),map (setCapExpr ctxtsort iscap) b,c,setCapNCC ctxtsort iscap d),((setCapExpr ctxtsort iscap a2),map (setCapExpr ctxtsort iscap) b2,c2,setCapNCC ctxtsort iscap d2))
setCapturingEither ctxtsort iscap (Right (a,b,c,d)) = Right ((setCapExpr ctxtsort iscap a),map (setCapExpr ctxtsort iscap) b,c,setCapNCC ctxtsort iscap d)
setCapNCC ctxtsort iscap nccs =  map (setCapOneNCC ctxtsort iscap) nccs
setCapOneNCC ctxtsort iscap (Left e,e') = (Left (setCapExpr ctxtsort iscap e),(setCapExpr ctxtsort iscap e'))
setCapOneNCC ctxtsort iscap (Right e,e') = (Right (setCapEnv ctxtsort iscap e),(setCapExpr ctxtsort iscap e'))

setCapExpr ctxtsort iscap expr = case expr of
                                       VarL _ -> expr
                                       Fn f args -> Fn f (map (setCapArgPos ctxtsort iscap) args)
                                       Letrec env e -> Letrec (setCapEnv ctxtsort iscap env) (setCapExpr ctxtsort iscap e)
                                       FlatLetrec env e -> FlatLetrec (setCapEnv ctxtsort iscap env) (setCapExpr ctxtsort iscap e)
                                       MetaExpression _ _ -> expr
                                       Constant _ _ -> expr
                                       Hole _ -> expr
                                       MetaCtxt sub (UserDefinedSort b srt) name -> if srt == ctxtsort then MetaCtxt sub (UserDefinedSort iscap srt) name else expr
                                       MetaCtxtSubst sub (UserDefinedSort b srt) name e -> if srt == ctxtsort then MetaCtxtSubst sub (UserDefinedSort iscap srt) name (setCapExpr ctxtsort iscap e) 
                                                                                                              else MetaCtxtSubst sub (UserDefinedSort b srt) name (setCapExpr ctxtsort iscap e) 
                                       ConstantCtxt sub (UserDefinedSort b srt) name -> if srt == ctxtsort then ConstantCtxt sub (UserDefinedSort iscap srt) name else expr
                                       ConstantCtxtSubst sub (UserDefinedSort b srt) name e -> if srt == ctxtsort then ConstantCtxtSubst sub (UserDefinedSort iscap srt) name (setCapExpr ctxtsort iscap e) 
                                                                                                              else ConstantCtxtSubst sub (UserDefinedSort b srt) name (setCapExpr ctxtsort iscap e) 
setCapEnv ctxtsort iscap env =       
     let bs = [x:= (setCapExpr ctxtsort iscap e) | x := e <- bindings env]
         ms = [MetaChain sub typ name z (setCapExpr ctxtsort iscap s) | MetaChain sub typ name z s <-  metaChains env]
         cs = [ConstantChain sub typ name z (setCapExpr ctxtsort iscap s) | ConstantChain sub typ name z s <- constantChains env]
     in env{bindings=bs,metaChains=ms,constantChains = cs}
setCapArgPos ctxtsort iscap (VarPos v) = VarPos v
setCapArgPos ctxtsort iscap (Binder vs e) = Binder vs (setCapExpr ctxtsort iscap e)
         
  
type SetOfCtxtVars = [Context]
type SetOfEnvironmentVars =  [EnvVar]
type NonCaptureConstraints = [NonCaptureConstraint] 
type NonCaptureConstraint  = (Either Expression Environment,Context)


showDelta1 [] = ""
showDelta1 xs = intercalate "," (map (\x -> show x ++ "/= []") xs)
showDelta2 [] = ""
showDelta2 xs = intercalate "," (map (\x -> show x ++ "/= {}") xs)
showDelta3 [] = ""
showDelta3 xs = intercalate "," (map (\x -> case x of 
                                                 (Left a,b) -> "(" ++ show a ++ "," ++ show b ++ ")"
                                                 (Right a,b) -> "[" ++ show a ++ "," ++ show b ++ "]"
                                        ) xs)




