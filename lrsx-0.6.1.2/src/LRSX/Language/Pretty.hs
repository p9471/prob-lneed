-----------------------------------------------------------------------------
-- |
-- Module      :  LRSX.Language.Pretty
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--  
-- Pretty printing of unifcation problems, overlaps and diagrams

-----------------------------------------------------------------------------
--
module LRSX.Language.Pretty where
import Data.List
import Data.Maybe
import Data.Either
import Data.Char
import Data.Function
import LRSX.Language.Syntax
import LRSX.Util.Util
class Pretty a where 
 pretty :: a ->  String

instance Pretty ArgPos where 
 pretty (Binder [] e)  = pretty e 
 pretty (Binder v e)   =
   let prf = (intercalate "." (map show v)) ++ "." 
   in  prf ++ ((intercalate ("\n" ++ replicate (length prf) ' ') . lines) (pretty e)) 
 pretty (VarPos v)     = show v

instance Pretty Expression where
 pretty (VarL v)        = "(var " ++ show v ++ ")"
 pretty (Fn f [])       = f
 pretty (Fn f args)     = 
  "(" ++ f   ++ " " ++ (concat (intersperse " " (map (\a -> (intercalate ("\n" ++ replicate (2+length f) ' ').lines) (pretty a) ) args))) ++ ")"
 pretty (Letrec env e)  = 
     let oneLine =  "(letrec " ++ pretty env ++ " in " ++ show e ++ ")"
     in if length oneLine <= 80 
           then oneLine 
           else
            unlines
             ["(letrec"
             , "   " ++ (intercalate "\n  ;" (map (intercalate "\n   " . lines) (prettyEnv env)))
             ,"in"
             , "   " ++ (intercalate "\n   " . lines ) (pretty e)
             ,")"
             ]
 pretty (FlatLetrec env e)  = 
     let oneLine =  "(flatrec " ++ pretty env ++ " in " ++ show e ++ ")"
     in if length oneLine <= 80 
           then oneLine 
           else
            unlines
             ["(flatrec"
             , "   " ++ (intercalate "\n  ;" (map (intercalate "\n   " . lines) (prettyEnv env)))
             ,"in"
             , "   " ++ (intercalate "\n   " . lines ) (pretty e)
             ,")"
             ]
      
 pretty (MetaExpression subst e) = 
   let prf = e
   in prf  ++ ((intercalate ("\n" ++ (replicate (length prf) ' ')) . lines) (pretty subst))
 pretty (Hole subst) = 
   let prf = "[.]"
   in prf  ++ ((intercalate ("\n" ++ (replicate (length prf) ' ')) . lines) (pretty subst))
 pretty (MetaCtxt subst srt  c) = 
   let prf = c
   in prf  ++ ((intercalate ("\n" ++ (replicate (length prf) ' ')) . lines) (pretty subst))
 pretty (MetaCtxtSubst subst srt c s) = 
   let prf = c
       psub = ((intercalate ("\n" ++ (replicate (length prf) ' ')) . lines) (pretty subst))
       l = maximum $ map length $ lines psub
   in  prf  ++ psub ++ "[" ++ (intercalate ("\n" ++ (replicate (length prf + 2) ' ')) (lines (pretty s))) ++ "]"
 pretty (Constant subst e) = 
   let prf = e ++ "*"
   in prf  ++ ((intercalate ("\n" ++ (replicate (length prf) ' ')) . lines) (pretty subst))
 pretty (ConstantCtxt subst srt  c) = 
   let prf = c ++ "*"
   in prf  ++ ((intercalate ("\n" ++ (replicate (length prf) ' ')) . lines) (pretty subst))
 pretty (ConstantCtxtSubst subst srt c s) =
   let prf = c ++ "*"
       psub = ((intercalate ("\n" ++ (replicate (length prf) ' ')) . lines) (pretty subst))
       l = maximum $ map length $ lines psub
   in  prf  ++ psub ++ "[" ++ (intercalate ("\n" ++ (replicate (length prf  + 2) ' ')) (lines (pretty s))) ++ "]"


instance Pretty Binding where
 pretty (a := b) = let stra = show a ++ "= "
                   in  stra ++ (intercalate ("\n" ++ replicate (length stra) ' ')  (lines (pretty b)))

instance Pretty ChainVar where

  pretty (MetaChain subst (UserDefinedChain k) s v e)       = 
     let prf = s ++ "^" ++ k
         psub = ((intercalate ("\n" ++ (replicate (length prf) ' ')) . lines) (pretty subst))
         l = maximum $ map length $ lines psub
     in  prf  ++ psub ++ "[" ++ show v  ++ "," ++
           (intercalate ("\n" ++ (replicate (1+length prf  + length (show v)) ' ')) (lines (pretty e))) ++ "]"
  
  pretty (MetaChain subst VarChain s v e)     = 
     let prf = s 
         psub = ((intercalate ("\n" ++ (replicate (length prf) ' ')) . lines) (pretty subst))
         l = maximum $ map length $ lines psub
     in  prf  ++ psub ++ "|" ++ show v  ++ "," ++
           (intercalate ("\n" ++ (replicate (1+length prf + length (show v)) ' ')) (lines (pretty e))) ++ "|"

 
        
  pretty (ConstantChain subst (UserDefinedChain k) s v e)   =  
     let prf = s ++ "*" ++ "^" ++ k
         psub = ((intercalate ("\n" ++ (replicate (length prf) ' ')) . lines) (pretty subst))
         l = maximum $ map length $ lines psub
     in  prf  ++ psub ++ "[" ++ show v  ++ "," ++
           (intercalate ("\n" ++ (replicate (1+length prf + length (show v)) ' ')) (lines (pretty e))) ++ "]"
           
  pretty (ConstantChain subst VarChain s v e) = 
     let prf = s ++ "*"
         psub = ((intercalate ("\n" ++ (replicate (length prf) ' ')) . lines) (pretty subst))
         l = maximum $ map length $ lines psub
     in  prf  ++ psub ++ "|" ++ show v  ++ "," ++
           (intercalate ("\n" ++ (replicate (1+length prf + length (show v)) ' ')) (lines (pretty e))) ++ "|"
 
instance Pretty Environment where
 pretty env =
  let bs = map show $ sort $ bindings env
      ms = map show $ sort $ metaEnvVars env
      mss = map (\x -> x ++ "*") $ map show $ sort $ constantEnvVars env
      mc = map show $ sort $ metaChains env
      mmc =  map show $ sort $ constantChains env
      all =  bs ++ ms ++ mc ++ mss ++ mmc
   in if null all then "{}" else "{" ++ (concat $ intersperse ";" all) ++ "}"
   
prettyEnv env =
  let bs = map pretty $ sort $ bindings env
      ms = map pretty $ sort $ metaEnvVars env
      mss = map (\(Env s n) -> pretty $ Env s (n++"*")) $ sort $ constantEnvVars env
      mc = map pretty $ sort $ metaChains env
      mmc =  map pretty $ sort $ constantChains env
      all =  bs ++ ms ++ mc ++ mss ++ mmc
   in all
   
instance Pretty Substitution where
  pretty sub 
    | null (substList sub) = ""
  pretty sub = 
    let oneLine = "<" ++ (intercalate "," (map pretty (substList sub))) ++ ">"
    in if length oneLine <= 40 then oneLine else 
          "<" ++ (intercalate "\n ," (map (intercalate "\n  ".lines.pretty) (substList sub))) ++ "\n>\n\n"

    
--
instance Pretty SubstItem where
  pretty (SubstSet set) = 
    let oneLine = "{" ++ (intercalate "," (map show set))  ++ "}"
    in if length oneLine <= 40 then oneLine else 
            "{" 
             ++ (intercalate "\n ," (map show set))  
             ++ "\n}"
  pretty x = show x             
    
instance Pretty EnvVar where
  pretty (Env subst s) = 
   let prf = s
   in prf  ++ ((intercalate ("\n" ++ (replicate (length prf) ' ')) . lines) (pretty subst))
