-----------------------------------------------------------------------------
-- | 
-- Module      :  LRSX.Language.Fresh
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--
--  Data types for the internal representation 
--  of unification expressions
--
-----------------------------------------------------------------------------
module LRSX.Language.Fresh  where
import Data.List
import Data.Maybe
import LRSX.Util.Util
import LRSX.Language.Syntax
import qualified Data.Set as Set


makeExprFresh expr fresh =
 let metavars          = (computeMetaVars expr)
     alllamlet         = nub (liftvars metavars)
     (mLamLetVars1,f2) = zipWithFresh (\x y -> (MapVarLift $ MetaVarLift emptySubstitution x :|-> MetaVarLift emptySubstitution ("X" ++ y))) alllamlet fresh
     (mEnvVars,f3)     = zipWithFresh (\x y -> MapEnv $  x :|-> metaEnv (Env emptySubstitution ("E" ++ y))) (map (Env emptySubstitution) $ nub $ envvars metavars) f2
     mEnvVarsShrink    = zip (map (Env emptySubstitution) $ nub $ envvars metavars) (map (\x -> (Env emptySubstitution ("E" ++ x))) f2)
     (mExprVars,f4)    = zipWithFresh (\x y -> MapExpr $ MetaExpression emptySubstitution x :|-> MetaExpression emptySubstitution ("S" ++ y)) (nub $ exprvars metavars) f3
     (mCtxtVars,f5)    = zipWithFresh (\(MetaCtxt sub srt x) y -> MapExpr $ MetaCtxt sub srt x :|-> MetaCtxt sub srt ((ctxtToString srt) ++y)) (nub $ computeMetaContexts expr) f4
     (mChainVars,f6)   = zipWithFresh (\x y -> (x,y)) (nub $ chainvars metavars) f5
     substitution = (mLamLetVars1 ++ mExprVars ++ mEnvVars ++ mCtxtVars)
     substituteEnvs subs []      = []
     substituteEnvs subs (z:zs)
        | Just y <- lookup z subs = y:(substituteEnvs subs zs)
        | otherwise               = z:(substituteEnvs subs zs)
 in (subsEE mChainVars (applySolution substitution expr),f6) 
 

makeExprWithConstraintsFresh (expr,delta1,delta2,delta3) fresh =
 let metavars          = (computeMetaVars expr) `combineMetaVars` (computeMetaVarsConst (delta1,delta2,delta3))
     alllamlet         = nub (liftvars metavars)
     (mLamLetVars1,f2) = zipWithFresh (\x y -> (MapVarLift $ MetaVarLift emptySubstitution x :|-> MetaVarLift emptySubstitution ("X" ++ y))) alllamlet fresh
     (mEnvVars,f3)     = zipWithFresh (\x y -> MapEnv $  x :|-> metaEnv (Env emptySubstitution ("E" ++ y))) (map (Env emptySubstitution) $ nub $ envvars metavars) f2
     mEnvVarsShrink    = zip (map (Env emptySubstitution) $ nub $ envvars metavars) (map (\x -> (Env emptySubstitution ("E" ++ x))) f2)
     (mExprVars,f4)    = zipWithFresh (\x y -> MapExpr $ MetaExpression emptySubstitution x :|-> MetaExpression emptySubstitution ("S" ++ y)) (nub $ exprvars metavars) f3
     (mCtxtVars,f5)    = zipWithFresh (\(MetaCtxt sub srt x) y -> MapExpr $ MetaCtxt sub srt x :|-> MetaCtxt sub srt ((ctxtToString srt) ++y)) (nub $ computeMetaContexts expr) f4
     (mChainVars,f6)   = zipWithFresh (\x y -> (x,y)) (nub $ chainvars metavars) f5
     substitution = (mLamLetVars1 ++ mExprVars ++ mEnvVars ++ mCtxtVars)
     substituteEnvs subs []      = []
     substituteEnvs subs (z:zs)
        | Just y <- lookup z subs = y:(substituteEnvs subs zs)
        | otherwise               = z:(substituteEnvs subs zs)
     computeMetaVarsConst (delta1,delta2,delta3) = combineMetaVarsList [computeMetaVarsDelta1 delta1,computeMetaVarsDelta2 delta2,computeMetaVarsDelta3 delta3]
     computeMetaVarsDelta3 l = combineMetaVarsList (map computeMetaVarsNCC l)
     computeMetaVarsNCC (Left e,ctxt) = (computeMetaVars e) `combineMetaVars` (computeMetaVars ctxt)
     computeMetaVarsNCC (Right e,ctxt) = (computeMetaVars e) `combineMetaVars` (computeMetaVars ctxt)
     computeMetaVarsDelta2 del2 = emptyMVars {envvars = [e | Env sub e <- del2]}
     computeMetaVarsDelta1 del1 = combineMetaVarsList (map computeMetaVars del1)
     nonEmptyCtxts = map (subsEE mChainVars) $ applySolution substitution delta1
     nonEmptyEnvs = substituteEnvs mEnvVarsShrink delta2
     nonCapCon = map (subsEE mChainVars) $ applySolution substitution delta3
     expr' = subsEE mChainVars (applySolution substitution expr)
 in ((expr',nonEmptyCtxts,nonEmptyEnvs,nonCapCon),f6)
 
 
{-
h ConstrainedRule{ruleName=name,rule=theRule,nonEmptyCtxts=ctxts,nonEmptyEnvs=envs,nonCapCon=capcon,priority=prio,standardReduction=st,answerRule=ar} fresh =
 let metavars          = (computeMetaVars theRule)
                            `combineMetaVars` (computeMetaVars (Delta {delta1=ctxts,delta2 =envs, delta3 =capcon,delta4=emptyGamma}))
     alllamlet         = nub (liftvars metavars)
     (mLamLetVars1,f2) = zipWithFresh (\x y -> (MapVarLift $ MetaVarLift emptySubstitution x :|-> MetaVarLift emptySubstitution ("X" ++ y))) alllamlet fresh
     (mEnvVars,f3)     = zipWithFresh (\x y -> MapEnv $  x :|-> metaEnv (Env emptySubstitution ("E" ++ y))) (map (Env emptySubstitution) $ nub $ envvars metavars) f2
     mEnvVarsShrink    = zip (map (Env emptySubstitution) $ nub $ envvars metavars) (map (\x -> (Env emptySubstitution ("E" ++ x))) f2)
     (mExprVars,f4)    = zipWithFresh (\x y -> MapExpr $ MetaExpression emptySubstitution x :|-> MetaExpression emptySubstitution ("S" ++ y)) (nub $ exprvars metavars) f3
     (mCtxtVars,f5)    = zipWithFresh (\(MetaCtxt sub srt x) y -> MapExpr $ MetaCtxt sub srt x :|-> MetaCtxt sub srt ((ctxtToString srt) ++y)) (nub $ computeMetaContexts theRule) f4
     (mChainVars,f6)   = zipWithFresh (\x y -> (x,y)) (nub $ chainvars metavars) f5
     substitution = (mLamLetVars1 ++ mExprVars ++ mEnvVars ++ mCtxtVars)
     substituteEnvs subs []      = []
     substituteEnvs subs (z:zs)
        | Just y <- lookup z subs = y:(substituteEnvs subs zs)
        | otherwise               = z:(substituteEnvs subs zs)
 in 
    ((ConstrainedRule {ruleName=name
                      ,priority=prio
                      ,standardReduction=st
                      ,answerRule=ar
                      ,rule = subsEE mChainVars (applySolution substitution theRule)
                      ,
                      ,nonEmptyEnvs = substituteEnvs mEnvVarsShrink envs
                      ,nonCapCon = map (subsEE mChainVars) $ applySolution substitution capcon
                      }),f6) -}
 
-- a type class for substitution of EE-variables, which is mainly used
-- during fresh renaming of rules
class SubsEE a where
 subsEE :: [(String,String)] -> a -> a
instance (SubsEE a, SubsEE b) => SubsEE (a, b) where
 subsEE subs (a,b) = (subsEE subs a,subsEE subs b)
instance (SubsEE a, SubsEE b) => SubsEE (Either a b) where
 subsEE subs (Left a) = Left (subsEE subs a)
 subsEE subs (Right a) = Right (subsEE subs a)
 
instance SubsEE ArgPos where
 subsEE subs (Binder v e) = Binder v (subsEE subs e)
 subsEE subs (VarPos v ) = VarPos v
instance SubsEE Expression where
 subsEE subs (VarL v) = VarL v
 subsEE subs (Fn f arg) = Fn f $ map (subsEE subs) arg
 subsEE subs (Letrec env e) = Letrec (subsEE subs env) (subsEE subs e)
 subsEE subs (FlatLetrec env e) = FlatLetrec (subsEE subs env) (subsEE subs e)
 subsEE subs (MetaExpression s e) = MetaExpression s e
 subsEE subs (Hole s) = (Hole s)
 subsEE subs (MultiHole s) = (MultiHole s)  
 subsEE subs (MetaCtxt sub srt  c) = MetaCtxt sub srt c
 subsEE subs (MetaCtxtSubst sub srt c s) = (MetaCtxtSubst sub srt c (subsEE subs s))
instance SubsEE Binding where
 subsEE sub (x := e) = (x := subsEE sub e)
instance SubsEE ChainVar where
 subsEE subs (MetaChain sub typ ee x s)
  | Just ee' <- lookup ee subs = MetaChain sub typ ((case typ of {VarChain -> "VV"++ee'; UserDefinedChain v -> ("EE" ++ ee')})) x s
  | otherwise = MetaChain sub typ ee x s
instance SubsEE Environment where
 subsEE subs env = 
  env {bindings = map (subsEE subs) (bindings env)
      ,metaChains = map (subsEE subs) (metaChains env)
     }
 
 

class ApplySolution a where
  applySolution :: Solution -> a -> a
    
   
instance ApplySolution a => ApplySolution [a] where
    applySolution sol = map (applySolution sol)
instance (ApplySolution a, ApplySolution b) => ApplySolution (a,b) where
    applySolution s (a,b) = (applySolution s a, applySolution s b)

    
instance (ApplySolution a, ApplySolution b) => ApplySolution (Either a b) where
    applySolution s (Left a) = Left $ applySolution s a
    applySolution s (Right a) = Right $ applySolution s a

    
          


 
instance ApplySolution VarLift where
 applySolution = applySolutionGen
 
instance ApplySolution Binding where
 applySolution = applySolutionGen
  
instance ApplySolution Environment where
 applySolution = applySolutionGen
   
instance ApplySolution Expression where    
 applySolution = applySolutionGen
 
applySolutionGen [] expr = expr
applySolutionGen (m@(MapVarLift (l :|-> r)):maps') expr = 
   applySolutionGen maps' (subst r l expr)
applySolutionGen (m@(MapExpr (l :|-> r)):maps') expr = 
   applySolutionGen maps' (subst r l expr)
applySolutionGen (m@(MapEnv ((Env sub l) :|-> r)):maps') expr
    | isEmpty r = applySolutionGen maps' (substEmpty l expr) 
    | otherwise = applySolutionGen maps' (substEnv r l expr)
applySolutionGen(m@(MapEnvSpec (l :|-> r)):maps') expr
    | isEmpty r = applySolutionGen maps' (substEmpty l expr) 
    | otherwise = applySolutionGen maps' (substEnv r l expr)
applySolutionGen(m@(MapCV (l :|-> r)):maps') expr =
    applySolutionGen maps' (substChainVar r l expr) 
applySolutionGen(m@(MapConstraint (l :|-> r)):maps') expr
    | otherwise = applySolutionGen maps' expr -- (substEnvConstraint r l expr)
        
   
-- ============================================================================

lookupMapVarLift v ((MapVarLift (l :|-> r)):maps) 
 | v == l = Just r
 | otherwise = lookupMapVarLift v maps
lookupMapVarLift v (m:ms) = lookupMapVarLift v ms
lookupMapVarLift v [] = Nothing
 
 
lookupMapExpr v ((MapExpr (l :|-> r)):maps) 
 | v == l = Just r
 | otherwise = lookupMapExpr v maps
lookupMapExpr v (m:ms) = lookupMapExpr v ms
lookupMapExpr v [] = Nothing

lookupMapEnv v ((MapEnv ((Env sub l) :|-> r)):maps)
 | v == l = Just r
 | otherwise = lookupMapEnv v maps
lookupMapEnv v ((MapEnvSpec (l :|-> r)):maps)
 | v == l = Just r
 | otherwise = lookupMapEnv v maps
lookupMapEnv v (m:ms) = lookupMapEnv v ms
lookupMapEnv v [] = Nothing


lookupMapCV (MetaChain sub typ ee z s) ((MapCV ((MetaChain sub' typ' ee' z' s') :|-> r)):maps)
 | ee == ee'  = Just r
lookupMapCV v (m:ms) = lookupMapCV v ms
lookupMapCV v [] = Nothing
 

lookupMapConstraint v ((MapConstraint (l :|-> r)):maps)
 | null [b | b <- bindings v, b `notElem` (bindings l)]
 , null [b | b <- metaEnvVars v, b `notElem` (metaEnvVars l)]
 , null [b | b <- metaChains v, b `notElem` (metaChains l)]
 , null [b | b <- constantEnvVars v, b `notElem` (constantEnvVars l)]
 , null [b | b <- constantChains v, b `notElem` (constantChains l)] = Just (l,r)
 | otherwise = lookupMapConstraint v maps
lookupMapConstraint v (_:maps) = lookupMapConstraint v maps
lookupMapConstraint v [] = Nothing




-- compute all meta variables

-- metaVarsVar (MetaVarLift sub name) = [(SCVar $ MetaVarLift sub name)]
-- metaVarsVar (ConstantVarLift sub name) = [(SCVar $ ConstantVarLift sub name)]
-- metaVarsVar (VarLift sub name) = []

-- metaVarsArgPos (VarPos _) = []
-- metaVarsArgPos (Binder v e) = (concatMap metaVarsVar v) ++ (metaVars e)

-- metaVars (VarL x) = metaVarsVar x
-- metaVars (Fn _ args) = concatMap metaVarsArgPos args
-- metaVars (Letrec env e) = (metaVars e) ++ (metaVarsEnv env)
-- metaVars e@(MetaExpression sub name) = [SCExpr e]
-- metaVars e@(Constant _ _) = [SCExpr e]
-- metaVars (Hole _) = []
-- metaVars d@(MetaCtxt _ _ _) = [SCCtxt d]
-- metaVars d@(ConstantCtxt _ _ _) = [SCCtxt d]
-- metaVars (MetaCtxtSubst a b c expr) = (SCCtxt (MetaCtxt a b c)):(metaVars expr)
-- metaVars (ConstantCtxtSubst a b c expr) = (SCCtxt (ConstantCtxt a b c)):(metaVars expr)

-- metaVarsEnv env =
      -- (concat [ (metaVarsVar x) ++ metaVars e |  (x := e) <- bindings env])
   -- ++ (concat [ (SCMetaChain sub typ name):(metaVarsVar z ++ metaVars s) | (MetaChain sub typ name z s) <- metaChains env])
   -- ++ (concat [ (SCConstantChain sub typ name):(metaVarsVar z ++ metaVars s) | (ConstantChain sub typ name z s) <- constantChains env])
   -- ++ [ (SCMetaEnv v) | v@(Env sub e) <- metaEnvVars env]
   -- ++ [ (SCConstantEnv v) | v@(Env sub e) <- constantEnvVars env]


   


-- ============================================================================
-- Solution: Several mappings from meta-variables to objects
-- ============================================================================
-- The solution is a list of mappings
type Solution = [SolutionEntry]

-- several types of mappings
data SolutionEntry = MapVarLift  (Mapping VarLift VarLift)
                   | MapVarLet  (Mapping VarLift VarLift)
                   | MapExpr    (Mapping Expression Expression)
                   | MapEnv     (Mapping EnvVar Environment) -- String is the name of the MetaEnv                  
                   | MapCV      (Mapping ChainVar (VarLift -> Expression -> Environment))                  
                   | MapEnvSpec (Mapping String Environment) -- for mappings to -:s and x.-
                   | MapConstraint (Mapping Environment Environment)
-- a mapping is more or less a pair 
data Mapping a b = a :|-> b
 
-- Show-instances
instance (Show a, Show b) => Show (Mapping a b) where
 show (a :|-> b) = show a ++ " |-> " ++ show b

instance Show SolutionEntry where
  show (MapVarLift m) = show m
  show (MapVarLet m) = show m
  show (MapExpr m) = show m
  show (MapEnv (a :|-> b)) = show a ++ " |-> "  ++ show b
  show (MapCV (l:|-> r)) = showCV l ++ "|->" ++ show (r (MetaVarLift emptySubstitution "[.1]") (MetaExpression emptySubstitution "[.2]")) 
    where
      showCV (MetaChain sub VarChain s v e)     = s ++ "|" ++ ".1" ++ "," ++ ".2" ++ "|"
      showCV (MetaChain sub _ s v e)       = s ++ "[" ++ ".1" ++ "," ++ ".2" ++ "]"
      showCV (ConstantChain sub VarChain s v e) = s ++ "(cnst)|" ++ ".1" ++ "," ++ ".2" ++ "|"
      showCV (ConstantChain sub _ s v e)   = s ++ "(cnst)[" ++ ".1" ++ "," ++ ".2" ++ "]"      
  show (MapEnvSpec (l :|-> r)) = show l ++ "|->" ++ show r
  show (MapConstraint (l :|-> r)) = show l ++ "|->" ++ show r


-- Substitute-instances
instance (Substitute b) => Substitute (Mapping a b) where
 subst e1 e2 (a :|-> b)         = a :|-> subst e1 e2 b
 substEmpty n (a :|-> b)        = a :|-> substEmpty n b 
 substEnv env n (a :|-> b)      = a :|-> substEnv env n b 
 substChainVar env n (a :|-> b) = a :|-> (substChainVar env n b)
 substEnvConstraint env n (a :|-> b) = a :|-> (substEnvConstraint env n b)

instance Substitute SolutionEntry where
 subst e1 e2 (MapVarLift m)         = MapVarLift (subst e1 e2 m) 
 subst e1 e2 (MapExpr m)           = MapExpr (subst e1 e2 m) 
 subst e1 e2 (MapEnv m)            = MapEnv (subst e1 e2 m)
 subst e1 e2 (MapEnvSpec m)        = MapEnvSpec (subst e1 e2 m)
 subst e1 e2 (MapConstraint m)     = MapConstraint (subst e1 e2 m)
 subst e1 e2 (MapCV (a :|-> fn))   = MapCV (a :|-> \z s -> (subst e1 e2 (fn z s)))
 -- 
 substEnv e1 e2 (MapVarLift m)      = MapVarLift (substEnv e1 e2 m) 
 substEnv e1 e2 (MapExpr m)        = MapExpr (substEnv e1 e2 m) 
 substEnv e1 e2 (MapEnv m)         = MapEnv (substEnv e1 e2 m)
 substEnv e1 e2 (MapEnvSpec m)     = MapEnvSpec (substEnv e1 e2 m)
 substEnv e1 e2 (MapConstraint m)     = MapConstraint (substEnv e1 e2 m)
 substEnv e1 e2 (MapCV (a :|-> fn))= MapCV (a :|-> \z s -> (substEnv e1 e2 (fn z s)))
 --
 substEmpty e2 (MapVarLift m)       = MapVarLift (substEmpty e2 m) 
 substEmpty e2 (MapExpr m)         = MapExpr (substEmpty e2 m) 
 substEmpty e2 (MapEnv m)          = MapEnv (substEmpty e2 m)
 substEmpty e2 (MapEnvSpec m)      = MapEnvSpec (substEmpty e2 m)
 substEmpty e2 (MapConstraint m)      = MapConstraint (substEmpty e2 m)
 substEmpty e2 (MapCV (a :|-> fn)) = MapCV (a :|-> \z s -> (substEmpty e2 (fn z s)))
 --
 substChainVar e1 e2 (MapVarLift m)  = MapVarLift (substChainVar e1 e2 m) 
 substChainVar e1 e2 (MapExpr m)    = MapExpr (substChainVar e1 e2 m) 
 substChainVar e1 e2 (MapEnv m)     = MapEnv (substChainVar e1 e2 m)
 substChainVar e1 e2 (MapEnvSpec m) = MapEnvSpec (substChainVar e1 e2 m)
 substChainVar e1 e2 (MapConstraint m) = MapConstraint (substChainVar e1 e2 m)
 substChainVar e1 e2 (MapCV (a :|-> fn))  = MapCV (a :|-> \z s -> (substChainVar e1 e2 (fn z s)))
  
 substEnvConstraint e1 e2 (MapVarLift m)      = MapVarLift (substEnvConstraint e1 e2 m) 
 substEnvConstraint e1 e2 (MapExpr m)        = MapExpr (substEnvConstraint e1 e2 m) 
 substEnvConstraint e1 e2 (MapEnv m)         = MapEnv (substEnvConstraint e1 e2 m)
 substEnvConstraint e1 e2 (MapEnvSpec m)     = MapEnvSpec (substEnvConstraint e1 e2 m)
 substEnvConstraint e1 e2 (MapConstraint m)  = MapConstraint (substEnvConstraint e1 e2 m)
 substEnvConstraint e1 e2 (MapCV (a :|-> fn)) = MapCV (a :|-> \z s -> (substEnvConstraint e1 e2 (fn z s)))
-- ---------------------------
-- several helper functions
-- ---------------------------

-- composition of the solution, iteratively apply the 
-- substitutions to the solution

composeAll :: Solution -> Solution  
composeAll [] = []  
composeAll maps = let (a:rest) = reverse maps 
                  in reverse $ go [a] rest 
 where 
  go maps [] = maps
  go maps (m@(MapVarLift (l :|-> r)):maps') = 
   go (m:(subst r l maps)) maps'
  go maps (m@(MapExpr (l :|-> r)):maps') = 
   go (m:(subst r l maps)) maps'
  go maps (m@(MapEnv ((Env  sub l) :|-> r)):maps') 
    | isEmpty r                    = go (m:substEmpty l maps) maps'
    | otherwise                    = go (m:(substEnv r l maps)) maps'
  go maps (m@(MapEnvSpec (l :|-> r)):maps') 
    | isEmpty r                    = go (m:substEmpty l maps) maps'
    | otherwise                    = go (m:(substEnv r l maps)) maps'
  go maps (m@(MapCV (l :|-> r)):maps') = 
    go (m:substChainVar r l maps) maps'
  go maps (m@(MapConstraint (l :|-> r)):maps') = 
   go (m:(substEnvConstraint r l maps)) maps'
-- --------------------------------------------------
-- applying a solution, implemented via a type class
-- --------------------------------------------------
   