-----------------------------------------------------------------------------
-- | 
-- Module      :  LRSX.Language.Syntax
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--
-- This module contains the data types and utility function for the internal representation  of LRSX-expressions,
-- where also symbolic alpha renamings and computations of abstract sets (e.g. using 'SyntaxComponent's) are included.
--
-----------------------------------------------------------------------------
module LRSX.Language.Syntax (
-- * Expressions
-- ** The 'Expression' type
    Expression(..)
  , FName
  , Name
-- ** Distinguish between expressions and contexts
  , hasHole
  , isHole
  , isExpr
  , isMetaCtxtSubst
-- ** Compute all variables occuring in an expression
  , allVariables
-- ** Compute all letrec-environments occuring in an expression  
  , getEnvironments
-- ** drop all symbolic alpha renamings  
  , dropAllSubsExpr
-- ** check the let variable convention  
  , checkLVC
-- ** checks for the subterm-relation
  , subTermOf
  , properSubTermOf
 
-- ** Variables
  , VarLift(..)
  , varName
-- ** Arguments to function symbols
  , ArgPos(..)
-- ** Letrec-Environments  
  , Environment(..)
  , isEmpty
  , empty
  , allVariablesInEnv
  , checkLVCEnv
  , hasBinding
  , hasMetaChain
  , binding
  , metaEnv
  , isMetaEnv
-- *** Letrec-Bindings
  , Binding(..)
-- *** Meta-Variables for Environments
  , EnvVar(..)
-- *** Chain-Variables
  , ChainVar(..)
  , ChainType(..)
  , prefixEE 
-- * Contexts  
  , Context
  , CtxtSort(..)
  , IsCapturing
  , capturedInHole
  , hole
  , ctxtToString
  , HasMetaContexts(..)  
-- * Storing Information on defined function symbols
  , FunInfo 
  , Arity
  , BindArity
  , HasFnSymbol(..)
  
  , nubFnSyms
-- * Symbolic Alpha Renamings  
  , Substitution(..)
  , SubstItem(..)
  , Index
  , emptySubstitution
  , fromLVCV
  , noneToCVVars  
  , noneToLetVars
  , isEmptySubstitution
  , append
  , toSubstSet
-- * Syntax Components  
  , SyntaxComponent(..)
  , dropSubstFromComp
  , getAllVarsFromExpr
  , getAllSCVarsFromSubstitution  
  , isFreshSym
  , getSubstFromSC  
  , metaVars
  , getSCAllVarsAndAlpha
  , getSCCapVarsAndAlpha
  , getNCCsEnvLVC  
  , SyntaxComponents(..)
-- * Meta-Variables  
  , MetaVars(..)
  , emptyMVars
  , combineMetaVars
  , combineMetaVarsList
  , addMetaVars
  , HasMetaVars(..)
-- * Structural similarity of expressions
  , Skeleton(..)
  , SkeletonArgPos(..)
  , SkeletonVarLift(..)
  , fingerprint
-- * Substitutions
  , Substitute(..)
-- * Constantification 
  , Constantify(..)
  ) where
import Data.List
import Data.Maybe
import LRSX.Util.Util
import Data.Char
import qualified Data.Set as Set




-- ============================================================================
-- Some type synonyms
-- ============================================================================


-- Variables:
type Name = String
-- Names of function symbols
type FName = String



-- | Variables come with a subsitution and are either concrete ones, or meta variables.
--   There are two kinds of meta variables: instantiable ones ('MetaVarLift') and fixed ones ('ConstantVarLift')

data VarLift = 
    VarLift Substitution Name         -- ^ a concrete variable with a subsitution and its name 
  | MetaVarLift Substitution Name     -- ^ an instantiable meta-variable 
  | ConstantVarLift Substitution Name -- ^ a fixed meta-variable
 deriving(Eq)

 
instance Ord VarLift where
 (VarLift sub1 v1) <= (VarLift sub2 v2) = if v1 == v2 then sub1 <= sub2 else v1 <= v2
 (VarLift sub1 v1) <= (MetaVarLift sub2 v2) = True
 (VarLift sub1 v1) <= (ConstantVarLift sub2 v2) = True
 (MetaVarLift sub1 v1) <= (VarLift sub2 v2) = False
 (MetaVarLift sub1 v1) <= (MetaVarLift sub2 v2) = if v1 == v2 then sub1 <= sub2 else v1 <= v2
 (MetaVarLift sub1 v1) <= (ConstantVarLift sub2 v2) = True
 (ConstantVarLift sub1 v1) <= (VarLift sub2 v2) = False
 (ConstantVarLift sub1 v1) <= (MetaVarLift sub2 v2) = False
 (ConstantVarLift sub1 v1) <= (ConstantVarLift sub2 v2) = if v1 == v2 then sub1 <= sub2 else v1 <= v2

-- | A 'Context' is a synonym for an 'Expression'
type Context = Expression

-- | Expressions may be lifted variables ('VarLift'), a function symbol applied to higher-order expressions ('Fn'), letrec expressions ('Letrec'),
--   an instantiable meta-variable for expressions ('MetaExpression'), a fixed meta-variable for expressions ('Constant'),
--   an instantiable context with a filled hole ('MetaCtxtSubst'), a fixed context with a filled hole ('ConstantCtxtSubst'),
--   a context hole ('HOLE'), an instantiable context-variable ('MetaCtxt'), or a fixed context-variable ('ConstantCtxt').
--
data Expression = 
    VarL VarLift                                              -- ^ Variables lifted to expressions
  | Fn FName [ArgPos]                                         -- ^ Function symbols, name, arguments, where arguments are expressions or binders
  | Letrec Environment Expression                             -- ^ Letrec env inexp
  | FlatLetrec Environment Expression                         -- ^ Letrec where subexpressions are letrec-free
  | MetaExpression Substitution Name                        -- ^ for meta variables representing expressions (S)
  | Hole Substitution                                         -- ^ for the empty context, the substitution is irrelevant at the moment
  | MultiHole Int                                             -- ^ a hole of a multi context
  | MetaCtxt Substitution CtxtSort Name                     -- ^ for meta variables representing contexts     (C,T,A)
  | MetaCtxtSubst Substitution CtxtSort Name Expression     -- ^ ContextSubstitution, e.g. @A[s] = MetaCtxtSubst ACtxt \"A\" s@
  | ConstantCtxt Substitution CtxtSort Name                 -- ^ a fixed constant meta variable (relevant for matching)
  | ConstantCtxtSubst Substitution CtxtSort Name Expression -- ^ a fixed context substitution (relevant for matching)
  | Constant Substitution Name                              -- ^ a fixed S-variable for expressions (relevant for matching)
 deriving(Eq)


-- | Argument-Positions may be higher-order expressions ('Binder') or variable positions ('VarPos')
data ArgPos = 
   Binder [VarLift] Expression -- ^ a higher order expression 
 | VarPos VarLift              -- ^ a variable position
 deriving(Eq)

    
-- | An environment is a set of 5 different kind of components, and thus it has 5 arguments, which are lists of the corresponding entries

data Environment = 
 Environment { bindings         :: [Binding],  -- a list of bindings
               metaEnvVars      :: [EnvVar],   -- a list of instantiable meta-variables for environments
               metaChains       :: [ChainVar], -- a list of instantiable meta-variables for chains
               constantEnvVars  :: [EnvVar],   -- a list of fixed meta-variables for environments
               constantChains   :: [ChainVar]  -- a list of fixed meta-variabled for chains
              }
                  
    
-- Bindings:
-- =========
data Binding = VarLift := Expression
 deriving(Eq)


data EnvVar = Env Substitution Name 
 deriving(Eq)
 
instance Ord EnvVar where
  (Env sub1 x1) <= (Env sub2 x2) 
    | x1 == x2 = sub1 <= sub2
    | otherwise = x1 <= x2
    
instance Show EnvVar where
  show (Env sub s) = show sub ++ s


-- Chain Variables
-- =============== 
data ChainVar = MetaChain Substitution ChainType Name VarLift Expression     -- EE[x,s] == MetaChain sub srt "EE" x s
              | ConstantChain Substitution ChainType Name VarLift Expression -- only used for matching
 deriving(Eq)
 
data ChainType = VarChain -- ^  a chain of variable-to-variable bindings
               | UserDefinedChain Name -- a chain of variable-to-context-with-variable bindings where the context class is determined by the given name
 deriving(Eq,Show)
 
prefixEE :: ChainType -> String 
-- | compute the string represenation of a chain type, which is EE for all chain variables except for variable-to-variable chains where it is VV 
prefixEE (VarChain) = "VV"
prefixEE _ = "EE"    

    
    
data Substitution = Substitution{substList::[SubstItem]}
 deriving(Eq,Ord) 
 
data SubstItem =
-- note that the derived Ord-instance corresponds to the ordering of the constructors from low to high.
       AlphaVarLift  Name Index          -- ^ alpha_{x,i}  where x is a concrete variable
     | AlphaMetaVarLift  Name Index      -- ^ alpha_{X,i}  where X is an instantiable meta-variable
     | AlphaConstantVarLift  Name Index  -- ^ alpha_{X,i}  where X is a fixed meta-variable
     | AlphaExpr Name Index              -- ^ alpha_{S,i}  where S is an instantiable meta-variable
     | AlphaConstant Name Index          -- ^ alpha_{S,i}  where S is a fixed meta-variable
     | AlphaEnv  Name Index              -- ^ alpha_{E,i}  where E is an instantiable meta-variable
     | AlphaConstantEnv  Name Index      -- ^ alpha_{E,i}  where E is a fixed meta-variable
     | AlphaCtxt CtxtSort Name Index     -- ^ alpha_{D,i}  where D is an instantiable meta-variable
     | AlphaConstantCtxt CtxtSort Name Index       -- ^ alpha_{D,i}  where D is a fixed meta-variable
     | AlphaChain   ChainType Name Index -- ^ alpha_{CC,i} where CC is an instantiable meta-variable
     | AlphaConstantChain  ChainType Name Index -- ^ alpha_{CC,i} where CC is a fixed meta-variable      
     | CVAlphaCtxt CtxtSort Name Index -- ^ CV(alpha_{D,i}) where D is an  instantiable meta-variable
     | CVAlphaConstantCtxt CtxtSort Name Index    -- ^ CV(alpha_{D,i}) where D is a fixed meta-variable
     | LVAlphaEnv  Name Index -- ^ LV(alpha_{E,i}) wher E  is an instantiable meta-variable 
     | LVAlphaConstantEnv Name Index      -- ^ LV(alpha_{E,i}) where E is a fixed meta-variable
     | LVAlphaChain  ChainType Name Index -- ^ LV(alpha_{CC,i})  where CC is an instantiable meta-variable
     | LVAlphaConstantChain ChainType Name Index  -- ^ LV (alpha_{CC,i}) where CC  is a fixed meta-variable
     | SubstSet [SubstItem] -- ^ set of items
 deriving(Ord,Eq)     
 
type Index = String 

 
instance Show SubstItem where
  show (AlphaVarLift  name num)     = "a_{"++ name ++ "," ++ num ++ "}"
  show (AlphaMetaVarLift  name num) = "a_{"++ name ++ "," ++ num ++ "}"
  show (AlphaConstantVarLift  name num) = "a_{"++ name ++ "," ++ num ++ "}"
  show (AlphaExpr name num)            = "a_{"++ name ++ "," ++ num ++ "}"
  show (AlphaConstant name num)= "a_{"++ name ++ "," ++ num ++ "}"
  show (AlphaEnv   name num)= "a_{"++ name ++ "," ++ num ++ "}"
  show (AlphaConstantEnv   name num)= "a_{"++ name ++ "," ++ num ++ "}"
  show (AlphaCtxt srt name num)= "a_{"++ name ++ "," ++ num ++ "}"
  show (AlphaConstantCtxt srt  name num)= "a_{"++ name ++ "," ++ num ++ "}"
  show (AlphaChain  srt name num)= "a_{"++ name ++ "," ++ num ++ "}"
  show (AlphaConstantChain srt   name num)= "a_{"++ name ++ "," ++ num ++ "}"
  show (CVAlphaCtxt srt name num)= "CV(a_{"++ name ++ "," ++ num ++ "})"
  show (CVAlphaConstantCtxt srt name num)= "CV(Cn)(a_{"++ name ++ "," ++ num ++ "})"
  show (LVAlphaEnv name num)= "LV(a_{"++ name ++ "," ++ num ++ "})"
  show (LVAlphaConstantEnv  name num)= "LV(Cn)(a_{"++ name ++ "," ++ num ++ "})"
  show (LVAlphaChain srt name num)= "LV(a_{"++ name ++ "," ++ num ++ "})"
  show (LVAlphaConstantChain  srt name num)= "LV(Cn)(a_{"++ name ++ "," ++ num ++ "})"
  show (SubstSet items) = "{|" ++ intercalate "," (map  show items) ++ "|}"
  
sepChar = chr 183 
noneToLetVars :: SubstItem -> SubstItem    
noneToLetVars (AlphaEnv  s i) = (LVAlphaEnv  s i)
noneToLetVars (AlphaChain x s i)  = (LVAlphaChain x s i)
noneToLetVars (AlphaConstantEnv s i) = (LVAlphaConstantEnv s i)
noneToLetVars (AlphaConstantChain x s i)  = (LVAlphaConstantChain x s i)
noneToLetVars _ = undefined
 
noneToCVVars :: SubstItem -> SubstItem    
noneToCVVars (AlphaCtxt srt name num) = (CVAlphaCtxt srt name num)
noneToCVVars (AlphaConstantCtxt srt name num) = (CVAlphaConstantCtxt srt name num)
noneToCVVars _ = undefined
fromLVCV (CVAlphaCtxt srt name num) = AlphaCtxt srt name num
fromLVCV (CVAlphaConstantCtxt srt name num) = AlphaConstantCtxt srt name num
fromLVCV (LVAlphaEnv name num) = AlphaEnv name num
fromLVCV (LVAlphaChain s name num) = AlphaChain s name num
fromLVCV (LVAlphaConstantEnv name num) = AlphaConstantEnv name num
fromLVCV (LVAlphaConstantChain s name num) = AlphaConstantChain s name num
fromLVCV x = x 
instance Show Substitution where
  show sub 
    | null (substList sub) = ""
  show sub = "<" ++ (intercalate "," (map show (substList sub))) ++ ">" ++ [sepChar]

isEmptySubstitution :: Substitution -> Bool
isEmptySubstitution sub =  null (substList sub)  

emptySubstitution :: Substitution
emptySubstitution = Substitution {substList = []}

-- | generates the empty context with an empty substitution
hole :: Expression
hole = Hole emptySubstitution                            

isMetaCtxtSubst :: Expression -> Bool
-- | check if an expression is a 'MetaCtxtSubst'
isMetaCtxtSubst (MetaCtxtSubst _ _ _ _) = True
isMetaCtxtSubst _ = False
-- ============================================================================= 
-- Ord-instances 
instance Ord ArgPos where
 compare (VarPos _) (Binder _ _) = LT
 compare (Binder _ _) (VarPos _) = GT
 compare (VarPos v1) (VarPos v2) = compare v1 v2  
 compare (Binder v e) (Binder v' e') 
   | EQ <- compare v v'         = compare e e'
   | otherwise                  = compare v v'
   
instance Ord Expression where
 compare (VarL x) (VarL y)   = compare x y
 compare (VarL _) _          = LT
   
 compare (Fn f args) (Fn g args')
  | EQ <- compare f g            = compare args args'
  | otherwise                    = compare f g
 compare (Fn _ _)  (VarL _)    = GT
 compare (Fn _ _)  _             = LT
 
 compare (Letrec env1 e1) (Letrec env2 e2)
  | EQ <- compare e1 e2           = compare env1 env2
  | otherwise                     = compare e1 e2
 compare (Letrec _ _) (VarL _)  = GT
 compare (Letrec _ _) (Fn _ _)    = GT
 compare (Letrec _ _) _           = LT

 compare (FlatLetrec env1 e1) (FlatLetrec env2 e2)
  | EQ <- compare e1 e2           = compare env1 env2
  | otherwise                     = compare e1 e2
 compare (FlatLetrec _ _) (VarL _)  = GT
 compare (FlatLetrec _ _) (Fn _ _)    = GT
 compare (FlatLetrec _ _) (Letrec _ _)    = GT
 compare (FlatLetrec _ _) _    = LT

 compare (MetaExpression sub1 s) (MetaExpression sub2 s') = case compare s s' of 
                                                               EQ -> compare sub1 sub2
                                                               o -> o
 compare (MetaExpression _ _) (VarL _)  = GT
 compare (MetaExpression _ _) (Fn _ _)    = GT
 compare (MetaExpression _ _) (Letrec _ _)    = GT
 compare (MetaExpression _ _) (FlatLetrec _ _)    = GT 
 compare (MetaExpression _ _) _           = LT
 
 compare (Hole sub1) (Hole sub2) = compare sub1 sub2
 compare (Hole _) (VarL _)  = GT
 compare (Hole _) (Fn _ _)    = GT
 compare (Hole _) (Letrec _ _)    = GT
 compare (Hole _) (FlatLetrec _ _)    = GT
 compare (Hole _) (MetaExpression _ _) = GT
 compare (Hole _) _           = LT
 
 compare (MetaCtxt sub1 srt1 n1) (MetaCtxt sub2 srt2 n2)
   | EQ <- compare n1 n2 = case compare srt1 srt2 of 
                             EQ -> compare sub1 sub2 
                             other -> other
   | otherwise = compare n1 n2
 compare (MetaCtxt _ srt1 name1) (MetaCtxtSubst _ srt2 name2 (Hole _))
   | srt1 == srt2 && name1 == name2 = EQ
   
 compare (MetaCtxt _ _ _) (VarL _)  = GT
 compare (MetaCtxt _ _ _) (Fn _ _)    = GT
 compare (MetaCtxt _ _ _) (Letrec _ _)    = GT
 compare (MetaCtxt _ _ _) (FlatLetrec _ _)    = GT 
 compare (MetaCtxt _ _ _) (MetaExpression _ _) = GT
 compare (MetaCtxt _ _ _) (Hole _) = GT
 compare (MetaCtxt _ _ _) _           = LT
 
 compare (MetaCtxtSubst sub1 srt1 n1 e1) (MetaCtxtSubst sub2 srt2 n2 e2)
   | EQ <- compare n1 n2 = case compare e1 e2 of
                             EQ -> compare (MetaCtxt sub1 srt1 n1) (MetaCtxt sub2 srt2 n2)
                             other -> other
   | otherwise = compare n1 n2
 compare (MetaCtxtSubst _ _ _ _) (VarL _)  = GT
 compare (MetaCtxtSubst _ _ _ _) (Fn _ _)    = GT
 compare (MetaCtxtSubst _ _ _ _) (Letrec _ _)    = GT
 compare (MetaCtxtSubst _ _ _ _) (FlatLetrec _ _)    = GT 
 compare (MetaCtxtSubst _ _ _ _) (MetaExpression _ _) = GT
 compare (MetaCtxtSubst _ _ _ _) (Hole _) = GT
 compare (MetaCtxtSubst _ srt1 name1 (Hole _)) (MetaCtxt _ srt2 name2)
   | srt1 == srt2 && name1 == name2 = EQ
 
 compare (MetaCtxtSubst _ _ _ _) (MetaCtxt _ _ _) = GT
 compare (MetaCtxtSubst _ _ _ _) _           = LT

 compare (ConstantCtxtSubst sub1 srt1 n1 e1) (ConstantCtxtSubst sub2 srt2 n2 e2)
   | EQ <- compare n1 n2 = case compare e1 e2 of
                             EQ -> compare (ConstantCtxt sub1 srt1 n1) (ConstantCtxt sub2 srt2 n2)
                             other -> other
   | otherwise = compare n1 n2
   
 compare (ConstantCtxtSubst sub1 srt1 name1 (Hole _)) (ConstantCtxt sub2 srt2 name2)
   | srt1 == srt2 && name1 == name2 && sub1 == sub2 = EQ
 compare (ConstantCtxtSubst _ _ _ _) (VarL _)  = GT
 compare (ConstantCtxtSubst _ _ _ _) (Fn _ _)    = GT
 compare (ConstantCtxtSubst _ _ _ _) (Letrec _ _)    = GT
 compare (ConstantCtxtSubst _ _ _ _) (FlatLetrec _ _)    = GT 
 compare (ConstantCtxtSubst _ _ _ _) (MetaExpression _ _) = GT
 compare (ConstantCtxtSubst _ _ _ _) (Hole _) = GT
 compare (ConstantCtxtSubst _ _ _ _) (MetaCtxt _ _ _) = GT
 compare (ConstantCtxtSubst _ _ _ _) (MetaCtxtSubst _ _ _ _) = GT
 compare (ConstantCtxtSubst _ _ _ _) _           = LT

 compare (ConstantCtxt sub1 srt1 n1 ) (ConstantCtxt sub2 srt2 n2)
   | EQ <- compare n1 n2 = case compare srt1 srt2 of 
                             EQ -> compare sub1 sub2 
                             o -> o
   | otherwise = compare n1 n2
   
 compare (ConstantCtxt _ _ _) (VarL _)  = GT
 compare (ConstantCtxt _ _ _) (Fn _ _)    = GT
 compare (ConstantCtxt _ _ _ ) (Letrec _ _)    = GT
 compare (ConstantCtxt _ _ _ ) (FlatLetrec _ _)    = GT 
 compare (ConstantCtxt _ _ _) (MetaExpression _ _) = GT
 compare (ConstantCtxt _ _ _) (Hole _) = GT
 compare (ConstantCtxt _ _ _) (MetaCtxt _ _ _) = GT
 compare (ConstantCtxt _ _ _) (MetaCtxtSubst _ _ _ _) = GT
 compare (ConstantCtxt _ srt1 name1) (ConstantCtxtSubst _ srt2 name2 (Hole _))
   | srt1 == srt2 && name1 == name2 = EQ
 compare (ConstantCtxt _ _ _) (ConstantCtxtSubst _ _ _ _) = GT
 compare (ConstantCtxt _ _ _) _           = LT

 compare (Constant sub1 s) (Constant sub2 s') = case compare s s' of
                                                  EQ -> compare sub1 sub2
                                                  o -> o
 compare (Constant _ _) _ = GT
 
instance Ord Binding where
 compare (x1:=e1) (x2:=e2)
  | EQ <- compare x1 x2 = compare e1 e2
  | otherwise = compare x1 x2  
   
instance Ord Environment where 
 compare env1 env2 =
   let c1 = compare  
             (sort (bindings env1))
             (sort (bindings env2))
       c2 = compare (sort $ metaEnvVars env1) (sort $ metaEnvVars env2)
       c3 = compare (sort (metaChains env1)) (sort  (metaChains env2))
       c4 = compare (sort $ constantEnvVars env1) (sort $ constantEnvVars env2)
       c5 = compare (sort (constantChains env1)) (sort (constantChains env2))
   in case c1 of 
       EQ -> case c2 of 
               EQ -> case c3 of 
                       EQ -> case c4 of 
                               EQ -> c5
                               other -> other
                       other -> other
               other -> other
       other -> other               
 

instance Ord ChainVar where
 compare (MetaChain sub1 typ1 ee1 z1 s1) (MetaChain sub2 typ2 ee2 z2 s2) =
   case compare ee1 ee2 of 
     EQ -> case compare typ1 typ2 of
             EQ -> case compare z1 z2 of
                     EQ -> case compare s1 s2 of
                             EQ -> compare sub1 sub2
                             o -> o
                     other -> other
             other -> other
     other -> other
 compare (MetaChain _ _ _ _ _) _ = LT
 
 compare (ConstantChain sub1 typ1 ee1 z1 s1) (ConstantChain sub2 typ2 ee2 z2 s2) =
   case compare ee1 ee2 of 
     EQ -> case compare typ1 typ2 of
             EQ -> case compare z1 z2 of
                     EQ -> case compare s1 s2 of
                              EQ -> compare sub1 sub2
                              o -> o
                     other -> other
             other -> other
     other -> other
 compare (ConstantChain _ _ _ _ _) _ = GT
 
instance Ord ChainType where
 VarChain <= _ = True
 _ <= VarChain = False
 UserDefinedChain srt <= UserDefinedChain srt' =  srt <= srt'

instance Eq Environment where
 env1 == env2 = compare env1 env2 == EQ

 
 
 
-- data type for syntactic components 
data SyntaxComponent =
     SCVar VarLift
  |  SCExpr Expression
  |  SCCtxt Expression
  |  SCMetaEnv EnvVar
  |  SCMetaChain Substitution ChainType String  
  |  SCConstantEnv EnvVar
  |  SCConstantChain Substitution ChainType String  
 deriving(Eq)
 
instance Ord SyntaxComponent where
 compare (SCVar v1) (SCVar v2) = compare v1 v2
 compare (SCVar _) _ = LT

 compare (SCExpr _) (SCVar  _ ) = GT
 compare (SCExpr (ConstantCtxt _ _ _)) (SCExpr e2) =  error "STOPPED wrong! Syntax.hs"
 compare (SCExpr e1) (SCExpr (ConstantCtxt _ _ _)) =  error "STOPPED wrong! Syntax.hs"
 compare (SCExpr (MetaCtxt _ _ _)) (SCExpr e2) =  error "STOPPED wrong! Syntax.hs"
 compare (SCExpr e1) (SCExpr (MetaCtxt _ _ _)) =  error "STOPPED wrong! Syntax.hs"
 compare (SCExpr e1) (SCExpr e2) = compare e1 e2
 compare (SCExpr _) _ = LT

 compare (SCCtxt _) (SCVar _) = GT
 compare (SCCtxt _) (SCExpr _) = GT
 compare (SCCtxt c1) (SCCtxt c2) = compare c1 c2 
 compare (SCCtxt _) _ = LT
 
 compare (SCMetaEnv _) (SCVar _) = GT
 compare (SCMetaEnv _) (SCExpr _) = GT
 compare (SCMetaEnv _) (SCCtxt _) = GT
 compare (SCMetaEnv e1) (SCMetaEnv e2) = compare e1 e2
 compare (SCMetaEnv _) _ = LT 

 compare (SCMetaChain _ _ _) (SCVar _) = GT
 compare (SCMetaChain _ _ _) (SCExpr _) = GT
 compare (SCMetaChain _ _ _) (SCCtxt _) = GT
 compare (SCMetaChain _ _ _) (SCMetaEnv _) = GT
 compare (SCMetaChain sub t s) (SCMetaChain sub' t' s') 
  | EQ <- compare s s' = case compare t t' of
                          EQ -> compare sub sub'
                          oter -> oter
  | otherwise = compare s s'  
 compare (SCMetaChain _ _ _) _ = LT
 
 compare (SCConstantChain _ _ _) (SCVar _) = GT
 compare (SCConstantChain _ _ _) (SCExpr _) = GT
 compare (SCConstantChain _ _ _) (SCCtxt _) = GT
 compare (SCConstantChain _ _ _) (SCMetaEnv _) = GT
 compare (SCConstantChain _ _ _) (SCMetaChain _ _ _) = GT
 compare (SCConstantChain sub t s) (SCConstantChain sub' t' s') 
  | EQ <- compare s s' = case compare t t' of
                          EQ -> compare sub sub'
                          oter -> oter
  | otherwise = compare s s'  
 compare (SCConstantChain _ _ _) _ = LT
 
 compare (SCConstantEnv _ ) (SCVar _) = GT
 compare (SCConstantEnv _ ) (SCExpr _) = GT
 compare (SCConstantEnv _ ) (SCCtxt _) = GT
 compare (SCConstantEnv _ ) (SCMetaEnv _) = GT
 compare (SCConstantEnv _ ) (SCMetaChain _ _ _) = GT
 compare (SCConstantEnv _ ) (SCConstantChain _ _ _) = GT
 compare (SCConstantEnv e1 ) (SCConstantEnv e2 ) = compare e1 e2
 
instance Show SyntaxComponent where
 show (SCVar v) = show v
 show (SCExpr e) = show e
 show (SCCtxt d ) = show d
 show (SCMetaEnv e) = show e
 show (SCConstantEnv (Env sub name)) = name ++ "*" ++ show sub
 show (SCMetaChain sub _ string) = string ++ show sub
 show (SCConstantChain sub _ string) = string ++ "*" ++ show sub

isConcreteSCVar :: SyntaxComponent -> Bool
isConcreteSCVar (SCVar (VarLift (Substitution []) _)) = True
isConcreteSCVar (SCVar (ConstantVarLift (Substitution []) _)) = True
isConcreteSCVar _ = False
-- type class to compute syntact components
class SyntaxComponents a where
 -- compute syntactic components that may have bound variables
 getSCBoundVars :: a -> Set.Set SyntaxComponent
 -- compute syntactic components that may have free or bound variables
 getSCAllVars :: a -> Set.Set SyntaxComponent
 -- compute syntactic components that may have binders that affect the hole of a context (compute CV(.))
 getSCCapVars :: a -> Set.Set SyntaxComponent

instance SyntaxComponents VarLift where
 getSCBoundVars _ = Set.empty
 getSCAllVars  (VarLift (Substitution []) name)
   | "__internalvar_"  `isPrefixOf` name = Set.empty -- remove placeholder vars 
 getSCAllVars   v = Set.singleton (SCVar v)
 getSCCapVars   _ = Set.empty
 

instance SyntaxComponents ArgPos where
 getSCBoundVars (VarPos _)    = Set.empty
 getSCBoundVars (Binder vs e) = (Set.unions (map getSCAllVars vs)) `Set.union` (getSCBoundVars e)
 getSCAllVars   (VarPos v)    = Set.singleton (SCVar v)
 getSCAllVars   (Binder vs e) = (Set.unions (map getSCAllVars vs)) `Set.union` (getSCAllVars e)
 getSCCapVars   (VarPos _)    = Set.empty
 getSCCapVars   (Binder vs e)
   | hasHole e                = (Set.unions (map getSCAllVars vs)) `Set.union` (getSCCapVars e)
   | otherwise                = (getSCCapVars e)
 
instance SyntaxComponents Expression where
 getSCBoundVars (VarL v)        = getSCBoundVars v 
 getSCBoundVars (Fn _ args)     = (Set.unions (map getSCBoundVars args))
 getSCBoundVars (Letrec env e)  = getSCBoundVars e `Set.union` getSCBoundVars env
 getSCBoundVars (FlatLetrec env e)  = getSCBoundVars e `Set.union` getSCBoundVars env 
 getSCBoundVars m@(MetaExpression _ _) = Set.singleton $ SCExpr m 
 getSCBoundVars m@(Constant _ _)       = Set.singleton $ SCExpr m 
 getSCBoundVars m@(MetaCtxt _ _ _)     = Set.singleton $ SCCtxt m
 getSCBoundVars m@(ConstantCtxt _ _ _) = Set.singleton $ SCCtxt m
 getSCBoundVars (MetaCtxtSubst sub srt name expr) = 
   (Set.singleton $ SCCtxt (MetaCtxt sub srt name))
   `Set.union`
   (getSCBoundVars expr)
 getSCBoundVars (ConstantCtxtSubst sub srt name expr) = 
   (Set.singleton $ SCCtxt (ConstantCtxt sub srt name))
   `Set.union`
   (getSCBoundVars expr)
 getSCBoundVars (Hole _) = Set.empty

 getSCAllVars (VarL v)          = getSCAllVars v 
 getSCAllVars (Fn _ args)     = (Set.unions (map getSCAllVars args))
 getSCAllVars (Letrec env e)  = getSCAllVars e `Set.union` getSCAllVars env
 getSCAllVars (FlatLetrec env e)  = getSCAllVars e `Set.union` getSCAllVars env 
 getSCAllVars m@(MetaExpression _ _) = Set.singleton $ SCExpr m 
 getSCAllVars m@(Constant _ _)       = Set.singleton $ SCExpr m 
 getSCAllVars m@(MetaCtxt _ _ _)     = Set.singleton $ SCCtxt m
 getSCAllVars m@(ConstantCtxt _ _ _) = Set.singleton $ SCCtxt m
 getSCAllVars (MetaCtxtSubst sub srt name expr) = 
   (Set.singleton $ SCCtxt (MetaCtxt sub srt name))
   `Set.union`
   (getSCAllVars expr)
 getSCAllVars (ConstantCtxtSubst sub srt name expr) = 
   (Set.singleton $ SCCtxt (ConstantCtxt sub srt name))
   `Set.union`
   (getSCAllVars expr)
 getSCAllVars (Hole _) = Set.empty

 getSCCapVars (VarL v)          = getSCCapVars v 
 getSCCapVars (Fn _ args)       = (Set.unions (map getSCCapVars args))
 getSCCapVars (Letrec env e)    
   | hasHole e   = 
          Set.unions
           [Set.unions   [ (getSCAllVars z) `Set.union` (Set.singleton $ SCMetaChain sub typ name) |  (MetaChain sub typ name z _) <- metaChains env]
           ,Set.unions   [ (getSCAllVars z) `Set.union` (Set.singleton $ SCConstantChain sub typ name) |  (ConstantChain sub typ name z _) <- constantChains env]
           ,Set.fromList [ (SCMetaEnv envvar) | envvar <- metaEnvVars env]
           ,Set.fromList [ (SCConstantEnv envvar) | envvar <- constantEnvVars env]
           ,Set.unions   [ (getSCAllVars z) | (z:= _) <- bindings env]
           ,getSCCapVars e
           ] 
   | otherwise = getSCCapVars env
 getSCCapVars (FlatLetrec env e)    
   | hasHole e   = 
          Set.unions
           [Set.unions   [ (getSCAllVars z) `Set.union` (Set.singleton $ SCMetaChain sub typ name) |  (MetaChain sub typ name z _) <- metaChains env]
           ,Set.unions   [ (getSCAllVars z) `Set.union` (Set.singleton $ SCConstantChain sub typ name) |  (ConstantChain sub typ name z _) <- constantChains env]
           ,Set.fromList [ (SCMetaEnv envvar) | envvar <- metaEnvVars env]
           ,Set.fromList [ (SCConstantEnv envvar) | envvar <- constantEnvVars env]
           ,Set.unions   [ (getSCAllVars z) | (z:= _) <- bindings env]
           ,getSCCapVars e
           ] 
   | otherwise = getSCCapVars env
 getSCCapVars   (MetaExpression _ _) = Set.empty 
 getSCCapVars   (Constant _ _)       = Set.empty 
 getSCCapVars m@(MetaCtxt _ (UserDefinedSort b _) _)     = 
   if b then 
    Set.singleton $ SCCtxt m
    else Set.empty
 getSCCapVars m@(ConstantCtxt _ (UserDefinedSort b _) _) = 
   if b then 
    Set.singleton $ SCCtxt m
    else Set.empty
 getSCCapVars (MetaCtxtSubst sub srt@(UserDefinedSort b _) name expr) 
  | hasHole expr = (if b
                     then
                         Set.singleton $ SCCtxt (MetaCtxt sub srt name)
                     else Set.empty
                   ) `Set.union` (getSCCapVars expr)
  | otherwise = Set.empty
 getSCCapVars (ConstantCtxtSubst sub srt@(UserDefinedSort b _) name expr) 
  | hasHole expr = (if b
                     then 
                          Set.singleton $ SCCtxt (ConstantCtxt sub srt name)
                     else Set.empty
                   ) `Set.union` (getSCCapVars expr)
  | otherwise = Set.empty
 getSCCapVars (Hole _) = Set.empty
 
instance SyntaxComponents Environment where
 getSCBoundVars env =
  Set.unions
   [Set.unions   [ (getSCAllVars z) `Set.union` (getSCBoundVars s) `Set.union` (Set.singleton $  SCMetaChain sub typ name)
                    |  (MetaChain sub typ name z s) <- metaChains env
                 ]
   ,Set.unions   [ (getSCAllVars z) `Set.union` (getSCBoundVars s) `Set.union` (Set.singleton $  SCConstantChain sub typ name)
                    |  (ConstantChain sub typ name z s) <- constantChains env
                 ]
   ,Set.fromList [ (SCMetaEnv envvar) | envvar <- metaEnvVars env]
   ,Set.fromList [ (SCConstantEnv envvar) | envvar <- constantEnvVars env]
   ,Set.unions   [(getSCAllVars z) `Set.union` (getSCBoundVars s) | (z:= s) <- bindings env]
   ]
 getSCAllVars env =
  Set.unions
   [Set.unions   [ (getSCAllVars z) `Set.union` (getSCAllVars s) `Set.union` (Set.singleton $  SCMetaChain sub typ name)
                    |  (MetaChain sub typ name z s) <- metaChains env
                 ]
   ,Set.unions   [ (getSCAllVars z) `Set.union` (getSCAllVars s) `Set.union` (Set.singleton $  SCConstantChain sub typ name)
                    |  (ConstantChain sub typ name z s) <- constantChains env
                 ]
   ,Set.fromList [ (SCMetaEnv envvar) | envvar <- metaEnvVars env]
   ,Set.fromList [ (SCConstantEnv envvar) | envvar <- constantEnvVars env]
   ,Set.unions   [(getSCAllVars z) `Set.union` (getSCAllVars s) | (z:= s) <- bindings env]
   ]
 getSCCapVars env =
  let binders = 
          Set.unions
           [Set.unions   [ (getSCAllVars z) `Set.union` (Set.singleton $ SCMetaChain sub typ name) |  (MetaChain sub typ name z _) <- metaChains env]
           ,Set.unions   [ (getSCAllVars z) `Set.union` (Set.singleton $ SCConstantChain sub typ name) |  (ConstantChain sub typ name z _) <- constantChains env]
           ,Set.fromList [ (SCMetaEnv envvar) | envvar <- metaEnvVars env]
           ,Set.fromList [ (SCConstantEnv envvar) | envvar <- constantEnvVars env]
           ,Set.unions   [ (getSCAllVars z) | (z:= _) <- bindings env]
           ]
      exprWithHole =
          Set.unions
           [Set.unions   [ (getSCCapVars s)   | (MetaChain _ _ _ _ s) <- metaChains env, hasHole s]
           ,Set.unions   [ (getSCCapVars s)    | (ConstantChain _ _ _ _ s) <- constantChains env, hasHole s]
           ,Set.unions   [ (getSCCapVars s) | (_ := s) <- bindings env, hasHole s]
           ]
      noHole   = null
          (concat 
          [   [ ()   | (MetaChain _ _ _ _ s) <- metaChains env, hasHole s]
           ,  [ ()   | (ConstantChain _ _ _ _ s) <- constantChains env, hasHole s]
           ,  [ ()   | (_ := s) <- bindings env, hasHole s]
           ])

  in 
      
      if noHole then 
     
        Set.empty else binders `Set.union` exprWithHole


instance (SyntaxComponents a, SyntaxComponents b) => SyntaxComponents (Either a b) where
 getSCBoundVars (Left a)  = getSCBoundVars a
 getSCBoundVars (Right a) = getSCBoundVars a
 getSCAllVars (Left a)    = getSCAllVars a
 getSCAllVars (Right a)   = getSCAllVars a
 getSCCapVars (Left a)    = getSCCapVars a
 getSCCapVars (Right a)   = getSCCapVars a
   
   

-- fresh symbols are meta variables: 
isFreshSym :: SyntaxComponent -> Bool
-- | 'isFreshSym' gives 'True' iff the syntax component is an instantiable meta-variable
isFreshSym (SCVar (MetaVarLift _ _)) = True
isFreshSym (SCExpr (MetaExpression _ _)) = True
isFreshSym (SCCtxt (MetaCtxt _ _ _)) = True
isFreshSym (SCMetaEnv _) = True
isFreshSym (SCMetaChain _ _ _) = True
isFreshSym _ = False
 
-- =============================================================================
 
-- several helper functions:
empty :: Environment
-- | create an empty environment
empty = Environment {bindings =[], metaEnvVars =[], metaChains =[],constantChains =[], constantEnvVars = []}
-- create Environment from a single binding:
binding :: Binding -> Environment
binding b = empty{bindings = [b]}
-- create Environment from a meta chain
metaChain :: ChainType -> String -> VarLift -> Expression -> Environment
metaChain typ ee v e = empty{metaChains = [MetaChain emptySubstitution typ ee v e]}
-- create Environment containing only a Meta-environment
metaEnv :: EnvVar -> Environment
metaEnv var = empty{metaEnvVars = [var]}
-- check whether an Environment is only a meta-environment
isMetaEnv :: Environment -> Bool
isMetaEnv env = null (bindings env) && null (metaChains env) && null (constantChains env) && null (constantEnvVars env) && isSingleton (metaEnvVars env)
-- check whether an Environment contains a binding
hasBinding :: Environment -> Bool
hasBinding env = not (null (bindings env))
-- check whether an Environment contains a meta-chain
hasMetaChain :: Environment -> Bool
hasMetaChain env = not (null (metaChains env))
isEmpty :: Environment -> Bool
-- | check whether an Environment is empty
isEmpty env = and [null (bindings env), null (metaEnvVars env), null (metaChains env), null (constantChains env), null (constantEnvVars env)]

 

-- Context-Sorts
-- =============
type IsCapturing = Bool
data CtxtSort = UserDefinedSort IsCapturing String
 deriving(Eq)
 
instance Show CtxtSort where
  show (UserDefinedSort b s) = s
 
 
 
instance Ord CtxtSort where
 (UserDefinedSort b srt1) <= (UserDefinedSort b' srt2) = srt1 <= srt2

-- compute the string representation of a context class
ctxtToString :: CtxtSort -> String
ctxtToString (UserDefinedSort _ srt)  = srt

-- =======================================
-- class for computing meta-contexts
 
class HasMetaContexts a where
 computeMetaContexts :: a -> [Expression] 
  
instance HasMetaContexts ArgPos where
 computeMetaContexts (Binder _ e)  = computeMetaContexts e
 computeMetaContexts (VarPos _) = []
 
instance HasMetaContexts Expression where
 computeMetaContexts (VarL _)  = []
 computeMetaContexts (Fn _ arg) =  concatMap (computeMetaContexts) arg
 computeMetaContexts (Letrec env e) = (computeMetaContexts env) ++ (computeMetaContexts e)
 computeMetaContexts (FlatLetrec env e) = (computeMetaContexts env) ++ (computeMetaContexts e) 
 computeMetaContexts (MetaExpression _ _) = []
 computeMetaContexts (Hole _) = []
 computeMetaContexts (MultiHole _) = [] 
 computeMetaContexts (MetaCtxt sub srt  c) = [MetaCtxt sub srt c]
 computeMetaContexts (MetaCtxtSubst sub srt c s) = [MetaCtxt sub srt c] ++ computeMetaContexts s
 computeMetaContexts (Constant _ _) = []
 computeMetaContexts (ConstantCtxt _ _ _) = [] -- ?
 computeMetaContexts (ConstantCtxtSubst _ _ _ s) =  computeMetaContexts s -- ?
 
instance HasMetaContexts Binding where
 computeMetaContexts (_ := e) = (computeMetaContexts e) 
 
instance HasMetaContexts ChainVar where
 computeMetaContexts (MetaChain _ _ _ _ s) = computeMetaContexts s
 computeMetaContexts _ = []
 
instance HasMetaContexts Environment where
 computeMetaContexts env = 
  (concatMap computeMetaContexts (bindings env))
  ++ (concatMap computeMetaContexts (metaChains env))
  ++ (concatMap computeMetaContexts (constantChains env))
 
-- ============================================================================


-- ============================================================================
-- Show-instances
-- ============================================================================
 
instance Show ArgPos where 
 show (Binder [] e) = show e
 show (Binder v e) =  (intercalate "." (map show v)) ++ "." ++ show e 
 show (VarPos v) = show v

instance Show Expression where
 show (VarL v) = "(var " ++ show v ++ ")"
 show (Fn f args) = "(" ++ f   ++ " " ++ (concat (intersperse " " (map show args))) ++ ")"
 -- show (FnExt f spos args) = "(" ++ f   ++ " " ++ show spos ++  (concat (intersperse " " (map show args))) ++ ")"
 show (Letrec env e) = "(letrec " ++ show env ++ " in " ++ show e ++ ")"
 show (FlatLetrec env e) = "(flatrec " ++ show env ++ " in " ++ show e ++ ")" 
 show (MetaExpression sub e) = show sub ++ e
 show (Hole sub) = "[" ++ show sub ++ ".]" 
 show (MultiHole i) = "[._" ++ show i ++ "]"
 show (MetaCtxt sub srt  c) = show sub  ++ c -- ++ "_" ++ show srt
 show (MetaCtxtSubst sub srt c s) = show sub ++ c  ++ "[" ++ show s ++ "]"
 show (Constant sub e) = show sub ++ e ++ "*"
 show (ConstantCtxt sub srt  c) = show sub ++ c ++ "*" 
 show (ConstantCtxtSubst sub srt c s) = show sub ++ c++ "*"  ++ "[" ++ show s ++ "]"
instance Show VarLift where 
 show (VarLift sub v)           = show sub ++ v 
 show (MetaVarLift sub v)       = show sub ++ v
 show (ConstantVarLift sub v)   = show sub ++ v ++ "*"

instance Show Binding where
 show (a := b) = show a ++ "=" ++ show b 

instance Show ChainVar where
  show (MetaChain sub (UserDefinedChain k) s v e)       = show sub ++ s ++ "^" ++ k ++ "[" ++ show v ++ "," ++ show e ++ "]"  
  show (MetaChain sub VarChain s v e)     = show sub ++ s ++ "|" ++ show v ++ "," ++ show e ++ "|"
  show (ConstantChain sub (UserDefinedChain k) s v e)       = show sub ++ s ++ "^" ++ k ++ show sub ++ "*[" ++ show v ++ "," ++ show e ++ "]"  
  show (ConstantChain sub VarChain s v e) = show sub ++ s ++ "*|" ++ show v ++ "," ++ show e ++ "|"
 
instance Show Environment where
 show env =
  let bs = map show $ sort $ bindings env
      ms = map show $ sort $ metaEnvVars env
      mss = map (\x -> "*" ++ x) $ map show $ sort $ constantEnvVars env
      mc = map show $ sort $ metaChains env
      mmc =  map show $ sort $ constantChains env
      allcomp =  bs ++ ms ++ mc ++ mss ++ mmc
   in if null allcomp then "{}" else "{" ++ (concat $ intersperse ";" allcomp) ++ "}"
   
-- ============================================================================

-- ============================================================================
-- Instances for ConvExpression
-- ============================================================================

  
instance ConvExpression VarLift where
  toExpression var = VarL var
  fromExpression (VarL var) = var
  fromExpression _ = MetaVarLift emptySubstitution "__hidden__" -- this of course is a hack
   
instance ConvExpression Expression where
  toExpression = id
  fromExpression = id  
-- ============================================================================



-- ============================================================================
-- Instances for the substitution
-- ============================================================================

instance Substitute VarLift where
 subst e1 e2 = substVarL (fromExpression $ toExpression e1) (fromExpression $ toExpression e2)
  where
    substVarL vary (MetaVarLift _ x) (MetaVarLift _ z)
      | x == z = vary
    substVarL vary (ConstantVarLift sub1 x) (ConstantVarLift sub2 z)
      | x == z && sub1 == sub2  = vary
    substVarL     _ _ e = e 
 substEmpty  _ e = e
 substEnv  _ _ e = e 
 substChainVar _ _ e = e
 substEnvConstraint  _ _ e = e 
--  


instance Substitute ArgPos where
 subst expr1 expr2 b = 
    substIt (toExpression expr1) (toExpression expr2) b
  where 
   substIt (VarL vary) (VarL (MetaVarLift _ x)) (VarPos (MetaVarLift _ z)) 
    | x == z = VarPos vary
   substIt _ _ (VarPos v) = VarPos v
   substIt e1@(VarL vary) e2@(VarL varx) (Binder vars e) = Binder (map (subst vary varx) vars) (subst e1 e2 e)
   substIt e1 e2 (Binder v e)              = Binder v (subst e1 e2 e)
 substEmpty _ (VarPos v)               = VarPos v
 substEmpty n (Binder v e)             = Binder v (substEmpty n e)
 substEnv _ _ (VarPos v )            = VarPos v
 substEnv env n (Binder v e)           = Binder v (substEnv env n e)
 substChainVar _ _ (VarPos v)        = VarPos v
 substChainVar env n (Binder v e)      = Binder v (substChainVar env n e)
 substEnvConstraint _ _  (VarPos v )  = VarPos v
 substEnvConstraint env n (Binder v e) = Binder v (substEnvConstraint env n e)
 
instance Substitute Expression where
 subst expr1 expr2 = substExpression (toExpression expr1) (toExpression expr2)
  where
   -- usual cases, where expressions are substituted by expressions:
   substExpression e1 (VarL varx) (VarL varz)
    | varx == varz = e1
    | otherwise = (VarL varz)
   substExpression _ _            (VarL varz) = VarL varz
    

   substExpression e1 e2 (Fn name args) = Fn name (map (subst e1 e2) args)

   substExpression e1 e2 (Letrec env inexpr) = Letrec (subst e1 e2 env) (substExpression e1 e2 inexpr)
   substExpression e1 e2 (FlatLetrec env inexpr) = FlatLetrec (subst e1 e2 env) (substExpression e1 e2 inexpr)       

   substExpression e1 (MetaExpression subst1 s1) (MetaExpression subst2 s2)
    | s1 == s2 && subst1 == subst2 = e1
   substExpression _ _ (MetaExpression sub s2) = MetaExpression sub s2
   substExpression e1 (Constant subst1 s1) (Constant subst2 s2)
    | s1 == s2 && subst1 == subst2 = e1
   substExpression _ _ (Constant sub s) = Constant sub s -- do not substitute into constants
   -- substitution of a hole by an expression or a context
   substExpression e1 (Hole _) (Hole _) = e1
   substExpression e1 (MultiHole i) (MultiHole j)
     | i == j = e1
   substExpression e1 (MultiHole i) (MultiHole j)
     | i /= j = (MultiHole j)
   substExpression _  _        (Hole e) = (Hole e)
   substExpression _  _        (MultiHole i) = (MultiHole i)
   -- substitution of a context symbol by a context
   substExpression e1 (MetaCtxt _ _ name) (MetaCtxt _ _ name2)
    | name == name2 = e1
   substExpression e1 (ConstantCtxt _ _ name) (ConstantCtxt _ _ name2)
    | name == name2 = e1
   -- substitution of the hole of a meta-ctxt, i.e. C[ctxt/[.]] ---> C[ctxt] 
   substExpression context (Hole _) (MetaCtxt sub s name) = MetaCtxtSubst sub s name  context 
   substExpression context (Hole _) (ConstantCtxt sub s name) = ConstantCtxtSubst sub s name  context 
   -- substitution of an expression by an expression in a meta-ctxt has no effect:   
   substExpression _ _ (MetaCtxt sub s name2) = MetaCtxt sub s name2
   substExpression _ _ (ConstantCtxt sub s name2) = ConstantCtxt sub s name2
   -- substitution of a meta-ctxt by another meta-ctxt in sth like C[s], i.e. C[s][D/C] = D[s]
   substExpression e1@(MetaCtxt subst1 s0 new) e2@(MetaCtxt subst2 _ name) (MetaCtxtSubst subst3 _ name2 e) 
    | name == name2 && subst2 ==subst3 = MetaCtxtSubst subst1 s0 new (substExpression e1 e2 e)                    
   substExpression e1@(ConstantCtxt subst1 s0 new) e2@(ConstantCtxt subst2 _ name) (ConstantCtxtSubst subst3 _ name2 e) 
    | name == name2 && subst2 ==subst3 = ConstantCtxtSubst subst1 s0 new (substExpression e1 e2 e)                    
   -- substitution of a meta-ctxt by another context in sth like C[s], i.e. C[s][ctxt/C]=ctxt[s] = ctxt[.][s/[.]
   substExpression context (MetaCtxt subst1 _ name) (MetaCtxtSubst subst2 _ name2 e)  
    | name == name2 && subst1 == subst2 = subst e (Hole emptySubstitution) context                                
   substExpression context (ConstantCtxt subst1 _ name) (ConstantCtxtSubst subst2 _ name2 e)  
    | name == name2 && subst1 == subst2 = subst e (Hole emptySubstitution) context                                
   -- substitution e1/e2 in sth like C[s] => C[s[e1/e2]]    
   substExpression e1 e2 (MetaCtxtSubst sub s name2 e) = (MetaCtxtSubst sub s name2 (substExpression e1 e2 e))
   substExpression e1 e2 (ConstantCtxtSubst sub s name2 e) = (ConstantCtxtSubst sub s name2 (substExpression e1 e2 e))
   substExpression e1 e2 e3 = error (show (e1,e2,e3))

-- substEmpty n (Lam v e) = Lam v (substEmpty n e)
 substEmpty n (Fn name args) = Fn name (map (substEmpty n) args)
 substEmpty n (MetaCtxtSubst sub c name e) = MetaCtxtSubst sub c name (substEmpty n e) 
 substEmpty n (ConstantCtxtSubst sub c name e) = ConstantCtxtSubst sub c name (substEmpty n e) 
 substEmpty n (Letrec env inexpr) = Letrec (substEmpty n env) (substEmpty n inexpr)
 substEmpty n (FlatLetrec env inexpr) = FlatLetrec (substEmpty n env) (substEmpty n inexpr)     
 substEmpty _ e = e 

-- substEnv env n (Lam v e) = Lam v (substEnv env n e)
 substEnv env n (Fn name args) = Fn name (map (substEnv env n) args)
 substEnv env n (MetaCtxtSubst s c name e) = MetaCtxtSubst s c name (substEnv env n e) 
 substEnv env n (ConstantCtxtSubst s c name e) = ConstantCtxtSubst s c name (substEnv env n e) 
 substEnv env n (Letrec env' inexpr) = Letrec (substEnv env n env') (substEnv env n inexpr)
 substEnv env n (FlatLetrec env' inexpr) = FlatLetrec (substEnv env n env') (substEnv env n inexpr)     
 substEnv _ _ e = e 

-- substChainVar env n (Lam v e) = Lam v (substChainVar env n e)
 substChainVar env n (Fn name args) = Fn name (map (substChainVar env n) args)
 substChainVar env n (MetaCtxtSubst s c name e) = MetaCtxtSubst s c name (substChainVar env n e) 
 substChainVar env n (ConstantCtxtSubst s c name e) = ConstantCtxtSubst s c name (substChainVar env n e) 
 substChainVar env n (Letrec env' inexpr) = Letrec (substChainVar env n env') (substChainVar env n inexpr)
 substChainVar env n (FlatLetrec env' inexpr) = FlatLetrec (substChainVar env n env') (substChainVar env n inexpr)     
 substChainVar _ _ e = e 
 
-- substEnvConstraint env n (Lam v e) = Lam v (substEnvConstraint env n e)
 substEnvConstraint env n (Fn name args) = Fn name (map (substEnvConstraint env n) args)
 substEnvConstraint env n (MetaCtxtSubst s c name e) = MetaCtxtSubst s c name (substEnvConstraint env n e) 
 substEnvConstraint env n (ConstantCtxtSubst s c name e) = ConstantCtxtSubst s c name (substEnvConstraint env n e) 
 substEnvConstraint env n (Letrec env' inexpr) = Letrec (substEnvConstraint env n env') (substEnvConstraint env n inexpr)
 substEnvConstraint env n (FlatLetrec env' inexpr) = FlatLetrec (substEnvConstraint env n env') (substEnvConstraint env n inexpr)     
 substEnvConstraint _ _ e = e 

instance Substitute Binding where
 subst e1 e2 (x :=  e) = (subst e1 e2 x) := (subst e1 e2 e) 
 substEmpty n (x := e) = (substEmpty n x) := (substEmpty n e) 
 substEnv env n (x := e) = (substEnv env n x) := (substEnv env n e)
 substEnvConstraint env n (x := e) = (substEnvConstraint env n x) := (substEnvConstraint env n e)
 substChainVar env n (x := e) = (substChainVar env n x) := (substChainVar env n e)
 
instance Substitute Environment where
 subst e1 e2 env = 
                    env {
                          bindings = [subst e1 e2 b | b <- bindings env]
                         ,metaChains  = [MetaChain s typ name w (subst e1 e2 e) | (MetaChain s typ name v e) <- metaChains env, let w = subst e1 e2 v ]
                         ,constantChains  = [ConstantChain s typ name w (subst e1 e2 e) | (ConstantChain s typ name v e) <- constantChains env, let w = subst e1 e2 v ]
                         }
   
 substEmpty n env = env {
                          bindings = [substEmpty n b | b <- bindings env]
                         ,metaEnvVars =     [Env s v | (Env s v) <- (metaEnvVars env), v /= n]
                                        
                         ,metaChains  = [MetaChain s typ name v (substEmpty n e) | (MetaChain s typ name v e) <- metaChains env ]
                         ,constantChains  = [ConstantChain s typ name v (substEmpty n e) | (ConstantChain s typ name v e) <- constantChains env ]
                         }
                         
 substEnv env1 (n) env 
   | (n) `elem` ([name | (Env _ name) <- metaEnvVars env]) =
                       env {
                          bindings = (bindings env1) ++ [substEnv env1 n b | b <- bindings env]
                         ,metaEnvVars =  (metaEnvVars env1) ++ [Env s v | Env s v <- (metaEnvVars env), v /= n] 
                         ,constantEnvVars = (constantEnvVars env1) ++ (constantEnvVars env)
                         ,metaChains  = (metaChains env1) ++ [MetaChain s typ name v (substEnv env1 n e) | (MetaChain s typ name v e) <- metaChains env]
                         ,constantChains  = (constantChains env1) ++ [ConstantChain s typ name v (substEnv env1 n e) | (ConstantChain s typ name v e) <- constantChains env]
                         }
   | (n) `elem` ([name | (Env _ name) <- constantEnvVars env]) =
                       env {
                          bindings = (bindings env1) ++ [substEnv env1 n b | b <- bindings env]
                         ,metaEnvVars =  (metaEnvVars env1) ++ (metaEnvVars env) 
                         ,constantEnvVars = (constantEnvVars env1) ++ [Env s v | Env s v <- (constantEnvVars env), v /= n]
                         ,metaChains  = (metaChains env1) ++ [MetaChain s typ name v (substEnv env1 n e) | (MetaChain s typ name v e) <- metaChains env]
                         ,constantChains  = (constantChains env1) ++ [ConstantChain s typ name v (substEnv env1 n e) | (ConstantChain s typ name v e) <- constantChains env]
                         }
   | otherwise =   env {
                          bindings = [substEnv env1 n b | b <- bindings env]
                         ,metaChains  = [MetaChain s typ name v (substEnv env1 n e) | (MetaChain s typ name v e) <- metaChains env]
                         ,constantChains  = [ConstantChain s typ name v (substEnv env1 n e) | (ConstantChain s typ name v e) <- constantChains env]
                         }
                         
 substChainVar env1 v@(MetaChain _ _ name _ _) env =
    let newEnvironments = [env1 z' s' | (MetaChain _ _  name' z' s',_) <- splits (metaChains env), name == name']
        newBindings     = concatMap bindings newEnvironments
        newChains       = concatMap metaChains newEnvironments
        newEnvVars      = concatMap metaEnvVars newEnvironments
        newConstantEnvVars = concatMap constantEnvVars newEnvironments
        newConstantChains = concatMap constantChains newEnvironments
        allBindings     = newBindings ++ [substChainVar env1 v b | b <- bindings env]
        allChains       = newChains   ++ [MetaChain s typ' name' z' (substChainVar env1 v s') | (MetaChain s typ' name' z' s') <- metaChains env, name /= name' ]
        allEnvVars      = newEnvVars ++ (metaEnvVars env)
        allConstantEnvVars = newConstantEnvVars ++ (constantEnvVars env)
        allConstantChains = newConstantChains ++ [ConstantChain s typ' name' z' (substChainVar env1 v s') | (ConstantChain s typ' name' z' s') <-constantChains env]
    in env{bindings = allBindings
          ,metaChains = allChains
          ,metaEnvVars = allEnvVars
          ,constantEnvVars = allConstantEnvVars
          ,constantChains = allConstantChains}    
                     
 substChainVar env1 v@(ConstantChain _ _ name _ _) env =
    let newEnvironments = [env1 z' s' | (ConstantChain _ _  name' z' s',_) <- splits (constantChains env), name == name']
        newBindings     = concatMap bindings newEnvironments
        newChains       = concatMap metaChains newEnvironments
        newEnvVars      = concatMap metaEnvVars newEnvironments
        newConstantEnvVars = concatMap constantEnvVars newEnvironments
        newConstantChains = concatMap constantChains newEnvironments
        allBindings     = newBindings ++ [substChainVar env1 v b | b <- bindings env]
        allChains       = newChains   ++ [MetaChain s typ' name' z' (substChainVar env1 v s') | (MetaChain s typ' name' z' s') <- metaChains env]
        allEnvVars      = newEnvVars ++ (metaEnvVars env)
        allConstantEnvVars = newConstantEnvVars ++ (constantEnvVars env)
        allConstantChains       = newConstantChains   ++ [ConstantChain s typ' name' z' (substChainVar env1 v s') | (ConstantChain s typ' name' z' s') <- constantChains env, name /= name' ]
     
    in env{bindings = allBindings
          ,metaChains = allChains
          ,metaEnvVars = allEnvVars
          ,constantEnvVars = allConstantEnvVars
          ,constantChains = allConstantChains}    

-- substChainVar _ (ConstantChain _ _ _ _ _) _ =  error "in substChainVar: try substitute a constant chain?"
                         
 substEnvConstraint env1 env2 env 
   | not $ null [n | MetaChain _ _ n _ _ <- constantChains env1] = error "metachain in constant chains env1"
   | not $ null [n | MetaChain _ _ n _ _ <- constantChains env2] = error "metachain in constant chains env2"
   | not $ null [n | MetaChain _ _ n _ _ <- constantChains env] = error "metachain in constant chains env"      
   | not $ null [n | ConstantChain _ _ n _ _ <- metaChains env1] = error "constantchain in meta chains env1"
   | not $ null [n | ConstantChain _ _ n _ _ <- metaChains env2] = error "constantchain in meta chains env2"
   | not $ null [n | ConstantChain _ _ n _ _ <- metaChains env] = error "constantchain in meta chains env"      
   | null [b | b <- bindings env2, b `notElem` (bindings env)]
   , null [b | b <- metaChains env2, b `notElem` (metaChains env)]  
   , null [b | b <- metaEnvVars env2, b `notElem` (metaEnvVars env)]  
   , null [b | b <- constantEnvVars env2, b `notElem` (constantEnvVars env)]  
   , null [b | b <- constantChains env2, b `notElem` (constantChains env)]  
     =  let res = env {
              bindings        = (bindings env1) ++ [substEnvConstraint env1 env2 b | b <- bindings env, b `notElem` (bindings env2)]
             ,metaEnvVars     = (metaEnvVars env1) ++ [v | v <- (metaEnvVars env), v `notElem` (metaEnvVars env2)]
             ,constantEnvVars = (constantEnvVars env1) ++ [c | c <- (constantEnvVars env), c `notElem` (constantEnvVars env2)]
             ,metaChains      = (metaChains env1) ++     [MetaChain s typ name v (substEnvConstraint env1 env2 e)     | c@(MetaChain s typ name v e) <- metaChains env, c `notElem` (metaChains env2)]
             ,constantChains  = (constantChains env1) ++ [ConstantChain s typ name v (substEnvConstraint env1 env2 e) | c@(ConstantChain s typ name v e) <- constantChains env, c `notElem` (constantChains env2)]
                         }
        in res
                         
                         
   | otherwise =   env {
                          bindings        = [substEnvConstraint env1 env2 b | b <- bindings env]
                         ,metaChains      = [MetaChain s typ name v (substEnvConstraint env1 env2 e)     | (MetaChain s typ name v e) <- metaChains env]
                         ,constantChains  =   [ConstantChain s typ name v (substEnvConstraint env1 env2 e) | (ConstantChain s typ name v e) <- constantChains env]
                         }
                            
-- ============================================================================


-- ============================================================================
-- Treating variable names by a type class, since there are some 'special' names 
class GetName a where
 getName :: a -> Name
 hasName :: a -> Bool

instance GetName VarLift where
 getName (MetaVarLift _ "--") = error "in getName, -- is not a name"
 getName (MetaVarLift _ v) = v
 getName (VarLift _ "--") = error "in getName, -- is not a name"
 getName (VarLift _ v) = v
 getName (ConstantVarLift _ v) = v
 hasName (MetaVarLift _ x) = x /= "--"
 hasName (VarLift _ x) = x /= "--"
 hasName (ConstantVarLift _ _) = True
 

-- ============================================================================
-- Treating the meta-variables
-- ============================================================================

-- | a container for the different types of meta-variables
data MetaVars = MetaVars {liftvars  :: [Name],  -- ^ X variables
                          envvars  :: [Name],   -- ^ E variables
                          ctxtvars :: [Name],   -- ^ D variables
                          exprvars :: [Name],   -- ^ S variables
                          chainvars :: [Name]   -- ^ CC variables
                          }
  deriving(Show)                        

-- empty container  
emptyMVars :: MetaVars
emptyMVars = MetaVars [] [] [] [] []

-- union two containers:
combineMetaVars :: MetaVars -> MetaVars -> MetaVars
combineMetaVars m m' = 
 m {liftvars = liftvars m ++ liftvars m'
       ,envvars = envvars m ++ envvars m'
       ,chainvars = chainvars m ++ chainvars m'
       ,ctxtvars = ctxtvars m ++ ctxtvars m'
       ,exprvars = exprvars m ++ exprvars m'
       }
-- union a list of containers       
combineMetaVarsList :: [MetaVars] -> MetaVars
combineMetaVarsList list = foldr (combineMetaVars) emptyMVars list       

-- compute the meta-vars and add them to a container
addMetaVars :: HasMetaVars a => a -> MetaVars -> MetaVars
addMetaVars a m =
  let m' = computeMetaVars a
  in combineMetaVars m m'
  
-- compute the meta-vars of a list of objects  
computeMetaVarsList :: HasMetaVars a => [a] -> MetaVars
computeMetaVarsList xs = foldr (addMetaVars) emptyMVars  xs
       
-- type class for object which have meta-vars       
class HasMetaVars a where
 computeMetaVars :: a -> MetaVars
 
-- instances
instance HasMetaVars VarLift where 
 computeMetaVars (MetaVarLift _ x) = emptyMVars {liftvars = [x]}
 computeMetaVars _ = emptyMVars
 
instance HasMetaVars ArgPos where
 computeMetaVars (Binder v e)  = addMetaVars e (computeMetaVarsList v) 
 computeMetaVars (VarPos v )   = (computeMetaVars v)  

instance HasMetaVars Expression where
 computeMetaVars (VarL v)  = computeMetaVars v
 computeMetaVars (Fn _ arg) =  computeMetaVarsList arg
 computeMetaVars (Letrec env e) = addMetaVars e (computeMetaVars env)
 computeMetaVars (FlatLetrec env e) = addMetaVars e (computeMetaVars env)  
 computeMetaVars (MetaExpression _ e) = emptyMVars {exprvars =[e]}
 computeMetaVars (Hole _) = emptyMVars
 computeMetaVars (MultiHole _) = emptyMVars 
 computeMetaVars (MetaCtxt _ _  c) = emptyMVars {ctxtvars = [c]}
 computeMetaVars (MetaCtxtSubst _ _ c s) = addMetaVars s (emptyMVars {ctxtvars = [c]}) 
 computeMetaVars (Constant _ _) =emptyMVars
 computeMetaVars (ConstantCtxt _ _ _) = emptyMVars
 computeMetaVars (ConstantCtxtSubst _ _ _ s) = computeMetaVars s
 
instance HasMetaVars Binding where
 computeMetaVars (x := e) = addMetaVars e (computeMetaVars x) 
instance HasMetaVars ChainVar where
 computeMetaVars (MetaChain _ _ ee x s) = addMetaVars s (addMetaVars x (emptyMVars {chainvars = [ee]}))
 computeMetaVars (ConstantChain _ _ _ x s) = addMetaVars s (computeMetaVars x)
instance HasMetaVars Environment where
 computeMetaVars env = 
  combineMetaVarsList [computeMetaVarsList (bindings env)
                      ,emptyMVars{envvars = [v | (Env _ v) <- metaEnvVars env]}
                      ,computeMetaVarsList (metaChains env)
                      ,computeMetaVarsList (constantChains env)]                      
instance HasMetaVars a =>  HasMetaVars [a] where
    computeMetaVars l = combineMetaVarsList (map computeMetaVars l)
                      -- ============================================================================



-- ============================================================================
-- check the 'weak distinct variable convention' of an expression
  
checkScope :: Expression -> Maybe String
checkScope expr =   
  go expr [] []
 where
  goArgPos (VarPos _)    _        _ = Nothing
  goArgPos (Binder v e)  scopeLam scopeLet = go e ((map removeSubst v)++scopeLam) scopeLet
  go (VarL _)    _        _ = Nothing
  go (Fn _ args) scopeLam scopeLet = 
            let rec = map (\x -> goArgPos x scopeLam scopeLet) args
            in if all (isNothing) rec then Nothing else (head (filter  isJust rec))
  go (Letrec env e) scopeLam scopeLet     =
    let capvars =   map removeSubst 
                        ([v | (MetaChain _ _ _ v _) <- metaChains env, not (isPseudoMV v)]
                        ++  [ v | (ConstantChain _ _ _ v _) <- constantChains env]
                        ++  [ b | (b := _) <- bindings env])
        expressions = e:  ((map (\(MetaChain  _ _ _ _ s) -> s)  $ metaChains env) 
                             ++  (map (\(ConstantChain  _ _ _ _ s) -> s)  $ constantChains env) 
                             ++  (map (\(_ := ex) -> ex) $ bindings env))
        scopeLet' = scopeLet ++ capvars
        checkDoubleLetVar = (nub capvars) == capvars
        check     = [c | c <- capvars, c `elem` scopeLet]
        rec       = filter isJust (  map (\x -> go x scopeLam scopeLet') expressions)
    in if checkDoubleLetVar then 
          if null check then 
            if null rec then Nothing else  head (rec)
          else Nothing -- Just $ "Let-binder for variable " ++ (show $ head check) ++ "occurs in a scope of another let-binder for the same variable"
        else Just $ "Multiple let binders in the same environmet"                                                                                                                
  go (FlatLetrec env e) scopeLam scopeLet     =
    let capvars =   map removeSubst 
                        ([v | (MetaChain _ _ _ v _) <- metaChains env, not (isPseudoMV v)]
                        ++  [ v | (ConstantChain _ _ _ v _) <- constantChains env]
                        ++  [ b | (b := _) <- bindings env])
        expressions = e:  ((map (\(MetaChain  _ _ _ _ s) -> s)  $ metaChains env) 
                             ++  (map (\(ConstantChain  _ _ _ _ s) -> s)  $ constantChains env) 
                             ++  (map (\(_ := ex) -> ex) $ bindings env))
        scopeLet' = scopeLet ++ capvars
        checkDoubleLetVar = (nub capvars) == capvars
        check     = [c | c <- capvars, c `elem` scopeLet]
        rec       = filter isJust (  map (\x -> go x scopeLam scopeLet') expressions)
    in if checkDoubleLetVar then 
          if null check then 
            if null rec then Nothing else  head (rec)
          else Nothing -- Just $ "Let-binder for variable " ++ (show $ head check) ++ "occurs in a scope of another let-binder for the same variable"
        else Just $ "Multiple let binders in the same environmet"                                                                                                                
  go (MetaExpression _ _)           _        _          = Nothing
  go (Constant _ _)                 _        _          = Nothing
  go (Hole _)                       _        _          = Nothing
  go (MultiHole _)                  _        _          = Nothing
  go (MetaCtxt _ _ _)               _        _          = Nothing
  go (ConstantCtxt _ _ _)           _        _          = Nothing
  go (MetaCtxtSubst _ _ _ e)        scopeLam scopeLet   = go e scopeLam scopeLet
  go (ConstantCtxtSubst _ _ _ e)    scopeLam scopeLet   = go e scopeLam scopeLet


checkLVC :: Expression -> Maybe String
-- | check whether an expression fulfills the let variable convention (LVC), i.e.
--   whether all let-binders in a single environment bind different names.
--   The result is either 'Nothing' (the LVC holds) or 'Just' err, where 
--   err is an error-string
checkLVC expr =
 let 
   bv = boundVars expr
   fv = freeVars expr
   liftvariables =  [getName v |  v <- bv, hasName v]
 in case (checkScope expr) of
      Just e -> Just e
      Nothing -> Nothing 
      --
      --   let l = [v | v <- liftvariables, v `elem` (map (\x -> getName x) fv)] -- bound letvars /=  freevars
      --   in if not (null l) then Just $ "bound vars /= freevars" ++ show l ++ "\n" ++ show fv ++ "\n" ++ show liftvariables
      --                         else Nothing                            

checkLVCEnv :: Environment -> Maybe String
checkLVCEnv env =
 let 
   bv = boundVarsEnv env
   fv = freeVarsEnv env
   liftvariables =  [getName v | v <- bv, hasName v]
   expressions = ((map (\(MetaChain _ _ _ _ s) -> s)  $ metaChains env) 
                          ++  (map (\(ConstantChain _ _ _ _ s) -> s)  $ constantChains env) 
                          ++  (map (\(_ := e) -> e) $ bindings env))
   dvcex = (filter isJust (map (\x -> checkLVC x) expressions))                           
  in -- if not (allDifferent letvars) then Just $ "bound letvars aren't different: " ++ show letvars
      -- else  
   -- let -- l = [v | v <- liftvariables, v `elem` (map (\x -> getName x) fv)] -- bound letvars /=  freevars
       --             in if not (null l) then Just $ "bound vars /= freevars" ++ show l
                                       -- else 
                                       if null dvcex then Nothing
                                                          else head dvcex
-- ============================================================================
     
-- weaker Variant for mathcing
{-
checkLVCMatching expr =
 let 
   bv = boundVars expr
   fv = freeVars expr
   liftvars =  [getName v | v <- bv, hasName v]
   --if not (allDifferent letvars) then Just $ "bound letvars aren't different: " ++ show letvars
      -- else
  in case (checkScope expr) of
      Just e -> Just e
      Nothing ->       
                    let l = [v | v <- liftvars, v `elem` (map (\x -> getName x) fv)] -- bound letvars /=  freevars
                    in if not (null l) then Just $ "bound letvars /= freevars" ++ show l
                                       else Nothing                            
checkLVCEnvMatching env =
 let 
   bv = boundVarsEnv env
   fv = freeVarsEnv env
   liftvars =  [getName v | v <- bv, hasName v]
   expressions = ((map (\(MetaChain _ _ _ _ s) -> s)  $ metaChains env) 
                          ++  (map (\(ConstantChain _ _ _ _ s) -> s)  $ constantChains env) 
                          ++  (map (\(b := e) -> e) $ bindings env))
   dvcex = (filter isJust (map (\x -> checkLVCMatching x) expressions))      
  in --if not (allDifferent letvars) then Just $ "bound letvars aren't different: " ++ show letvars
     -- else  
                            let l = [v | v <- liftvars, v `elem` (map (\x -> getName x) fv)] -- bound liftvars /=  freevars
                            in if not (null l) then Just $ "bound liftvars /= freevars" ++ show l
                                else if null dvcex then Nothing
                                 else head dvcex            
-}     
-- ============================================================================
-- check the subTerm-Relation
-- ============================================================================
subTermOf :: Expression -> Expression -> Bool
-- | 'subTermOf' e1 e2 checks wether e1 occurs inside e2
e1 `subTermOf` e2
 | e1 == e2 = True
e1 `subTermOf` (Fn _ args) = any (\x -> e1 `subTermOfArgPos` x) args
e1 `subTermOf` (MetaCtxtSubst _ _ _ e2) = e1 `subTermOf` e2
e1 `subTermOf` (ConstantCtxtSubst _ _ _ e2) = e1 `subTermOf` e2
_  `subTermOf` _ = False

subTermOfArgPos :: Expression -> ArgPos -> Bool
e1       `subTermOfArgPos` ((Binder _ e2)) = e1 `subTermOf` e2
(VarL v) `subTermOfArgPos` (VarPos v') = v == v'
_        `subTermOfArgPos` (VarPos _) = False


properSubTermOf :: Expression -> Expression-> Bool
-- | 'properSubTermOf e1 e2' checks whether e1 is a proper subterm of e2. 
--   note that the check is performed syntactically but not semantically and thus
--   e.g. for properSubTermOf S C[S]  yield True, even C may represent the empty context
properSubTermOf e1 e2  = e1 /= e2 && e1 `subTermOf` e2 

  
-- ============================================================================


-- ============================================================================
-- free and bound variables, captured variables in contexts
-- ============================================================================

allVariables :: Expression -> [VarLift]
-- | compute all variables occuring in an expression
allVariables e = freeVars e ++ boundVars e
allVariablesInEnv :: Environment -> [VarLift]
-- | compute all variables occuring in an environment
allVariablesInEnv env = freeVarsEnv env ++ boundVarsEnv env

-- compute the free variables, where only Meta-Variables are considered
freeVarsMetaVars :: Expression -> [VarLift]
freeVarsMetaVars e = go $ freeVars e 
  where
    go [] = []
    go (x:xs)
     | ((MetaVarLift _ _)) <- x = x:(go xs)
     | otherwise = go xs
     
-- compute the free variables, where only concrete Variables are considered
freeVarsConcrete :: Expression -> [VarLift]
freeVarsConcrete e = go $ freeVars e
  where
    go [] = []
    go (x:xs)
     | ((VarLift  _ _)) <- x = x:(go xs)
     | otherwise = go xs
     
     
-- compute the free variables, where only Constant Variables are considered
freeVarsConstant :: Expression -> [VarLift]
freeVarsConstant e = go $ freeVars e
  where
    go [] = []
    go (x:xs)
     | ((ConstantVarLift  _ _)) <- x = x:(go xs)
     | otherwise = go xs
     
freeVarsArgPos :: ArgPos -> [VarLift]      
freeVarsArgPos (VarPos v)   = [removeSubst v]
freeVarsArgPos (Binder v e) = [x | x <- (freeVars e), x `notElem` (map removeSubst v)] 

-- compute all free variables (meta and concrete variables)      
freeVars :: Expression -> [VarLift]
-- freeVars (Lam v e) = filter (/= (Left v)) $ freeVars e

freeVars (VarL (MetaVarLift _ "--"))    = [] -- hack
freeVars (VarL v)                       = [removeSubst v]
freeVars (Fn _ args)                    = concatMap freeVarsArgPos args
freeVars (Letrec env e)                 =
 let capvars =  [v | (MetaChain  _ _ _ v _) <- metaChains env, not (isPseudoMV v)]
                ++[v | (ConstantChain _ _ _ v _) <- constantChains env]
                ++ [b | (b := _) <- bindings env]
     expressions = e:((map (\(MetaChain _ _ _ _ s) -> s)  $ metaChains env) 
                          ++  (map (\(ConstantChain _ _ _ _ s) -> s)  $ constantChains env) 
                          ++  (map (\(_ := ex) -> ex) $ bindings env))
  in [v | v <- concatMap freeVars expressions, v `notElem` (map removeSubst capvars)]
freeVars (FlatLetrec env e)                 =
 let capvars =  [v | (MetaChain  _ _ _ v _) <- metaChains env, not (isPseudoMV v)]
                ++[v | (ConstantChain _ _ _ v _) <- constantChains env]
                ++ [b | (b := _) <- bindings env]
     expressions = e:((map (\(MetaChain _ _ _ _ s) -> s)  $ metaChains env) 
                          ++  (map (\(ConstantChain _ _ _ _ s) -> s)  $ constantChains env) 
                          ++  (map (\(_ := ex) -> ex) $ bindings env))
  in [v | v <- concatMap freeVars expressions, v `notElem` (map removeSubst capvars)]
freeVars (MetaExpression _ _)           = []
freeVars (Constant _ _)                 = []
freeVars (Hole _)                       = []
freeVars (MetaCtxt _  _ _)              = []
freeVars (ConstantCtxt _ _ _)           = []
freeVars (MetaCtxtSubst _ _ _ expr)     = freeVars expr     
freeVars (ConstantCtxtSubst _ _ _ expr) = freeVars expr     

freeVarsEnv :: Environment -> [VarLift]
freeVarsEnv env                         =
 let capvars =  [v | (MetaChain _ _ _ v _) <- metaChains env, not (isPseudoMV v)]
                ++ [b | (b := _) <- bindings env]
                ++ [v | (ConstantChain _ _ _ v _) <- constantChains env]
                 
     expressions = ((map (\(MetaChain _ _ _ _ s) -> s)  $ metaChains env) 
                   ++ (map (\(ConstantChain _ _ _ _ s) -> s)  $ constantChains env) 
                   ++  (map (\(_ := e) -> e) $ bindings env))
  in [v | v <- concatMap freeVars expressions, v `notElem` (map removeSubst capvars)]

removeSubst :: VarLift -> VarLift
removeSubst (VarLift _ v) = VarLift emptySubstitution v  
removeSubst (MetaVarLift _ v) = MetaVarLift emptySubstitution v  
removeSubst (ConstantVarLift _ v) = ConstantVarLift emptySubstitution v  

-- compute all bound variables (meta and concrete variables)  
boundVarsArgPos :: ArgPos -> [VarLift]
boundVarsArgPos (VarPos _) = []
boundVarsArgPos (Binder v e) = (map removeSubst v) ++ (boundVars e)

boundVars :: Expression -> [VarLift]
boundVars (VarL _) = []
boundVars (Fn _ args) = concatMap boundVarsArgPos args
boundVars (Letrec env e) =
 let capvars = (map (\(MetaChain _ _ _ v _) -> v)  $ metaChains env) ++  (map (\(b := _) -> b) $ bindings env)
                           ++ (map (\(ConstantChain _ _ _ v _) -> v)  $ constantChains env)
     expressions = e:((map (\(MetaChain _ _ _ _ s) -> s)  $ metaChains env) ++  (map (\(_ := ex) -> ex) $ bindings env)
                     ++ (map (\(ConstantChain _ _ _ _ s) -> s)  $ constantChains env))
  in (map removeSubst capvars) ++ (concatMap boundVars expressions)
boundVars (FlatLetrec env e) =
 let capvars = (map (\(MetaChain _ _ _ v _) -> v)  $ metaChains env) ++  (map (\(b := _) -> b) $ bindings env)
                           ++ (map (\(ConstantChain _ _ _ v _) -> v)  $ constantChains env)
     expressions = e:((map (\(MetaChain _ _ _ _ s) -> s)  $ metaChains env) ++  (map (\(_ := ex) -> ex) $ bindings env)
                     ++ (map (\(ConstantChain _ _ _ _ s) -> s)  $ constantChains env))
  in (map removeSubst capvars) ++ (concatMap boundVars expressions)
boundVars (MetaExpression _ _) = []
boundVars (Constant _ _) = []
boundVars (Hole _) = []
boundVars (MetaCtxt _ _ _) = []
boundVars (MetaCtxtSubst _ _ _ expr) = boundVars expr     
boundVars (ConstantCtxt _ _ _) = []
boundVars (ConstantCtxtSubst _ _ _ expr) = boundVars expr     

boundVarsEnv :: Environment -> [VarLift]
boundVarsEnv env =
 let capvars = (map (\(MetaChain _ _ _ v _) -> v)  $ metaChains env) ++  (map (\(b := _) -> b) $ bindings env)
                           ++ (map (\(ConstantChain _ _ _ v _) -> v)  $ constantChains env)
     expressions = ((map (\(MetaChain _ _ _ _ s) -> s)  $ metaChains env) ++  (map (\(_ := e) -> e) $ bindings env))
                    ++ (map (\(ConstantChain _ _ _ _ s) -> s)  $ constantChains env)
  in (map removeSubst capvars) ++ (concatMap boundVars expressions)
  
isExpr :: Expression -> Bool  
-- | check whether an 'Expression' is an expression ('True') or a context ('False')
--   the equation isExpre e == not ('hasHole' e) holds
isExpr e 
    | not (hasHole e)  = True
    | otherwise        = False
    
-- given a context, compute the variables which are captured by the hole 
capturedInHoleArgPos :: ArgPos -> [VarLift]
capturedInHoleArgPos (Binder v e) = (map removeSubst v) ++ capturedInHole e
capturedInHoleArgPos (VarPos _) = []

capturedInHole :: Context -> [VarLift]
capturedInHole (VarL _) = []
capturedInHole (Fn _ args) = concatMap snd $ filter (\(a,_) -> a == True) $ map (\a -> (hasHoleArgPos a, capturedInHoleArgPos a)) args
capturedInHole (Letrec env e) =
 let capvars =   (map (\(MetaChain _ _ _ v _) -> v)  $ metaChains env) ++  (map (\(b := _) -> b) $ bindings env) 
                           ++ (map (\(ConstantChain _ _ _ v _) -> v)  $ constantChains env)
     expressions = e:((map (\(MetaChain _ _ _ _ s) -> s)  $ metaChains env) ++  (map (\(_ := ex) -> ex) $ bindings env)
                     ++ (map (\(ConstantChain _ _ _ _ s) -> s)  $ constantChains env))
  in (map removeSubst capvars) ++ (concatMap snd $ filter (\(a,_) -> a == True) $ map (\a -> (hasHole a, capturedInHole a)) expressions)
capturedInHole (FlatLetrec env e) =
 let capvars =   (map (\(MetaChain _ _ _ v _) -> v)  $ metaChains env) ++  (map (\(b := _) -> b) $ bindings env) 
                           ++ (map (\(ConstantChain _ _ _ v _) -> v)  $ constantChains env)
     expressions = e:((map (\(MetaChain _ _ _ _ s) -> s)  $ metaChains env) ++  (map (\(_ := ex) -> ex) $ bindings env)
                     ++ (map (\(ConstantChain _ _ _ _ s) -> s)  $ constantChains env))
  in (map removeSubst capvars) ++ (concatMap snd $ filter (\(a,_) -> a == True) $ map (\a -> (hasHole a, capturedInHole a)) expressions)
capturedInHole (MetaExpression _ _) = []
capturedInHole (Constant _ _) = []
capturedInHole (Hole _) = []
capturedInHole (MetaCtxt _ _ _) = []
capturedInHole (ConstantCtxt _ _ _) = []
capturedInHole (MetaCtxtSubst _ _ _ expr) =  capturedInHole expr     
capturedInHole (ConstantCtxtSubst _ _ _ expr) =  capturedInHole expr     
 
isHole :: Expression -> Bool
-- | check whether an 'Expression' is a context 'Hole'
isHole (Hole _) = True
isHole _ = False


hasHoleArgPos :: ArgPos -> Bool
-- | check whether an 'ArgPos' has a context hole
hasHoleArgPos (Binder _ e) = hasHole e
hasHoleArgPos (VarPos _) = False


hasHole :: Expression -> Bool
-- | check whether an 'Expression' has a hole (and thus is a 'Context' but not an expression)
hasHole (VarL _) = False
hasHole (Fn _ args) = any hasHoleArgPos args
hasHole (Letrec env e) = hasHole e || hasHoleEnv env
hasHole (FlatLetrec env e) = hasHole e || hasHoleEnv env
hasHole (MetaExpression _ _) = False
hasHole (Constant _  _) = False
hasHole (Hole _)= True
hasHole (MetaCtxt _ _ _) = True
hasHole (ConstantCtxt _ _ _) = True
hasHole (MetaCtxtSubst _ _ _ expr) = hasHole expr
hasHole (ConstantCtxtSubst _ _ _ expr) = hasHole expr

hasHoleEnv :: Environment -> Bool
hasHoleEnv env =
      (any (\(MetaChain _ _ _ _ e) -> hasHole e)  $ metaChains env)
  ||  (any (\(ConstantChain _ _ _ _ e) -> hasHole e)  $ constantChains env)
  ||  (any (\(_ := e) -> hasHole e) $ bindings env)
  
  
  
-- ============================================================================
  
 
 

-- ============================================================================
-- class for converting in and from expressions
-- ============================================================================

class ConvExpression b where
 toExpression :: b -> Expression
 fromExpression :: Expression -> b

-- ============================================================================ 


 
-- ============================================================================ 
-- class for applying substitutions  
-- ============================================================================
class Substitute a where 
-- subst e1 e2 e3 = e3[e2/e1]
-- everything which can be converted into an expression can be used for subst 
 subst :: ConvExpression b => b -> b -> a -> a

-- substEmpty replace the environment variable named by the String by an empty
-- environment: 
 substEmpty :: String -> a -> a 
 
-- substEnv: substitutions for environments, i.e.
-- substEnv env E e = e[env/E] 
 substEnv   :: Environment -> String -> a -> a

-- substChainVar EE[z,s] env e = e[env/EE[z,s]] 
 substChainVar :: (VarLift -> Expression -> Environment) -> ChainVar -> a -> a
 substEnvConstraint :: Environment -> Environment -> a -> a
-- some instances for usual types
instance Substitute a => Substitute [a] where
  subst e1 e2 = map (subst e1 e2) 
  substEmpty n = map (substEmpty n)
  substEnv env n = map (substEnv env n)
  substChainVar v1 v2 = map (substChainVar v1 v2)  
  substEnvConstraint env n = map (substEnvConstraint env n)
            
instance (Substitute a, Substitute b) => Substitute (Either a b) where
 subst e1 e2 (Left e) = Left (subst e1 e2 e)
 subst e1 e2 (Right e) = Right (subst e1 e2 e)
 substEmpty n (Left e) = Left (substEmpty n e)
 substEmpty n (Right e) = Right (substEmpty n e)
 substEnv env n (Left e) = Left (substEnv env n e)
 substEnv env n (Right e) = Right (substEnv env n e)
 substChainVar v1 v2 (Left e) = Left (substChainVar v1 v2 e)  
 substChainVar v1 v2 (Right e) = Right (substChainVar v1 v2 e)  
 substEnvConstraint env n (Left e) = Left (substEnvConstraint env n e)
 substEnvConstraint env n (Right e) = Right (substEnvConstraint env n e)
 
instance (Substitute a, Substitute b) => Substitute (a,b) where
 subst e1 e2 (a,b) = (subst e1 e2 a,subst e1 e2 b)
 substEmpty n (a,b) = (substEmpty n a,substEmpty n b)
 substEnv e1 e2 (a,b) = (substEnv e1 e2 a,substEnv e1 e2 b)
 substChainVar v1 v2 (a,b) = (substChainVar v1 v2 a,substChainVar v1 v2 b)  
 substEnvConstraint e1 e2 (a,b) = (substEnvConstraint e1 e2 a,substEnvConstraint e1 e2 b)

-- ============================================================================ 

nubFnSyms :: FunInfo -> FunInfo
-- | remove duplicated information for function symbols
nubFnSyms syms = 
 let sorted = sortBy (\(name,_,_) (name2,_,_) -> compare name name2) syms
     grouped = groupBy (\(name,_,_) (name2,_,_) -> name == name2) sorted
     handleGroup g@((n,a,s):_) = 
         let positions = [spos | (_,_,spos) <- g]
         in if [] `elem` positions 
                then (n,a,[])
                else let l = [p | p <- positions, p /= [1], p /= []]  
                     in if null l 
                           then (n,a,s) 
                           else (n,a,head l)
     handleGroup [] = error "in handleGroup: applied to []"
 in map handleGroup grouped
              
 
class HasFnSymbol a where
 getFnSymbols :: a -> [(String,Int,[Int])]
 
instance HasFnSymbol a => HasFnSymbol [a] where
 getFnSymbols xs = concatMap getFnSymbols xs 
 
instance HasFnSymbol ArgPos where
 getFnSymbols (Binder _ e) = getFnSymbols e
 getFnSymbols (VarPos _ ) = []
 
instance HasFnSymbol (Expression) where
 getFnSymbols (Fn sym []) = [(sym,0,[])]
 getFnSymbols (Fn sym args) = (sym,length args,[1]):(concatMap getFnSymbols args)
 getFnSymbols (VarL _)  = []
 getFnSymbols (Letrec env e) = (getFnSymbols env) ++ (getFnSymbols e)
 getFnSymbols (FlatLetrec env e) = (getFnSymbols env) ++ (getFnSymbols e)
 getFnSymbols (MetaExpression _ _) = []
 getFnSymbols (Hole _) = []
 getFnSymbols (MetaCtxt _ _ _) = []
 getFnSymbols (MetaCtxtSubst _ _ _ s) = getFnSymbols s
 getFnSymbols (Constant _ _) = []
 getFnSymbols (ConstantCtxt _ _ _ ) = [] 
 getFnSymbols (ConstantCtxtSubst _ _ _ s) =  getFnSymbols s 
 
instance HasFnSymbol (Binding) where
 getFnSymbols (_ := e) = (getFnSymbols e) 
 
instance HasFnSymbol (ChainVar) where
 getFnSymbols (MetaChain _ _ _ _  s) = getFnSymbols s
 getFnSymbols (ConstantChain _ _ _ _ s) = getFnSymbols s
instance HasFnSymbol (Environment) where
 getFnSymbols env = 
  (concatMap getFnSymbols (bindings env))
  ++ (concatMap getFnSymbols (metaChains env))
  ++ (concatMap getFnSymbols (constantChains env))
   
class Constantify a where 
 constantify :: a -> a
 deconstantify :: a -> a

instance Constantify a => Constantify [a] where
 constantify = map constantify
 deconstantify = map deconstantify
 
instance (Constantify a,Constantify b) => Constantify (a,b) where
 constantify (a,b) = (constantify a, constantify b)
 deconstantify (a,b) = (deconstantify a, deconstantify b)

-- instance (Constantify a,Constantify b) => Constantify (Either a b) where
--  constantify (Left a) = Left (constantify a)
--  constantify (Right a) = Right (constantify a) 
--  deconstantify (Left a) = Left (deconstantify a)
--  deconstantify (Right a) = Right (deconstantify a) 
 
isPseudoMV :: VarLift -> Bool 
isPseudoMV (MetaVarLift _ "--") = True
isPseudoMV _                    = False

instance Constantify VarLift where
 constantify (MetaVarLift s x)          = ConstantVarLift s x
 constantify e                          = e
 deconstantify (ConstantVarLift s x)    = MetaVarLift s x
 deconstantify e                        = e
 
instance Constantify ArgPos where 
 constantify (VarPos v) = VarPos (constantify v)
 constantify (Binder v e) = Binder (map constantify v) (constantify e)
 deconstantify (Binder v e) = Binder (map deconstantify v) (deconstantify e)
 deconstantify (VarPos v) = VarPos (deconstantify v)
 
instance Constantify Expression where
 constantify  (VarL v) = VarL (constantify v)
 constantify  (Fn f args) = Fn f (map constantify args)
 constantify  (Letrec env e) = Letrec (constantify env) (constantify e)
 constantify  (FlatLetrec env e) = FlatLetrec (constantify env) (constantify e)
 constantify  (MetaExpression sub s) = Constant sub s
 constantify  (MetaCtxt s srt name) = ConstantCtxt s srt name
 constantify  (MetaCtxtSubst s srt name (Hole _)) = ConstantCtxt s srt name 
 constantify  (MetaCtxtSubst s srt name e) = ConstantCtxtSubst s srt name (constantify e)
 constantify  (Hole s) = (Hole s)
 constantify  (Constant sub s) = Constant sub s
 constantify  (ConstantCtxt sub srt name) = ConstantCtxt sub srt name
 constantify  (ConstantCtxtSubst sub srt name (Hole _)) = ConstantCtxt sub srt name 
 constantify  (ConstantCtxtSubst sub srt name e) = ConstantCtxtSubst sub srt name (constantify e)

 deconstantify  (VarL v) = VarL (deconstantify v)
 deconstantify  (Fn f args) = Fn f (map deconstantify args)
 deconstantify  (Letrec env e) = Letrec (deconstantify env) (deconstantify e)
 deconstantify  (FlatLetrec env e) = FlatLetrec (deconstantify env) (deconstantify e)
 deconstantify  (MetaExpression sub s) = MetaExpression sub s 
 deconstantify  (MetaCtxt s srt name) = MetaCtxt s srt name
 deconstantify  (MetaCtxtSubst s srt name e) = MetaCtxtSubst s srt name (deconstantify e)
 deconstantify  (Hole s) = (Hole s)
 deconstantify  (Constant sub s) = MetaExpression sub s
 deconstantify  (ConstantCtxt s srt name) = MetaCtxt s srt name
 deconstantify  (ConstantCtxtSubst s srt name e) = MetaCtxtSubst s srt name (deconstantify e)
 
instance Constantify Environment where
 constantify env = env {  bindings = map constantify (bindings env)
                        , metaChains = []
                        , metaEnvVars = []
                        , constantEnvVars =  (constantEnvVars env) ++ metaEnvVars env
                        , constantChains =   [ ConstantChain sub typ name (constantify z) (constantify s) | (ConstantChain sub typ name z s) <- constantChains env]
                                         ++  [ ConstantChain sub typ name (constantify z) (constantify s) | (MetaChain sub typ name z s) <- constantChains env]
                                         ++  [ ConstantChain sub typ name (constantify z) (constantify s) | (MetaChain sub typ name z s) <- metaChains env]
                                         ++  [ ConstantChain sub typ name (constantify z) (constantify s) | (ConstantChain sub typ name z s) <- metaChains env]                                         
                       }
 deconstantify env = env { bindings = map deconstantify (bindings env)
                        , constantChains = []
                        , constantEnvVars = []
                        , metaEnvVars =  (constantEnvVars env) ++ metaEnvVars env
                        , metaChains = [ MetaChain sub typ name (deconstantify z) (deconstantify s) | (ConstantChain sub typ name z s) <- constantChains env]
                                         ++  [ MetaChain sub typ name (deconstantify z) (deconstantify s) | (MetaChain sub typ name z s) <- metaChains env]
                       }
                       
instance Constantify Binding where
 constantify (b := e) = (constantify b) := (constantify e)
 deconstantify (b := e) = (deconstantify b) := (deconstantify e)  

instance (Constantify a,Constantify b) => Constantify (Either a b) where
 constantify    (Left a)        = Left  (constantify a)
 constantify    (Right a)       = Right (constantify a) 
 deconstantify  (Left a)        = Left  (deconstantify a)
 deconstantify  (Right a)       = Right (deconstantify a) 

 
type FunInfo   = [(FName,Arity,[BindArity])]
type Arity     = Int
type BindArity = Int



data SkeletonVarLift = SKVarLift | SKMetaVarLift | SKConstantVarLift
 deriving(Eq,Ord,Show)
data Skeleton = 
    SKVarL SkeletonVarLift
  | SKFn FName [SkeletonArgPos]
  | SKLetrec Int Int Int Int Int Skeleton 
  | SKFlatLetrec Int Int Int Int Int Skeleton 
  | SKMetaExpression String
  | SKConstant String
  | SKHole
  | SKMetaCtxt CtxtSort String
  | SKConstantCtxt CtxtSort String 
  | SKMetaCtxtSubst CtxtSort String Skeleton
  | SKConstantCtxtSubst CtxtSort String Skeleton
 deriving(Eq,Ord,Show)
data SkeletonArgPos = SKBinder [SkeletonVarLift] Skeleton 
                    | SKVarPos SkeletonVarLift 
 deriving(Eq,Ord,Show)

fingerprintVar (MetaVarLift _ _) = SKMetaVarLift
fingerprintVar (ConstantVarLift _ _) = SKConstantVarLift
fingerprintVar (VarLift _ _) = SKVarLift

fingerprintArgPos (Binder args e) =
  SKBinder (map fingerprintVar args) (fingerprint e)
fingerprintArgPos (VarPos v) =
  SKVarPos (fingerprintVar v)

fingerprint :: Expression -> Skeleton
fingerprint (VarL v) = SKVarL (fingerprintVar v)
fingerprint (Fn n args) = SKFn n (map fingerprintArgPos args)
fingerprint (Letrec env e) =
 let b  = length $ bindings env
     me = length $ metaEnvVars env
     ce = length $ constantEnvVars env
     mc = length $ metaChains env
     cc = length $ constantChains env
 in SKLetrec b me ce mc cc (fingerprint e)
fingerprint (FlatLetrec env e) =
 let b  = length $ bindings env
     me = length $ metaEnvVars env
     ce = length $ constantEnvVars env
     mc = length $ metaChains env
     cc = length $ constantChains env
 in SKFlatLetrec b me ce mc cc (fingerprint e)
fingerprint (MetaExpression _ n) = SKMetaExpression n
fingerprint (Constant _ n) = SKConstant n
fingerprint (MetaCtxt sub srt n) = SKMetaCtxt srt n 
fingerprint (ConstantCtxt sub srt n) = SKConstantCtxt srt n
fingerprint (MetaCtxtSubst sub srt n e) = SKMetaCtxtSubst srt n (fingerprint e) 
fingerprint (ConstantCtxtSubst sub srt n e) = SKConstantCtxtSubst srt n (fingerprint e)
fingerprint (Hole _) = SKHole



-- compute all meta variables

metaVarsVar (MetaVarLift sub name) = [(SCVar $ MetaVarLift sub name)]
metaVarsVar (ConstantVarLift sub name) = [(SCVar $ ConstantVarLift sub name)]
metaVarsVar (VarLift sub name) = []

metaVarsArgPos (VarPos _) = []
metaVarsArgPos (Binder v e) = (concatMap metaVarsVar v) ++ (metaVars e)

metaVars (VarL x) = metaVarsVar x
metaVars (Fn _ args) = concatMap metaVarsArgPos args
metaVars (Letrec env e) = (metaVars e) ++ (metaVarsEnv env)
metaVars (FlatLetrec env e) = (metaVars e) ++ (metaVarsEnv env)
metaVars e@(MetaExpression sub name) = [SCExpr e]
metaVars e@(Constant _ _) = [SCExpr e]
metaVars (Hole _) = []
metaVars d@(MetaCtxt _ _ _) = [SCCtxt d]
metaVars d@(ConstantCtxt _ _ _) = [SCCtxt d]
metaVars (MetaCtxtSubst a b c expr) = (SCCtxt (MetaCtxt a b c)):(metaVars expr)
metaVars (ConstantCtxtSubst a b c expr) = (SCCtxt (ConstantCtxt a b c)):(metaVars expr)

metaVarsEnv env =
      (concat [ (metaVarsVar x) ++ metaVars e |  (x := e) <- bindings env])
   ++ (concat [ (SCMetaChain sub typ name):(metaVarsVar z ++ metaVars s) | (MetaChain sub typ name z s) <- metaChains env])
   ++ (concat [ (SCConstantChain sub typ name):(metaVarsVar z ++ metaVars s) | (ConstantChain sub typ name z s) <- constantChains env])
   ++ [ (SCMetaEnv v) | v@(Env sub e) <- metaEnvVars env]
   ++ [ (SCConstantEnv v) | v@(Env sub e) <- constantEnvVars env]
   
   
   
 

getEnvironments :: Expression -> [Environment]
-- | computing all environments of an expression
getEnvironments (Fn f args) = concatMap getEnvironmentsArgPos args
getEnvironments (Letrec env s) = env:(getEnvironments s)
getEnvironments (FlatLetrec env s) = env:(getEnvironments s)
getEnvironments (MetaCtxtSubst subs sort name e) = getEnvironments e
getEnvironments (ConstantCtxtSubst subs sort name e) = getEnvironments e
getEnvironments  _ = []
getEnvironmentsArgPos (Binder _ e) = getEnvironments e
getEnvironmentsArgPos _ = [] 


getNCCsEnvLVC env =
  let bindvars  =     [SCVar x | x@(VarLift (Substitution []) var) := s <- bindings env]
                   ++ [SCVar x | x@(ConstantVarLift (Substitution []) var) := s <- bindings env]
                   ++ [SCVar x | x@(MetaVarLift (Substitution []) var) := s <- bindings env]
      chainvars =     [SCVar z | ConstantChain sub typ name z@(VarLift (Substitution []) var) s <- constantChains env]
                  ++  [SCVar z | ConstantChain sub typ name z@(ConstantVarLift (Substitution []) var) s <- constantChains env]
                  ++  [SCVar z | ConstantChain sub typ name z@(MetaVarLift (Substitution []) var) s <- constantChains env]
                  ++  [SCVar z | MetaChain sub typ name z@(VarLift (Substitution []) var) s <- metaChains env]
                  ++  [SCVar z | MetaChain sub typ name z@(ConstantVarLift (Substitution []) var) s <- metaChains env]
                  ++  [SCVar z | MetaChain sub typ name z@(MetaVarLift (Substitution []) var) s <- metaChains env]
                  
      envs      = [SCConstantEnv envar | envar@(Env (Substitution []) e) <- constantEnvVars env]
                  ++
                  [SCMetaEnv envar | envar@(Env (Substitution []) e) <- metaEnvVars env]
      cc        = [SCConstantChain sub typ name | ConstantChain sub typ name z s <- constantChains env  ]
                  ++ [SCMetaChain sub typ name | MetaChain sub typ name z s <- metaChains env  ]
  in [(x,y) | x <- bindvars, y <- bindvars, x /= y]
      ++ [(x,y) | x <- bindvars, y <- chainvars, x /= y]
      ++ [(x,y) | x <- chainvars, y <- bindvars, x /= y]
      ++ [(x,y) | x <- chainvars, y <- chainvars, x /= y]
      ++ [(x,y) | x <- bindvars, y <- envs      ]
      ++ [(x,y) | x <- chainvars, y <- envs      ]
      ++ [(x,y) | x <- bindvars, y <- cc      ]
      ++ [(x,y) | x <- chainvars, y <- cc      ]
      ++ [(x,y) | x <- chainvars, y <- envs]      
      ++ [(x,y) | x <- chainvars, y <- chainvars, x /= y ]     
 
getAllSCVarsFromSubstitution sub = Set.fromList $ process (substList sub) 
 where 
    process = (concatMap processItem)
    processItem (AlphaVarLift name num) = [SCVar (VarLift emptySubstitution name)]
    processItem (AlphaMetaVarLift name num) = [SCVar (MetaVarLift emptySubstitution name)]    
    processItem (AlphaConstantVarLift name num) = [SCVar (ConstantVarLift emptySubstitution name)]    
    processItem (AlphaExpr name num) = [SCExpr (MetaExpression emptySubstitution name)]    
    processItem (AlphaConstant name num) = [SCExpr (Constant emptySubstitution name)]        
    processItem (AlphaCtxt srt name num) = [SCCtxt (MetaCtxt  emptySubstitution srt name)]
    processItem (AlphaConstantCtxt srt name num) = [SCCtxt (ConstantCtxt  emptySubstitution srt name)]                
    processItem (CVAlphaCtxt srt name num) = [SCCtxt (MetaCtxt  emptySubstitution srt name)]
    processItem (CVAlphaConstantCtxt srt name num) = [SCCtxt (ConstantCtxt  emptySubstitution srt name)]                
    processItem (AlphaEnv name num) = [SCMetaEnv (Env emptySubstitution name)]
    processItem (AlphaConstantEnv name num) = [SCConstantEnv (Env emptySubstitution name)]   
    processItem (AlphaChain srt name num) = [SCMetaChain emptySubstitution srt name]    
    processItem (AlphaConstantChain srt name num) = [SCConstantChain emptySubstitution srt name]        
    processItem (LVAlphaEnv name num) = [SCMetaEnv (Env emptySubstitution name)]
    processItem (LVAlphaConstantEnv name num) = [SCConstantEnv (Env emptySubstitution name)]   
    processItem (LVAlphaChain srt name num) = [SCMetaChain  emptySubstitution srt name]    
    processItem (LVAlphaConstantChain srt name num) = [SCConstantChain emptySubstitution srt name]        
    processItem (SubstSet list) =  process list


getSCAllVarsAndAlpha expr = [  (dropSubstFromComp comp,getSubstFromSC comp) | comp <- Set.elems $ getSCAllVars expr ]
getSCCapVarsAndAlpha expr = [  (dropSubstFromComp comp,getSubstFromSC comp) | comp <- Set.elems $ getSCCapVars expr ]
    
getSubstFromSC (SCVar (VarLift sub v)) = sub 
getSubstFromSC (SCVar (MetaVarLift sub v)) = sub 
getSubstFromSC (SCVar (ConstantVarLift sub v)) = sub 
getSubstFromSC (SCExpr (MetaExpression sub v)) = sub 
getSubstFromSC (SCExpr (Constant sub v)) = sub 
getSubstFromSC (SCCtxt (MetaCtxt sub  sort name)) = sub 
getSubstFromSC (SCCtxt (ConstantCtxt sub  sort name)) = sub 
getSubstFromSC (SCMetaEnv (Env sub e)) = sub
getSubstFromSC (SCConstantEnv (Env sub e)) = sub
getSubstFromSC (SCMetaChain sub typ s) = sub
getSubstFromSC (SCConstantChain sub typ s) = sub

-- getSubstFromSC _ = error "undefined: Syntax 1890"

getAllVarsFromExpr expr =
  let comp = getSCAllVars expr
      subs = Set.unions $ map getAllSCVarsFromSubstitution $ map getSubstFromSC (Set.elems comp)
  in comp `Set.union` subs

toSubstSet :: [SubstItem] -> [SubstItem]  
toSubstSet [] = []
toSubstSet [x] = [x]
toSubstSet xs = [SubstSet xs] 

   
append subst1 subst2 = subst1{substList = substList subst1 ++ substList subst2}
 
varName (MetaVarLift _ n) = n
varName (VarLift _ n) = n
varName (ConstantVarLift _ n) = n
   
   
dropAllSubsVars  (VarLift sub name) =
    VarLift emptySubstitution name

dropAllSubsVars  (MetaVarLift sub name) =
    MetaVarLift emptySubstitution name
    
dropAllSubsVars  (ConstantVarLift sub name) =
    ConstantVarLift emptySubstitution name
    
    
dropAllSubsExpr  :: Expression -> Expression 
-- | removes all symbolic alpha renamings by replacing the 'Substitution'-components by an 'emptySubstitution'
dropAllSubsExpr  (VarL var) = VarL (dropAllSubsVars  var)

dropAllSubsExpr  e@(MetaExpression sub s) =
  MetaExpression emptySubstitution s
    
dropAllSubsExpr  e@(Constant sub s) =
  Constant emptySubstitution s
 
dropAllSubsExpr  e@(MetaCtxtSubst sub typ name s) =
  MetaCtxtSubst emptySubstitution typ name (dropAllSubsExpr  s)
  
dropAllSubsExpr  e@(ConstantCtxtSubst sub typ name s) =
  ConstantCtxtSubst emptySubstitution typ name (dropAllSubsExpr  s)

dropAllSubsExpr  e@(MetaCtxt sub typ name) =
  MetaCtxt emptySubstitution typ name
  
dropAllSubsExpr  e@(ConstantCtxt sub typ name ) =
  ConstantCtxt emptySubstitution typ name
   
dropAllSubsExpr  (Letrec env s) =
 let 
  s' = dropAllSubsExpr  s
  env' = dropAllSubsEnv  env
 in Letrec env' s'

dropAllSubsExpr  (FlatLetrec env s) =
 let 
  s' = dropAllSubsExpr  s
  env' = dropAllSubsEnv  env
 in FlatLetrec env' s'
  
  
  
dropAllSubsExpr  (Fn f args) = Fn f (map (dropAllSubsArgPos ) args)
dropAllSubsExpr  (Hole subs) = Hole emptySubstitution
dropAllSubsEnv  env =
  let
    mc = [MetaChain emptySubstitution typ name (dropAllSubsVars  z) (dropAllSubsExpr  s) | (MetaChain sub typ name z s) <- metaChains env]
    cc = [ConstantChain emptySubstitution typ name (dropAllSubsVars  z) (dropAllSubsExpr  s) | (ConstantChain sub typ name z s) <- constantChains env]
    me = [Env emptySubstitution name | (Env sub name) <- metaEnvVars env]
    ce = [Env emptySubstitution name | (Env sub name) <- constantEnvVars env]    
    b  = [(dropAllSubsVars  x) := (dropAllSubsExpr  e) | x:=e <- bindings env]
  in env{metaChains = mc, constantChains = cc, metaEnvVars = me, constantEnvVars = ce, bindings = b}

dropAllSubsArgPos  (VarPos v) =  VarPos (dropAllSubsVars v)
dropAllSubsArgPos  (Binder ys s) =
 let
     ys' = map (dropAllSubsVars ) ys
     s' = dropAllSubsExpr  s
 in Binder ys' s'    
   
dropSubstFromComp (SCVar (VarLift sub v))         = SCVar (VarLift emptySubstitution v)
dropSubstFromComp (SCVar (MetaVarLift sub v))     = SCVar (MetaVarLift emptySubstitution v)
dropSubstFromComp (SCVar (ConstantVarLift sub v)) = SCVar (ConstantVarLift emptySubstitution v) 
dropSubstFromComp (SCExpr (MetaExpression sub v)) = SCExpr (MetaExpression emptySubstitution v)
dropSubstFromComp (SCExpr (Constant sub v))       = SCExpr (Constant emptySubstitution v)
dropSubstFromComp (SCCtxt (MetaCtxt sub  sort name))     = SCCtxt (MetaCtxt emptySubstitution sort name)
dropSubstFromComp (SCCtxt (ConstantCtxt sub  sort name)) = SCCtxt (ConstantCtxt emptySubstitution sort name)
dropSubstFromComp (SCMetaEnv (Env sub e))                = SCMetaEnv (Env emptySubstitution e)
dropSubstFromComp (SCConstantEnv (Env sub e))            = SCConstantEnv (Env emptySubstitution e)
dropSubstFromComp (SCMetaChain sub typ s)                = SCMetaChain emptySubstitution typ s
dropSubstFromComp (SCConstantChain sub typ s)            = SCConstantChain emptySubstitution typ s
