-----------------------------------------------------------------------------
-- | 
-- Module      :  LRSX.Language.Alpha 
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--
--  Alpha-Renaming  and Alpha-Equivalence
--
-----------------------------------------------------------------------------
module LRSX.Language.Alpha where
import Data.List
import Data.Function
import Data.Maybe
import LRSX.Util.Util
import LRSX.Language.Syntax
import LRSX.Language.ContextDefinition
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import LRSX.Language.Alpha.Rename
import LRSX.Language.Alpha.Simplify 
import LRSX.Language.Alpha.Similar 
import LRSX.Language.Alpha.Equivalence
import LRSX.Language.Alpha.Refresh
-- import Debug.Trace
trace a b = b
v x = VarLift emptySubstitution x
lam f x s = Fn "\\" [Binder [f x] s]
m x = MetaVarLift emptySubstitution x
c x = ConstantVarLift emptySubstitution x
env x = Env emptySubstitution x

r e = let (a,b,c) = renameExpr [show i | i <- [1..]] e  in b
r' e = let (a,b,c) = renameExpr [show i | i <- [1000..]] e  in b
 