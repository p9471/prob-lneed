-----------------------------------------------------------------------------
-- | 
-- Module      :  LRSX.Language.Alpha.Simplify
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--
--  Alpha-Renaming  and Alpha-Equivalence
--
-----------------------------------------------------------------------------
module LRSX.Language.Alpha.Simplify where
import Data.List
import Data.Function
import Data.Maybe
import LRSX.Util.Util
import LRSX.Language.Syntax
import LRSX.Language.ContextDefinition
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
-- import Debug.Trace
trace a b = b

-- =============================================================================

-- simplification of alpha renamings

-- Representing Domains:
data Dom = Vars SyntaxComponent        -- All Variables of a Component
           | Cod SubstItem              -- Codomain of an alpha-renaming 
           | SingleVar VarLift          -- A single variable
 deriving(Eq,Show,Ord)
 


-- -----------------------------------------------------------------
--
-- Rule SimplU of |=
--
--   {Vars(U),Cod(a_{U,i}), eta |- eta'
--   ------------------------------------
--   a_{U,i}: eta � U |= a_{U,i}:eta' � U
--
-- U = S
simplU info (MetaExpression (Substitution xi@((AlphaExpr name1 num):eta)) name)
  | Just xi' <- removeDuplicatesOfARCs xi = -- trace ("XXX: " ++ show (xi,xi')) $ 
                                            simplU info (MetaExpression (Substitution xi') name)
  | name1 == name  = 
     
      let eta' = simplVEtaStart info [Vars (SCExpr (MetaExpression emptySubstitution name)),Cod  (AlphaExpr name1 num)] eta 
          
          
          
      in  MetaExpression (Substitution ((AlphaExpr name1 num):eta')) name
-- U = S*
simplU info (Constant (Substitution xi@((AlphaConstant name1 num):eta)) name)
  | Just xi' <- removeDuplicatesOfARCs xi = -- trace ("XXX: " ++ show (xi,xi')) $ 
                                            simplU info (Constant (Substitution xi') name)
  | name1 == name  = 
      let eta' = simplVEtaStart info [Vars (SCExpr (Constant emptySubstitution name)),Cod  (AlphaConstant name1 num)] eta 
      in  Constant (Substitution ((AlphaConstant name1 num):eta')) name
      
     
      
-- U = D
simplU info (MetaCtxt (Substitution xi@((AlphaCtxt typ' name1 num):eta)) typ name)
  | Just xi' <- removeDuplicatesOfARCs xi = -- trace ("XXX: " ++ show (xi,xi')) $ 
                                            simplU info (MetaCtxt (Substitution xi') typ name)
  | name1 == name  = 
      let eta' = simplVEtaStart info [Vars (SCCtxt (MetaCtxt emptySubstitution typ name)),Cod  (AlphaCtxt typ' name1 num)] eta 
      in  MetaCtxt (Substitution ((AlphaCtxt typ' name1 num):eta')) typ name
-- U = D*
simplU info (ConstantCtxt (Substitution xi@((AlphaConstantCtxt typ' name1 num):eta)) typ name)
  | Just xi' <- removeDuplicatesOfARCs xi = -- trace ("XXX: " ++ show (xi,xi')) $ 
                                            simplU info (ConstantCtxt (Substitution xi') typ name)
  | name1 == name  = 
      let eta' = simplVEtaStart info [Vars (SCCtxt (ConstantCtxt emptySubstitution typ name)),Cod  (AlphaConstantCtxt typ' name1 num)] eta 
      in  ConstantCtxt (Substitution ((AlphaConstantCtxt typ' name1 num):eta')) typ name
      
simplU info x = x       
-- simplU info x = error $ "in simplU: " ++ show x ++ "\n info: " ++ show info      
-- U = E      
simplUEnv info (Env (Substitution xi@((AlphaEnv name1 num):eta)) name)
  | Just xi' <- removeDuplicatesOfARCs xi = -- trace ("XXX: " ++ show (xi,xi')) $ 
                                            simplUEnv info (Env (Substitution xi') name)
  | name1 == name  = 
      let eta' = simplVEtaStart info [Vars (SCMetaEnv (Env emptySubstitution name)),Cod (AlphaEnv name num)] eta 
      in  Env  (Substitution ((AlphaEnv name1 num):eta')) name
-- U = E*
simplUEnv info (Env (Substitution xi@((AlphaConstantEnv name1 num):eta)) name)
  | Just xi' <- removeDuplicatesOfARCs xi = -- trace ("XXX: " ++ show (xi,xi')) $ 
                                            simplUEnv info (Env (Substitution xi') name)
  | name1 == name  = 
      let eta' = simplVEtaStart info [Vars (SCConstantEnv (Env emptySubstitution name)),Cod  (AlphaConstantEnv name num)] eta 
      in  Env  (Substitution ((AlphaConstantEnv name1 num):eta')) name
simplUEnv info x = x       
      
-- U = EE      
simplUChain info  (MetaChain (Substitution xi@((AlphaChain ty name1 num):eta)) typ name z e)
  | Just xi' <- removeDuplicatesOfARCs xi = -- trace ("XXX: " ++ show (xi,xi')) $ 
                                            simplUChain info (MetaChain (Substitution xi') typ name z e)
  | name1 == name  = 
      let eta' = simplVEtaStart info [Vars (SCMetaChain emptySubstitution typ name),Cod  (AlphaChain ty name num)] eta 
      in  MetaChain (Substitution ((AlphaChain ty name1 num):eta')) typ name z e
-- U = EE*
simplUChain info  (ConstantChain (Substitution xi@((AlphaConstantChain ty name1 num):eta)) typ name z e)
  | Just xi' <- removeDuplicatesOfARCs xi = -- trace ("XXX: " ++ show (xi,xi')) $ 
                                            simplUChain info (ConstantChain (Substitution xi') typ name z e)
  | name1 == name  = 
      let eta' = simplVEtaStart info [Vars (SCConstantChain emptySubstitution typ name),Cod  (AlphaConstantChain ty name num)] eta 
      in  ConstantChain (Substitution ((AlphaConstantChain ty name1 num):eta')) typ name z e
simplUChain info x = x      
      
-- -----------------------------------------------------------------


-- Rule SubstVar of |=
--
--         xi�x |= (a_{x,i} cup rc):eta�x
--       -------------------------------------
--             xi�x |= a_{x,i}�x
-- 
substVar info v = 
    case simplVar info v  of
         (VarLift (Substitution ((AlphaVarLift name1 num):eta)) name)
            | name1 == name            ->  VarLift (Substitution [AlphaVarLift name1 num]) name
         (VarLift (Substitution ((SubstSet set):eta)) name)
            | h:t <- [AlphaVarLift name1 num | AlphaVarLift name1 num <- set, name1 == name] ->  VarLift (Substitution [h]) name
         (VarLift xi' name) -> (VarLift xi' name)
         (MetaVarLift (Substitution ((AlphaMetaVarLift name1 num):eta)) name)
            | name1 == name            ->  MetaVarLift (Substitution [AlphaMetaVarLift name1 num]) name
         (MetaVarLift (Substitution ((SubstSet set):eta)) name)
            | h:t <- [AlphaMetaVarLift name1 num | AlphaMetaVarLift name1 num <- set, name1 == name] ->  MetaVarLift (Substitution [h]) name
         (MetaVarLift xi' name)  ->  (MetaVarLift xi' name)
         (ConstantVarLift (Substitution ((AlphaConstantVarLift name1 num):eta)) name)
            | name1 == name            -> ConstantVarLift (Substitution [AlphaConstantVarLift name1 num]) name
         (ConstantVarLift (Substitution ((SubstSet set):eta)) name)
            | h:t <- [AlphaConstantVarLift name1 num | AlphaConstantVarLift name1 num <- set, name1 == name] -> 
            --trace ("XXXXXXXXXXXXXXXXX" ++ show h) $ 
            ConstantVarLift (Substitution [h]) name
         (ConstantVarLift xi' name) ->  (ConstantVarLift xi' name)

-- Rule SimplVar of |=
--
--         {x},eta |- eta'
--       -----------------------
--          eta�x |= eta'�x
--          
simplVar info v@(ConstantVarLift (Substitution eta) name) 
 | Just eta' <- removeDuplicatesOfARCs eta = -- trace ("XXX: " ++ show (eta,eta')) $ 
                                             simplVar info (ConstantVarLift (Substitution eta') name)
 | otherwise = 
     let eta' = simplVEtaStart info [SingleVar (ConstantVarLift emptySubstitution name)] eta 
     in if eta == eta' then   ConstantVarLift (Substitution eta') name
                       else simplVar info  $ ConstantVarLift (Substitution eta') name
 
simplVar info v@(VarLift xi@(Substitution eta) name) 
 | Just eta' <- removeDuplicatesOfARCs eta = -- trace ("XXX: " ++ show (eta,eta')) $ 
                                             simplVar info (VarLift (Substitution eta') name)
 | otherwise = 
    let eta' =  simplVEtaStart info [SingleVar (VarLift emptySubstitution name)] eta 
    in if eta == eta' then  VarLift (Substitution eta') name
                      else simplVar info $  VarLift (Substitution eta') name
 
simplVar info v@(MetaVarLift xi@(Substitution eta) name) 
 | Just eta' <- removeDuplicatesOfARCs eta = -- trace ("XXX: " ++ show (eta,eta')) $ 
                                             simplVar info (MetaVarLift (Substitution eta') name)
 | otherwise = 
     let eta' = simplVEtaStart info [SingleVar (MetaVarLift emptySubstitution name)] eta 
     in if eta == eta' then 
             MetaVarLift (Substitution eta') name
             else
             simplVar info $ MetaVarLift (Substitution eta') name
  
removeARCByName arc [] = Nothing
removeARCByName arc ((SubstSet set):xs) = 
  case removeARCByName arc set of 
       Just set' -> case removeARCByName arc xs of
                      Nothing -> Just ((SubstSet set'):xs)
                      Just xs' -> Just ((SubstSet set'):xs')
       Nothing -> case removeARCByName arc xs of
                      Nothing -> Nothing
                      Just xs' -> Just ((SubstSet set):xs')                      
                      
removeARCByName arc (arc':xs) 
  | subsumeNameEqual arc arc' = case removeARCByName arc xs of 
                          Nothing -> Just xs
                          Just xs' -> Just xs'
  | otherwise = case removeARCByName arc xs of
                   Nothing -> Nothing
                   Just xs' -> Just (arc':xs')
                   
-- note that nameEqual must not be symmetric, since in a_{E,i}, LV(a_{E,j})   
-- LV(a_{E,j} can be removed, but in 
-- LV(a_{E,j}), a_{E,i}
-- a_{E,i} should not be removed (even this does not occur...)


nameEqual (AlphaVarLift n1 _) (AlphaVarLift n2 _)                               = n1 == n2
nameEqual (AlphaMetaVarLift  n1 _) (AlphaMetaVarLift  n2 _)                     = n1 == n2
nameEqual (AlphaConstantVarLift  n1 _) (AlphaConstantVarLift  n2 _)             = n1 == n2

nameEqual (AlphaExpr n1 _) (AlphaExpr n2 _)                                     = n1 == n2
nameEqual (AlphaConstant n1 _) (AlphaConstant n2 _)                             = n1 == n2

nameEqual (AlphaEnv n1 _) (AlphaEnv n2 _)                                       = n1 == n2
nameEqual (AlphaConstantEnv n1 _)(AlphaConstantEnv n2 _)                        = n1 == n2

-- nameEqual (AlphaEnv n1 _) (LVAlphaEnv n2 _)                                     = n1 == n2
-- nameEqual (AlphaConstantEnv n1 _)(LVAlphaConstantEnv n2 _)                      = n1 == n2

nameEqual (AlphaCtxt _ n1 _)(AlphaCtxt _ n2 _)                                  = n1 == n2
nameEqual (AlphaConstantCtxt _ n1 _)(AlphaConstantCtxt _ n2 _)                  = n1 == n2
-- nameEqual (AlphaCtxt _ n1 _)(CVAlphaCtxt _ n2 _)                                = n1 == n2
-- nameEqual (AlphaConstantCtxt _ n1 _)(CVAlphaConstantCtxt _ n2 _)                = n1 == n2

nameEqual (AlphaChain t n1 _)(AlphaChain t2 n2 _)                               = n1 == n2
-- nameEqual (AlphaChain t n1 _)(LVAlphaChain t2 n2 _)                             = n1 == n2
nameEqual (AlphaConstantChain t n1 _)(AlphaConstantChain t2 n2 _)               = n1 == n2
-- nameEqual (AlphaConstantChain t n1 _)(LVAlphaConstantChain t2 n2 _)             = n1 == n2

nameEqual (CVAlphaCtxt _ n1 _)(CVAlphaCtxt _ n2 _)                              = n1 == n2
nameEqual (CVAlphaConstantCtxt _ n1 _)(CVAlphaConstantCtxt _ n2 _)              = n1 == n2

nameEqual (LVAlphaEnv n1 _)(LVAlphaEnv n2 _)                                    = n1 == n2
nameEqual (LVAlphaConstantEnv n1 _)(LVAlphaConstantEnv n2 _)                    = n1 == n2
nameEqual (LVAlphaChain t n1 _)(LVAlphaChain t2 n2 _)                           = n1 == n2
nameEqual (LVAlphaConstantChain t n1 _)(LVAlphaConstantChain t2 n2 _)           = n1 == n2
nameEqual _ _ = False     


nameEqual (AlphaVarLift n1 _) (AlphaVarLift n2 _)                               = n1 == n2
nameEqual (AlphaMetaVarLift  n1 _) (AlphaMetaVarLift  n2 _)                     = n1 == n2
nameEqual (AlphaConstantVarLift  n1 _) (AlphaConstantVarLift  n2 _)             = n1 == n2

nameEqual (AlphaExpr n1 _) (AlphaExpr n2 _)                                     = n1 == n2
nameEqual (AlphaConstant n1 _) (AlphaConstant n2 _)                             = n1 == n2

nameEqual (AlphaEnv n1 _) (AlphaEnv n2 _)                                       = n1 == n2
nameEqual (AlphaConstantEnv n1 _)(AlphaConstantEnv n2 _)                        = n1 == n2

-- nameEqual (AlphaEnv n1 _) (LVAlphaEnv n2 _)                                     = n1 == n2
-- nameEqual (AlphaConstantEnv n1 _)(LVAlphaConstantEnv n2 _)                      = n1 == n2

nameEqual (AlphaCtxt _ n1 _)(AlphaCtxt _ n2 _)                                  = n1 == n2
nameEqual (AlphaConstantCtxt _ n1 _)(AlphaConstantCtxt _ n2 _)                  = n1 == n2
-- nameEqual (AlphaCtxt _ n1 _)(CVAlphaCtxt _ n2 _)                                = n1 == n2
-- nameEqual (AlphaConstantCtxt _ n1 _)(CVAlphaConstantCtxt _ n2 _)                = n1 == n2

nameEqual (AlphaChain t n1 _)(AlphaChain t2 n2 _)                               = n1 == n2
-- nameEqual (AlphaChain t n1 _)(LVAlphaChain t2 n2 {-_-})                             = n1 == n2
nameEqual (AlphaConstantChain t n1 _)(AlphaConstantChain t2 n2 _)               = n1 == n2
-- nameEqual (AlphaConstantChain t n1 _)(LVAlphaConstantChain t2 n2 _)             = n1 == n2

nameEqual (CVAlphaCtxt _ n1 _)(CVAlphaCtxt _ n2 _)                              = n1 == n2
nameEqual (CVAlphaConstantCtxt _ n1 _)(CVAlphaConstantCtxt _ n2 _)              = n1 == n2

nameEqual (LVAlphaEnv n1 _)(LVAlphaEnv n2 _)                                    = n1 == n2
nameEqual (LVAlphaConstantEnv n1 _)(LVAlphaConstantEnv n2 _)                    = n1 == n2
nameEqual (LVAlphaChain t n1 _)(LVAlphaChain t2 n2 _)                           = n1 == n2
nameEqual (LVAlphaConstantChain t n1 _)(LVAlphaConstantChain t2 n2 _)           = n1 == n2
nameEqual _ _ = False     














subsumeNameEqual (AlphaVarLift n1 _) (AlphaVarLift n2 _)                               = n1 == n2
subsumeNameEqual (AlphaMetaVarLift  n1 _) (AlphaMetaVarLift  n2 _)                     = n1 == n2
subsumeNameEqual (AlphaConstantVarLift  n1 _) (AlphaConstantVarLift  n2 _)             = n1 == n2

subsumeNameEqual (AlphaExpr n1 _) (AlphaExpr n2 _)                                     = n1 == n2
subsumeNameEqual (AlphaConstant n1 _) (AlphaConstant n2 _)                             = n1 == n2

subsumeNameEqual (AlphaEnv n1 _) (AlphaEnv n2 _)                                       = n1 == n2
subsumeNameEqual (AlphaConstantEnv n1 _)(AlphaConstantEnv n2 _)                        = n1 == n2

subsumeNameEqual (AlphaEnv n1 _) (LVAlphaEnv n2 _)                                     = n1 == n2
subsumeNameEqual (AlphaConstantEnv n1 _)(LVAlphaConstantEnv n2 _)                      = n1 == n2

subsumeNameEqual (AlphaCtxt _ n1 _)(AlphaCtxt _ n2 _)                                  = n1 == n2
subsumeNameEqual (AlphaConstantCtxt _ n1 _)(AlphaConstantCtxt _ n2 _)                  = n1 == n2
subsumeNameEqual (AlphaCtxt _ n1 _)(CVAlphaCtxt _ n2 _)                                = n1 == n2
subsumeNameEqual (AlphaConstantCtxt _ n1 _)(CVAlphaConstantCtxt _ n2 _)                = n1 == n2

subsumeNameEqual (AlphaChain t n1 _)(AlphaChain t2 n2 _)                               = n1 == n2
subsumeNameEqual (AlphaChain t n1 _)(LVAlphaChain t2 n2 _)                             = n1 == n2
subsumeNameEqual (AlphaConstantChain t n1 _)(AlphaConstantChain t2 n2 _)               = n1 == n2
subsumeNameEqual (AlphaConstantChain t n1 _)(LVAlphaConstantChain t2 n2 _)             = n1 == n2

subsumeNameEqual (CVAlphaCtxt _ n1 _)(CVAlphaCtxt _ n2 _)                              = n1 == n2
subsumeNameEqual (CVAlphaConstantCtxt _ n1 _)(CVAlphaConstantCtxt _ n2 _)              = n1 == n2

subsumeNameEqual (LVAlphaEnv n1 _)(LVAlphaEnv n2 _)                                    = n1 == n2
subsumeNameEqual (LVAlphaConstantEnv n1 _)(LVAlphaConstantEnv n2 _)                    = n1 == n2
subsumeNameEqual (LVAlphaChain t n1 _)(LVAlphaChain t2 n2 _)                           = n1 == n2
subsumeNameEqual (LVAlphaConstantChain t n1 _)(LVAlphaConstantChain t2 n2 _)           = n1 == n2
subsumeNameEqual _ _ = False     

getName (AlphaVarLift n1 _)             = Just n1
getName (AlphaMetaVarLift  n1 _)        = Just n1
getName (AlphaConstantVarLift  n1 _)    = Just n1
getName (AlphaExpr n1 _)                = Just n1
getName (AlphaConstant n1 _)            = Just n1 
getName (AlphaEnv n1 _)                 = Just n1
getName (AlphaCtxt _ n1 _)              = Just n1
getName (AlphaConstantCtxt _ n1 _)      = Just n1
getName (AlphaChain t n1 _)             = Just n1
getName (AlphaConstantChain t n1 _)     = Just n1
getName (CVAlphaCtxt _ n1 _)            = Just n1
getName (CVAlphaConstantCtxt _ n1 _)    = Just n1
getName (LVAlphaEnv n1 _)               = Just n1
getName (LVAlphaConstantEnv n1 _)       = Just n1
getName (LVAlphaChain t n1 _)           = Just n1
getName (LVAlphaConstantChain t n1 _)   = Just n1
getName other                           = Nothing 

removeDuplicatesOfARCs [] = Nothing
removeDuplicatesOfARCs ((SubstSet (arc:set)):xs) = 
  case removeARCByName arc ((SubstSet set):xs) of 
    Just ((SubstSet set'):xs') ->   case removeDuplicatesOfARCs ((SubstSet set'):xs') of 
                                      Nothing -> Just ((SubstSet (arc:set')):xs')
                                      Just ((SubstSet set''):xs'') -> Just ((SubstSet (arc:set'')):xs'')
    Nothing -> case removeDuplicatesOfARCs ((SubstSet set):xs) of 
               Nothing -> Nothing
               Just ((SubstSet set'):xs') -> Just ((SubstSet (arc:set')):xs')
       

  
removeDuplicatesOfARCs (arc:xs) = 
  case removeARCByName arc xs of 
     Nothing -> case removeDuplicatesOfARCs xs of 
                   Nothing -> Nothing
                   Just xs' -> Just ( arc:xs')
     Just xs' -> case removeDuplicatesOfARCs xs' of 
                   Nothing -> Just $ arc:xs'
                   Just xs'' -> Just $ arc:xs''

  
simplVEtaStart info vs eta = simplVEta info vs (flattenSubst eta)
  
simplVEta info vs eta
  | Just simplerEta <- simplifySubst eta = simplVEta info vs simplerEta
  
simplVEta info vs eta
  | success@((_,_:_:_,_):_) <- [ (before,x:(b++[y]),a)| (before,x,after) <- splits2 eta, (b,y,a) <- splits2 after, canBeTransformedIntoSet info (x:(b++[y]))]
  = 
    let (before,set,after) = head (sortBy (\(_,x,_) (_,y,_) -> compare (length y) (length x)) success)
        etanew =   (before ++(SubstSet set):after)   
    in  simplVEta info vs etanew
  

--  
-- Order  
--
-- simplVEta info vs oldeta@((SubstSet set):eta) =
--  let possibilities = map snd $ sortBy (compare `on` fst) [  (length rek,rek) | (arci,arcs) <- splits set,let rek = simplVEta info vs (arci:(arcs ++ eta))]
--  in case possibilities of
--       []    -> oldeta
--       (h:t) -> h


--
-- RemARC
--
 
{-
simplVEta info vs ((SubstSet set):eta)
   | (arc,rest) <- splits set
   , checkPrefix vs arc (nablaSets info) = simplVEta info  vs ((SubstSet rest):eta)
   
simplVEta info vs (arc:eta)
   | checkPrefix vs arc (nablaSets info) = simplVEta info vs eta
-}
   
simplVEta info vs ((SubstSet set):eta)
  | ((before,after,rest):_) <- 
       [ (before,after,rest)
              | (before,after) <-  [splitAt i vs | i <- [0..length vs]]
              , (arc,rest) <- splits set
              , (before,arc) `elemNameEq` (nablaSets info)] =  simplVEta info vs ((SubstSet rest):eta)

  
simplVEta info vs (arc:eta)
  | ((before,after):_) <- 
       [(before,after)
       | (before,after) <-  [splitAt i vs | i <- [0..length vs]]
       , (before,arc) `elemNameEq` (nablaSets info)] = simplVEta info vs eta

       
simplVEta info vs (arc:eta) 
  | case arc of 
       SubstSet _ -> False
       _ -> True
  , all (\v -> nocap (nccs info) arc v)  vs =
    -- trace ("\n REMOVED: " ++ show arc) $
    simplVEta info vs eta
    
simplVEta info vs ((SubstSet set):eta) 
  | (arc,rest):_ <- 
     [ (arc,rest) 
      | (arc,rest) <- splits set, all (\v -> nocap (nccs info) arc v) vs
     ]
 = 
 -- trace ("\n REMOVED: " ++ show arc) $
   
   if null rest then (simplVEta info vs eta)
                else simplVEta info vs ((SubstSet rest):eta)
                
--
--
-- ProcARC
--
simplVEta info vs (arc:eta) 
  |  case arc of 
       (SubstSet eta) -> False
       _ -> True =  case simplVEta info (vs ++ [(Cod arc)]) eta of 
                      eta' -> arc:eta'
simplVEta info vs ((SubstSet set):eta) = case simplVEta info (vs ++ (map Cod set)) eta of 
                                           eta' -> (SubstSet set):eta'
simplVEta info vs [] = []                      
simplVEta info vs eta = error $ show eta                      


checkPrefix :: [Dom] -> SubstItem -> PPMap -> Bool
checkPrefix vs arc nSets  = 
  case getName arc of 
    Nothing -> False
    Just n -> case Map.lookup n nSets of 
                 Nothing -> False
                 Just mp' -> case Map.lookup (KS arc) mp' of
                               Just pmap -> go (map domToKeyOfDom vs) pmap
                               Nothing -> False
 where
   go []     (PMap pmap) = False
   go (v:vs) (PMap pmap) = case Map.lookup v pmap of
                             Just Nothing -> null vs
                             Just (Just pmap')   -> go vs pmap'
                             Nothing      -> False
                      
                 

data PMap = PMap (Map.Map KeyOfDom (Maybe PMap))
type PPMap = Map.Map String (Map.Map KeyNameSubstItem PMap)
{-

computePPMap :: SplittedNCCs -> PPMap
computePPMap info = 
  let splitted = [(comp,s) | (comp,set) <- Map.assocs info, s <- Set.elems set]
  in  Map.fromList 
    [ (a,( Map.fromList
            [ (u,Set.fromList (map (\(u,v) -> v) gr)) | gr@((u,v):ggs)  <- (groupBy ((==) `on` (\(b,c) -> b)))
                                                              (sortBy (compare `on` (\(b,c) -> b))
                                                              (map (\(a,b,c) -> (b,c)) g))]
            
            
            
            ))
    
    
    
    | g@((a,b,c):gs) <- (groupBy ((==) `on` (\(a,b,c) -> a)) $
             sortBy (compare `on` (\(a,b,c) -> a)) $
               [(v',KS v,map domToKeyOfDom x)|   Just (xs,v) <- map proc splitted, x <-xs, isJust (getName v), let v' = fromJust (getName v) ])
    ]                                                                                             
                                                                                                 
                                                                                              
                                                                                                     
 where      
      proc (SCVar (MetaVarLift (Substitution eta@((AlphaMetaVarLift name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
      proc (SCVar (MetaVarLift (Substitution eta@((AlphaMetaVarLift name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCVar (MetaVarLift (Substitution eta@((AlphaMetaVarLift name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCVar (MetaVarLift (Substitution eta@((AlphaMetaVarLift name0 num):_)) name1),v@(SCExpr (Constant (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstant name2 "UDEF")
      proc (SCVar (MetaVarLift (Substitution eta@((AlphaMetaVarLift name0 num):_)) name1),v@(SCExpr (MetaExpression (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaExpr name2 "UDEF")
      proc (SCVar (MetaVarLift (Substitution eta@((AlphaMetaVarLift name0 num):_)) name1),v@(SCCtxt (MetaCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaCtxt srtA name2  "UDEF")
      proc (SCVar (MetaVarLift (Substitution eta@((AlphaMetaVarLift name0 num):_)) name1),v@(SCCtxt (ConstantCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstantCtxt srtA name2  "UDEF")
      proc (SCVar (MetaVarLift (Substitution eta@((AlphaMetaVarLift name0 num):_)) name1),v@(SCMetaEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaEnv  name2  "UDEF")
      proc (SCVar (MetaVarLift (Substitution eta@((AlphaMetaVarLift name0 num):_)) name1),v@(SCConstantEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstantEnv  name2  "UDEF")
      proc (SCVar (MetaVarLift (Substitution eta@((AlphaMetaVarLift name0 num):_)) name1),v@(SCMetaChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaChain typ  name2  "UDEF")
      proc (SCVar (MetaVarLift (Substitution eta@((AlphaMetaVarLift name0 num):_)) name1),v@(SCConstantChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstantChain typ name2  "UDEF")
        
        
      proc (SCVar (VarLift (Substitution eta@((AlphaVarLift name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCVar (VarLift (Substitution eta@((AlphaVarLift name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
      proc (SCVar (VarLift (Substitution eta@((AlphaVarLift name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCVar (VarLift (Substitution eta@((AlphaVarLift name0 num):_)) name1),v@(SCExpr (Constant (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstant name2 "UDEF")
      proc (SCVar (VarLift (Substitution eta@((AlphaVarLift name0 num):_)) name1),v@(SCExpr (MetaExpression (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name0)] ++ (map Cod eta),AlphaExpr name2 "UDEF")
      proc (SCVar (VarLift (Substitution eta@((AlphaVarLift name0 num):_)) name1),v@(SCCtxt (MetaCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name0)] ++ (map Cod eta),AlphaCtxt srtA name2  "UDEF")
      proc (SCVar (VarLift (Substitution eta@((AlphaVarLift name0 num):_)) name1),v@(SCCtxt (ConstantCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstantCtxt srtA name2  "UDEF")
      proc (SCVar (VarLift (Substitution eta@((AlphaVarLift name0 num):_)) name1),v@(SCMetaEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name0)] ++ (map Cod eta),AlphaEnv  name2  "UDEF")
      proc (SCVar (VarLift (Substitution eta@((AlphaVarLift name0 num):_)) name1),v@(SCConstantEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstantEnv  name2  "UDEF")
      proc (SCVar (VarLift (Substitution eta@((AlphaVarLift name0 num):_)) name1),v@(SCMetaChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name0)] ++ (map Cod eta),AlphaChain typ  name2  "UDEF")
      proc (SCVar (VarLift (Substitution eta@((AlphaVarLift name0 num):_)) name1),v@(SCConstantChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstantChain typ name2  "UDEF")        
        
        
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta), AlphaConstantVarLift name2 "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCExpr (Constant (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstant name2 "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCExpr (MetaExpression (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaExpr name2 "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCCtxt (MetaCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaCtxt srtA name2  "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCCtxt (ConstantCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstantCtxt srtA name2  "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCMetaEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaEnv  name2  "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCConstantEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstantEnv  name2  "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCMetaChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaChain typ  name2  "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCConstantChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstantChain typ name2  "UDEF")   
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCExpr (Constant (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstant name2 "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCExpr (MetaExpression (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaExpr name2 "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCCtxt (MetaCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaCtxt srtA name2  "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCCtxt (ConstantCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstantCtxt srtA name2  "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCMetaEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaEnv  name2  "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCConstantEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstantEnv  name2  "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCMetaChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaChain typ  name2  "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCConstantChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstantChain typ name2  "UDEF")
        
        
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ ([Vars (SCExpr $ Constant emptySubstitution name0)] ++ (map Cod eta)),AlphaConstantVarLift name2 "UDEF")
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ ([Vars (SCExpr $ Constant emptySubstitution name0)] ++ (map Cod eta)),AlphaVarLift name2 "UDEF")
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ ([Vars (SCExpr $ Constant emptySubstitution name0)] ++ (map Cod eta)),AlphaMetaVarLift name2 "UDEF")
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCExpr (Constant (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ Constant emptySubstitution name0)]++ (map Cod eta),AlphaConstant name2 "UDEF")
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCExpr (MetaExpression (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ Constant emptySubstitution name0)]++ (map Cod eta),AlphaExpr name2 "UDEF")
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCCtxt (MetaCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ Constant emptySubstitution name0)]++ (map Cod eta),AlphaCtxt srtA name2  "UDEF")
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCCtxt (ConstantCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ Constant emptySubstitution name0)]++ (map Cod eta),AlphaConstantCtxt srtA name2  "UDEF")
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCMetaEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ Constant emptySubstitution name0)]++ (map Cod eta),AlphaEnv  name2  "UDEF")
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCConstantEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ Constant emptySubstitution name0)]++ (map Cod eta),AlphaConstantEnv  name2  "UDEF")
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCMetaChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ Constant emptySubstitution name0)]++ (map Cod eta),AlphaChain typ  name2  "UDEF")
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCConstantChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ Constant emptySubstitution name0)]++ (map Cod eta),AlphaConstantChain typ name2  "UDEF")        
        
        
        
        
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaExpr name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaExpr name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaExpr name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCExpr (Constant (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)]++ (map Cod eta),AlphaConstant name2 "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaExpr name0 num):_)) name1),v@(SCExpr (MetaExpression (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)]++ (map Cod eta),AlphaExpr name2 "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaExpr name0 num):_)) name1),v@(SCCtxt (MetaCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)]++ (map Cod eta),AlphaCtxt srtA name2  "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaExpr name0 num):_)) name1),v@(SCCtxt (ConstantCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)]++ (map Cod eta),AlphaConstantCtxt srtA name2  "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaExpr name0 num):_)) name1),v@(SCMetaEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)]++ (map Cod eta),AlphaEnv  name2  "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaExpr name0 num):_)) name1),v@(SCConstantEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)]++ (map Cod eta),AlphaConstantEnv  name2  "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaExpr name0 num):_)) name1),v@(SCMetaChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)]++ (map Cod eta),AlphaChain typ  name2  "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaExpr name0 num):_)) name1),v@(SCConstantChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)]++ (map Cod eta),AlphaConstantChain typ name2  "UDEF")        
        
      proc (SCCtxt (MetaCtxt  (Substitution eta@((AlphaCtxt srt' name0 num):_)) srt name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCCtxt (MetaCtxt  (Substitution eta@((AlphaCtxt srt' name0 num):_)) srt name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCCtxt (MetaCtxt  (Substitution eta@((AlphaCtxt srt' name0 num):_)) srt name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
      proc (SCCtxt (MetaCtxt  (Substitution eta@((AlphaCtxt srt' name0 num):_)) srt name1),v@(SCExpr (MetaExpression (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaExpr name2 "UDEF")
      proc (SCCtxt (MetaCtxt  (Substitution eta@((AlphaCtxt srt' name0 num):_)) srt name1),v@(SCCtxt (MetaCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaCtxt srtA name2  "UDEF")
      proc (SCCtxt (MetaCtxt  (Substitution eta@((AlphaCtxt srt' name0 num):_)) srt name1),v@(SCCtxt (ConstantCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaConstantCtxt srtA name2  "UDEF")
      proc (SCCtxt (MetaCtxt  (Substitution eta@((AlphaCtxt srt' name0 num):_)) srt name1),v@(SCMetaEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaEnv  name2  "UDEF")
      proc (SCCtxt (MetaCtxt  (Substitution eta@((AlphaCtxt srt' name0 num):_)) srt name1),v@(SCConstantEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaConstantEnv  name2  "UDEF")
      proc (SCCtxt (MetaCtxt  (Substitution eta@((AlphaCtxt srt' name0 num):_)) srt name1),v@(SCMetaChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaChain typ  name2  "UDEF")
      proc (SCCtxt (MetaCtxt  (Substitution eta@((AlphaCtxt srt' name0 num):_)) srt name1),v@(SCConstantChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaConstantChain typ name2  "UDEF")            
        
        
      proc (SCCtxt (ConstantCtxt  (Substitution eta@((AlphaConstantCtxt srt' name0 num):_)) srt name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ ConstantCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCCtxt (ConstantCtxt  (Substitution eta@((AlphaConstantCtxt srt' name0 num):_)) srt name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ ConstantCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCCtxt (ConstantCtxt  (Substitution eta@((AlphaConstantCtxt srt' name0 num):_)) srt name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ ConstantCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
      proc (SCCtxt (ConstantCtxt  (Substitution eta@((AlphaConstantCtxt srt' name0 num):_)) srt name1),v@(SCExpr (MetaExpression (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ ConstantCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaExpr name2 "UDEF")
      proc (SCCtxt (ConstantCtxt  (Substitution eta@((AlphaConstantCtxt srt' name0 num):_)) srt name1),v@(SCCtxt (MetaCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ ConstantCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaCtxt srtA name2  "UDEF")
      proc (SCCtxt (ConstantCtxt  (Substitution eta@((AlphaConstantCtxt srt' name0 num):_)) srt name1),v@(SCCtxt (ConstantCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ ConstantCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaConstantCtxt srtA name2  "UDEF")
      proc (SCCtxt (ConstantCtxt  (Substitution eta@((AlphaConstantCtxt srt' name0 num):_)) srt name1),v@(SCMetaEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ ConstantCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaEnv  name2  "UDEF")
      proc (SCCtxt (ConstantCtxt  (Substitution eta@((AlphaConstantCtxt srt' name0 num):_)) srt name1),v@(SCConstantEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ ConstantCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaConstantEnv  name2  "UDEF")
      proc (SCCtxt (ConstantCtxt  (Substitution eta@((AlphaConstantCtxt srt' name0 num):_)) srt name1),v@(SCMetaChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ ConstantCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaChain typ  name2  "UDEF")
      proc (SCCtxt (ConstantCtxt  (Substitution eta@((AlphaConstantCtxt srt' name0 num):_)) srt name1),v@(SCConstantChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ ConstantCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaConstantChain typ name2  "UDEF")            
                              
      proc (SCMetaEnv (Env (Substitution eta@((AlphaEnv name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCMetaEnv (Env (Substitution eta@((AlphaEnv name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCMetaEnv (Env (Substitution eta@((AlphaEnv name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
      proc (SCMetaEnv (Env (Substitution eta@((AlphaEnv name0 num):_)) name1),v@(SCExpr (MetaExpression (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaExpr name2 "UDEF")
      proc (SCMetaEnv (Env (Substitution eta@((AlphaEnv name0 num):_)) name1),v@(SCCtxt (MetaCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaCtxt srtA name2  "UDEF")
      proc (SCMetaEnv (Env (Substitution eta@((AlphaEnv name0 num):_)) name1),v@(SCCtxt (ConstantCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaConstantCtxt srtA name2  "UDEF")
      proc (SCMetaEnv (Env (Substitution eta@((AlphaEnv name0 num):_)) name1),v@(SCMetaEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name0)] ++  (map Cod eta),AlphaEnv  name2  "UDEF")
      proc (SCMetaEnv (Env (Substitution eta@((AlphaEnv name0 num):_)) name1),v@(SCConstantEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name0)] ++  (map Cod eta),AlphaConstantEnv  name2  "UDEF")
      proc (SCMetaEnv (Env (Substitution eta@((AlphaEnv name0 num):_)) name1),v@(SCMetaChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name0)] ++  (map Cod eta),AlphaChain typ  name2  "UDEF")
      proc (SCMetaEnv (Env (Substitution eta@((AlphaEnv name0 num):_)) name1),v@(SCConstantChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaConstantChain typ name2  "UDEF")            

        
        
      proc (SCConstantEnv (Env (Substitution eta@((AlphaConstantEnv name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCConstantEnv (Env (Substitution eta@((AlphaConstantEnv name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCConstantEnv (Env (Substitution eta@((AlphaConstantEnv name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
      proc (SCConstantEnv (Env (Substitution eta@((AlphaConstantEnv name0 num):_)) name1),v@(SCExpr (MetaExpression (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaExpr name2 "UDEF")
      proc (SCConstantEnv (Env (Substitution eta@((AlphaConstantEnv name0 num):_)) name1),v@(SCCtxt (MetaCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaCtxt srtA name2  "UDEF")
      proc (SCConstantEnv (Env (Substitution eta@((AlphaConstantEnv name0 num):_)) name1),v@(SCCtxt (ConstantCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaConstantCtxt srtA name2  "UDEF")
      proc (SCConstantEnv (Env (Substitution eta@((AlphaConstantEnv name0 num):_)) name1),v@(SCMetaEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name0)] ++  (map Cod eta),AlphaEnv  name2  "UDEF")
      proc (SCConstantEnv (Env (Substitution eta@((AlphaConstantEnv name0 num):_)) name1),v@(SCConstantEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name0)] ++  (map Cod eta),AlphaConstantEnv  name2  "UDEF")
      proc (SCConstantEnv (Env (Substitution eta@((AlphaConstantEnv name0 num):_)) name1),v@(SCMetaChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name0)] ++  (map Cod eta),AlphaChain typ  name2  "UDEF")
      proc (SCConstantEnv (Env (Substitution eta@((AlphaConstantEnv name0 num):_)) name1),v@(SCConstantChain (Substitution []) typ name2))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaConstantChain typ name2  "UDEF")            
        
      proc (SCMetaChain (Substitution eta@((AlphaChain typ0 name0 num):_)) typ name1,v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCMetaChain (Substitution eta@((AlphaChain typ0 name0 num):_)) typ name1,v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCMetaChain (Substitution eta@((AlphaChain typ0 name0 num):_)) typ name1,v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
      proc (SCMetaChain (Substitution eta@((AlphaChain typ0 name0 num):_)) typ name1,v@(SCExpr (MetaExpression (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name0)] ++  (map Cod eta),AlphaExpr name2 "UDEF")
      proc (SCMetaChain (Substitution eta@((AlphaChain typ0 name0 num):_)) typ name1,v@(SCCtxt (MetaCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name0)] ++  (map Cod eta),AlphaCtxt srtA name2  "UDEF")
      proc (SCMetaChain (Substitution eta@((AlphaChain typ0 name0 num):_)) typ name1,v@(SCCtxt (ConstantCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name0)] ++  (map Cod eta),AlphaConstantCtxt srtA name2  "UDEF")
      proc (SCMetaChain (Substitution eta@((AlphaChain typ0 name0 num):_)) typ name1,v@(SCMetaEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name0)] ++   (map Cod eta),AlphaEnv  name2  "UDEF")
      proc (SCMetaChain (Substitution eta@((AlphaChain typ0 name0 num):_)) typ name1,v@(SCConstantEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name0)] ++   (map Cod eta),AlphaConstantEnv  name2  "UDEF")
      proc (SCMetaChain (Substitution eta@((AlphaChain typ0 name0 num):_)) typ name1,v@(SCMetaChain (Substitution []) typ1 name2))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name0)] ++   (map Cod eta),AlphaChain typ1  name2  "UDEF")
      proc (SCMetaChain (Substitution eta@((AlphaChain typ0 name0 num):_)) typ name1,v@(SCConstantChain (Substitution []) typ1 name2))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name0)] ++  (map Cod eta),AlphaConstantChain typ1 name2  "UDEF")            
        
      proc (SCConstantChain (Substitution eta@((AlphaConstantChain typ0 name0 num):_)) typ name1,v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCConstantChain (Substitution eta@((AlphaConstantChain typ0 name0 num):_)) typ name1,v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCConstantChain (Substitution eta@((AlphaConstantChain typ0 name0 num):_)) typ name1,v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
      proc (SCConstantChain (Substitution eta@((AlphaConstantChain typ0 name0 num):_)) typ name1,v@(SCExpr (MetaExpression (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name0)] ++  (map Cod eta),AlphaExpr name2 "UDEF")
      proc (SCConstantChain (Substitution eta@((AlphaConstantChain typ0 name0 num):_)) typ name1,v@(SCCtxt (MetaCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name0)] ++  (map Cod eta),AlphaCtxt srtA name2  "UDEF")
      proc (SCConstantChain (Substitution eta@((AlphaConstantChain typ0 name0 num):_)) typ name1,v@(SCCtxt (ConstantCtxt (Substitution []) srtA name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name0)] ++  (map Cod eta),AlphaConstantCtxt srtA name2  "UDEF")
      proc (SCConstantChain (Substitution eta@((AlphaConstantChain typ0 name0 num):_)) typ name1,v@(SCMetaEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name0)] ++   (map Cod eta),AlphaEnv  name2  "UDEF")
      proc (SCConstantChain (Substitution eta@((AlphaConstantChain typ0 name0 num):_)) typ name1,v@(SCConstantEnv (Env (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name0)] ++   (map Cod eta),AlphaConstantEnv  name2  "UDEF")
      proc (SCConstantChain (Substitution eta@((AlphaConstantChain typ0 name0 num):_)) typ name1,v@(SCMetaChain (Substitution []) typ1 name2))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name0)] ++   (map Cod eta),AlphaChain typ1  name2  "UDEF")
      proc (SCConstantChain (Substitution eta@((AlphaConstantChain typ0 name0 num):_)) typ name1,v@(SCConstantChain (Substitution []) typ1 name2))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name0)] ++  (map Cod eta),AlphaConstantChain typ1 name2  "UDEF")            
      proc _ = Nothing                             

splitSets ((Cod (SubstSet set)):rest) =
  [list ++  r | p <- permutations set, let list = map Cod p, r <- splitSets rest]
splitSets (x:rest) = [x:r | r <- splitSets rest]
splitSets [] = [[]]  
-}






  
-- elemNameEq (xs,v) xxs
--  | trace (show (xs,v)) False = undefined
--   | trace (show xxs) False = undefined
--  | trace (show (elemNameEqIT (xs,v) xxs)) True = elemNameEqIT (xs,v) xxs
-- elemNameEq ::  ([Dom],SubstItem) ->  Map.Map String (SubstItem,[Dom]) -> Bool

isPr :: [KeyOfDom] -> [KeyOfDom] -> Bool
-- isPr xs ys 
--   |  trace ("isPR: " ++ show (xs,ys)) False = undefined
isPr [] [] = True
isPr xs ((Codd (SubstSet [])):rest) = isPr xs rest
isPr ((Codd e):xs) ((Codd (SubstSet set)):rest)
  | (Codd e) `elem` (map Codd set) = isPr xs ((Codd $ SubstSet (deleteBy (nameEqual) e set)):rest)
isPr (a:xs) (b:ys) 
  | a == b = isPr xs ys
  | otherwise = False
isPr _ _ = False  
  
  


elemNameEq ::  ([Dom],SubstItem) ->  Map.Map String   (Map.Map KeyNameSubstItem (Set.Set [KeyOfDom])) -> Bool
-- (SubstItem,[Dom]) -> Bool

-- elemNameEq (xs,v) xxs 
--   | if (not (null xxs)) then trace ("xs  = " ++ show xs ++ "\nv   = " ++ show v ++ "\n xxs = " ++ show xxs) False else False = undefined


elemNameEq (xs,v) xxs 
--   | trace ("v:" ++ (show v)) True
--   , trace ("xs:" ++ show xs) True
--   , trace ("getName v:" ++ (show (getName v))) True
  | Just v' <- getName v
--   , trace ("Map.lookup v' xxs:" ++ (show (Map.lookup v' xxs))) True
  , Just map2 <- Map.lookup v' xxs
  , Just set2 <- Map.lookup (KS v) map2
--   , trace ("YYY: " ++ show (Set.size set2)) True
  , any (isPr (map domToKeyOfDom xs) )  (Set.elems set2) = True
--  
--   | Just v' <- getName v
--   , Just map2 <- Map.lookup v' xxs 
--   , Just set2 <- Map.lookup (KS v) map2
--   , trace ("YYY: " ++ show (Set.size set2)) True
--   , trace (show (Set.fromList $ concatMap splitSets' $ Set.elems set2)) True
--   , let res = (Set.fromList $ concatMap splitSets' $ Set.elems set2) in trace (show $ Set.size res) (Set.member (map domToKeyOfDom xs) res)               =
  

--         error  (("xs: " ++ show (map domToKeyOfDom xs) ++ "\n" ++ 
--                ("set: " ++ show (Set.elems set2))))
 | otherwise = False

 
 {-
  
elemNameEq (xs,v) mp   =
 let xxs = [(ys,w) | (v',mps) <- Map.assocs mp, (KS w, set) <- Map.assocs mps, ys <- Set.elems set]
     res = [ (ys,w) | (ys,w) <- xxs, w `nameEqual` v, nameEqualDomLists xs (map fromDomKey ys)]
     run (a:b:xs) = (show (a,b,compare a b,compare b a, a == b)):(run (b:xs))
     run _ = []
 in if not (null  res) then 
     trace ("OLDSUCNEWFAIL "  ++ "\n" ++
           "xs= " ++ show xs ++ "\n" ++
           "res = " ++ show res ++ "\n" ++
           "getName v : " ++ show (getName v) ++ "\n" ++
           -- "Map.lookup v' xxs: " ++ show (Map.lookup (fromJust $ getName v) mp) ++ "\n"
           "Map.lookup KS v map2" ++ (show (Map.lookup (KS v) (fromJust (Map.lookup (fromJust $ getName v) mp)))) ++ "\n" ++
           "member: " ++ (show (Set.member (map domToKeyOfDom xs) (fromJust (Map.lookup (KS v) (fromJust (Map.lookup (fromJust $ getName v) mp)))))) ++ "\n" ++
           
           "SORTED" ++ (intercalate "\n" $  map show $ (sort $ Set.elems (fromJust $ (Map.lookup (KS v) (fromJust (Map.lookup (fromJust $ getName v) mp)))))) ++ 
           "XXX" ++ "\n" ++ intercalate "\n"  [show (xs,u,compare (map domToKeyOfDom xs) u) | u <-  (sort $ Set.elems (fromJust $ (Map.lookup (KS v) (fromJust (Map.lookup (fromJust $ getName v) mp)))))] ++ "\n"  ++
           "UUUU" ++ "\n\n" ++ (intercalate "\n" (run (sort $ Set.elems (fromJust $ (Map.lookup (KS v) (fromJust (Map.lookup (fromJust $ getName v) mp))))))) ++
           "elemr: " ++ (show ((map domToKeyOfDom xs) `elem` (Set.elems (fromJust (Map.lookup (KS v) (fromJust (Map.lookup (fromJust $ getName v) mp)))))))
           
           
           ) error "STOP NOW!" True
           
                                                                                      else False
 where
   nameEqualDomLists ((Vars x):xs) ((Vars y):ys)           = x == y && nameEqualDomLists xs ys
   nameEqualDomLists ((SingleVar x):xs) ((SingleVar y):ys) = x == y && nameEqualDomLists xs ys   
   nameEqualDomLists ((Cod x):xs) ((Cod y):ys)             = nameEqual x y && nameEqualDomLists xs ys   
   nameEqualDomLists [] []                                 = True
   nameEqualDomLists _ _                                   = False-}
  
newtype KeyNameSubstItem = KS SubstItem
instance Show KeyNameSubstItem where
  show (KS a) = "KS " ++ show a
  
instance Eq KeyNameSubstItem where
   (==) (KS a) (KS b) = nameEqual a b
instance Ord KeyNameSubstItem where
   (KS a) <= (KS b) = if nameEqual a b then True else a <= b
   
   
data KeyOfDom  = KDOld Dom | Codd SubstItem
 deriving(Show)
instance Eq (KeyOfDom) where
  (KDOld a) == (KDOld b) = a == b
  (Codd sa) == (Codd sb) = sa `nameEqual` sb
instance Ord (KeyOfDom) where
  (KDOld a) <= (Codd b) = True
  (Codd a) <= (KDOld b) = False
  (KDOld a) <= (KDOld b) = a <= b
  (Codd a) <= (Codd b) = if a `nameEqual` b then True else a <= b

domToKeyOfDom (Cod a) = Codd a
domToKeyOfDom other = KDOld other
fromDomKey (Codd a) = Cod a
fromDomKey (KDOld a) = a
  
  
  
  
  
isAlphaVar (AlphaVarLift _ _) = True
isAlphaVar (AlphaMetaVarLift _ _) = True
isAlphaVar (AlphaConstantVarLift _ _) = True
isAlphaVar _  = False

alphaVarToVar (AlphaVarLift name _) = VarLift emptySubstitution name
alphaVarToVar (AlphaMetaVarLift name _) = MetaVarLift emptySubstitution name
alphaVarToVar (AlphaConstantVarLift name _) = ConstantVarLift emptySubstitution name

compareOnTypeAndName (AlphaVarLift name _) (AlphaVarLift name' _) =  name == name'
compareOnTypeAndName (AlphaMetaVarLift name _) (AlphaMetaVarLift name' _) =  name == name'
compareOnTypeAndName (AlphaConstantVarLift name _) (AlphaConstantVarLift name' _) =  name == name'
compareOnTypeAndName _ _ = False

canBeTransformedIntoSet info alphas = 
  (all isAlphaVar alphas)
  && 
  (nubBy compareOnTypeAndName alphas == alphas)  -- x_i /= x_j
  &&
  -- a_{x_i,i} nocap x_j for all i/=j:
  and 
     [nocap (nccs info) a (SingleVar xj) | (a,rest) <- splits alphas
                , aj <- rest
                , let xj = alphaVarToVar aj]
  --                       

type SplittedNCCs = Map.Map SyntaxComponent (Set.Set SyntaxComponent)
nocap ::  SplittedNCCs -> SubstItem -> Dom -> Bool                      
nocap info arc (Cod (SubstSet _)) = error "in nocap: subset in 1. arg"
nocap info (SubstSet _) _         = error "in nocap: subset in 2. arg"
nocap info arc item  =
   -- trace ("NOCAP:" ++ show (arc,item,nocapIt info arc item)) 
   nocapIt (info) arc item

-- CodRen
nocapIt info arc (Cod _)             = True
-- EmptyCV
nocapIt info (CVAlphaConstantCtxt (UserDefinedSort False _) _ _)  v  = True
nocapIt info (CVAlphaCtxt (UserDefinedSort False _) _ _)  v  = True
-- NCCDU
nocapIt info (CVAlphaConstantCtxt srt@(UserDefinedSort True _) name num)  (Vars u) 
  | Just ds <- Map.lookup u info
  , (SCCtxt (ConstantCtxt emptySubstitution  srt name)) `Set.member` ds
  = True
nocapIt info (CVAlphaCtxt srt@(UserDefinedSort True _) name num)  (Vars u) 
  | Just ds <- Map.lookup u info
  , (SCCtxt (MetaCtxt emptySubstitution  srt name)) `Set.member` ds
  = True
-- NCCDX  
nocapIt info (CVAlphaConstantCtxt srt@(UserDefinedSort True _) name num)  (SingleVar v) 
  | Just ds <- Map.lookup (SCVar v) info
  , (SCCtxt (ConstantCtxt emptySubstitution  srt name)) `Set.member` ds
  = True
nocapIt info (CVAlphaCtxt srt@(UserDefinedSort True _) name num)  (SingleVar v) 
  | Just ds <- Map.lookup (SCVar v) info
  , (SCCtxt (MetaCtxt emptySubstitution  srt name)) `Set.member` ds
  = True  
-- NCCEU
nocapIt info (LVAlphaEnv name num)  (Vars u) 
  | Just ds <- Map.lookup u info
  , (SCMetaEnv (Env emptySubstitution  name)) `Set.member` ds
  = True
nocapIt info (LVAlphaChain typ name num)  (Vars u) 
  | Just ds <- Map.lookup u info
  , (SCMetaChain emptySubstitution typ name) `Set.member` ds
  = True
nocapIt info (LVAlphaConstantChain typ name num)  (Vars u) 
  | Just ds <- Map.lookup u info
  , (SCConstantChain emptySubstitution typ name) `Set.member` ds
  = True
  
nocapIt info (LVAlphaConstantEnv name num)  (Vars u) 
  | Just ds <- Map.lookup u info
  , (SCConstantEnv (Env emptySubstitution  name)) `Set.member` ds
  = True
-- NCCEX
nocapIt info (LVAlphaEnv name num)  (SingleVar v) 
  | Just ds <- Map.lookup (SCVar v) info
  , (SCMetaEnv (Env emptySubstitution  name)) `Set.member` ds
  = True
nocapIt info (LVAlphaConstantEnv name num)  (SingleVar v) 
  | Just ds <- Map.lookup (SCVar v) info
  , (SCConstantEnv (Env emptySubstitution  name)) `Set.member` ds
  = True
  
nocapIt info (LVAlphaChain typ name num)  (SingleVar v) 
  | Just ds <- Map.lookup (SCVar v)  info
  , (SCMetaChain  emptySubstitution typ name) `Set.member` ds
  = True
nocapIt info (LVAlphaConstantChain typ name num) (SingleVar v)  
  | Just ds <- Map.lookup(SCVar v)  info
  , (SCConstantChain  emptySubstitution typ name) `Set.member` ds
  = True
  
  
-- NCCUX
nocapIt info (AlphaVarLift name num)  (Vars u) 
  | Just ds <- Map.lookup u info
  , (SCVar (VarLift emptySubstitution name)) `Set.member` ds
  = True
nocapIt info (AlphaConstantVarLift name num)  (Vars u) 
  | Just ds <- Map.lookup u info
  , (SCVar (ConstantVarLift emptySubstitution name)) `Set.member` ds
  = True
nocapIt info (AlphaMetaVarLift name num)  (Vars u) 
  | Just ds <- Map.lookup u info
  , (SCVar (MetaVarLift emptySubstitution name)) `Set.member` ds
  = True
-- NCCXX
nocapIt info (AlphaVarLift name num)  (SingleVar v) 
  | Just ds <- Map.lookup (SCVar v) info
  , (SCVar (VarLift emptySubstitution name)) `Set.member` ds
  = True
nocapIt info (AlphaVarLift name num)  (SingleVar v) 
  | Just ds <- Map.lookup (SCVar (VarLift emptySubstitution name)) info
  , (SCVar v) `Set.member` ds
  = True
nocapIt info (AlphaVarLift name num)  (SingleVar (VarLift sub name'))
  | name /= name' = True
nocapIt info (AlphaMetaVarLift name num)  (SingleVar v) 
  | Just ds <- Map.lookup (SCVar v) info
  , (SCVar (MetaVarLift emptySubstitution name)) `Set.member` ds
  = True
nocapIt info (AlphaMetaVarLift name num)  (SingleVar v) 
  | Just ds <- Map.lookup (SCVar (MetaVarLift emptySubstitution name)) info
  , (SCVar v) `Set.member` ds
  = True
nocapIt info (AlphaConstantVarLift name num)  (SingleVar v) 
  | Just ds <- Map.lookup (SCVar v) info
  , (SCVar (ConstantVarLift emptySubstitution name)) `Set.member` ds
  = True
nocapIt info (AlphaConstantVarLift name num)  (SingleVar v) 
  | Just ds <- Map.lookup (SCVar (ConstantVarLift emptySubstitution name)) info
  , (SCVar v) `Set.member` ds
  = True
  
nocapIt info _ _ = False  
  
data Info = Info {nccs      :: SplittedNCCs
                 ,nablaSets :: Map.Map 
                                 String 
                                 (Map.Map KeyNameSubstItem (Set.Set [KeyOfDom]))
-- 
--                  Map.Map String (SubstItem,[Dom]) -- [([Dom],SubstItem)] 
                 }
  

  
computeNablaSets :: SplittedNCCs -> Map.Map String (Map.Map KeyNameSubstItem (Set.Set [KeyOfDom]))
computeNablaSets info = 
 let splitted = [(comp,s) | (comp,set) <- Map.assocs info, s <- Set.elems set]
 in -- trace (show (length splitted,length $ [(v',KS v,map domToKeyOfDom x)|   Just (xs,v) <- map proc splitted, x <-xs, isJust (getName v), let v' = fromJust (getName v) ]))
  let 
   res =                                                                                                                                                   
     (
     Map.fromList 
      [ (a,( Map.fromList
            [ (u,Set.fromList (map (\(u,v) -> v) gr)) | gr@((u,v):ggs)  <- (groupBy ((==) `on` (\(b,c) -> b)))
                                                              (sortBy (compare `on` (\(b,c) -> b))
                                                              (map (\(a,b,c) -> (b,c)) g))]
            
            
            
            ))
    
    
    
      | g@((a,b,c):gs) <- (groupBy ((==) `on` (\(a,b,c) -> a)) $
               sortBy (compare `on` (\(a,b,c) -> a)) $
                 [(v',KS v,map domToKeyOfDom x)|   Just (xs,v) <- map proc splitted, x <-xs, isJust (getName v), let v' = fromJust (getName v) ])
      ]                                                                                             
     )                                                                                               
  in  res
--     trace (show info) $
--      trace (show res) $ res
                                                                                                     
 where      
   


-- ==========================================================================================================================================   
      proc (SCVar (MetaVarLift (Substitution eta) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name1)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
      proc (SCVar (MetaVarLift (Substitution eta) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name1)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCVar (MetaVarLift (Substitution eta) name1),v@(SCVar (VarLift (Substitution []) name2)))
        = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name1)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCVar (VarLift (Substitution eta) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name1)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
      proc (SCVar (VarLift (Substitution eta) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name1)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCVar (VarLift (Substitution eta) name1),v@(SCVar (VarLift (Substitution []) name2)))
        = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name1)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name1)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name1)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta) name1),v@(SCVar (VarLift (Substitution []) name2)))
        = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name1)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ ([Vars (SCExpr $ Constant emptySubstitution name0)] ++ (map Cod eta)),AlphaConstantVarLift name2 "UDEF")
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ ([Vars (SCExpr $ Constant emptySubstitution name0)] ++ (map Cod eta)),AlphaVarLift name2 "UDEF")
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ ([Vars (SCExpr $ Constant emptySubstitution name0)] ++ (map Cod eta)),AlphaMetaVarLift name2 "UDEF")
        
        
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaExpr name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaExpr name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaExpr name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")

      proc (SCCtxt (MetaCtxt  (Substitution eta@((AlphaCtxt srt' name0 num):_)) srt name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCCtxt (MetaCtxt  (Substitution eta@((AlphaCtxt srt' name0 num):_)) srt name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCCtxt (MetaCtxt  (Substitution eta@((AlphaCtxt srt' name0 num):_)) srt name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
        
        
      proc (SCCtxt (ConstantCtxt  (Substitution eta@((AlphaConstantCtxt srt' name0 num):_)) srt name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ ConstantCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCCtxt (ConstantCtxt  (Substitution eta@((AlphaConstantCtxt srt' name0 num):_)) srt name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ ConstantCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCCtxt (ConstantCtxt  (Substitution eta@((AlphaConstantCtxt srt' name0 num):_)) srt name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ ConstantCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")

        
      proc (SCMetaEnv (Env (Substitution eta@((AlphaEnv name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCMetaEnv (Env (Substitution eta@((AlphaEnv name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCMetaEnv (Env (Substitution eta@((AlphaEnv name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
        
      proc (SCConstantEnv (Env (Substitution eta@((AlphaConstantEnv name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCConstantEnv (Env (Substitution eta@((AlphaConstantEnv name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCConstantEnv (Env (Substitution eta@((AlphaConstantEnv name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
        
        
      proc (SCMetaChain (Substitution eta@((AlphaChain typ0 name0 num):_)) typ name1,v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCMetaChain (Substitution eta@((AlphaChain typ0 name0 num):_)) typ name1,v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCMetaChain (Substitution eta@((AlphaChain typ0 name0 num):_)) typ name1,v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
 
      proc (SCConstantChain (Substitution eta@((AlphaConstantChain typ0 name0 num):_)) typ name1,v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCConstantChain (Substitution eta@((AlphaConstantChain typ0 name0 num):_)) typ name1,v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCConstantChain (Substitution eta@((AlphaConstantChain typ0 name0 num):_)) typ name1,v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
        {-        
        
      proc (SCExpr (Constant (Substitution eta) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
         = Just $ (splitSets $ ([Vars (SCExpr $ Constant emptySubstitution name1)] ++ (map Cod eta)),AlphaConstantVarLift name2 "UDEF")
      proc (SCExpr (Constant (Substitution eta) name1),v@(SCVar (VarLift (Substitution []) name2)))
         = Just $ (splitSets $ ([Vars (SCExpr $ Constant emptySubstitution name1)] ++ (map Cod eta)),AlphaVarLift name2 "UDEF")
      proc (SCExpr (Constant (Substitution eta) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
         = Just $ (splitSets $ ([Vars (SCExpr $ Constant emptySubstitution name1)] ++ (map Cod eta)),AlphaMetaVarLift name2 "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
         = Just $ (splitSets $ ([Vars (SCExpr $ MetaExpression emptySubstitution name1)] ++ (map Cod eta)),AlphaConstantVarLift name2 "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta) name1),v@(SCVar (VarLift (Substitution []) name2)))
         = Just $ (splitSets $ ([Vars (SCExpr $ MetaExpression emptySubstitution name1)] ++ (map Cod eta)),AlphaVarLift name2 "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
         = Just $ (splitSets $ ([Vars (SCExpr $ MetaExpression emptySubstitution name1)] ++ (map Cod eta)),AlphaMetaVarLift name2 "UDEF")
      proc (SCCtxt (MetaCtxt  (Substitution eta) srt name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        = Just $ (splitSets $ [Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name1)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCCtxt (MetaCtxt  (Substitution eta) srt name1),v@(SCVar (VarLift (Substitution []) name2)))
        = Just $ (splitSets $ [Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name1)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCCtxt (MetaCtxt  (Substitution eta) srt name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        = Just $ (splitSets $ [Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name1)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")

        
      proc (SCCtxt (ConstantCtxt  (Substitution eta) srt name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        = Just $ (splitSets $ [Vars (SCCtxt $ ConstantCtxt  emptySubstitution srt name1)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCCtxt (ConstantCtxt  (Substitution eta) srt name1),v@(SCVar (VarLift (Substitution []) name2)))
        = Just $ (splitSets $ [Vars (SCCtxt $ ConstantCtxt  emptySubstitution srt name1)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCCtxt (ConstantCtxt  (Substitution eta) srt name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        = Just $ (splitSets $ [Vars (SCCtxt $ ConstantCtxt  emptySubstitution srt name1)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")

      proc (SCMetaEnv (Env (Substitution eta) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name1)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCMetaEnv (Env (Substitution eta) name1),v@(SCVar (VarLift (Substitution []) name2)))
        = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name1)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCMetaEnv (Env (Substitution eta) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name1)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")

      proc (SCConstantEnv (Env (Substitution eta) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name1)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCConstantEnv (Env (Substitution eta) name1),v@(SCVar (VarLift (Substitution []) name2)))
        = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name1)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCConstantEnv (Env (Substitution eta) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name1)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
        
 
 
      proc (SCMetaChain (Substitution eta) typ name1,v@(SCVar (ConstantVarLift (Substitution []) name2)))
         = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name1)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCMetaChain (Substitution eta) typ name1,v@(SCVar (VarLift (Substitution []) name2)))
         = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name1)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCMetaChain (Substitution eta) typ name1,v@(SCVar (MetaVarLift (Substitution []) name2)))
         = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name1)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
 
        
        
      proc (SCConstantChain (Substitution eta) typ name1,v@(SCVar (ConstantVarLift (Substitution []) name2)))
         = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name1)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCConstantChain (Substitution eta) typ name1,v@(SCVar (VarLift (Substitution []) name2)))
         = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name1)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCConstantChain (Substitution eta) typ name1,v@(SCVar (MetaVarLift (Substitution []) name2)))
         = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name1)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")-}
      proc _ = Nothing                             
   

-- ==========================================================================================================================================   
      proc (SCVar (MetaVarLift (Substitution eta@((AlphaMetaVarLift name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
      proc (SCVar (MetaVarLift (Substitution eta@((AlphaMetaVarLift name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCVar (MetaVarLift (Substitution eta@((AlphaMetaVarLift name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (MetaVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
        
        
      proc (SCVar (VarLift (Substitution eta@((AlphaVarLift name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCVar (VarLift (Substitution eta@((AlphaVarLift name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
      proc (SCVar (VarLift (Substitution eta@((AlphaVarLift name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (VarLift emptySubstitution name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
        
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta), AlphaConstantVarLift name2 "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCVar (ConstantVarLift (Substitution eta@((AlphaConstantVarLift name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [SingleVar (ConstantVarLift emptySubstitution name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
                
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ ([Vars (SCExpr $ Constant emptySubstitution name0)] ++ (map Cod eta)),AlphaConstantVarLift name2 "UDEF")
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ ([Vars (SCExpr $ Constant emptySubstitution name0)] ++ (map Cod eta)),AlphaVarLift name2 "UDEF")
      proc (SCExpr (Constant (Substitution eta@((AlphaConstant name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ ([Vars (SCExpr $ Constant emptySubstitution name0)] ++ (map Cod eta)),AlphaMetaVarLift name2 "UDEF")
        
        
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaExpr name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaExpr name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCExpr (MetaExpression (Substitution eta@((AlphaExpr name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaExpression emptySubstitution name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")

      proc (SCCtxt (MetaCtxt  (Substitution eta@((AlphaCtxt srt' name0 num):_)) srt name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCCtxt (MetaCtxt  (Substitution eta@((AlphaCtxt srt' name0 num):_)) srt name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ MetaCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCCtxt (MetaCtxt  (Substitution eta@((AlphaCtxt srt' name0 num):_)) srt name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ MetaCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
        
        
      proc (SCCtxt (ConstantCtxt  (Substitution eta@((AlphaConstantCtxt srt' name0 num):_)) srt name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ ConstantCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCCtxt (ConstantCtxt  (Substitution eta@((AlphaConstantCtxt srt' name0 num):_)) srt name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCExpr $ ConstantCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCCtxt (ConstantCtxt  (Substitution eta@((AlphaConstantCtxt srt' name0 num):_)) srt name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCCtxt $ ConstantCtxt  emptySubstitution srt name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")

        
      proc (SCMetaEnv (Env (Substitution eta@((AlphaEnv name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCMetaEnv (Env (Substitution eta@((AlphaEnv name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCMetaEnv (Env (Substitution eta@((AlphaEnv name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
        
      proc (SCConstantEnv (Env (Substitution eta@((AlphaConstantEnv name0 num):_)) name1),v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCConstantEnv (Env (Substitution eta@((AlphaConstantEnv name0 num):_)) name1),v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCConstantEnv (Env (Substitution eta@((AlphaConstantEnv name0 num):_)) name1),v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantEnv $ Env  emptySubstitution  name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
        
        
      proc (SCMetaChain (Substitution eta@((AlphaChain typ0 name0 num):_)) typ name1,v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCMetaChain (Substitution eta@((AlphaChain typ0 name0 num):_)) typ name1,v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCMetaChain (Substitution eta@((AlphaChain typ0 name0 num):_)) typ name1,v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCMetaChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
 
      proc (SCConstantChain (Substitution eta@((AlphaConstantChain typ0 name0 num):_)) typ name1,v@(SCVar (ConstantVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaConstantVarLift name2 "UDEF")
      proc (SCConstantChain (Substitution eta@((AlphaConstantChain typ0 name0 num):_)) typ name1,v@(SCVar (VarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaVarLift name2 "UDEF")
      proc (SCConstantChain (Substitution eta@((AlphaConstantChain typ0 name0 num):_)) typ name1,v@(SCVar (MetaVarLift (Substitution []) name2)))
        | name1 == name0 = Just $ (splitSets $ [Vars (SCConstantChain emptySubstitution typ name0)] ++ (map Cod eta),AlphaMetaVarLift name2 "UDEF")
        
      proc _ = Nothing                          
      
                   
splitSets xs = [xs]


splitSets' ((Codd (SubstSet set)):rest) =
  [list ++  r | p <- permutations set, let list = map Codd p, r <- splitSets' rest]
splitSets' (x:rest) = 
  [x:r | r <- splitSets' rest]
splitSets' [] = [[]] 
  
--            <a_{S11,#33},{|LV(Cn)(a_{VV16,#37}),a_{X5,#36},LV(Cn)(a_{E10,#38})|},{|a_{X#18,#35},a_{X#17,#34}|}>�S11*
           
--            (<a_{S11,#2},{|LV(Cn)(a_{VV16,#3}),a_{X5,#4},LV(Cn)(a_{E10,#5})|}>�S11*, X#18)
             
  
-- (<a_{S12,#1},{|LV(Cn)(a_{VV16,#3}),a_{X5,#4},LV(Cn)(a_{E10,#5})|}>�S12*,X#18*)

-- ===> (VAR S12, {(Cod LVAlphaChain 16), Cod LVAlhaEnv 10),   SingleVar X#18


-- (SCCtxt (ConstantCtxt emptySubstitution typ name)),Cod  (AlphaConstantCtxt typ' name1 num)] eta 
  
-- simplifyExpr info _ 
--    | trace (show $ nablaSets info) False = undefined
simplifyExpr info (VarL v) = VarL (substVar info v)
simplifyExpr info (e@(MetaExpression (Substitution sub) name))  = simplU info e
simplifyExpr info (e@(Constant (Substitution sub) name))        = simplU info e
simplifyExpr info (e@(Fn f args))
 = Fn f $ map (simplifyArgPos info) args
 
simplifyExpr info (e@(Letrec env expr))
 = Letrec (simplifyEnv info env) (simplifyExpr info expr)
 
simplifyExpr info (e@(FlatLetrec env expr))
 = FlatLetrec (simplifyEnv info env) (simplifyExpr info expr)
 
simplifyExpr info (e@(MetaCtxtSubst (Substitution sub) typ name s)) =
  case simplU info (MetaCtxt (Substitution sub) typ name) of
     MetaCtxt sub' typ' name' ->  (MetaCtxtSubst ( sub') typ' name' (simplifyExpr info s))
     
simplifyExpr info (e@(ConstantCtxtSubst (Substitution sub) typ name s)) =
  case simplU info (ConstantCtxt (Substitution sub) typ name) of
     ConstantCtxt sub' typ' name' -> (ConstantCtxtSubst ( sub') typ' name' (simplifyExpr info s))
    
simplifyExpr info (Hole (Substitution sub)) = Hole (Substitution sub)

simplifyExpr info (e@(MetaCtxt (Substitution sub) typ name )) =
   simplU info (MetaCtxt (Substitution sub) typ name) 
     
simplifyExpr info (e@(ConstantCtxt (Substitution sub) typ name)) =
   simplU info (ConstantCtxt (Substitution sub) typ name) 
  

simplifyExpr info other = error $ "in l.182 Alpha.hs:" ++ show other
 
simplifyArgPos info (e@(Binder v r))  =
 let r' = simplifyExpr info r 
     v' = map (substVar info) v
 in Binder v' r'

simplifyArgPos info (e@(VarPos v)) = (VarPos (substVar info v))

-- simplVEta ncc v eta = eta -- tbd  

simplifyEnv  info env =
 let
   mc' = [simplifyMetaChain info m | m <- metaChains env]
   cc' = [simplifyConstantChain info m | m <- constantChains env]
   me' = [simplifyMetaEnv info m | m <- metaEnvVars env]
   ce' = [simplifyConstantEnv info m | m <- constantEnvVars env]
   b'  = [simplifyBinding info b | b <- bindings env]
 in env{bindings=b', constantEnvVars = ce', metaEnvVars = me', constantChains = cc', metaChains = mc'}
 
simplifyConstantEnv info env = simplUEnv info env
simplifyMetaEnv info env = simplUEnv info env 
 
simplifyMetaChain info e =
  case simplUChain info e of 
       (MetaChain sub typ name z s) -> MetaChain sub typ name (substVar info z) (simplifyExpr info s)
       
simplifyConstantChain info e =
  case simplUChain info e of 
       (ConstantChain sub typ name z s) -> ConstantChain sub typ name (substVar info z) (simplifyExpr info s)
 
simplifyBinding info (e@(v := r))  =
 let r' = simplifyExpr info r 
     v' = substVar info v
 in v' := r'


flattenSubst [] = [] 
flattenSubst ((SubstSet xs):rest) =
 (SubstSet (concat 
    [case x of 
        SubstSet ys -> ys
        other -> [other] 
      | x <- xs]):(flattenSubst rest))
flattenSubst (x:xs) = x:(flattenSubst xs)      


simplifySubst ((SubstSet [x]):rest) = 
  case simplifySubst rest of 
    Nothing -> Just (x:rest)
    Just rest' -> Just (x:rest')
    
simplifySubst ((SubstSet []):rest) = 
  case simplifySubst rest of 
    Nothing -> Just rest
    Just rest' -> Just rest'
    
simplifySubst (item:rest) = 
  case simplifySubst rest of 
    Nothing -> Nothing
    Just rest' -> Just (item:rest')
    
simplifySubst [] = Nothing    
    

sortSubstVar (MetaVarLift sub name) = MetaVarLift (sortSubst sub) name
sortSubstVar (VarLift sub name) = VarLift (sortSubst sub) name
sortSubstVar (ConstantVarLift sub name) = ConstantVarLift (sortSubst sub) name
    

sortSubstExpr (VarL v) = VarL (sortSubstVar v)

sortSubstExpr (e@(MetaExpression (sub) name))  = 
 MetaExpression (sortSubst sub) name
 
sortSubstExpr (e@(Constant (sub) name))  = 
 Constant (sortSubst sub) name

sortSubstExpr (e@(Fn f args))
 = Fn f $ map (sortSubstArgPos) args
 
sortSubstExpr (e@(Letrec env expr))
 = Letrec (sortSubstEnv env) (sortSubstExpr expr)
 
sortSubstExpr (e@(FlatLetrec env expr))
 = FlatLetrec (sortSubstEnv env) (sortSubstExpr expr)
 
sortSubstExpr (e@(MetaCtxtSubst sub typ name s)) =
 MetaCtxtSubst (sortSubst sub) typ name (sortSubstExpr s)
 
sortSubstExpr (e@(ConstantCtxtSubst (sub) typ name s)) =
 ConstantCtxtSubst (sortSubst sub) typ name (sortSubstExpr s)
         
sortSubstExpr (Hole (sub)) = Hole (sortSubst sub)
     
sortSubstExpr other = error $ "in sortSubst" ++ show other
 
sortSubstArgPos (e@(Binder v r))  =
  Binder (map sortSubstVar v) (sortSubstExpr r) 

sortSubstArgPos (e@(VarPos v)) = (VarPos (sortSubstVar v))

sortSubstEnv  env =
 let
   mc' = [MetaChain (sortSubst sub) typ name (sortSubstVar z) (sortSubstExpr s) | (MetaChain sub typ name z s) <- metaChains env]
   cc' = [ConstantChain (sortSubst sub) typ name (sortSubstVar z) (sortSubstExpr s) | (ConstantChain sub typ name z s) <- constantChains env]
   me' = [Env (sortSubst sub) name | (Env sub name) <- metaEnvVars env]
   ce' = [Env (sortSubst sub) name | (Env sub name) <- constantEnvVars env]
   b'  = [(sortSubstVar x):=(sortSubstExpr e) | (x:=e) <- bindings env]
 in env{bindings=b', constantEnvVars = ce', metaEnvVars = me', constantChains = cc', metaChains = mc'}
 
sortSubst (Substitution sub) = (Substitution (go sub))
  where 
     go ((SubstSet set):rest) = (SubstSet (sort set)):(go rest)
     go (x:rest) = x:(go rest)
     go [] = []
