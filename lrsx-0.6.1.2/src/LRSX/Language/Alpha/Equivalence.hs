-----------------------------------------------------------------------------
-- | 
-- Module      :  LRSX.Language.Alpha.Equivalence
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--
--  Alpha-Renaming  and Alpha-Equivalence
--
-----------------------------------------------------------------------------
module LRSX.Language.Alpha.Equivalence where
import Data.List
import Data.Function
import Data.Maybe
import LRSX.Util.Util
import LRSX.Language.Syntax
import LRSX.Language.Alpha.Similar hiding(trace)
import LRSX.Language.Alpha.Simplify hiding(trace)
import LRSX.Language.ContextDefinition
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
-- import Debug.Trace
trace a b = b

-- ********************************************************************************
-- The main function to check alpha-equivalence of two expressions 
-- The expressions must be alpha-renamed and simplification (because of NCCs) should have been applied.
-- 
-- Note that otherwise e.g. \x.let E in x   and \y.let E in y 
--  are not alpha-equivalent, since E may capture y asf.
--
-- Treatment of bound variables at binders:
--         a_{x,i}:eta  � x . ...
--   and   a_{y,j}:eta' � y . ...
--
--  Rename a_{x,i} into a_{x,k}
--         a_{y,j} into a_{y,k}
--   which is saved in the scope
--   the same k should mean here that a_{x,k} = {x |-> z_k} and a_{y,k} = {y |-> z_k}
--   thus Cod(a_{x,k}) = Cod(a_{y,k}) 
--  but we avoid to perform the explicit renaming of variables which would complicate the algorithm and the representation
--  and it is error-prone
--
-- Occurences of variables:
--          a_{x,i} cup rc :eta�x
--          a_{y,j} cup rc':eta'�y
--  ==> check whether in the scope a_{x,i} |-> a_{x,k} and a_{y,j} |-> a_{y,k}  (same k!)
--      the check equivalence of rc:eta and rc':eta'
--
--          eta�x
--          eta'�x 
--  ==> check whether eta and eta' are equivalent w.r.t. the scope
--           


alphaEquiv fresh expr1 expr2 =
    let (res,_,fullScope) = alphaEquivWithScopeExpr fresh (Map.empty,Map.empty) (simplifyExpr (Info {nccs=Map.empty,nablaSets=computeNablaSets Map.empty}) expr1) 
                                                                                (simplifyExpr (Info {nccs=Map.empty,nablaSets=computeNablaSets Map.empty}) expr2) 
    in (res,fullScope)
-- types and helper functions for renamings
type ScopeRenaming = (Map.Map SubstItem SubstItem, Map.Map SubstItem SubstItem)
type SingleFullRenaming = Map.Map SubstItem [SubstItem]
type FullRenaming  = (SingleFullRenaming, SingleFullRenaming)  -- two renamings for the two expressions shown to be alpha-equivalent

-- insertIntoMultiSet :: String -> String -> Map.Map String [String] -> Map.Map String [String]
insertIntoMultiSet key value mp 
 | Just entry <- Map.lookup key mp = Map.insert key (value:entry) mp
 | otherwise                       = Map.insert key [value] mp 
 
emptyFullRenaming :: FullRenaming
emptyFullRenaming = (Map.empty,Map.empty)
 
                 
unionMultiSets mp1 mp2 = foldr 
                             (\(k,v) m -> insertIntoMultiSet k v m) 
                             mp2 
                            [(k,v) | (k,vlist) <- (Map.assocs mp1),v <- vlist]

-- unionListOfMultiSets :: [Map.Map String [String]] -> Map.Map String [String]
unionListOfMultiSets list =  foldr (\m1 m2 -> unionMultiSets m1 m2) Map.empty list 

unionListOfFullScopes :: [FullRenaming] -> FullRenaming
unionListOfFullScopes list = 
 let (leftComps,rightComps) = unzip list 
 in (unionListOfMultiSets leftComps,unionListOfMultiSets rightComps)     
--
  
  
                              
alphaEquivWithScopeExpr f sc e1 e2 =
    let res@(b,s,t) = alphaEquivWithScopeExprIt f sc e1 e2 
    in res
      -- seq b (trace (("AEQ: \n" ++ show e1 ++ "\n" ++ show e2 ++ "\nResult:" ++ show b ++ "\nEND")) res)

alphaEquivWithScopeExprIt :: [String]          -- ^ fresh names
                           -> ScopeRenaming 
                           -> Expression 
                           -> Expression 
                           -> (Bool
                              ,[String]
                              ,FullRenaming) 
                              
alphaEquivWithScopeExprIt fresh scope@(scope1,scope2) expr1@(MetaExpression sub1 e1) expr2@(MetaExpression sub2 e2) 
 | (e1 == e2) = 
     let 
      alpha1 = case sub1 of
                (Substitution (a1@(AlphaExpr name num):_)) -> if name == e1 then [a1] else error "in alphaEquiv"
                (Substitution []) -> []
      alpha2 = case sub2  of
                (Substitution (a2@(AlphaExpr name num):_)) -> if name == e2 then [a2] else error "in alphaEquiv"
                (Substitution []) -> []
       in
        case () of 
         _ | null alpha1 && null alpha2 ->  (alphaEquivWithScopeSubst scope sub1 sub2,fresh,emptyFullRenaming)
           | null alpha1 || null alpha2  -> (False,fresh,emptyFullRenaming) 
           | otherwise ->
              let 
               (new:fresh')= fresh
               newAlpha = (AlphaExpr name new)
               alpha1'@(AlphaExpr name _) = head alpha1
               alpha2' = head alpha2
               
               scope' = (Map.insert alpha1' newAlpha scope1,Map.insert alpha2' newAlpha scope2)
               (m1,m2) = emptyFullRenaming
              in (alphaEquivWithScopeSubst scope' sub1 sub2 
                 ,fresh'
                 ,(insertIntoMultiSet alpha1' newAlpha m1,insertIntoMultiSet alpha2' newAlpha m2)) 
              
alphaEquivWithScopeExprIt fresh scope@(scope1,scope2) expr1@(Constant sub1 e1) expr2@(Constant sub2 e2) 
 | (e1 == e2) = 
     let 
      alpha1 = case sub1 of
                (Substitution (a1@(AlphaConstant name num):_)) -> if name == e1 then [a1] else error "in alphaEquiv"
                (Substitution []) -> []
      alpha2 = case sub2  of
                (Substitution (a2@(AlphaConstant name num):_)) -> if name == e2 then [a2] else error "in alphaEquiv"
                (Substitution []) -> []
       in
        case () of 
         _ | null alpha1 && null alpha2 ->  (alphaEquivWithScopeSubst scope sub1 sub2,fresh,emptyFullRenaming)
           | null alpha1 || null alpha2  -> (False,fresh,emptyFullRenaming) 
           | otherwise ->
              let 
               (new:fresh')= fresh
               newAlpha = (AlphaConstant name new)
               alpha1'@(AlphaConstant name _) = head alpha1
               alpha2' = head alpha2
               
               scope' = (Map.insert alpha1' newAlpha scope1,Map.insert alpha2' newAlpha scope2)
               (m1,m2) = emptyFullRenaming
              in (alphaEquivWithScopeSubst scope' sub1 sub2
                 ,fresh'
                 ,(insertIntoMultiSet alpha1' newAlpha m1,insertIntoMultiSet alpha2' newAlpha m2)) 

alphaEquivWithScopeExprIt fresh scope  (Fn f1 args1) (Fn f2 args2) 
    | f1 == f2  = 
       let 
         go fr [] []  = (True,fr,emptyFullRenaming)
         go fr (a:as) (b:bs) =
          let (isEquiv, fr', fullscopeLocal) = alphaEquivWithScopeArgPos  fr scope a b
              (restEquiv, fr'', fullscopeRest) = go fr' as bs
          in  (isEquiv && restEquiv, fr'', unionListOfFullScopes [fullscopeLocal,fullscopeRest]) 
       in 
         go fresh args1 args2
    | otherwise = (False,fresh,emptyFullRenaming)
    
alphaEquivWithScopeExprIt fresh scope (VarL v1) (VarL v2) = 
    (alphaEquivWithScopeVar scope v1 v2,fresh,emptyFullRenaming)

alphaEquivWithScopeExprIt fresh scope@(scope1,scope2)  c1@(MetaCtxtSubst sub1 srt1 name1 e1) c2@(MetaCtxtSubst sub2 srt2 name2 e2)  
    | srt1 == srt2 
    , name1 == name2 
    =   
        let 
            alpha1 =
               case sub1 of
                 (Substitution (alpha@(AlphaCtxt srt1 a1 num1):_))
                   -> if name1 == a1 then [alpha] else error "in alphaEquiv"
                 (Substitution []) -> []   
            alpha2 =
               case sub2  of
                 (Substitution (alpha@(AlphaCtxt srt2 a2 num2):_))
                   -> if name2 == a2 then [alpha] else error "in alphaEquiv"
                 (Substitution []) -> []   
        in case () of
            _ | (null alpha1 && null alpha2) -> 
                let b1 = alphaEquivWithScopeSubst scope sub1 sub2
                    (b2,fresh1,sc2) = alphaEquivWithScopeExpr fresh scope e1 e2
                in (b1 && b2,fresh1,sc2)
            _ | null alpha1 || null alpha2 -> (False,fresh,emptyFullRenaming) 
            _ | otherwise ->
                  let 
                    (new:fresh')= fresh
                    newAlpha = AlphaCtxt srt name new
                    alpha1'@(AlphaCtxt srt name num) = head alpha1
                    alpha2' = head alpha2
                    scope' = (Map.insert alpha1' newAlpha scope1,Map.insert alpha2' newAlpha scope2)
                    b1 = alphaEquivWithScopeSubst scope' sub1 sub2
                    (b2,fresh2,sc2) = alphaEquivWithScopeExpr  fresh' scope' e1 e2
                    (m1,m2) = sc2
                   in (b1 && b2, fresh2, (insertIntoMultiSet alpha1' newAlpha m1,insertIntoMultiSet alpha2' newAlpha m2))

alphaEquivWithScopeExprIt fresh scope@(scope1,scope2)  c1@(ConstantCtxtSubst sub1 srt1 name1 e1) c2@(ConstantCtxtSubst sub2 srt2 name2 e2)  
    | srt1 == srt2 
    , name1 == name2 
    =   
        let 
            alpha1 =
               case sub1 of
                 (Substitution (alpha@(AlphaConstantCtxt srt1 a1 num1):_))
                   -> if name1 == a1 then [alpha] else error "in alphaEquiv"
                 (Substitution []) -> []   
            alpha2 =
               case sub2  of
                 (Substitution (alpha@(AlphaConstantCtxt srt2 a2 num2):_))
                   -> if name2 == a2 then [alpha] else error "in alphaEquiv"
                 (Substitution []) -> []   
        in case () of
            _ | (null alpha1 && null alpha2) -> 
                let b1 = alphaEquivWithScopeSubst scope sub1 sub2
                    (b2,fresh1,sc2) = alphaEquivWithScopeExpr fresh scope e1 e2
                in (b1 && b2,fresh1,sc2)
            _ | null alpha1 || null alpha2 -> (False,fresh,emptyFullRenaming) 
            _ | otherwise ->
                  let 
                    (new:fresh')= fresh
                    newAlpha = AlphaConstantCtxt srt name new
                    alpha1'@(AlphaConstantCtxt srt name _) = head alpha1
                    alpha2' = head alpha2
                    scope' = (Map.insert alpha1' newAlpha scope1,Map.insert alpha2' newAlpha scope2)
                    b1 = alphaEquivWithScopeSubst scope' sub1 sub2
                    (b2,fresh2,sc2) = alphaEquivWithScopeExpr fresh' scope' e1 e2
                    (m1,m2) = sc2
                   in (b1 && b2, fresh2, (insertIntoMultiSet alpha1' newAlpha m1, insertIntoMultiSet alpha2' newAlpha m2))

alphaEquivWithScopeExprIt fresh scope  (FlatLetrec env1 inexpr1) (FlatLetrec env2 inexpr2)   =
  alphaEquivWithScopeExpr fresh scope  (Letrec env1 inexpr1) (Letrec env2 inexpr2) 

alphaEquivWithScopeExprIt fresh scope  (Letrec env1 inexpr1) (Letrec env2 inexpr2) =   
 let 
   -- first  assign components to components            
    bindings1 = bindings env1
    bindings2 = bindings env2
    -- bindingsAssignments  = [zip bindings1 bs2 | bs2 <- permutations bindings2] 
    bindingsAssignments =
     let
      mergeGroups :: [([Binding],[Binding])] -> [[Binding]] -> [[Binding]] -> [([Binding],[Binding])] 
      mergeGroups res [] []  = res
      mergeGroups res (g1@((_ := b1):_):gs1) (g2@((_ := b2):_):gs2) 
       | fingerprint b1 == fingerprint b2 = (mergeGroups ((g1,g2):res) gs1 gs2)
      mergeGroups _ _ _ = []
      assignGroups [] = [[]]
      assignGroups ((g1,g2):rest)
       | length g1 == length g2 = 
          let rek = assignGroups rest
              now = [zip g1 g2' | g2' <- permutations g2]
          in  [n ++ r | n <- now, r <- rek]
       | otherwise = []
     in 
       assignGroups $
         mergeGroups []  
          (groupBy (\(_ := b1) (_ := b2) -> (fingerprint b1) == (fingerprint b2))
             (sortBy (\(_ := b1) (_ := b2) -> compare (fingerprint b1) (fingerprint b2)) bindings1))
          (groupBy (\(_ := b1) (_ := b2) -> (fingerprint b1) == (fingerprint b2))
             (sortBy (\(_ := b1) (_ := b2) -> compare (fingerprint b1) (fingerprint b2)) bindings2))


    mc1       = metaChains env1
    mc2       = metaChains env2            
    -- metaChainAssignments = [zip mc1 mcs2 | mcs2 <- permutations mc2]            
    metaChainAssignments =             
     let
      mergeGroups res [] []  = res
      mergeGroups res (g1@((MetaChain _ typ1 name1 _ s1):_):gs1) (g2@((MetaChain _ typ2 name2 _ s2):_):gs2) 
       | name1 == name2
       , typ1 == typ2
       , fingerprint s1 == fingerprint s2  = (mergeGroups ((g1,g2):res) gs1 gs2)
      mergeGroups _ _ _ = []
      assignGroups [] = [[]]
      assignGroups ((g1,g2):rest)
       | length g1 == length g2 = 
          let rek = assignGroups rest
              now = [zip g1 g2' | g2' <- permutations g2]
          in  [n ++ r | n <- now, r <- rek]
       | otherwise = []
      sortFn (MetaChain _ typ1 name1 _ s1) (MetaChain _ typ2 name2 _ s2) =
        case compare typ1 typ2 of
          EQ -> case compare name1 name2 of
                   EQ -> compare (fingerprint s1) (fingerprint s2)
                   other -> other
          other -> other
      groupFn m1 m2 = sortFn m1 m2 == EQ
     in 
       assignGroups $
         mergeGroups []  
          (groupBy groupFn
             (sortBy sortFn mc1))
          (groupBy groupFn
             (sortBy sortFn mc2))

    cc1       = constantChains env1
    cc2       = constantChains env2            
    -- constantChainAssignments = [zip cc1 ccs2 | ccs2 <- permutations cc2]            
    constantChainAssignments = 
     let
      mergeGroups res [] []  = res
      mergeGroups res (g1@((ConstantChain _ typ1 name1 _ s1):_):gs1) (g2@((ConstantChain _ typ2 name2 _ s2):_):gs2) 
       | name1 == name2
       , typ1 == typ2
       , fingerprint s1 == fingerprint s2  = (mergeGroups ((g1,g2):res) gs1 gs2)
      mergeGroups _ _ _ = []
      assignGroups [] = [[]]
      assignGroups ((g1,g2):rest)
       | length g1 == length g2 = 
          let rek = assignGroups rest
              now = [zip g1 g2' | g2' <- permutations g2]
          in  [n ++ r | n <- now, r <- rek]
       | otherwise = []
      sortFn (ConstantChain _ typ1 name1 _ s1) (ConstantChain _ typ2 name2 _ s2) =
        case compare typ1 typ2 of
          EQ -> case compare name1 name2 of
                   EQ -> compare (fingerprint s1) (fingerprint s2)
                   other -> other
          other -> other
      groupFn m1 m2 = sortFn m1 m2 == EQ
     in 
       assignGroups $
         mergeGroups []  
          (groupBy groupFn
             (sortBy sortFn cc1))
          (groupBy groupFn
             (sortBy sortFn cc2))
    me1       = metaEnvVars env1
    me2       = metaEnvVars env2            
    -- metaEnvsAssignments = [zip me1 mes2 | mes2 <- permutations me2]                        
    metaEnvsAssignments = 
     let
      mergeGroups res [] []  = res
      mergeGroups res (g1@((Env _ n1):_):gs1) (g2@((Env _ n2):_):gs2) 
       | n1 == n2 = (mergeGroups ((g1,g2):res) gs1 gs2)
      mergeGroups _ _ _ = []
      assignGroups [] = [[]]
      assignGroups ((g1,g2):rest)
       | length g1 == length g2 = 
          let rek = assignGroups rest
              now = [zip g1 g2' | g2' <- permutations g2]
          in  [n ++ r | n <- now, r <- rek]
       | otherwise = []
      sortFn (Env _ n1) (Env _ n2) = compare n1 n2
      groupFn m1 m2 = sortFn m1 m2 == EQ
     in 
       assignGroups $
         mergeGroups []  
          (groupBy groupFn
             (sortBy sortFn me1))
          (groupBy groupFn
             (sortBy sortFn me2))
    ce1       = constantEnvVars env1
    ce2       = constantEnvVars env2
    -- constantEnvsAssignments = [zip ce1 ces2 | ces2 <- permutations ce2]                                    
    constantEnvsAssignments =
     let
      mergeGroups res [] []  = res
      mergeGroups res (g1@((Env _ n1):_):gs1) (g2@((Env _ n2):_):gs2) 
       | n1 == n2 =(mergeGroups ((g1,g2):res) gs1 gs2)
      mergeGroups _ _ _ = []
      assignGroups [] = [[]]
      assignGroups ((g1,g2):rest)
       | length g1 == length g2 = 
          let rek = assignGroups rest
              now = [zip g1 g2' | g2' <- permutations g2]
          in  [n ++ r | n <- now, r <- rek]
       | otherwise = []
      sortFn (Env _ n1) (Env _ n2) = compare n1 n2
      groupFn m1 m2 = sortFn m1 m2 == EQ
     in 
       assignGroups $
         mergeGroups []  
          (groupBy groupFn
             (sortBy sortFn ce1))
          (groupBy groupFn
             (sortBy sortFn ce2))
    allAssignments = [(aBS,aMC,aCC,aME,aCE) 
                        |  aBS <- bindingsAssignments
                        ,  aMC <- metaChainAssignments
                        ,  aCC <- constantChainAssignments
                        ,  aME <- metaEnvsAssignments
                        ,  aCE <- constantEnvsAssignments]
    -- now use the assignments to proceed further, first all binders must be renamed
    -- and the scopes have to be adjusted
    allChecksForBinders = 
     [   let
           (ok1,fresh1,scope1,fullScope1) =
             let 
               go fr [] scope  = (True,fr,scope,emptyFullRenaming) 
               go fr ((x1:=_,x2:=_):rest) scope = 
                  let 
                    (ok',fr',scope',fsc)      = alphaEquivBoundVar fr scope x1 x2  
                    (ok'',fr'',scope'',fsc'') = go fr' rest scope'  
                  in (ok' && ok'',fr'', scope'', unionListOfFullScopes [fsc,fsc''])                    
              in go fresh aBS scope 
              
           (ok2,fresh2,scope2,fullScope2) =
             let 
               go fr [] scope = (True,fr,scope,emptyFullRenaming) 
               go fr ((MetaChain sub1 typ1 name1 z1 s1,MetaChain sub2 typ2 name2 z2 s2):rest) scope@(scope1,scope2) = 
                  let 
                    (ok',fr',scope',fsc)      = alphaEquivBoundVar fr scope z1 z2  
                    alpha1 = case sub1 of 
                               (Substitution (a1@(AlphaChain t name1' num1):_))
                                  -> if  name1 == name1' then [a1] else error "in alphaEquiv"
                               (Substitution []) -> []
                    alpha2 = case sub2 of 
                               (Substitution (a2@(AlphaChain t name2' num2):_))
                                  -> if name2 == name2' then [a2] else error "in alphaEquiv"
                               (Substitution []) -> []
                    (okR,frR,scR,fscR) =
                       case () of 
                         _ | (null alpha1 && null alpha2)  -> (True,fr',scope',fsc)
                           |  null alpha1 || null alpha2   -> (False,fr',scope',fsc)
                           | otherwise ->
                                let 
                                  (scope1',scope2') = scope'
                                  (new:fr'')= fr'
                                  newAlpha = AlphaChain t name new
                                  alpha1'@(AlphaChain t name _) = head alpha1
                                  alpha2' = head alpha2
                                  scope'' = (Map.insert alpha1' newAlpha scope1',Map.insert alpha2' newAlpha scope2')
                                  (m1,m2) = fsc
                                in (ok'&&typ1 == typ2&&name1 == name2,fr'',scope'',(insertIntoMultiSet alpha1' newAlpha m1,insertIntoMultiSet alpha2' newAlpha m2))
                    (okRest,frRest,scopeRest,fscRest) = (go frR rest scR) 
                  in (okR && okRest, frRest, scopeRest, unionListOfFullScopes [fscR,fscRest])
              in go fresh1  aMC scope1
              
              
           (ok3,fresh3,scope3,fullScope3) =
             let 
               go fr [] scope = (True,fr,scope,emptyFullRenaming) 
               go fr ((ConstantChain sub1 typ1 name1 z1 s1,ConstantChain sub2 typ2 name2 z2 s2):rest) scope@(scope1,scope2) = 
                  let 
                    (ok',fr',scope',fsc)      = alphaEquivBoundVar fr scope z1 z2  
                    alpha1 = case sub1 of 
                               (Substitution (a1@(AlphaConstantChain t name1' num1):_))
                                  -> if  name1 == name1' then [a1] else error "in alphaEquiv"
                               (Substitution []) -> []
                    alpha2 = case sub2 of 
                               (Substitution (a2@(AlphaConstantChain t name2' num2):_))
                                  -> if name2 == name2' then [a2] else error "in alphaEquiv"
                               (Substitution []) -> []
                    (okR,frR,scR,fscR) =
                       case () of 
                         _ | (null alpha1 && null alpha2)  -> (True,fr',scope',fsc)
                           |  null alpha1 || null alpha2   -> (False,fr',scope',fsc)
                           | otherwise ->
                                let 
                                  (scope1',scope2') = scope'
                                  (new:fr'')= fr'
                                  newAlpha = AlphaConstantChain t name new
                                  alpha1'@(AlphaConstantChain t name _) = head alpha1
                                  alpha2' = head alpha2
                                  scope'' = (Map.insert alpha1' newAlpha scope1',Map.insert alpha2' newAlpha scope2')
                                  (m1,m2) = fsc
                                in (ok'&&typ1 == typ2&&name1 == name2,fr'',scope'',(insertIntoMultiSet alpha1' newAlpha m1,insertIntoMultiSet alpha2' newAlpha m2))
                    (okRest,frRest,scopeRest,fscRest) = (go frR  rest scR)
                  in (okR && okRest, frRest, scopeRest, unionListOfFullScopes [fscR,fscRest])
              in go fresh2 aCC scope2  
                
           (ok4,fresh4,scope4,fullScope4) =
             let 
               go fr [] scope = (True,fr,scope,emptyFullRenaming) 
               go fr ((Env sub1 name1,Env sub2 name2):rest) scope@(scope1,scope2) = 
                  let 
                    alpha1 = case sub1 of 
                                          (Substitution (a1@(AlphaEnv name1' num1):_) )
                                             -> if  name1 == name1' then [a1] else error "in alphaEquiv"
                                          (Substitution []) -> []
                    alpha2 = case sub2 of 
                                          (Substitution (a2@(AlphaEnv name2' num2):_))
                                             -> if  name2 == name2' then [a2] else error "in alphaEquiv"
                                          (Substitution []) -> []
                    (okR,frR,scR,fscR) =
                       case () of 
                         _ | (null alpha1 && null alpha2)  -> (True,fr,scope,emptyFullRenaming)
                           |  null alpha1 || null alpha2   -> (False,fr,scope,emptyFullRenaming)
                           | otherwise ->
                                let 
                                  (new:fr')= fr
                                  newAlpha = AlphaEnv name new
                                  alpha1'@(AlphaEnv name _) = head alpha1
                                  alpha2' = head alpha2
                                  scope' = (Map.insert alpha1' newAlpha scope1,Map.insert alpha2' newAlpha scope2)
                                  (m1,m2) = emptyFullRenaming
                                in (name1 == name2,fr',scope',(insertIntoMultiSet alpha1' newAlpha m1,insertIntoMultiSet alpha2' newAlpha m2))
                    (okRest,frRest,scopeRest,fscRest) = (go frR  rest scR)
                  in (okR && okRest, frRest, scopeRest, unionListOfFullScopes [fscR,fscRest])
              in  (go fresh3 aME scope3)  
              
                
           (ok5,fresh5,scope5,fullScope5) =
             let 
               go fr [] scope =(True,fr,scope,emptyFullRenaming) 
               go fr ((Env sub1 name1,Env sub2 name2):rest) scope@(scope1,scope2) = 
                  let 
                    alpha1 = case sub1 of 
                                          (Substitution (a1@(AlphaConstantEnv name1' num1):_) )
                                             -> if  name1 == name1' then [a1] else error "in alphaEquiv"
                                          (Substitution []) -> []
                    alpha2 = case sub2 of 
                                          (Substitution (a2@(AlphaConstantEnv name2' num2):_))
                                             -> if  name2 == name2' then [a2] else error "in alphaEquiv"
                                          (Substitution []) -> []
                    (okR,frR,scR,fscR) =
                       case () of 
                         _ | (null alpha1 && null alpha2)  -> (True,fr,scope,emptyFullRenaming)
                           |  null alpha1 || null alpha2   -> (False,fr,scope,emptyFullRenaming)
                           | otherwise ->
                                let 
                                  (new:fr')= fr
                                  newAlpha = AlphaConstantEnv name new
                                  alpha1'@(AlphaConstantEnv name _) = head alpha1
                                  alpha2' = head alpha2
                                  scope' = (Map.insert alpha1' newAlpha scope1,Map.insert alpha2' newAlpha scope2)
                                  (m1,m2) = emptyFullRenaming
                                in (name1 == name2,fr',scope',(insertIntoMultiSet alpha1' newAlpha m1,insertIntoMultiSet alpha2' newAlpha m2))
                    (okRest,frRest,scopeRest,fscRest) = (go frR rest scR)
                  in (okR && okRest, frRest, scopeRest, unionListOfFullScopes [fscR,fscRest])
              in go fresh4   aCE scope4
         in (ok1 && ok2 && ok3 && ok4 && ok5,fresh5,scope5,unionListOfFullScopes [fullScope1,fullScope2,fullScope3,fullScope4,fullScope5],aBS,aMC,aCC,aME,aCE)
                
     |  (aBS,aMC,aCC,aME,aCE) <- allAssignments  
     ]
    validAllChecksForBinders = [(freshNew,scopeNew,fullScopeNew,aBS,aMC,aCC,aME,aCE) | (ok,freshNew,scopeNew,fullScopeNew,aBS,aMC,aCC,aME,aCE) <- allChecksForBinders, ok]
    checkRHSAndInExprOfValids =
     [
      let (okRes1,freshRes1,fullScopeNew1) = 
              let       
                go fr []                     = (True,fr,emptyFullRenaming)
                go fr ((x1:=e1,x2:=e2):rest) =
                     let (ok,fr',fsc)    = alphaEquivWithScopeExpr fr scopeNew e1 e2
                         (ok',fr'',fsc') = (go fr' rest)
                      in (ok && ok',fr'', unionListOfFullScopes [fsc,fsc'])
              in go freshNew aBS                      
             
          (okRes2,freshRes2,fullScopeNew2) =     
              let       
                go fr []     = (True,fr,emptyFullRenaming)
                go fr ((MetaChain sub1 typ1 name1 z1 s1,MetaChain sub2 typ2 name2 z2 s2):rest) =
                     let ok1 = alphaEquivWithScopeVar   scopeNew z1 z2
                         ok2 = alphaEquivWithScopeSubst scopeNew sub1 sub2
                         (ok3,fr',fsc) = alphaEquivWithScopeExpr fr scopeNew s1 s2
                         (ok4,fr'',fsc') = go fr' rest
                      in (ok1 && ok2 && ok3 && ok4,fr'', unionListOfFullScopes [fsc,fsc'])
              in go freshRes1 aMC 
              
          (okRes3,freshRes3,fullScopeNew3) =     
              let       
                go fr []     = (True,fr,emptyFullRenaming)
                go fr ((ConstantChain sub1 typ1 name1 z1 s1,ConstantChain sub2 typ2 name2 z2 s2):rest) =
                     let ok1 = alphaEquivWithScopeVar   scopeNew z1 z2
                         ok2 = alphaEquivWithScopeSubst scopeNew sub1 sub2
                         (ok3,fr',fsc) = alphaEquivWithScopeExpr fr scopeNew s1 s2
                         (ok4,fr'',fsc') = go fr' rest
                      in (ok1 && ok2 && ok3 && ok4,fr'', unionListOfFullScopes [fsc,fsc'])
              in go freshRes2 aCC 
          okRes4 =           
              let       
                go []     = True 
                go ((Env sub1 name1,Env sub2 name2):rest) =
                     let ok = alphaEquivWithScopeSubst scopeNew sub1 sub2
                         ok' = go rest 
                     in (ok && ok')
              in go aME 
              
          okRes5 =           
              let       
                go []     = True 
                go ((Env sub1 name1,Env sub2 name2):rest) =
                     let ok = alphaEquivWithScopeSubst scopeNew sub1 sub2
                         ok' = go rest 
                     in (ok && ok')
              in go aCE 
          (okRek,freshRes4,fullScopeNew4) = alphaEquivWithScopeExpr freshRes3 scopeNew inexpr1 inexpr2
      in
        (okRes1 && okRes2 && okRes3 && okRes4 && okRes5 && okRek
        ,freshRes4
        ,unionListOfFullScopes [fullScopeNew1,fullScopeNew2,fullScopeNew3,fullScopeNew4,fullScopeBefore]
        )
     |
       (freshNew,scopeNew,fullScopeBefore,aBS,aMC,aCC,aME,aCE) <- validAllChecksForBinders
     ]
    valid = [(o,f,ffsc) | (o,f,ffsc) <- checkRHSAndInExprOfValids, o]
 in
  case () of 
   _ | and [length bindings1 == length bindings2
            ,length mc1 == length mc2
            ,length cc1 == length cc2
            ,length me1 == length me2
            ,length ce1 == length ce2
            ] 
          -> if null valid then (False,fresh,emptyFullRenaming) 
                           else head valid 
                             
     | otherwise ->  (False,fresh,emptyFullRenaming)
            
            
alphaEquivWithScopeExprIt f sc e1 e2 = error ("forgotten case?: " ++ show (e1,e2)) (False,f,emptyFullRenaming)            
            
unionsScopes xs = 
  let (a,b) = unzip xs                
  in (Map.unions a,Map.unions b)
    
    
-- alphaEquivWithScopeVar:    
-- check whether two variables are equivalent respecting the current scope
-- which represents the alpha renaming 
    
alphaEquivWithScopeVar :: ScopeRenaming 
                          -> VarLift 
                          -> VarLift 
                          -> Bool    
-- alphaEquivWithScopeVar _ e1 e2 
--  | trace ("AEQ: \n" ++ show e1 ++ "\n" ++ show e2 ++ "END") False = undefined                              
alphaEquivWithScopeVar  scope@(scope1,scope2)   (MetaVarLift (Substitution (a1@(AlphaMetaVarLift name1 num1):sub1)) v1) (MetaVarLift (Substitution (a2@(AlphaMetaVarLift name2 num2):sub2)) v2)
  | Just (AlphaMetaVarLift name1' num1') <- Map.lookup a1 scope1
  , Just (AlphaMetaVarLift name2' num2') <- Map.lookup a2 scope2
  , v1 == name1
  , v2 == name2  
  , num1' == num2'
    =  alphaEquivWithScopeSubst scope (Substitution sub1) (Substitution sub2)
    
alphaEquivWithScopeVar  scope@(scope1,scope2)   (ConstantVarLift (Substitution (a1@(AlphaConstantVarLift name1 num1):sub1)) v1) (ConstantVarLift (Substitution (a2@(AlphaConstantVarLift name2 num2):sub2)) v2)
  | Just (AlphaConstantVarLift name1' num1') <- Map.lookup a1 scope1
  , Just (AlphaConstantVarLift name2' num2') <- Map.lookup a2 scope2
  , v1 == name1
  , v2 == name2    
  , num1' == num2'
    =  alphaEquivWithScopeSubst scope (Substitution sub1) (Substitution sub2)

alphaEquivWithScopeVar  scope@(scope1,scope2)   (VarLift (Substitution (a1@(AlphaVarLift name1 num1):sub1)) v1) (VarLift (Substitution (a2@(AlphaVarLift name2 num2):sub2)) v2)
  | Just (AlphaVarLift name1' num1') <- Map.lookup a1 scope1
  , Just (AlphaVarLift name2' num2') <- Map.lookup a2 scope2
  , v1 == name1
  , v2 == name2    
  , num1' == num2'
    =   alphaEquivWithScopeSubst scope (Substitution sub1) (Substitution sub2)
    
alphaEquivWithScopeVar  scope@(scope1,scope2)   (MetaVarLift (Substitution ((SubstSet set1):sub1')) v1) (MetaVarLift (Substitution ((SubstSet set2):sub2')) v2)
  | ((a1@(AlphaMetaVarLift name1 num1),sub1),(a2@(AlphaMetaVarLift name2 num2),sub2)):_ <-
   [(r1,r2) |
        (r1,num1') <-  [ (s1,num1) | s1@(a1@(AlphaMetaVarLift name1 num1),rest1) <- splits set1
                                   ,name1 == v1
                                   ,case Map.lookup a1 scope1 of
                                      Just _ -> True
                                      Nothing -> False 
                                   ,let num1 = fromJust (Map.lookup a1 scope1)
                        ]
        ,
        (r2,num2') <-  [ (s2,num2) | s2@(a2@(AlphaMetaVarLift name2 num2),rest2) <- splits set2
                                   ,name2 == v2
                                   ,case Map.lookup a2 scope2 of
                                      Just _ -> True
                                      Nothing -> False 
                                   ,let num2 = fromJust (Map.lookup a2 scope2)
                       ]
        ,num1' == num2'
   ]
    =  alphaEquivWithScopeSubst scope (Substitution sub1) (Substitution sub2)
    
alphaEquivWithScopeVar  scope@(scope1,scope2)   (ConstantVarLift (Substitution ((SubstSet set1):sub1')) v1) (ConstantVarLift (Substitution ((SubstSet set2):sub2')) v2)
  | ((a1@(AlphaConstantVarLift name1 num1),sub1),(a2@(AlphaConstantVarLift name2 num2),sub2)):_ <-
   [(r1,r2) |
        (r1,num1') <-  [ (s1,num1) | s1@(a1@(AlphaConstantVarLift name1 num1),rest1) <- splits set1
                                   ,name1 == v1
                                   ,case Map.lookup a1 scope1 of
                                      Just _ -> True
                                      Nothing -> False 
                                   ,let num1 = fromJust (Map.lookup a1 scope1)
                        ]
        ,
        (r2,num2') <-  [ (s2,num2) | s2@(a2@(AlphaConstantVarLift name2 num2),rest2) <- splits set2
                                   ,name2 == v2
                                   ,case Map.lookup a2 scope2 of
                                      Just _ -> True
                                      Nothing -> False 
                                   ,let num2 = fromJust (Map.lookup a2 scope2)
                       ]
        ,num1' == num2'
   ]
    =  alphaEquivWithScopeSubst scope (Substitution sub1) (Substitution sub2)    
    
alphaEquivWithScopeVar  scope@(scope1,scope2)   (VarLift (Substitution ((SubstSet set1):sub1')) v1) (VarLift (Substitution ((SubstSet set2):sub2')) v2)
  | ((a1@(AlphaVarLift name1 num1),sub1),(a2@(AlphaVarLift name2 num2),sub2)):_ <-
   [(r1,r2) |
        (r1,num1') <-  [ (s1,num1) | s1@(a1@(AlphaVarLift name1 num1),rest1) <- splits set1
                                   ,name1 == v1
                                   ,case Map.lookup a1 scope1 of
                                      Just _ -> True
                                      Nothing -> False 
                                   ,let num1 = fromJust (Map.lookup a1 scope1)
                        ]
        ,
        (r2,num2') <-  [ (s2,num2) | s2@(a2@(AlphaVarLift name2 num2),rest2) <- splits set2
                                   ,name2 == v2
                                   ,case Map.lookup a2 scope2 of
                                      Just _ -> True
                                      Nothing -> False 
                                   ,let num2 = fromJust (Map.lookup a2 scope2)
                       ]
        ,num1' == num2'
   ]
    =  alphaEquivWithScopeSubst scope (Substitution sub1) (Substitution sub2)    
    
alphaEquivWithScopeVar  scope@(scope1,scope2)   (MetaVarLift sub1 v1)  (MetaVarLift sub2 v2) 
  | v1 == v2  = alphaEquivWithScopeSubst scope sub1 sub2
alphaEquivWithScopeVar  scope@(scope1,scope2)   (VarLift sub1 v1)  (VarLift sub2 v2) 
  | v1 == v2  = alphaEquivWithScopeSubst scope sub1 sub2
alphaEquivWithScopeVar  scope@(scope1,scope2)   (ConstantVarLift sub1 v1)  (ConstantVarLift sub2 v2) 
  | v1 == v2  = alphaEquivWithScopeSubst scope sub1 sub2
    
alphaEquivWithScopeVar scope@(scope1,scope2)   x y = False
    
 

  
    
alphaEquivBoundVar :: [String] -- fresh names 
                       -> ScopeRenaming -- current scope 
                       -> VarLift
                       -> VarLift 
                       -> (Bool
                          ,[String]
                          ,ScopeRenaming
                          ,FullRenaming
                          )                       

alphaEquivBoundVar fresh scope@(scope1,scope2)   v1@(MetaVarLift sub1 v1')  v2@(MetaVarLift sub2 v2') =
 
     let 
      alpha1 = case sub1 of
                (Substitution (a1@(AlphaMetaVarLift name num):_)) -> if name == v1' then [a1] else error "in alphaEquiv"
                (Substitution []) -> []
      alpha2 = case sub2  of
                (Substitution (a2@(AlphaMetaVarLift name num):_)) -> if name == v2' then [a2] else error "in alphaEquiv"
                (Substitution []) -> []
       in
        case () of 
         _ | null alpha1 && null alpha2 && v1' == v2' ->  (alphaEquivWithScopeSubst scope sub1 sub2,fresh,scope,emptyFullRenaming)
           | null alpha1 && null alpha2  -> (False,fresh,scope,emptyFullRenaming) 
           | null alpha1 || null alpha2  -> (False,fresh,scope,emptyFullRenaming) 
 
           | otherwise ->
              let 
               (new:fresh')= fresh
               newAlpha1 = (AlphaMetaVarLift name1 new)
               newAlpha2 = (AlphaMetaVarLift name2 new)
               alpha1'@(AlphaMetaVarLift name1 _) = head alpha1
               alpha2'@(AlphaMetaVarLift name2 _) = head alpha2
               scope' = (Map.insert alpha1' newAlpha1 scope1,Map.insert alpha2' newAlpha2 scope2)
               (m1,m2) = emptyFullRenaming
              in (alphaEquivWithScopeSubst scope' (tailSub sub1) (tailSub sub2)
                 ,fresh'
                 ,scope'
                 ,(insertIntoMultiSet alpha1' newAlpha1 m1,insertIntoMultiSet alpha2' newAlpha2 m2)) 

alphaEquivBoundVar fresh scope@(scope1,scope2)   v1@(VarLift sub1 v1')  v2@(VarLift sub2 v2')  = 
     let 
      alpha1 = case sub1 of
                (Substitution (a1@(AlphaVarLift name num):_)) -> if name == v1' then [a1] else error "in alphaEquiv"
                (Substitution []) -> []
      alpha2 = case sub2  of
                (Substitution (a2@(AlphaVarLift name num):_)) -> if name == v2' then [a2] else error "in alphaEquiv"
                (Substitution []) -> []
     in
        case () of 
         _ | null alpha1 && null alpha2 && v1' == v2' ->  (alphaEquivWithScopeSubst scope sub1 sub2,fresh,scope,emptyFullRenaming)
           | null alpha1 && null alpha2  -> (False,fresh,scope,emptyFullRenaming) 
           | null alpha1 || null alpha2  -> (False,fresh,scope,emptyFullRenaming) 
           | otherwise ->
              let 
               (new:fresh')= fresh
               newAlpha1 = (AlphaVarLift name1 new)
               newAlpha2 = (AlphaVarLift name2 new)
               alpha1'@(AlphaVarLift name1 _) = head alpha1
               alpha2'@(AlphaVarLift name2 _) = head alpha2
               scope' = (Map.insert alpha1' newAlpha1 scope1,Map.insert alpha2' newAlpha2 scope2)
               (m1,m2) = emptyFullRenaming
              in (alphaEquivWithScopeSubst scope' (tailSub sub1) (tailSub sub2)
                 ,fresh'
                 ,scope'
                 ,(insertIntoMultiSet alpha1' newAlpha1 m1,insertIntoMultiSet alpha2' newAlpha2 m2)) 


alphaEquivBoundVar fresh scope@(scope1,scope2)   v1@(ConstantVarLift sub1 v1')  v2@(ConstantVarLift sub2 v2')  = 
     let 
      alpha1 = case sub1 of
                (Substitution (a1@(AlphaConstantVarLift name num):_)) -> if name == v1' then [a1] else error "in alphaEquiv"
                (Substitution []) -> []
      alpha2 = case sub2  of
                (Substitution (a2@(AlphaConstantVarLift name num):_)) -> if name == v2' then [a2] else error "in alphaEquiv"
                (Substitution []) -> []
       in
        case () of 
         _ | null alpha1 && null alpha2 && v1' == v2' ->  (alphaEquivWithScopeSubst scope sub1 sub2,fresh,scope,emptyFullRenaming)
           | null alpha1 && null alpha2  -> (False,fresh,scope,emptyFullRenaming) 
           | null alpha1 || null alpha2  -> (False,fresh,scope,emptyFullRenaming) 
           | otherwise ->
              let 
               (new:fresh')= fresh
               newAlpha1 = (AlphaConstantVarLift name1 new)
               newAlpha2 = (AlphaConstantVarLift name2 new)
               alpha1'@(AlphaConstantVarLift name1 _) = head alpha1
               alpha2'@(AlphaConstantVarLift name2 _) = head alpha2
               scope' = (Map.insert alpha1' newAlpha1 scope1,Map.insert alpha2' newAlpha2 scope2)
               (m1,m2) = emptyFullRenaming
              in (alphaEquivWithScopeSubst scope' (tailSub sub1) (tailSub sub2)
                 ,fresh'
                 ,scope'
                 ,(insertIntoMultiSet alpha1' newAlpha1 m1,insertIntoMultiSet alpha2' newAlpha2 m2)) 

alphaEquivBoundVar fresh scope@(scope1,scope2)   _ _ = (False,fresh,scope,emptyFullRenaming)
                 
--     
-- alphaEquivWithScopeArgPos fresh scope e1 e2    
--   | trace ("AEQ: \n" ++ show e1 ++ "\n" ++ show e2 ++ "END") False = undefined    
  
alphaEquivWithScopeArgPos fresh scope (VarPos v1) (VarPos v2) =
    (alphaEquivWithScopeVar scope v1 v2,fresh,emptyFullRenaming)

alphaEquivWithScopeArgPos fresh scope@(scope1,scope2) (Binder vs1 e1) (Binder vs2 e2) =     
 let
    go fresh scope [] [] = (True,fresh,scope,emptyFullRenaming)
    go fresh scope (v1:vs1) (v2:vs2) = 
        let
            (b,fresh',scope',fsc) = alphaEquivBoundVar fresh scope  v1 v2
            (b',fresh'',scope'',fsc') = go fresh' scope'  vs1 vs2
        in (b && b',fresh'',scope'',unionListOfFullScopes [fsc,fsc'])
    (b1,fresh1,scope1,fullScope) = go fresh scope vs1 vs2
    (b2,fresh2,fullScope2)       = alphaEquivWithScopeExpr fresh1 scope1 e1 e2
 in (b1 && b2, fresh2, unionListOfFullScopes [fullScope,fullScope2])
 
 
 
 
       
-- alphaEquivWithScopeSubst:       
--   checks if the current scope makes two renaming substitutions similar
--   no fresh names, and no adjustment of the scope needed
alphaEquivWithScopeSubst :: ScopeRenaming
                            -> Substitution 
                            -> Substitution 
                            -> Bool 
alphaEquivWithScopeSubst scope@(scope1,scope2) (Substitution sub1) (Substitution sub2) 
  | trace ("AEQsub: \n" ++ show sub1 ++ "\n" ++ show sub2 ++ "END") False = undefined                 
--   | trace ("AEQsub:" ++ show scope1) False = undefined
--   | trace ("AEQsub:" ++ show scope2) False = undefined  
  | trace ("AEQsubaf: \n" ++ (show (applyScope scope1 sub1)) ++ "\n" ++ (show (applyScope scope2 sub2)) ++ "END") False = undefined                 
  
alphaEquivWithScopeSubst scope@(scope1,scope2) (Substitution sub1) (Substitution sub2) =
   let sub1' = applyScope scope1 sub1
       sub2' = applyScope scope2 sub2
   in if similarListsOfListItems sub1' sub2'   then True
       else  False


applyScope scope [] = [] 

applyScope scope (a@(AlphaExpr name num):sub) = 
   case Map.lookup a scope of 
     Just a' -> a':(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)
     
applyScope scope (a@(AlphaConstant name num):sub) = 
   case Map.lookup a scope of 
     Just a' -> a':(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)
     
applyScope scope (a@(AlphaCtxt srt name num):sub) = 
   case Map.lookup a scope of 
     Just a' -> a':(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)

applyScope scope (a@(AlphaConstantCtxt srt name num):sub) = 
    case Map.lookup a scope of 
     Just a' -> a':(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)
     
applyScope scope (a@(AlphaChain t name num):sub) = 
   case Map.lookup a scope of 
     Just a' -> a':(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)
     
applyScope scope (a@(AlphaConstantChain t name num):sub) = 
   case Map.lookup a scope of 
     Just a' -> a':(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)
     
applyScope scope (a@(AlphaEnv name num):sub) = 
    case Map.lookup a scope of 
     Just a' -> a':(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)
     
applyScope scope (a@(AlphaConstantEnv name num):sub) = 
    case Map.lookup a scope of 
     Just a' -> a':(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)
     
applyScope scope (a@(AlphaVarLift name num):sub) = 
  case Map.lookup a scope of 
     Just a' -> a':(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)
     
applyScope scope (a@(AlphaMetaVarLift name num):sub) = 
  case Map.lookup a scope of 
     Just a' -> a':(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)
     
applyScope scope (a@(AlphaConstantVarLift name num):sub) = 
  case Map.lookup a scope of 
     Just a' -> a':(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)

applyScope scope (a@(LVAlphaEnv name num):sub) = 
    case Map.lookup (AlphaEnv name num) scope of 
     Just (AlphaEnv name' num') -> (LVAlphaEnv name' num'):(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)
     
applyScope scope (a@(LVAlphaConstantEnv name num):sub) = 
   case Map.lookup (AlphaConstantEnv name num) scope of 
     Just (AlphaConstantEnv name' num') -> (LVAlphaConstantEnv name' num'):(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)
     
applyScope scope (a@(LVAlphaChain t name num):sub) = 
  case Map.lookup (AlphaChain t name num) scope of 
     Just (AlphaChain t' name' num') -> (LVAlphaChain t' name' num'):(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)
     
applyScope scope (a@(LVAlphaConstantChain t name num):sub) = 
   case Map.lookup (AlphaConstantChain t name num) scope of 
     Just (AlphaConstantChain t' name' num') -> (LVAlphaConstantChain t' name' num'):(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)

     
applyScope scope (a@(CVAlphaCtxt srt name num):sub) = 
  case Map.lookup (AlphaCtxt srt name num) scope of 
     Just (AlphaCtxt srt' name' num') -> (CVAlphaCtxt srt' name' num'):(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)

applyScope scope (a@(CVAlphaConstantCtxt srt name num):sub) = 
  case Map.lookup (AlphaConstantCtxt srt name num) scope of 
     Just (AlphaConstantCtxt srt' name' num') -> (CVAlphaConstantCtxt srt' name' num'):(applyScope scope sub)
     Nothing -> a:(applyScope scope sub)
     
applyScope scope ((SubstSet set):sub)=
    let set' =  applyScope scope set
    in (SubstSet set'):(applyScope scope sub)
 
tailSub (Substitution (x:xs)) = Substitution xs    






applySingleFullRenamingNCC :: SingleFullRenaming -> NonCaptureConstraints -> NonCaptureConstraints
applySingleFullRenamingNCC ren nccs =
 let 
  res = 
   [(s',d')
    | (s,d) <- nccs
      ,s' <- applySingleFullRenamingEither ren s
      ,d' <- applySingleFullRenaming ren d
   ]
  in 
  {-
   trace 
     (   "\n DEBUG: apply full renamaing:\n"
      ++ "\n DEBUG:   renaming: " ++ show ren 
      ++ "\n DEBUG:   NCCs before: " ++ show nccs
      ++ "\n Debug:   NCCs after: " ++ show res
     ) 
   -}  
     res

applySingleFullRenamingEither ren (Left e) = 
 [Left e' | e' <- applySingleFullRenaming ren e]
applySingleFullRenamingEither ren (Right env) = 
 [Right env' | env' <- applySingleFullRenamingEnv ren env]

 
applySingleFullRenaming :: SingleFullRenaming -> Expression -> [Expression]
applySingleFullRenaming ren (MetaExpression sub s) =
  [MetaExpression sub' s | sub' <- (applySingleFullRenamingSubst ren sub)]   
applySingleFullRenaming ren (Constant sub s) =
  [Constant sub' s | sub' <- (applySingleFullRenamingSubst ren sub)]   
applySingleFullRenaming ren (MetaCtxt sub srt name) =
  [MetaCtxt sub' srt name | sub' <- (applySingleFullRenamingSubst ren sub)]   
applySingleFullRenaming ren (ConstantCtxt sub srt name) =
  [ConstantCtxt sub' srt name | sub' <- (applySingleFullRenamingSubst ren sub)]   
applySingleFullRenaming ren (MetaCtxtSubst sub srt name expr) =
  [MetaCtxtSubst sub' srt name expr'
   | sub' <- (applySingleFullRenamingSubst ren sub)
   , expr' <- (applySingleFullRenaming ren expr)
   ]
applySingleFullRenaming ren (ConstantCtxtSubst sub srt name expr) =
  [ConstantCtxtSubst sub' srt name expr'
   | sub' <- (applySingleFullRenamingSubst ren sub)
   , expr' <- (applySingleFullRenaming ren expr)
   ]
applySingleFullRenaming ren (VarL v) =
   [VarL v' | v' <- (applySingleFullRenamingVar ren v)]
   
applySingleFullRenaming ren (Letrec env expr) =
   [Letrec env' expr'
     | env' <- (applySingleFullRenamingEnv ren env)
     , expr' <- (applySingleFullRenaming ren expr)
     ]
     
applySingleFullRenaming ren (FlatLetrec env expr) =
   [FlatLetrec env' expr'
     | env' <- (applySingleFullRenamingEnv ren env)
     , expr' <- (applySingleFullRenaming ren expr)
     ]
applySingleFullRenaming ren (Hole sub) =
   [ Hole sub' | sub' <- (applySingleFullRenamingSubst ren sub)]
   
applySingleFullRenaming ren (Fn f args) =
  let args' = map (applySingleFullRenamingArgPos ren) args
      combs []       = [[]]
      combs (xs:xss) = [x:ys | x <- xs, ys <- combs xss ]
      argsCombs = combs args'
  in [Fn f newargs | newargs <- argsCombs]

applySingleFullRenamingArgPos ren (VarPos v) =
 [VarPos v' | v' <- applySingleFullRenamingVar ren v]
 
applySingleFullRenamingArgPos ren (Binder vs expr) =
 let vs' = map (applySingleFullRenamingVar ren) vs 
     combs []       = [[]]
     combs (xs:xss) = [x:ys | x <- xs, ys <- combs xss ]
     vsCombs = combs vs'
 in [Binder us expr' 
    |
    us <- vsCombs,
    expr' <- applySingleFullRenaming ren expr
    ]
    
applySingleFullRenamingEnv ren env =
    let 
     combs []       = [[]]
     combs (xs:xss) = [x:ys | x <- xs, ys <- combs xss ]
     applySingleFullRenamingBinding (x := e) = 
          [x' := e'
          | x' <- applySingleFullRenamingVar ren x  
          , e' <- applySingleFullRenaming ren e
          ]
     bindings' = combs (map applySingleFullRenamingBinding (bindings env))
     applySingleFullRenamingMetaChain (MetaChain sub typ name z s) = 
          [MetaChain sub' typ name z' s'
          | sub' <- applySingleFullRenamingSubst ren sub  
          , z' <- applySingleFullRenamingVar ren z
          , s' <- applySingleFullRenaming ren s
          ]
     metachains' = combs (map applySingleFullRenamingMetaChain (metaChains env))
     applySingleFullRenamingConstantChain (ConstantChain sub typ name z s) = 
          [ConstantChain sub' typ name z' s'
          | sub' <- applySingleFullRenamingSubst ren sub
          , z' <- applySingleFullRenamingVar ren z
          , s' <- applySingleFullRenaming ren s
          ]
     constantchains' = combs (map applySingleFullRenamingConstantChain (constantChains env))
     applySingleFullRenamingEnv (Env sub name) = 
          [Env sub' name
          | sub' <- applySingleFullRenamingSubst ren sub  
          ]
     metaenvs' = combs (map applySingleFullRenamingEnv (metaEnvVars env))
     constantenvs' = combs (map applySingleFullRenamingEnv (constantEnvVars env))
    in 
     [  empty{bindings = bs
             ,metaChains = ms
             ,constantChains = cs 
             ,metaEnvVars = me 
             ,constantEnvVars = ce
             }
        | bs <- bindings'
        , ms <- metachains'
        , cs <- constantchains'
        , me <- metaenvs'
        , ce <- constantenvs'
     ]
    
applySingleFullRenamingSubst :: SingleFullRenaming -> Substitution -> [Substitution]    

applySingleFullRenamingSubst ren sub =
  let itemlist = substList sub 
  in
         [sub{substList=sub'} | sub' <- applySingleFullRenamingSubstList ren itemlist]
  
applySingleFullRenamingSubstList :: SingleFullRenaming -> [SubstItem] -> [[SubstItem]]
applySingleFullRenamingSubstList ren list =
    let 
     combs []       = [[]]
     combs (xs:xss) = [x:ys | x <- xs, ys <- combs xss ]
    in combs (map (applySingleFullRenamingSubstItem ren) list)

applySingleFullRenamingSubstItem :: SingleFullRenaming -> SubstItem -> [SubstItem]
applySingleFullRenamingSubstItem ren (SubstSet list)
 = [SubstSet l | l <- applySingleFullRenamingSubstList ren list]


applySingleFullRenamingSubstItem ren item
  | Just alphas <- getRenamings item ren = alphas
  | otherwise = [item]
  
getRenamings (LVAlphaChain t name num) ren =
 case Map.lookup (AlphaChain t name num) ren of 
   Just list -> Just (map noneToLetVars list)
   Nothing -> Nothing
getRenamings (LVAlphaConstantChain  t name num) ren =
 case Map.lookup (AlphaConstantChain t name num) ren of 
   Just list -> Just (map noneToLetVars list)
   Nothing -> Nothing
getRenamings (LVAlphaConstantEnv name num) ren =
 case Map.lookup (AlphaConstantEnv name num) ren of 
   Just list -> Just (map noneToLetVars list)
   Nothing -> Nothing
getRenamings (LVAlphaEnv name num) ren =
 case Map.lookup (AlphaEnv name num) ren of 
   Just list -> Just (map noneToLetVars list)
   Nothing -> Nothing 
getRenamings (CVAlphaConstantCtxt srt name num) ren =
 case Map.lookup (AlphaConstantCtxt srt name num) ren of 
   Just list -> Just (map noneToCVVars list)
   Nothing -> Nothing
getRenamings (CVAlphaCtxt srt name num) ren =
 case Map.lookup (AlphaCtxt srt name num) ren of 
   Just list -> Just (map noneToCVVars list)
   Nothing -> Nothing
getRenamings alpha ren = Map.lookup alpha ren   
   


 
applySingleFullRenamingSC ren (SCVar v)
 = [SCVar v' | v' <- applySingleFullRenamingVar ren v]
applySingleFullRenamingSC ren (SCExpr e)
 = [SCExpr e' | e' <- applySingleFullRenaming ren e]
applySingleFullRenamingSC ren (SCCtxt e)
 = [SCCtxt e' | e' <- applySingleFullRenaming ren e]
applySingleFullRenamingSC ren (SCMetaEnv (Env sub e))
 = [SCMetaEnv (Env sub' e) 
   | sub' <- applySingleFullRenamingSubst ren sub 
   ]
applySingleFullRenamingSC ren (SCConstantEnv (Env sub e))
 = [SCConstantEnv (Env sub' e) 
   | sub' <- applySingleFullRenamingSubst ren sub 
   ]
applySingleFullRenamingSC ren (SCMetaChain sub typ name)
 = [SCMetaChain sub' typ name
   | sub' <- applySingleFullRenamingSubst ren sub 
   ]
applySingleFullRenamingSC ren (SCConstantChain sub typ name)
 = [SCConstantChain sub' typ name
   | sub' <- applySingleFullRenamingSubst ren sub 
   ]
   
applySingleFullRenamingVar ren (VarLift sub name)
 = [VarLift sub' name | sub' <- (applySingleFullRenamingSubst ren sub) ]
 
applySingleFullRenamingVar ren (MetaVarLift sub name)
 = [MetaVarLift sub' name | sub' <- (applySingleFullRenamingSubst ren sub) ]
 
applySingleFullRenamingVar ren (ConstantVarLift sub name)
 = [ConstantVarLift sub' name | sub' <- (applySingleFullRenamingSubst ren sub) ]
   
