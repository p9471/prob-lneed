-----------------------------------------------------------------------------
-- | 
-- Module      :  LRSX.Language.Alpha.Refresh
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--
--  Alpha-Renaming  and Alpha-Equivalence
--
-----------------------------------------------------------------------------
module LRSX.Language.Alpha.Refresh where
import Data.List
import Data.Function
import Data.Maybe
import LRSX.Util.Util
import LRSX.Language.Syntax
import LRSX.Language.Alpha.Rename
import LRSX.Language.Alpha.Simplify
import LRSX.Language.ContextDefinition
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
-- import Debug.Trace
trace a b = b


startAlphaRepair fresh theNCCs expr = 
  let (fresh',expr',ren) = alphaRepair fresh expr 
  in (fresh',simplifyExpr (Info{nccs=theNCCs,nablaSets=computeNablaSets theNCCs}) expr',ren)

-- variables: nothing to do
-- alphaRepair ::  [String] -> Expression -> ([String],Expression)
alphaRepair  fresh (VarL v) =  (fresh,VarL v,[])
-- meta expressions and constants:   
alphaRepair  fresh (MetaExpression (Substitution ((AlphaExpr name num):rest)) s)
  | name == s 
  , (f:fresh') <- fresh
  =  let newAlpha = AlphaExpr name f
     in (fresh',MetaExpression (Substitution (newAlpha:rest)) s, [(AlphaExpr name num,AlphaExpr name f)])

alphaRepair  fresh  e@(MetaExpression (Substitution []) s) =
  -- S is fresh and should be alpha renamed
  let
    (f:fresh') = fresh
    newExpr  = (MetaExpression subst s) 
    subst    = Substitution [AlphaExpr s f]
  in (fresh',newExpr,[(AlphaExpr s f,AlphaExpr s f)])
  
alphaRepair fresh  (Constant (Substitution ((AlphaConstant name num):rest)) s)
  | name == s
  , (f:fresh') <- fresh
  =  let newAlpha = AlphaConstant name f
     in (fresh',Constant (Substitution (newAlpha:rest)) s,[(AlphaConstant name num,AlphaConstant name f)])

alphaRepair fresh e@(Constant (Substitution []) s) =
  -- S is fresh and should be alpha renamed
  let
    (f:fresh') = fresh
    newExpr  = (Constant subst s) 
    subst    = Substitution [AlphaConstant s f]
  in (fresh',newExpr,[(AlphaConstant s f,AlphaConstant s f)])

alphaRepair fresh (MetaCtxtSubst (Substitution ((AlphaCtxt srt name num):rest)) srt' name' e)
  | name == name'
  , (f:fresh') <- fresh
  =  let (fresh'',e',ren1) = alphaRepair fresh' e
         newAlpha = AlphaCtxt srt name f
         e'' = replaceAndSiftDown [(CVAlphaCtxt srt' name num,CVAlphaCtxt srt' name f)] e'
     in (fresh'',MetaCtxtSubst (Substitution (newAlpha:rest)) srt' name' e'',(AlphaCtxt srt name num,newAlpha):ren1)

alphaRepair fresh (MetaCtxtSubst (Substitution []) srt name e) 
  =  let (fresh',e',ren1) = alphaRepair  fresh e
         (f:fresh'')=fresh'
         newAlpha = AlphaCtxt srt name f
         e'' = siftDown (Substitution[CVAlphaCtxt srt name f]) e'
     in (fresh'',MetaCtxtSubst (Substitution [newAlpha]) srt name e'',(newAlpha,newAlpha):ren1)

     
alphaRepair fresh (ConstantCtxtSubst (Substitution ((AlphaConstantCtxt srt name num):rest)) srt' name' e)
  | name == name'
  , (f:fresh') <- fresh
  =  let (fresh'',e',ren1) = alphaRepair fresh' e
         newAlpha = AlphaConstantCtxt srt name f
         e'' = replaceAndSiftDown [(CVAlphaConstantCtxt srt' name num,CVAlphaConstantCtxt srt' name f)] e'
     in (fresh'',ConstantCtxtSubst (Substitution (newAlpha:rest)) srt' name' e'',(AlphaConstantCtxt srt name num,newAlpha):ren1)

alphaRepair fresh (ConstantCtxtSubst (Substitution []) srt name e) 
  =  let (fresh',e',ren1) = alphaRepair fresh e
         (f:fresh'')=fresh'
         newAlpha = AlphaConstantCtxt srt name f
         e'' = siftDown (Substitution[CVAlphaConstantCtxt srt name f]) e'
     in (fresh'',ConstantCtxtSubst (Substitution [newAlpha]) srt name e'',(newAlpha,newAlpha):ren1)     
     
     
alphaRepair fresh (Fn name args) = 
  let go fr [] = (fr,[],[])
      go fr (x:xs) = 
        let (fr',x',r) = alphaRepairArgPos fr x
            (fr'',xs',rs) = go fr' xs
        in (fr'',x':xs',r ++ rs)
      (fresh',args',ren) = (go fresh args)
  in (fresh',Fn name args',ren)
      

alphaRepair fresh  (FlatLetrec env s) =
 case alphaRepair fresh  (Letrec env s) of
  (fresh',Letrec env' s',ren) -> (fresh',FlatLetrec env' s',ren)
  
  
     
     
    

alphaRepair fresh  (Letrec env s) =
  let (fresh1,s',rens) =  alphaRepair fresh s
      metaE  = metaEnvVars env 
      constE = constantEnvVars env 
      metaC  = metaChains env
      constC = constantChains env
      binds  = bindings env
      (fresh2,rBinds,ren1,fren1,rs1) = 
        let 
            
              go fresh  [] = (fresh,[],[],[],[])
              go fresh ((x:=e):rest) = 
                let 
                 (fresh',e',r) = alphaRepair fresh e
                 (fresh'',rest',ren,fren,rs) = go fresh' rest
                 (fresh''',old,new,x') = alphaRepairBoundVar fresh'' x
                in (fresh''',(x':=e'):rest',(old,new):ren,if old == new then old:fren else fren ,((old,new):ren)++(r++rs))
        in  go fresh1 binds
        
      (fresh3,rMetaC,ren2,fren2,rs2) = 
        let 
              go fresh  [] = (fresh,[],[],[],[])
              go fresh ((MetaChain sub typ name z s):rest) = 
                let 
                 (fresh',s',r) = alphaRepair fresh s
                 (fresh'',rest',ren,fren,rs) = go fresh' rest
                 (f:fresh''',old,new,z') = alphaRepairBoundVar fresh'' z
                 (sub',old1,new1) = case sub of
                             (Substitution ((AlphaChain t n num):rest)) 
                               | name == n -> (Substitution ((AlphaChain t n f):rest),LVAlphaChain t n num,LVAlphaChain t n f)
                             (Substitution []) -> (Substitution [(AlphaChain typ name f)],LVAlphaChain typ name f,LVAlphaChain typ name f)
                in (fresh''',(MetaChain sub' typ name z' s'):rest',(old,new):(old1,new1):ren,(if old == new then [old] else []) ++ (if old1 == new1 then [old1] else []) ++ fren, (old,new):((old1,new1):ren)++(r++rs))
        in go fresh2 metaC
      (fresh4,rConstC,ren3,fren3,rs3) = 
        let 
           
              go fresh  [] = (fresh,[],[],[],[])
              go fresh ((ConstantChain sub typ name z s):rest) = 
                let 
                 (fresh',s',r) = alphaRepair fresh s
                 (fresh'',rest',ren,fren,rs) = go fresh' rest
                 (f:fresh''',old,new,z') = alphaRepairBoundVar fresh'' z
                 (sub',old1,new1) = case sub of
                             (Substitution ((AlphaConstantChain t n num):rest)) 
                               | name == n -> (Substitution ((AlphaConstantChain t n f):rest),LVAlphaConstantChain t n num,LVAlphaConstantChain t n f)
                             (Substitution []) -> (Substitution [(AlphaConstantChain typ name f)],LVAlphaConstantChain typ name f,LVAlphaConstantChain typ name f)
                in (fresh'''
                   ,(ConstantChain sub' typ name z' s'):rest'
                   ,(old,new):(old1,new1):ren
                   ,((if old == new then [old] else []) ++ (if old1 == new1 then [old1] else []) ++ fren)
                   ,(old,new):((old1,new1):ren)++(r++rs)
                   )
        in go fresh3 constC
      (fresh5,rMetaE,fren4,ren4) = 
        let 
           
              go fresh  [] = (fresh,[],[],[])
              go fresh ((Env sub name):rest) = 
                let 
                 (f:fresh',rest',fren,ren) = go fresh rest
                 (sub',old1,new1) = case sub of
                             (Substitution ((AlphaEnv n num):rest)) 
                               | name == n -> (Substitution ((AlphaEnv n f):rest),LVAlphaEnv n num,LVAlphaEnv n f)
                             (Substitution []) -> (Substitution [(AlphaEnv name f)],LVAlphaEnv name f,LVAlphaEnv name f)
                in (fresh',(Env sub' name):rest',(if old1 == new1 then [old1] else []) ++ fren,(old1,new1):ren)
        in go fresh4 metaE
      (fresh6,rConstE,fren5,ren5) = 
        let 
             
              go fresh  [] = (fresh,[],[],[])
              go fresh ((Env sub name):rest) = 
                let 
                 (f:fresh',rest',fren,ren) = go fresh rest
                 (sub',old1,new1) = case sub of
                             (Substitution ((AlphaConstantEnv n num):rest)) 
                               | name == n -> (Substitution ((AlphaConstantEnv n f):rest),LVAlphaConstantEnv n num,LVAlphaConstantEnv n f)
                             (Substitution []) -> (Substitution [(AlphaConstantEnv name f)],LVAlphaConstantEnv name f,LVAlphaConstantEnv name f)
                in (fresh',(Env sub' name):rest',(if old1 == new1 then [old1] else [])++ fren,(old1,new1):ren)
        in go fresh5 constE
      newRen =  concat [ren1,ren2,ren3,ren4,ren5]
      freshRen = Substitution [(SubstSet $ concat [fren1,fren2,fren3,fren4,fren5])]
      theRen = map (\(x,y) -> (fromLVCV x,fromLVCV y)) $ concat [rs1,rs2,rs2,ren4,ren5,rens]
      env1 = env{metaEnvVars = rMetaE, metaChains = rMetaC,constantEnvVars = rConstE, constantChains = rConstC, bindings=rBinds}        
      env2 = siftDownEnv freshRen env1
      env3 = replaceAndSiftDownEnv newRen env2
      s'' = replaceAndSiftDown newRen $ siftDown freshRen s'
  in 
    (fresh6,Letrec env3 s'',theRen)
-- alphaRepair _ other = error (show other)


siftDownEnv sub@(Substitution sublist) env = 
  env {bindings        = [x:=(siftDown sub e) | (x:=e) <- bindings env]
      ,metaEnvVars     = [(Env (Substitution (sub1 ++ sublist)) name)   | (Env (Substitution sub1) name) <-  metaEnvVars env]
      ,constantEnvVars = [(Env (Substitution (sub1 ++ sublist)) name)   | (Env (Substitution sub1) name) <-  constantEnvVars env]      
      ,metaChains      = [MetaChain (Substitution (sub1 ++ sublist)) typ name z (siftDown sub s)  | MetaChain (Substitution sub1) typ name z s <- metaChains env]
      ,constantChains  = [ConstantChain (Substitution (sub1 ++ sublist)) typ name z (siftDown sub s)  | ConstantChain (Substitution sub1) typ name z s <- constantChains env]
      }

alphaRepairArgPos fresh (VarPos v) = 
  (fresh,VarPos v,[])
  
alphaRepairArgPos fresh (Binder [] s) =   
  let (fresh',s',r) = alphaRepair fresh s
  in (fresh',Binder [] s',r)
  
alphaRepairArgPos fresh (Binder (x:xs) s) = 
  let (fresh', Binder xs' s',r) = alphaRepairArgPos fresh (Binder xs s)
      (fresh'',oldAlpha,newAlpha,x')  = alphaRepairBoundVar fresh' x
      Binder xs'' s'' = if oldAlpha == newAlpha then siftDownArgPos (Substitution [oldAlpha]) (Binder xs' s')
                                                else replaceAndSiftDownArgPos [(oldAlpha,newAlpha)] (Binder xs' s')
  in (fresh'',Binder (x':xs'') s'',(oldAlpha,newAlpha):r)
   

alphaRepairBoundVar (f:fresh) (VarLift (Substitution []) name) = 
   (fresh,AlphaVarLift name f,AlphaVarLift name f,(VarLift (Substitution [AlphaVarLift name f]) name))
alphaRepairBoundVar (f:fresh) (VarLift (Substitution ((AlphaVarLift name1 num):rest)) name)
  | name1 == name =  (fresh,(AlphaVarLift name1 num),(AlphaVarLift name1 f),(VarLift (Substitution ((AlphaVarLift name1 f):rest) ) name))
  
alphaRepairBoundVar (f:fresh) (MetaVarLift (Substitution []) name) = 
   (fresh,AlphaMetaVarLift name f,AlphaMetaVarLift name f,(MetaVarLift (Substitution [AlphaMetaVarLift name f]) name))
   
alphaRepairBoundVar (f:fresh) (MetaVarLift (Substitution ((AlphaMetaVarLift name1 num):rest)) name)
  | name1 == name =  (fresh,(AlphaMetaVarLift name1 num),(AlphaMetaVarLift name1 f),(MetaVarLift (Substitution ((AlphaMetaVarLift name1 f):rest) ) name))
   
alphaRepairBoundVar (f:fresh) (ConstantVarLift (Substitution []) name) = 
   (fresh,AlphaConstantVarLift name f,AlphaConstantVarLift name f,(ConstantVarLift (Substitution [AlphaConstantVarLift name f]) name))
   
alphaRepairBoundVar (f:fresh) (ConstantVarLift (Substitution ((AlphaConstantVarLift name1 num):rest)) name)
  | name1 == name =  (fresh,(AlphaConstantVarLift name1 num),(AlphaConstantVarLift name1 f),(ConstantVarLift (Substitution ((AlphaConstantVarLift name1 f):rest) ) name))
  


replaceAndSiftDownArgPos :: [(SubstItem,SubstItem)] -> ArgPos -> ArgPos   
replaceAndSiftDownArgPos set (VarPos v) = 
   VarPos (replaceAndSiftDownVar set v)

replaceAndSiftDownArgPos set (Binder [] e) = Binder [] (replaceAndSiftDown set e)

replaceAndSiftDownArgPos set (Binder (v:vs) e) =
  case  replaceAndSiftDownArgPos set (Binder (vs) e) of
       Binder vs' e' -> Binder (v:vs') e'
        
replaceAndSiftDown :: [(SubstItem,SubstItem)] -> Expression -> Expression
replaceAndSiftDown   set (Hole (Substitution sub)) = Hole (Substitution ((replaceInSubst set sub)++([SubstSet (map snd set)])))

replaceAndSiftDown set (Fn f args) = 
 Fn f (map (replaceAndSiftDownArgPos set) args)
replaceAndSiftDown set (MetaExpression (Substitution sub) s) = MetaExpression (Substitution ((replaceInSubst set sub))) s
replaceAndSiftDown set (Constant (Substitution sub) s) = Constant (Substitution ((replaceInSubst set sub))) s

replaceAndSiftDown set (MetaCtxtSubst (Substitution sub) typ name s) = 
  MetaCtxtSubst
    (Substitution ((replaceInSubst set sub)))
    typ 
    name
    (replaceAndSiftDown set s)
  
replaceAndSiftDown set (ConstantCtxtSubst (Substitution sub) typ name s) = 
  ConstantCtxtSubst
    (Substitution ((replaceInSubst set sub)))
    typ 
    name
    (replaceAndSiftDown set s)
   
    
replaceAndSiftDown set (FlatLetrec env s) =    
   case replaceAndSiftDown set (Letrec env s) of
        Letrec env' s' -> FlatLetrec env' s'
        
replaceAndSiftDown set (Letrec env s) =
  Letrec
    ( env {
         metaChains = [MetaChain (Substitution ((replaceInSubst set sub))) t ee z (replaceAndSiftDown set s) 
                      | MetaChain (Substitution sub) t ee z s <- metaChains env]
                      
        ,constantChains = [ConstantChain (Substitution ((replaceInSubst set sub))) t ee z (replaceAndSiftDown set s) 
                          | ConstantChain (Substitution sub) t ee z s <- constantChains env]
                          
        ,metaEnvVars = [Env (Substitution ((replaceInSubst set sub)))  name
                       | (Env (Substitution sub) name) <- metaEnvVars env]
        ,constantEnvVars = [Env (Substitution ((replaceInSubst set sub) ))  name
                       | (Env (Substitution sub) name) <- constantEnvVars env]
        ,bindings = [x := (replaceAndSiftDown set e) | x := e <- bindings env]
    })
    (replaceAndSiftDown set s)
 
 
   
replaceAndSiftDown set (VarL  var)  = VarL (replaceAndSiftDownVar set  var)

replaceAndSiftDownVar set v
 = let res = replaceAndSiftDownVarR  set v
   in res
replaceAndSiftDownVarR  set (VarLift (Substitution sub) v)         = (VarLift (Substitution $ (replaceInSubst set sub)) v)
replaceAndSiftDownVarR  set (MetaVarLift (Substitution sub) v)     = (MetaVarLift (Substitution $ (replaceInSubst set sub)) v)
replaceAndSiftDownVarR  set (ConstantVarLift (Substitution sub) v) = (ConstantVarLift (Substitution $ (replaceInSubst set sub)) v)

replaceInSubst :: [(SubstItem,SubstItem)] -> [SubstItem] -> [SubstItem]

replaceInSubst theset ((SubstSet set):rest) = 
  let set' = replaceInSubst theset set 
  in (SubstSet set'):(replaceInSubst theset rest)
  
replaceInSubst set (item:rest)
  | ((a,b):_) <- [(a,b) | (a,b) <- set, item == a] =  b:(replaceInSubst set rest)
  | otherwise        = item:(replaceInSubst set rest)

replaceInSubst set [] = []  

replaceAndSiftDownEnv :: [(SubstItem,SubstItem)] -> Environment -> Environment
replaceAndSiftDownEnv set env = 
     env {
         metaChains = [MetaChain (Substitution ((replaceInSubst set sub))) t ee z (replaceAndSiftDown set s) 
                      | MetaChain (Substitution sub) t ee z s <- metaChains env]
                      
        ,constantChains = [ConstantChain (Substitution ((replaceInSubst set sub))) t ee z (replaceAndSiftDown set s) 
                          | ConstantChain (Substitution sub) t ee z s <- constantChains env]
                          
        ,metaEnvVars = [Env (Substitution ((replaceInSubst set sub)))  name
                       | (Env (Substitution sub) name) <- metaEnvVars env]
        ,constantEnvVars = [Env (Substitution ((replaceInSubst set sub) ))  name
                       | (Env (Substitution sub) name) <- constantEnvVars env]
        ,bindings = [x := (replaceAndSiftDown set e) | x := e <- bindings env]
    }
