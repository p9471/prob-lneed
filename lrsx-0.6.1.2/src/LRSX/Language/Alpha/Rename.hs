-----------------------------------------------------------------------------
-- | 
-- Module      :  LRSX.Language.Alpha 
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--
--  Alpha-Renaming  and Alpha-Equivalence
--
-----------------------------------------------------------------------------
module LRSX.Language.Alpha.Rename where
import Data.List
import Data.Function
import Data.Maybe
import LRSX.Util.Util
import LRSX.Language.Syntax
import LRSX.Language.ContextDefinition
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
-- import Debug.Trace
-- trace a b = b
-- ====================================================================       
-- Meta-Alpha-Renaming
-- ====================================================================       
-- renameExpr takes a list of fresh names and an expression,
-- it performs meta alpha-renaming of the expression, by
-- introducing new meta renamings
-- the output is a triple (fresh,expr',subst)
-- where 
--  * fresh are the unused fresh names
--  * expr' is the renamed expr
--  * subst is the substitution of variables by fresh variables and 
--    meta variables by meta variables and a alpha renaming
--    which can be seen as a 'making fresh constraint', 
--    e.g. S |-> S_{alpha} means, that alpha makes the bound variables
--    of S fresh.
--    Note that in the expression S may be replaced something 
--    like S_{alpha,furtherrenamings} if S is in scope of other renamings
--    introduced by making fresh constraints    
  
-- renameExpr :: NonCaptureConstraints -> [String] -> Expression -> ([String],Expression,[(SyntaxComponent,SubstItem)])        

renameExpr fresh (VarL var) = (fresh,(VarL var),[])
        
renameExpr (s':fresh) e@(MetaExpression (Substitution []) s) =
  let
    newExpr  = (MetaExpression subst s) 
    subst    = Substitution [AlphaExpr s (s')]
    renaming = [(SCExpr e, SCExpr newExpr)]
  in (fresh,newExpr,renaming)

renameExpr (s':fresh) e@(Constant (Substitution []) s) =
  let
    newExpr  = (Constant subst s) 
    subst    = Substitution [AlphaConstant s (s')]
    renaming = [(SCExpr  e, SCExpr newExpr)]
  in (fresh,newExpr,renaming)
  
renameExpr fresh (MetaCtxtSubst (Substitution []) typ name s) =
 let
  (f:fresh',s',renaming) = renameExpr fresh s
  subst      = Substitution [AlphaCtxt typ name (f)]
  substNewCV = Substitution [CVAlphaCtxt typ name (f)]
  renaming'  = (SCCtxt (MetaCtxt (Substitution []) typ name), SCCtxt (MetaCtxt subst typ name)):renaming
  s''        = siftDown substNewCV s'
 in (fresh', (MetaCtxtSubst subst typ name s''), renaming')

renameExpr fresh (ConstantCtxtSubst (Substitution []) typ name s) =
 let
  (f:fresh',s',renaming) = renameExpr fresh s
  subst      = Substitution [AlphaConstantCtxt typ name (f)]
  substNewCV = Substitution [CVAlphaConstantCtxt typ name (f)]
  renaming'  = (SCCtxt (ConstantCtxt (Substitution []) typ name), SCCtxt (ConstantCtxt subst typ name)):renaming
  s''        = siftDown substNewCV s'
 in (fresh', (ConstantCtxtSubst subst typ name s''), renaming')
 
 
renameExpr fresh (ConstantCtxt (Substitution []) typ name) =
 let
  (f:fresh',s',renaming) = renameExpr fresh (Hole emptySubstitution)
  subst      = Substitution [AlphaConstantCtxt typ name (f)]
  substNewCV = Substitution [CVAlphaConstantCtxt typ name (f)]
  renaming'  = (SCCtxt (ConstantCtxt (Substitution []) typ name), SCCtxt (ConstantCtxt subst typ name)):renaming
  s''        = siftDown substNewCV s'
 in (fresh', (ConstantCtxt subst typ name), renaming')

renameExpr fresh (MetaCtxt (Substitution []) typ name) =
 let
  (f:fresh',s',renaming) = renameExpr fresh (Hole emptySubstitution)
  subst      = Substitution [AlphaCtxt typ name (f)]
  substNewCV = Substitution [CVAlphaCtxt typ name (f)]
  renaming'  = (SCCtxt (MetaCtxt (Substitution []) typ name), SCCtxt (MetaCtxt subst typ name)):renaming
  s''        = siftDown substNewCV s'
 in (fresh', (MetaCtxt subst typ name), renaming')
 
renameExpr  fresh (FlatLetrec env s) = 
  case renameExpr  fresh (Letrec env s) of
     (a,Letrec env' s',b) -> (a,FlatLetrec env' s',b)
     

 
renameExpr  fresh (Letrec env s) =
 let 
  mc = metaChains env
  cc = constantChains env
  me = metaEnvVars env
  ce = constantEnvVars env
  -- Bindings
  -- extract let-variables and expressions
  (vars1,expr1)    = unzip [(x,e) | x:=e <- bindings env]
  -- rename the rhs of the let bindings
  (f1,expr1',ren1) = renameList fresh expr1
  -- create alpha-substitutions for let-vars
  vars1' = mkVar vars1 (take (length vars1) f1)
  subst1 = map getSubstFromVar vars1'
  ren1' = [(SCVar vold, SCVar vnew) | (vold,vnew) <- zip vars1 vars1']
  f2     = drop (length vars1) f1
  fulleta = Substitution [SubstSet (subst1 ++ subst2 ++ subst3 ++ subst4 ++ subst5) ]
  binds' =  zipWith 
                (\v e  -> v := (siftDown fulleta e))
                vars1'
                expr1'
  -- MetaChains
  (vars2,expr2) = unzip [(z,e) | MetaChain substold typ name z e <- metaChains env]
  (f3,expr2',ren2) = renameList f2 expr2
  metaAlpha1 =  take (length vars2) f3
  f4 = drop (length vars2) f3
  vars2' = mkVar vars2 $  take (length vars2) f4
  f5 = drop (length vars2) f4
  substChains2 = [AlphaChain typ name ma 
                  | (ma,MetaChain subold typ name z e) <- zip metaAlpha1 (metaChains env)
                 ]
  substVars2   = map getSubstFromVar vars2'
  allSubsts2   = [x: (toSubstSet ((map noneToLetVars xs) ++ substVars2 ++ otherSubst2)) | (x,xs) <- splits substChains2]
  subst2 = (map noneToLetVars substChains2)++substVars2
  ren2' = [ (SCMetaChain old typ name, SCMetaChain (Substitution [AlphaChain typ name alp])  typ name )
           | (MetaChain old typ name _ _, alp) <- zip (metaChains env) metaAlpha1
          ]
          ++ [(SCVar vold, SCVar vnew) | (vold,vnew) <- zip vars2 vars2']
  otherSubst2 =  concat [subst1,subst3,subst4,subst5]          
  mc' = [(MetaChain  Substitution{substList=old ++ new} typ name z' (siftDown subst e'))
           | (MetaChain (Substitution {substList=old}) typ name z e,(new,(z',e'))) <- zip (metaChains env) (zip allSubsts2 (zip vars2' expr2'))]          
          
  -- ConstantChains
  (vars3,expr3) = unzip [(z,e) | ConstantChain substold typ name z e <- constantChains env]
  (f6,expr3',ren3) = renameList f5 expr3
  metaAlpha2 = take (length vars3) f6
  f7 = drop (length vars3) f6
  vars3' = mkVar vars3 $ take (length vars3) f7
  f8 = drop (length vars3) f7
  substChains3 = [AlphaConstantChain typ name ma
                   | (ma,ConstantChain subold typ name z e) <- zip metaAlpha2 (constantChains env)
                 ]
  substVars3   = map getSubstFromVar vars3'
  allSubsts3   =  [x:(toSubstSet $ ((map noneToLetVars xs) ++ substVars3) ++ otherSubst3)| (x,xs) <- splits substChains3]
  subst3 = (map noneToLetVars substChains3)++substVars3
  ren3' = [ (SCConstantChain old typ name, SCConstantChain (Substitution [AlphaConstantChain typ name alp]) typ name)
           | (ConstantChain old typ name _ _, alp) <- zip (constantChains env) metaAlpha2
          ]
          ++ [(SCVar vold, SCVar vnew) | (vold,vnew) <- zip vars3 vars3']
  otherSubst3 = concat [subst1,subst2,subst4,subst5]          
  cc' = [(ConstantChain  Substitution{substList=old ++ new} typ name z' (siftDown subst e'))
           | (ConstantChain (Substitution {substList=old}) typ name z e,(new,(z',e'))) <- zip (constantChains env) (zip allSubsts3 (zip vars3' expr3'))]          
  -- MetaEnvs
  envVars = metaEnvVars env
  metaAlpha3 = take (length envVars) f8
  f9 = drop (length envVars) f8
  substEnvs4 =
              [ AlphaEnv name ma
                  | (ma,Env subold name) <- zip metaAlpha3 (metaEnvVars env)
               ]
  subst4 = (map noneToLetVars substEnvs4)
  ren4' = [(SCMetaEnv (Env sub name), SCMetaEnv (Env (Substitution [AlphaEnv name alp]) name)) | ((Env sub name),alp) <- zip envVars metaAlpha3]
  allSubsts4 = [x:(toSubstSet $ ((map noneToLetVars xs) ++ otherSubst4) ) | (x,xs) <- splits substEnvs4]
  otherSubst4 = concat [subst1,subst2,subst3,subst5]          
  envVars' =
 
            [ Env (Substitution (xs) ) name
            | xs@((AlphaEnv name ma):rest) <- allSubsts4
            ] 
 
  -- ConstantEnvs
  cenvVars = constantEnvVars env
  metaAlpha4 = take (length cenvVars) f9
  f10 = drop (length cenvVars) f9
  substEnvs5 = [AlphaConstantEnv name ma  | (ma,Env subold name) <- zip metaAlpha4 (constantEnvVars env)]
  subst5 = map noneToLetVars substEnvs5
  ren5' = [ (SCConstantEnv (Env sub name), SCConstantEnv (Env (Substitution [AlphaConstantEnv name alp]) name)) | ((Env sub name),alp) <- zip cenvVars metaAlpha4]
  allSubsts5 = [x:(toSubstSet $ (map noneToLetVars xs) ++ otherSubst5) | (x,xs) <- splits substEnvs5]
  otherSubst5 = concat [subst1,subst2,subst3,subst4]          
  cenvVars' = 
     [ Env (Substitution (xs) ) name
     | xs@((AlphaConstantEnv name ma):rest) <- allSubsts5
     ]
  -- InExpr  
  (f11,s',ren4)    = renameExpr  f10 s 
  s'' = siftDown subst s'
  subst = Substitution{substList = (toSubstSet $ concat [subst1,subst2,subst3,subst4,subst5])}
  env' = env{bindings=binds', metaChains=mc', constantChains=cc',metaEnvVars=envVars',constantEnvVars=cenvVars'}
  renaming' = ren1 ++ ren2 ++ ren3 ++ ren4  ++  ren1' ++ ren2' ++ ren3' ++ ren4' ++ ren5'
 in  
     (f11,Letrec env' s'',renaming')                       

renameExpr  fresh (Fn f args) =
    let (fresh',args',ren) = renameArgPosList fresh args 
    in (fresh',Fn f args',ren)
 
renameExpr  fresh (Hole subs) = 
  (fresh,Hole subs,[]) 

  
renameArgPos fresh (VarPos var) = (fresh,(VarPos var),[])  

renameArgPos fresh (Binder [] s) =
   let (fresh',s',renaming) = renameExpr fresh s
   in  (fresh',Binder [] s',renaming)
   
renameArgPos fresh (Binder (v:vs) s) =
   let (fresh',Binder vs' s',renaming) = renameArgPos fresh (Binder vs s)
       (f:fresh'') = fresh'
       (newVar:[]) = mkVar [v] [f]
       (Binder vs'' s'') = siftDownArgPos (Substitution{substList = [getSubstFromVar newVar]}) (Binder vs' s')
   in (fresh'', Binder (newVar:vs'') s'', (SCVar v,SCVar newVar):renaming)
       

renameArgPos fresh (Binder ys s) =
 let
  (fresh',s',renaming) = renameExpr  fresh s
  newnames = [name| (name,y) <-  (zip (take (length ys) fresh') ys)]
  fresh'' = drop (length ys) fresh'
  varSubst = zip ys newVars
  newVars = (mkVar ys newnames)                                   
  s'' = siftDown (Substitution{substList = map getSubstFromVar (reverse newVars)}) s'
  newexpr = Binder newVars s''  
  renaming' = [(SCVar x,SCVar y) | (x,y) <- varSubst] ++ renaming
 in (fresh'',newexpr,renaming')           

renameList fresh [] =
 (fresh,[],[])                  
renameList fresh (e:es) =
 let (f',e',r) = renameExpr  fresh e 
     (f'',es',r') = renameList f' es
 in (f'',e':es', r ++ r')
  

renameArgPosList fresh [] =
 (fresh,[],[])                  
renameArgPosList fresh (e:es) =
 let (f',e',r) = renameArgPos  fresh e 
     (f'',es',r') = renameArgPosList f' es
 in (f'',e':es', r ++ r')

mkVar [] []= []
mkVar ((VarLift (Substitution []) x):xs)  (y:ys) = (VarLift (Substitution [AlphaVarLift x y]) x):(mkVar xs ys)
mkVar ((ConstantVarLift (Substitution []) x):xs)  (y:ys) = (ConstantVarLift (Substitution [AlphaConstantVarLift x y]) x):(mkVar xs ys)
mkVar ((MetaVarLift (Substitution []) x):xs)  (y:ys) = (MetaVarLift (Substitution [AlphaMetaVarLift x y]) x):(mkVar xs ys)

mkVar e _ = error $ show e
 
getSubstFromVar (VarLift (Substitution [sub]) _) = sub
getSubstFromVar (ConstantVarLift (Substitution [sub]) _) = sub  
getSubstFromVar (MetaVarLift (Substitution [sub]) _) = sub  

-- ===========================================================================================================
-- siftDown a substitution


  







  

siftDownArgPos :: Substitution -> ArgPos -> ArgPos        
  
siftDownArgPos   sub (VarPos v) = 
   VarPos (siftDownVar sub v)

siftDownArgPos  sub (Binder [] e) = Binder [] (siftDown sub e)
siftDownArgPos  sub (Binder (v:vs) e) =
  case   siftDownArgPos  sub (Binder (vs) e) of
       Binder vs' e' -> Binder (v:vs') e'
        
siftDown sub (Hole subold) = Hole (append subold sub)        
siftDown sub (Fn f args) = 
 Fn f (map (siftDownArgPos sub) args)
siftDown sub (MetaExpression subold s) = MetaExpression (append subold sub) s
siftDown sub (Constant subold s) = Constant (append subold sub) s

siftDown sub (MetaCtxtSubst subold typ name s) = 
    MetaCtxtSubst (append subold sub) typ name (siftDown sub s)
    
siftDown sub (ConstantCtxtSubst subold typ name s) =
    ConstantCtxtSubst (append subold sub) typ name (siftDown sub s)
    
siftDown sub (FlatLetrec env s) =    
   case siftDown sub (Letrec env s) of
        Letrec env' s' -> FlatLetrec env' s'
        
siftDown sub (Letrec env s) =
  Letrec
    ( env {
         metaChains = [MetaChain (append subold sub) t ee z (siftDown  sub s) | MetaChain subold t ee z s <- metaChains env]
        ,constantChains = [ConstantChain (append subold sub) t ee z (siftDown  sub s) | ConstantChain subold t ee z s <- constantChains env]
        ,metaEnvVars = [Env (append oldsub sub) name| (Env oldsub name) <- metaEnvVars env]
        ,constantEnvVars = [Env (append oldsub sub) name| (Env oldsub name) <- constantEnvVars env]      
        ,bindings = [x := (siftDown  sub e) | x := e <- bindings env]
    })
    (siftDown  sub s)

   
siftDown sub (VarL  var)  = VarL (siftDownVar sub  var)


siftDownVar sub v
 = let res = siftDownVarR  sub v
   in 
            res

            
            
-- siftDownVarR  sub (VarLift oldsub v)
--    |  v `elem`  = VarLift oldsub v
--    
-- siftDownVarR  sub (MetaVarLift oldsub v)
--    | v `elem`  = MetaVarLift oldsub v
--    
-- siftDownVarR  sub (ConstantVarLift oldsub v)
--    | v `elem`  =  ConstantVarLift oldsub v

siftDownVarR   sub (VarLift oldsub v)         = (VarLift (append oldsub sub) v)
siftDownVarR   sub (MetaVarLift oldsub v)     = (MetaVarLift (append oldsub sub) v)
siftDownVarR   sub (ConstantVarLift oldsub v) = (ConstantVarLift (append oldsub sub) v)



