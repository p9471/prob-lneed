-----------------------------------------------------------------------------
-- | 
-- Module      :  LRSX.Language.Alpha.Similar
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--
--  Alpha-Renaming  and Alpha-Equivalence
--
-----------------------------------------------------------------------------
module LRSX.Language.Alpha.Similar where
import Data.List
import Data.Function
import Data.Maybe
import LRSX.Util.Util
import LRSX.Language.Syntax
import LRSX.Language.ContextDefinition
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Debug.Trace
import LRSX.Language.Alpha.Simplify hiding (trace)
-- trace a b = b


 
-- ============================================================================= 
-- Checking simlet and simsubst
-- ============================================================================= 
 
simlet (MetaExpression sub1 n1) (MetaExpression sub2 n2)
    | n1 == n2  =  sub1 `simsubst` sub2
    | otherwise = False
    
simlet (Constant sub1 n1) (Constant sub2 n2)
    | n1 == n2  =  sub1 `simsubst` sub2 
    | otherwise = False
    
simlet (Fn f1 args1) (Fn f2 args2)    
    | f1 == f2  = and (map (\(x,y) -> x `simletArgpos` y) (zip args1 args2))
    | otherwise = False

simlet (MetaCtxtSubst sub1 typ1 name1 s1)  (MetaCtxtSubst sub2 typ2 name2 s2)
    | name1 == name2
    , s1 `simlet` s2
    , typ1 == typ2   = sub1 `simsubst` sub2
    | otherwise = False
    
simlet (ConstantCtxtSubst sub1 typ1 name1 s1)  (ConstantCtxtSubst sub2 typ2 name2 s2)
    | name1 == name2
    , s1 `simlet` s2
    , typ1 == typ2   = sub1 `simsubst` sub2
    | otherwise = False
       
simlet (MetaCtxt sub1 typ1 name1)  (MetaCtxt sub2 typ2 name2)
    | name1 == name2
    , typ1 == typ2   = sub1 `simsubst` sub2
    | otherwise = False
       
simlet (ConstantCtxt sub1 typ1 name1)  (ConstantCtxt sub2 typ2 name2)
    | name1 == name2
    , typ1 == typ2   = sub1 `simsubst` sub2
    | otherwise = False
       
simlet (VarL v1) (VarL v2) = simletVar v1 v2
simlet (Hole sub1) (Hole sub2) = simsubst sub1 sub2

simlet (FlatLetrec env1 s1) (FlatLetrec env2 s2) = 
  simlet (Letrec env1 s1) (Letrec env2 s2)
  
simlet (Letrec env1 s1) (Letrec env2 s2) 
    | s1 `simlet` s2 =
          let me1 = metaEnvVars env1
              me2 = metaEnvVars env2
              meCheck = (length me1 == length me2) &&  
                          
              
                        (let 
                            isSimLetEnvVar xs = and (map (\(x,y) -> x `simLetEnvVar` y) xs)
                            simLetEnvVar (Env sub1 e1) (Env sub2 e2) = e1 == e2 && sub1 `simsubst` sub2
                         in
                          isSimLetEnvVar (zip (sort me1) (sort me2))
                         )
                 
                             
--                              any (isSimLetEnvVar) [zip me1 p | p <- (permutations me2)])
              ce1 = constantEnvVars env1
              ce2 = constantEnvVars env2
              ceCheck = (length ce1 == length ce2) &&  
                        (let 
                            isSimLetEnvVar xs = and (map (\(x,y) -> x `simLetEnvVar` y) xs)
                            simLetEnvVar (Env sub1 e1) (Env sub2 e2) = e1 == e2 && sub1 `simsubst` sub2
                         in isSimLetEnvVar (zip (sort ce1) (sort ce2)))
                             
--                              any (isSimLetEnvVar) [zip ce1 p | p <- (permutations ce2)])                         
              mc1 = metaChains env1
              mc2 = metaChains env2
              mcCheck = (length mc1 == length mc2) &&  
                        (let 
                            isSimChain xs = and (map (\(x,y) -> x `simChain` y) xs)
                            simChain (MetaChain sub1 typ1 name1 z1 s1) (MetaChain sub2 typ2 name2 z2 s2) = 
                                        sub1 `simsubst` sub2 && typ1 == typ2 && name1 == name2 && z1 `simletVar` z2 && s1 `simlet` s2
                         in isSimChain (zip (sort mc1) (sort mc2)))
                         -- any (isSimChain) [zip mc1 p | p <- (permutations mc2)])                         
              cc1 = constantChains env1
              cc2 = constantChains env2
              ccCheck = (length cc1 == length cc2) &&  
                        (let 
                            isSimChain xs = and (map (\(x,y) -> x `simChain` y) xs)
                            simChain (ConstantChain sub1 typ1 name1 z1 s1) (ConstantChain sub2 typ2 name2 z2 s2) = 
                                        sub1 `simsubst` sub2 && typ1 == typ2 && name1 == name2 && z1 `simletVar` z2 && s1 `simlet` s2
                         in isSimChain (zip (sort cc1) (sort cc2))) 
                         -- any (isSimChain) [zip cc1 p | p <- (permutations cc2)])                         
              b1 = bindings env1
              b2 = bindings env2
              bCheck = (length b1 == length b2) &&  
                        (let 
                            isSimBinds xs = and (map (\(x:=s,y:=t) -> (x `simletVar` y && s `simlet` t)) xs)
                         in isSimBinds (zip (sort b1) (sort b2)))
                             -- any (isSimBinds) [zip b1 p | p <- (permutations b2)])                         
                         
                         
          in meCheck && ceCheck && mcCheck && ccCheck && bCheck
simlet (Letrec env1 s1) (Letrec env2 s2) = False
simlet _ _ = False          
               
               
simletArgpos (VarPos v1) (VarPos v2) = simletVar v1 v2
simletArgpos (Binder xs1 s1) (Binder xs2 s2) 
 = 
  (length xs1 == length xs2) 
  &&
  all  (== True) [simletVar x1 x2 | (x1,x2) <- zip xs1 xs2]
  &&
  simlet s1 s2

simletArgpos _ _ = False  


simletVar (VarLift sub1 v1) (VarLift sub2 v2)
    | v1 == v2 = sub1 `simsubst` sub2
simletVar (MetaVarLift sub1 v1) (MetaVarLift sub2 v2)
    | v1 == v2 = sub1 `simsubst` sub2
simletVar (ConstantVarLift sub1 v1) (ConstantVarLift sub2 v2)
    | v1 == v2 = sub1 `simsubst` sub2
simletVar _ _ = False

similar (MetaExpression sub1 n1) (MetaExpression sub2 n2) = n1 == n2
similar (Constant sub1 n1) (Constant sub2 n2) = n1 == n2
similar (Fn f1 args1) (Fn f2 args2)    
    | f1 == f2  = and (map (\(x,y) -> x `similarArgpos` y) (zip args1 args2))
    | otherwise = False

similar (MetaCtxtSubst sub1 typ1 name1 s1)  (MetaCtxtSubst sub2 typ2 name2 s2)
    | name1 == name2
    , s1 `similar` s2
    , typ1 == typ2   = True
    | otherwise = False
    
similar (ConstantCtxtSubst sub1 typ1 name1 s1)  (ConstantCtxtSubst sub2 typ2 name2 s2)
    | name1 == name2
    , s1 `similar` s2
    , typ1 == typ2   = True 
    | otherwise = False
       
similar (MetaCtxt sub1 typ1 name1)  (MetaCtxt sub2 typ2 name2)
    | name1 == name2
    , typ1 == typ2   = True
    | otherwise = False
       
similar (ConstantCtxt sub1 typ1 name1)  (ConstantCtxt sub2 typ2 name2)
    | name1 == name2
    , typ1 == typ2   = True 
    | otherwise = False
       
similar (VarL v1) (VarL v2) = similarVar v1 v2
similar (Hole sub1) (Hole sub2) = True 

similar (FlatLetrec env1 s1) (FlatLetrec env2 s2)  =
  similar (Letrec env1 s1) (Letrec env2 s2) 
  
similar (Letrec env1 s1) (Letrec env2 s2) 
    | s1 `similar` s2 =
          let me1 = metaEnvVars env1
              me2 = metaEnvVars env2
              meCheck = (length me1 == length me2) &&  
                        (let 
                            issimilarEnvVar xs = and (map (\(x,y) -> x `similarEnvVar` y) xs)
                            similarEnvVar (Env sub1 e1) (Env sub2 e2) = e1 == e2 
                            sfn (Env _ e1) (Env _ e2) = compare e1 e2                            
                         in issimilarEnvVar (zip (sortBy sfn me1) (sortBy sfn me2)))                         

              ce1 = constantEnvVars env1
              ce2 = constantEnvVars env2
              ceCheck = (length ce1 == length ce2) &&  
                        (let 
                            issimilarEnvVar xs = and (map (\(x,y) -> x `similarEnvVar` y) xs)
                            similarEnvVar (Env sub1 e1) (Env sub2 e2) = e1 == e2 
                            sfn (Env _ e1) (Env _ e2) = compare e1 e2
                         in issimilarEnvVar (zip (sortBy sfn ce1) (sortBy sfn ce2)))    
              mc1 = metaChains env1
              mc2 = metaChains env2
              mcCheck = (length mc1 == length mc2) &&  
                        (let 
                            isSimChain xs = and (map (\(x,y) -> x `simChain` y) xs)
                            simChain (MetaChain sub1 typ1 name1 z1 s1) (MetaChain sub2 typ2 name2 z2 s2) = 
                                        typ1 == typ2 && name1 == name2 && z1 `similarVar` z2 && s1 `similar` s2
                            sfn (MetaChain sub1 typ1 name1 z1 s1)
                                (MetaChain sub2 typ2 name2 z2 s2) =
                                    case compare name1 name2 of
                                         EQ -> case compare s1 s2 of
                                                    EQ -> compare z1 z2
                                                    other -> other
                                         other -> other
                                        
                         in isSimChain (zip (sortBy sfn mc1) (sortBy sfn mc2))    )
              cc1 = constantChains env1
              cc2 = constantChains env2
              ccCheck = (length cc1 == length cc2) &&  
                        (let 
                            isSimChain xs = and (map (\(x,y) -> x `simChain` y) xs)
                            simChain (ConstantChain sub1 typ1 name1 z1 s1) (ConstantChain sub2 typ2 name2 z2 s2) = 
                                       typ1 == typ2 && name1 == name2 && z1 `similarVar` z2 && s1 `similar` s2
                            sfn (ConstantChain sub1 typ1 name1 z1 s1)
                                (ConstantChain sub2 typ2 name2 z2 s2) =
                                    case compare name1 name2 of
                                         EQ -> case compare s1 s2 of
                                                    EQ -> compare z1 z2
                                                    other -> other
                                         other -> other
                                         
                         in isSimChain (zip (sortBy sfn cc1) (sortBy sfn cc2))    )                   
              b1 = bindings env1
              b2 = bindings env2
              bCheck = (length b1 == length b2) &&  
                        (let 
                            isSimChain xs = and (map (\(x:=s,y:=t) -> (x `similarVar` y && s `similar` t)) xs)
                            sfn (x:=u1) (y:=u2) = case compare u1 u2 of
                                                   EQ -> compare x y
                                                   other -> other
                         in isSimChain (zip (sortBy sfn b1) (sortBy sfn b2)))    
                         
                         
          in 
              -- mytrace (show $ (meCheck , ceCheck , mcCheck , ccCheck , bCheck, (sort b1, sort b2)))  
              (meCheck && ceCheck && mcCheck && ccCheck && bCheck)
              
similar (Letrec env1 s1) (Letrec env2 s2) = False
similar _ _ = False          
               
               
similarArgpos (VarPos v1) (VarPos v2) = similarVar v1 v2
similarArgpos (Binder xs1 s1) (Binder xs2 s2) 
 = 
  (length xs1 == length xs2) 
  &&
  all  (== True) [similarVar x1 x2 | (x1,x2) <- zip xs1 xs2]
  &&
  similar s1 s2

similarArgpos _ _ = False  


similarVar (VarLift sub1 v1) (VarLift sub2 v2) = True
similarVar (MetaVarLift sub1 v1) (MetaVarLift sub2 v2) = True
similarVar (ConstantVarLift sub1 v1) (ConstantVarLift sub2 v2) = True
similarVar _ _ = False

simsubst (Substitution sub1) (Substitution sub2) =
 similarListsOfListItems sub1 sub2 
   
similarListsOfListItems sub1 sub2 = 
  let l1 = (flattenSubst sub1) 
      l2 = (flattenSubst sub2)
  in if simLists l1 l2 then True else False
     
simLists [] [] = True
simLists ((SubstSet []):xs) ys = simLists xs ys
simLists xs ((SubstSet []):ys) = simLists xs ys
simLists s1@((SubstSet set1):xs) s2@((SubstSet set2):ys) =
   let set1' = Set.fromList set1
       set2' = Set.fromList set2
       common = Set.intersection set1' set2'
   in 
     if Set.null common then -- trace ("simList: " ++ show s1 ++ " | " ++ show s2) 
                           False 
                        else
                               let set1'' = set1' Set.\\ common
                                   set2'' = set2' Set.\\ common
                               in simLists ((SubstSet (Set.elems set1'')):xs) ((SubstSet (Set.elems set2'')):ys)
                               
simLists (a:xs) ((SubstSet set):ys)
    | a `elem` set = simLists xs ((SubstSet $ delete a set):ys)
    
simLists  ((SubstSet set):xs) (a:ys)
    | a `elem` set = simLists ((SubstSet $ delete a set):xs) ys
     

simLists (a:xs) (b:ys)
  | case a of 
      SubstSet _ -> False
      _ -> case b of 
                SubstSet _ -> False
                _ -> True
  = if  a == b   then simLists xs ys else False
    
 
     
simLists s1 s2 = False -- trace ("simList: " ++ show s1 ++ " | " ++ show s2) False     




-- the substitution on the rhs must be equal or more general than on the right hand side:
simSubstWeakAsym sub1 sub2 
  = simSubstWeakAsym1 sub1 sub2

simSubstWeakAsym1 (Substitution sub1) (Substitution sub2) =
 similarListsOfListItemsWeak sub1 sub2 
   
similarListsOfListItemsWeak sub1 sub2 = 
  let l1 = (flattenSubst sub1) 
      l2 = (flattenSubst sub2)
  in if simListsWeak l1 l2 then True else False
     
simListsWeak [] [] = True
simListsWeak ((SubstSet []):xs) ys = simListsWeak xs ys
simListsWeak xs ((SubstSet []):ys) = simListsWeak xs ys
simListsWeak s1@((SubstSet set1@(x:xs)):xxs) s2@((SubstSet set2@(y:ys):yys))
  | Just (y,ys) <- splitBy (nameEqual x) set2 = simListsWeak ((SubstSet xs):xxs) ((SubstSet ys):yys)
  | Just (x,xs) <- splitBy (nameEqual y) set1 = simListsWeak ((SubstSet xs):xxs) ((SubstSet ys):yys)
  | otherwise = False -- trace ("simListWeak: " ++ show s1 ++ " | " ++ show s2) False 
                               
simListsWeak (a:xs) ((SubstSet set):ys)
    | Just (s,set') <- splitBy (nameEqual a) set = simListsWeak xs ((SubstSet set'):ys)
    
-- simListsWeak  ((SubstSet set):xs) (a:ys)
--     | Just (s,set') <- splitBy (nameEqual a) set = simListsWeak ((SubstSet set'):xs) ys

simListsWeak (a:xs) (b:ys)
  | case a of 
      SubstSet _ -> False
      _ -> case b of 
                SubstSet _ -> False
                _ -> True
  = if  a `nameEqual` b   then simListsWeak xs ys else False
    
 
     
simListsWeak s1 s2 = False -- trace ("simListWeak: " ++ show s1 ++ " | " ++ show s2) False     
a `elemWeak` list = any (nameEqual a) list
