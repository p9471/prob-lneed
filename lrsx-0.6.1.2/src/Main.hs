-----------------------------------------------------------------------------
-- |
-- Module      :  Main
-- Copyright   :  (c) D. Sabel, Goethe-University, Frankfurt a.M., Germany
-- License     :  BSD-style 
-- 
-- Maintainer  :  sabel <at> ki.cs.uni-frankfurt.de
--
--  
-- The Main-module of the LRSX Tool, mainly handles input and
-- output and some pretty printing.
--
-----------------------------------------------------------------------------
module Main where
import System.Environment
import LRSX.Interface.CommandLine
-- * The main function

-- | The main function
--   First get the arguments from the command line, the parse the arguments and then process the parsed options
main :: IO ()
main = do 
    -- get arguments from the command line
    args <- getArgs
    -- first parse arguments into options of type 'OptCommand', then process the options
    processOpts $ parseArgs args



