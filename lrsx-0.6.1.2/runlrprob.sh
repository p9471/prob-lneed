# rm ./*.time
# mkdir -p out/LRprob/trs 
# mkdir -p out/LRprob/itrs 
# PFX=FORK
# rm LR-$PFX.inp.proofs.statistics
# echo "overlaps" >> $PFX.time
# /usr/bin/time -p -a -o$PFX.time stack exec -- lrsx overlap  examples/calculi/LRprob/LR-$PFX.inp > out/LRprob/LR-$PFX.overlaps
# echo "diagrams" >> $PFX.time
# /usr/bin/time -p -a -o$PFX.time stack exec -- lrsx join bound=10 proofs  examples/calculi/LRprob/LR-$PFX.inp > out/LRprob/LR-$PFX.inp.proofs +RTS -N -sLR-$PFX.inp.proofs.statistics
# stack exec -- lrsx convert proofs long out/LRprob/LR-$PFX.inp.proofs > out/LRprob/LR-$PFX.long
# stack exec -- lrsx convert proofs short unions-from=examples/calculi/LRprob/LR-MoreUnions.inp out/LRprob/LR-$PFX.inp.proofs > out/LRprob/LR-$PFX.short
# echo "induct-trs" >> $PFX.time
#  /usr/bin/time -p -a -o$PFX.time stack exec -- lrsx induct long  atp-path=extra/aprove/ unions-from=examples/calculi/LRprob/LR-MoreUnions.inp  output-to=out/LRprob/trs out/LRprob/LR-$PFX.long > out/LRprob/LR-$PFX.inp.terminaton_proof_trs
#  echo "induct-itrs" >> $PFX.time
#  /usr/bin/time -p -a -o$PFX.time stack exec -- lrsx induct long use-itrs atp-path=extra/aprove/ unions-from=examples/calculi/LRprob/LR-MoreUnions.inp  output-to=out/LRprob/itrs out/LRprob/LR-$PFX.long > out/LRprob/LR-$PFX.inp.terminaton_proof_itrs

# stack exec -- lrsx convert short trs  unions-from=examples/calculi/LRprob/LR-MoreUnions.inp out/LRprob/LR-$PFX.short > out/LRprob/LR-$PFX.short.trs

PFX=COMMUTE
# rm LR-$PFX.inp.proofs.statistics
# echo "overlaps" >> $PFX.time
# /usr/bin/time -p -a -o$PFX.time stack exec -- lrsx overlap  examples/calculi/LRprob/LR-$PFX.inp > out/LRprob/LR-$PFX.overlaps
# echo "diagrams" >> $PFX.time
# /usr/bin/time -p -a -o$PFX.time stack exec -- lrsx join bound=10 proofs  examples/calculi/LRprob/LR-$PFX.inp > out/LRprob/LR-$PFX.inp.proofs +RTS -N -sLR-$PFX.inp.proofs.statistics
# stack exec -- lrsx convert proofs long out/LRprob/LR-$PFX.inp.proofs > out/LRprob/LR-$PFX.long
# stack exec -- lrsx convert proofs short out/LRprob/LR-$PFX.inp.proofs > out/LRprob/LR-$PFX.short
stack exec -- lrsx convert proofs short unions-from=examples/calculi/LRprob/LR-MoreUnions.inp out/LRprob/LR-$PFX.inp.proofs > out/LRprob/LR-$PFX.short

# echo "induct-trs" >> $PFX.time
# /usr/bin/time -p -a -o$PFX.time stack exec -- lrsx induct long  atp-path=extra/aprove/ unions-from=examples/calculi/LRprob/LR-MoreUnions.inp  output-to=out/LRprob/trs out/LRprob/LR-$PFX.long  > out/LRprob/LR-$PFX.inp.terminaton_proof_trs
# echo "induct-itrs" >> $PFX.time
# /usr/bin/time -p -a -o$PFX.time stack exec -- lrsx induct long use-itrs atp-path=extra/aprove/ unions-from=examples/calculi/LRprob/LR-MoreUnions.inp  output-to=out/LRprob/itrs out/LRprob/LR-$PFX.long > out/LRprob/LR-$PFX.inp.terminaton_proof_itrs
stack exec -- lrsx convert short trs  unions-from=examples/calculi/LRprob/LR-MoreUnions.inp out/LRprob/LR-$PFX.short > out/LRprob/LR-$PFX.short.trs
