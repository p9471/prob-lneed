rm ./*.time
mkdir -p out/lneedprob/trs 
mkdir -p out/lneedprob/itrs 


PFX=fork
rm lneed_$PFX.inp.proofs.statistics
echo "overlaps" >> $PFX.time
/usr/bin/time -p -a -o$PFX.time stack exec -- lrsx overlap  examples/calculi/lneedprob/lneed_$PFX.inp > out/lneedprob/lneed_$PFX.overlaps
echo "diagrams" >> $PFX.time
/usr/bin/time -p -a -o$PFX.time stack exec -- lrsx join bound=10 proofs  examples/calculi/lneedprob/lneed_$PFX.inp > out/lneedprob/lneed_$PFX.inp.proofs +RTS -N -slneed_$PFX.inp.proofs.statistics 2> out/lneedprob/$PFX.stderr

stack exec -- lrsx convert proofs long out/lneedprob/lneed_$PFX.inp.proofs > out/lneedprob/lneed_$PFX.long
stack exec -- lrsx convert proofs short out/lneedprob/lneed_$PFX.inp.proofs > out/lneedprob/lneed_$PFX.nounion.short
stack exec -- lrsx convert long short  unions-from=examples/calculi/lneedprob/lneed_more_unions.inp out/lneedprob/lneed_$PFX.long > out/lneedprob/lneed_$PFX.short
stack exec -- lrsx convert short trs  unions-from=examples/calculi/lneedprob/lneed_more_unions.inp out/lneedprob/lneed_$PFX.short > out/lneedprob/lneed_$PFX.trs

echo "induct-trs" >> $PFX.time
/usr/bin/time -p -a -o$PFX.time stack exec -- lrsx induct long  atp-path=extra/aprove/ unions-from=examples/calculi/lneedprob/lneed_more_unions.inp  output-to=out/lneedprob/trs out/lneedprob/lneed_$PFX.long  > out/lneedprob/lneed_$PFX.inp.terminaton_proof_trs
echo "induct-itrs" >> $PFX.time
/usr/bin/time -p -a -o$PFX.time stack exec -- lrsx induct long use-itrs atp-path=extra/aprove/ unions-from=examples/calculi/lneedprob/lneed_more_unions.inp  output-to=out/lneedprob/trs out/lneedprob/lneed_$PFX.long > out/lneedprob/lneed_$PFX.inp.terminaton_proof_itrs



PFX=commute
rm lneed_$PFX.inp.proofs.statistics
echo "overlaps" >> $PFX.time
/usr/bin/time -p -a -o$PFX.time stack exec -- lrsx overlap  examples/calculi/lneedprob/lneed_$PFX.inp > out/lneedprob/lneed_$PFX.overlaps
echo "diagrams" >> $PFX.time
/usr/bin/time -p -a -o$PFX.time stack exec -- lrsx join  bound=10 proofs  examples/calculi/lneedprob/lneed_$PFX.inp > out/lneedprob/lneed_$PFX.inp.proofs +RTS -N -slneed_$PFX.inp.proofs.statistics 2> out/lneedprob/$PFX.stderr
stack exec -- lrsx convert proofs long out/lneedprob/lneed_$PFX.inp.proofs > out/lneedprob/lneed_$PFX.long
stack exec -- lrsx convert proofs short out/lneedprob/lneed_$PFX.inp.proofs > out/lneedprob/lneed_$PFX.nounion.short
stack exec -- lrsx convert long short  unions-from=examples/calculi/lneedprob/lneed_more_unions.inp out/lneedprob/lneed_$PFX.long > out/lneedprob/lneed_$PFX.short
stack exec -- lrsx convert short trs  unions-from=examples/calculi/lneedprob/lneed_more_unions.inp out/lneedprob/lneed_$PFX.short > out/lneedprob/lneed_$PFX.trs
echo "induct-trs" >> $PFX.time
/usr/bin/time -p -a -o$PFX.time stack exec -- lrsx induct long  atp-path=extra/aprove/ unions-from=examples/calculi/lneedprob/lneed_more_unions.inp  output-to=out/lneedprob/trs out/lneedprob/lneed_$PFX.long > out/lneedprob/lneed_$PFX.inp.terminaton_proof_trs
echo "induct-itrs" >> $PFX.time
/usr/bin/time -p -a -o$PFX.time stack exec -- lrsx induct long use-itrs atp-path=extra/aprove/ unions-from=examples/calculi/lneedprob/lneed_more_unions.inp  output-to=out/lneedprob/trs out/lneedprob/lneed_$PFX.long > out/lneedprob/lneed_$PFX.inp.terminaton_proof_itrs
